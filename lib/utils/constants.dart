import 'package:familytrust/services/localization_services.dart';
import 'package:flutter/material.dart';
import 'size_config.dart';
import 'package:google_fonts/google_fonts.dart';

const String kImageDir = 'assets/images/';
const String kIconsDir = 'assets/icons';
const String kImageUrl = 'xyz';
const String kBaseUrl = 'xyz';
String kIsBangla = LocalizationService().getCurrentLang();

const String facebookId = 'FamilyTrust.shop';
const String countryCode = '+880';
const String phoneNumber = '0000000000000';

const kPrimaryColor = Color(0xFF041E42);//#041E42
const kSecondaryColor = Color(0xFFFFC220);
const kBottomItemColor = Color(0xFFFE9901);
const kWhiteColor = Colors.white;
const kBlackColor = Colors.black;
const kOrdinaryColor = Color(0xFFF5F5F5);
const kBackGroundColor = Color(0xFF2A2C36);
const kDarkColor = Color(0xFF1E1F28);
const kErrorColor = Color(0xFFFF2424);
const kSuccessColor = Color(0xFF55D85A);
const kSaleColor = Color(0xFFF14705);
const kBronzeColor = Color(0xFFA57164);
const kSilverColor = Color(0xFFA9A9A9);
const kGoldColor = Color(0xFFDAA520);
const kDiamondColor = Color(0xFF32508E);
const kSliderSelectColor = Color(0xFF53B175);
const kBorderColor = Color(0xFFE2E2E2);
const kAccentColor = Color(0xFFDC4655);
const kPlatinumColor = Colors.teal;

List<Color> categoryBgColor =[
  Color(0xFFCFFCFF),
  Color(0xFFDBDCFF),
  Color.fromRGBO(83, 177, 117, 0.1),
  Color.fromRGBO(247, 165, 147, 0.25),
  Color(0xFFFFEBF3),
  Color(0xFFF1F6FF),
  Color(0xFFEBFFEA),
  Color(0xFFFFF2CF),
  Color(0xFFCFFCFF),
  Color(0xFFDBDCFF),
  Color.fromRGBO(83, 177, 117, 0.1),
  Color.fromRGBO(247, 165, 147, 0.25),
  Color(0xFFFFEBF3),
  Color(0xFFF1F6FF),
  Color(0xFFEBFFEA),
  Color(0xFFFFF2CF),
  Color(0xFFCFFCFF),
  Color(0xFFDBDCFF),
  Color.fromRGBO(83, 177, 117, 0.1),
  Color.fromRGBO(247, 165, 147, 0.25),
  Color(0xFFFFEBF3),
  Color(0xFFF1F6FF),
  Color(0xFFEBFFEA),
  Color(0xFFFFF2CF),
];

List<Color> categoryBorderColor =[
  Color(0xFF65F6FF),
  Color(0xFF9B9FFF),
  Color(0xFF53B175).withOpacity(0.50),
  Color(0xFFF7A593).withOpacity(0.60),
  Color(0xFFFFBAD6),
  Color(0xFFBAD3FF),
  Color(0xFFBDFFBA),
  Color(0xFFFFE294),
  Color(0xFF65F6FF),
  Color(0xFF9B9FFF),
  Color(0xFF53B175).withOpacity(0.50),
  Color(0xFFF7A593).withOpacity(0.60),
  Color(0xFFFFBAD6),
  Color(0xFFBAD3FF),
  Color(0xFFBDFFBA),
  Color(0xFFFFE294),
  Color(0xFF65F6FF),
  Color(0xFF9B9FFF),
  Color(0xFF53B175).withOpacity(0.50),
  Color(0xFFF7A593).withOpacity(0.60),
  Color(0xFFFFBAD6),
  Color(0xFFBAD3FF),
  Color(0xFFBDFFBA),
  Color(0xFFFFE294),
];

//SizedBox Height
final kHeightBox5 = SizedBox(height: getProportionateScreenHeight(5));
final kHeightBox8 = SizedBox(height: getProportionateScreenHeight(8));
final kHeightBox10 = SizedBox(height: getProportionateScreenHeight(10));
final kHeightBox15 = SizedBox(height: getProportionateScreenHeight(15));
final kHeightBox20 = SizedBox(height: getProportionateScreenHeight(20));
final kHeightBox25 = SizedBox(height: getProportionateScreenHeight(25));
final kHeightBox30 = SizedBox(height: getProportionateScreenHeight(30));
final kHeightBox40 = SizedBox(height: getProportionateScreenHeight(40));

//SizedBox Width
final kWidthBox5 = SizedBox(width: getProportionateScreenHeight(5));
final kWidthBox10 = SizedBox(width: getProportionateScreenHeight(10));
final kWidthBox15 = SizedBox(width: getProportionateScreenHeight(15));
final kWidthBox20 = SizedBox(width: getProportionateScreenHeight(20));

final kHomeTitle = TextStyle(
  color: kPrimaryColor,
  fontWeight: FontWeight.w600,
  fontSize: getProportionateScreenWidth(16),
);

final kHeadLine = TextStyle(
  fontFamily: GoogleFonts.roboto().fontFamily,
  fontSize: getProportionateScreenWidth(34.0),
  fontWeight: FontWeight.w700,
  height: 1.25,
);
final kHeadLineTest = TextStyle(
  fontFamily: GoogleFonts.roboto().fontFamily,
  fontSize: getProportionateScreenWidth(34.0),
  fontWeight: FontWeight.w700,
  height: 1.25,
);
final kHeadLine2 = TextStyle(
  fontFamily: GoogleFonts.roboto().fontFamily,
  fontSize: getProportionateScreenWidth(24.0),
  fontWeight: FontWeight.w600,
  height: 1.25,
);
final kHeadLine3 = TextStyle(
  fontFamily: GoogleFonts.roboto().fontFamily,
  fontSize: getProportionateScreenWidth(22.0),
  fontWeight: FontWeight.w600,
  height: 1.25,
);
final kHeadLine4 = TextStyle(
  fontFamily: GoogleFonts.roboto().fontFamily,
  fontSize: getProportionateScreenWidth(20.0),
  fontWeight: FontWeight.w600,
  height: 1.25,
);
final kAppBarText = TextStyle(
  fontFamily: GoogleFonts.roboto().fontFamily,
  fontSize: getProportionateScreenWidth(18.0),
  fontWeight: FontWeight.w600,
  height: 1.25,
);
final kRegularText = TextStyle(
  fontFamily: GoogleFonts.roboto().fontFamily,
  fontSize: getProportionateScreenWidth(16.0),
  fontWeight: FontWeight.w400,
  height: 1.25,
);
final kDescriptionText = TextStyle(
  fontFamily: GoogleFonts.roboto().fontFamily,
  fontSize: getProportionateScreenWidth(14.0),
  fontWeight: FontWeight.w400,
  height: 1.25,
);
final kSmallText = TextStyle(
  fontFamily: GoogleFonts.roboto().fontFamily,
  fontSize: getProportionateScreenWidth(11.0),
  fontWeight: FontWeight.w400,
  height: 1.25,
);

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String kEmailNullError = "Please enter your email";
const String kInvalidEmailError = "Please enter valid Email";
const String kPassNullError = "Please enter your password";
const String kShortPassError = "Password is too short";
const String kMatchPassError = "Passwords don't match";
const String kNameNullError = "Please enter your name";
const String kPhoneNumberNullError = "Please enter your phone number";
const String kAddressNullError = "Please enter your address";
const String kInvalidNumberError = "Invalid phone number";
const String kAreaNullError = "Please enter your area";
