import 'package:familytrust/view/screens/auth/forget_pass.dart';
import 'package:familytrust/view/screens/auth/login.dart';
import 'package:familytrust/view/screens/auth/signup.dart';
import 'package:familytrust/view/screens/dashboard/dashboard_screen.dart';
import 'package:familytrust/view/screens/grocery_page/checkout/chekout_screen.dart';
import 'package:familytrust/view/screens/grocery_page/child_categories/child_categories_screen.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_cart/cart_screen.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/all_categories.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/productlist_screen.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/grocery_home.dart';
import 'package:familytrust/view/screens/grocery_page/search/search_page.dart';
import 'package:familytrust/view/screens/grocery_page/sheared/product_details.dart';
import 'package:familytrust/view/screens/homepage/homepage.dart';
import 'package:familytrust/view/screens/no_internet/no_internet_screen.dart';
import 'package:familytrust/view/screens/order_page/order_screens.dart';
import 'package:familytrust/view/screens/profile/profile_screen.dart';
import 'package:familytrust/view/screens/shop_page/checkout/shop_checkout.dart';
import 'package:familytrust/view/screens/shop_page/home_page/home_page.dart';
import 'package:familytrust/view/screens/shop_page/home_page/sub_cat/subcat_screen.dart';
import 'package:familytrust/view/screens/shop_page/products/brandproducts.dart';
import 'package:familytrust/view/screens/shop_page/products/childcatproducts.dart';
import 'package:familytrust/view/screens/shop_page/products/shopproductdetails.dart';
import 'package:familytrust/view/screens/shop_page/products/subcatproducts.dart';
import 'package:familytrust/view/screens/shop_page/search_page/shop_search.dart';
import 'package:familytrust/view/screens/shop_page/shop_cart/shop_cart_screen.dart';
import 'package:familytrust/view/screens/shop_page/shop_categories/shop_categories.dart';
import 'package:familytrust/view/screens/shop_page/shop_main.dart';
import 'package:familytrust/view/screens/shop_page/wislist/wish_page_screen.dart';
import 'package:familytrust/view/screens/splash/splash_screen.dart';
import 'package:flutter/material.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  HomePage.routeName: (context) => HomePage(),
  DashboardScreen.routeName: (context) => DashboardScreen(),
  GroceryHome.routeName: (context) => GroceryHome(),
  LogIn.routeName: (context) => LogIn(),
  SignUp.routeName: (context) => SignUp(),
  ForgetPass.routeName: (context) => ForgetPass(),
  ChildCatScreen.routeName: (context) => ChildCatScreen(),
  GroceryCartScreen.routeName: (context) => GroceryCartScreen(),
  ProductDetails.routeName: (context) => ProductDetails(),
  ProfileScreen.routeName: (context) => ProfileScreen(),
  CheckOutScreen.routeName: (context) => CheckOutScreen(),
  AllCategories.routeName: (context) => AllCategories(),
  SearchPage.routeName: (context) => SearchPage(),
  ProductListScreen.routeName: (context) => ProductListScreen(),
  ShopHomeScreen.routeName: (context) => ShopHomeScreen(),
  ShopSearchScreen.routeName: (context) => ShopSearchScreen(),
  ShopMainPage.routeName: (context) => ShopMainPage(),
  ShopCartScreen.routeName: (context) => ShopCartScreen(),
  ShopCategoriesScreen.routeName: (context) => ShopCategoriesScreen(),
  WishScreen.routeName: (context) => WishScreen(),
  SubCatScreen.routeName: (context) => SubCatScreen(),
  SubCatProductsScreen.routeName: (context) => SubCatProductsScreen(),
  ChildCatProductsScreen.routeName: (context) => ChildCatProductsScreen(),
  ShopProductDetails.routeName: (context) => ShopProductDetails(),
  BrandProducts.routeName: (context) => BrandProducts(),
  ShopCheckOut.routeName: (context) => ShopCheckOut(),
  OrderScreens.routeName: (context) => OrderScreens(),
  NoInternetScreen.routeName: (context) => NoInternetScreen(),
};
