// To parse this JSON data, do
//
//     final rpWalletModel = rpWalletModelFromJson(jsonString);

import 'dart:convert';

RpWalletModel rpWalletModelFromJson(String str) =>
    RpWalletModel.fromJson(json.decode(str));

String rpWalletModelToJson(RpWalletModel data) => json.encode(data.toJson());

class RpWalletModel {
  RpWalletModel({
    this.package,
    this.point,
    this.progress,
    this.histories,
  });

  Package package;
  int point;
  int progress;
  List<History> histories;

  factory RpWalletModel.fromJson(Map<String, dynamic> json) => RpWalletModel(
        package:
            json["package"] == null ? null : Package.fromJson(json["package"]),
        point: json["point"] == null ? null : json["point"],
        progress: json["progress"] == null ? null : json["progress"].round(),
        histories: json["histories"] == null
            ? null
            : List<History>.from(
                json["histories"].map((x) => History.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "package": package == null ? null : package.toJson(),
        "point": point == null ? null : point,
        "progress": progress == null ? null : progress,
        "histories": histories == null
            ? null
            : List<dynamic>.from(histories.map((x) => x.toJson())),
      };
}

class History {
  History({
    this.id,
    this.userId,
    this.point,
    this.remark,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int userId;
  int point;
  String remark;
  DateTime createdAt;
  DateTime updatedAt;

  factory History.fromJson(Map<String, dynamic> json) => History(
        id: json["id"] == null ? null : json["id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        point: json["point"] == null ? null : json["point"],
        remark: json["remark"] == null ? null : json["remark"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "point": point == null ? null : point,
        "remark": remark == null ? null : remark,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
      };
}

class Package {
  Package({
    this.id,
    this.name,
    this.minRange,
    this.maxRange,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String name;
  int minRange;
  int maxRange;
  dynamic createdAt;
  DateTime updatedAt;

  factory Package.fromJson(Map<String, dynamic> json) => Package(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        minRange: json["min_range"] == null ? null : json["min_range"],
        maxRange: json["max_range"] == null ? null : json["max_range"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "min_range": minRange == null ? null : minRange,
        "max_range": maxRange == null ? null : maxRange,
        "created_at": createdAt,
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
      };
}
