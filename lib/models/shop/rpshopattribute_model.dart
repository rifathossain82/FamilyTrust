// To parse this JSON data, do
//
//     final rpShopAttributes = rpShopAttributesFromJson(jsonString);

import 'dart:convert';

RpShopAttributes rpShopAttributesFromJson(String str) =>
    RpShopAttributes.fromJson(json.decode(str));

String rpShopAttributesToJson(RpShopAttributes data) =>
    json.encode(data.toJson());

class RpShopAttributes {
  RpShopAttributes({
    this.attrib,
  });

  List<Attrib> attrib;

  factory RpShopAttributes.fromJson(Map<String, dynamic> json) =>
      RpShopAttributes(
        attrib: json["attrib"] == null
            ? null
            : List<Attrib>.from(json["attrib"].map((x) => Attrib.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "attrib": attrib == null
            ? null
            : List<dynamic>.from(attrib.map((x) => x.toJson())),
      };
}

class Attrib {
  Attrib({
    this.attributeName,
    this.values,
  });

  String attributeName;
  String values;

  factory Attrib.fromJson(Map<String, dynamic> json) => Attrib(
        attributeName:
            json["attribute_name"] == null ? null : json["attribute_name"],
        values: json["values"] == null ? null : json["values"],
      );

  Map<String, dynamic> toJson() => {
        "attribute_name": attributeName == null ? null : attributeName,
        "values": values == null ? null : values,
      };
}
