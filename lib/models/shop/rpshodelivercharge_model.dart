// To parse this JSON data, do
//
//     final rpAllDistrictModel = rpAllDistrictModelFromJson(jsonString);

import 'dart:convert';

RpAllDistrictModel rpAllDistrictModelFromJson(String str) =>
    RpAllDistrictModel.fromJson(json.decode(str));

String rpAllDistrictModelToJson(RpAllDistrictModel data) =>
    json.encode(data.toJson());

class RpAllDistrictModel {
  RpAllDistrictModel({
    this.data,
  });

  List<Datum> data;

  factory RpAllDistrictModel.fromJson(Map<String, dynamic> json) =>
      RpAllDistrictModel(
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.name,
    this.shippingType,
  });

  int id;
  String name;
  String shippingType;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        shippingType:
            json["shipping_type"] == null ? null : json["shipping_type"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "shipping_type": shippingType == null ? null : shippingType,
      };
}
