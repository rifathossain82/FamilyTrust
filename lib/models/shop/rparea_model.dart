// To parse this JSON data, do
//
//     final rpAreaModel = rpAreaModelFromJson(jsonString);

import 'dart:convert';

RpAreaModelShop rpAreaModelFromJson(String str) =>
    RpAreaModelShop.fromJson(json.decode(str));

String rpAreaModelToJson(RpAreaModelShop data) => json.encode(data.toJson());

class RpAreaModelShop {
  RpAreaModelShop({
    this.data,
  });

  List<Datum> data;

  factory RpAreaModelShop.fromJson(Map<String, dynamic> json) => RpAreaModelShop(
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.areaName,
  });

  int id;
  String areaName;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        areaName: json["area_name"] == null ? null : json["area_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "area_name": areaName == null ? null : areaName,
      };
}
