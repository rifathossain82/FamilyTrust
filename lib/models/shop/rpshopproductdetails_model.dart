// To parse this JSON data, do
//
//     final rpShopProductDetailsModel = rpShopProductDetailsModelFromJson(jsonString);

import 'dart:convert';

RpShopProductDetailsModel rpShopProductDetailsModelFromJson(String str) =>
    RpShopProductDetailsModel.fromJson(json.decode(str));

String rpShopProductDetailsModelToJson(RpShopProductDetailsModel data) =>
    json.encode(data.toJson());

class RpShopProductDetailsModel {
  RpShopProductDetailsModel({
    this.data,
  });

  Data data;

  factory RpShopProductDetailsModel.fromJson(Map<String, dynamic> json) =>
      RpShopProductDetailsModel(
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.id,
    this.name,
    this.bnName,
    this.thumbnailImg,
    this.description,
    this.price,
    this.qty,
    this.bnPrice,
    this.attributes,
    this.colors,
    this.currentStock,
    this.unit,
    this.bnUnit,
    this.discount,
    this.discountAmount,
    this.bnDiscountAmount,
    this.bnDiscount,
    this.discountType,
    this.shipping,
    this.totalReview,
    this.totalStar,
  });

  int id;
  String name;
  String bnName;
  String thumbnailImg;
  String description;
  int price;
  int qty = 0;
  String bnPrice;
  List<Attribute> attributes;
  List<String> colors;
  int currentStock;
  String unit;
  dynamic bnUnit;
  int discount;
  int discountAmount;
  String bnDiscountAmount;
  String bnDiscount;
  String discountType;
  Shipping shipping;
  int totalReview;
  int totalStar;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        bnName: json["bn_name"] == null ? null : json["bn_name"],
        thumbnailImg:
            json["thumbnail_img"] == null ? null : json["thumbnail_img"],
        description: json["description"] == null ? null : json["description"],
        price: json["price"] == null ? null : json["price"].round(),
        qty: 0,
        bnPrice: json["bn_price"] == null ? null : json["bn_price"],
        attributes: json["attributes"] == null
            ? null
            : List<Attribute>.from(
                json["attributes"].map((x) => Attribute.fromJson(x))),
        colors: json["colors"] == null
            ? null
            : List<String>.from(json["colors"].map((x) => x)),
        currentStock: json["current_stock"] == null ? 0 : json["current_stock"],
        unit: json["unit"] == null ? null : json["unit"],
        bnUnit: json["bn_unit"],
        discount: json["discount"] == null ? null : json["discount"],
        discountAmount: json["discount_amount"] == null
            ? null
            : json["discount_amount"].round(),
        bnDiscountAmount: json["bn_discount_amount"] == null
            ? null
            : json["bn_discount_amount"],
        bnDiscount: json["bn_discount"] == null ? null : json["bn_discount"],
        discountType:
            json["discount_type"] == null ? null : json["discount_type"],
        shipping: json["shipping"] == null
            ? null
            : Shipping.fromJson(json["shipping"]),
        totalReview: json["total_review"] == null ? null : json["total_review"],
        totalStar: json["total_star"] == null ? null : json["total_star"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "bn_name": bnName == null ? null : bnName,
        "thumbnail_img": thumbnailImg == null ? null : thumbnailImg,
        "description": description == null ? null : description,
        "price": price == null ? null : price,
        "qty": 0,
        "bn_price": bnPrice == null ? null : bnPrice,
        "attributes": attributes == null
            ? null
            : List<dynamic>.from(attributes.map((x) => x.toJson())),
        "colors":
            colors == null ? null : List<dynamic>.from(colors.map((x) => x)),
        "current_stock": currentStock == null ? 0 : currentStock,
        "unit": unit == null ? null : unit,
        "bn_unit": bnUnit,
        "discount": discount == null ? null : discount,
        "discount_amount": discountAmount == null ? null : discountAmount,
        "bn_discount_amount":
            bnDiscountAmount == null ? null : bnDiscountAmount,
        "bn_discount": bnDiscount == null ? null : bnDiscount,
        "discount_type": discountType == null ? null : discountType,
        "shipping": shipping == null ? null : shipping.toJson(),
        "total_review": totalReview == null ? null : totalReview,
        "total_star": totalStar == null ? null : totalStar,
      };
}

class Attribute {
  Attribute({
    this.attributeName,
    this.values,
  });

  String attributeName;
  List<String> values;

  factory Attribute.fromJson(Map<String, dynamic> json) => Attribute(
        attributeName:
            json["attribute_name"] == null ? null : json["attribute_name"],
        values: json["values"] == null
            ? null
            : List<String>.from(json["values"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "attribute_name": attributeName == null ? null : attributeName,
        "values":
            values == null ? null : List<dynamic>.from(values.map((x) => x)),
      };
}

class Shipping {
  Shipping({
    this.insideDhaka,
    this.outsideDhaka,
    this.aroundDhaka,
  });

  Dhaka insideDhaka;
  Dhaka outsideDhaka;
  Dhaka aroundDhaka;

  factory Shipping.fromJson(Map<String, dynamic> json) => Shipping(
        insideDhaka: json["inside_dhaka"] == null
            ? null
            : Dhaka.fromJson(json["inside_dhaka"]),
        outsideDhaka: json["outside_dhaka"] == null
            ? null
            : Dhaka.fromJson(json["outside_dhaka"]),
        aroundDhaka: json["around_dhaka"] == null
            ? null
            : Dhaka.fromJson(json["around_dhaka"]),
      );

  Map<String, dynamic> toJson() => {
        "inside_dhaka": insideDhaka == null ? null : insideDhaka.toJson(),
        "outside_dhaka": outsideDhaka == null ? null : outsideDhaka.toJson(),
        "around_dhaka": aroundDhaka == null ? null : aroundDhaka.toJson(),
      };
}

class Dhaka {
  Dhaka({
    this.maxQuantity,
    this.minQuantity,
    this.shippingCharge,
  });

  List<String> maxQuantity;
  List<String> minQuantity;
  List<String> shippingCharge;

  factory Dhaka.fromJson(Map<String, dynamic> json) => Dhaka(
        maxQuantity: json["max_quantity"] == null
            ? null
            : List<String>.from(json["max_quantity"].map((x) => x)),
        minQuantity: json["min_quantity"] == null
            ? null
            : List<String>.from(json["min_quantity"].map((x) => x)),
        shippingCharge: json["shipping_charge"] == null
            ? null
            : List<String>.from(json["shipping_charge"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "max_quantity": maxQuantity == null
            ? null
            : List<dynamic>.from(maxQuantity.map((x) => x)),
        "min_quantity": minQuantity == null
            ? null
            : List<dynamic>.from(minQuantity.map((x) => x)),
        "shipping_charge": shippingCharge == null
            ? null
            : List<dynamic>.from(shippingCharge.map((x) => x)),
      };
}
