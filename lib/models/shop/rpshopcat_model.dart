// To parse this JSON data, do
//
//     final rpShopCatModel = rpShopCatModelFromJson(jsonString);

import 'dart:convert';

RpShopCatModel rpShopCatModelFromJson(String str) =>
    RpShopCatModel.fromJson(json.decode(str));

String rpShopCatModelToJson(RpShopCatModel data) => json.encode(data.toJson());

class RpShopCatModel {
  RpShopCatModel({
    this.data,
  });

  List<Datum> data;

  factory RpShopCatModel.fromJson(Map<String, dynamic> json) => RpShopCatModel(
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.categoryName,
    this.bnCategoryName,
    this.picture,
    this.banerPicture,
    this.subcategories,
  });

  int id;
  String categoryName;
  String bnCategoryName;
  String picture;
  String banerPicture;
  List<Subcategory> subcategories;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        categoryName:
            json["category_name"] == null ? null : json["category_name"],
        bnCategoryName:
            json["bn_category_name"] == null ? null : json["bn_category_name"],
        picture: json["picture"] == null ? null : json["picture"],
        banerPicture:
            json["baner_picture"] == null ? null : json["baner_picture"],
        subcategories: json["subcategories"] == null
            ? null
            : List<Subcategory>.from(
                json["subcategories"].map((x) => Subcategory.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "category_name": categoryName == null ? null : categoryName,
        "bn_category_name": bnCategoryName == null ? null : bnCategoryName,
        "picture": picture == null ? null : picture,
        "baner_picture": banerPicture == null ? null : banerPicture,
        "subcategories": subcategories == null
            ? null
            : List<dynamic>.from(subcategories.map((x) => x.toJson())),
      };
}

class Subcategory {
  Subcategory({
    this.id,
    this.subCategoryName,
    this.bnSubCategoryName,
    this.picture,
    this.bannerPicture,
    this.childCategories,
  });

  int id;
  String subCategoryName;
  String bnSubCategoryName;
  String picture;
  String bannerPicture;
  List<ChildCategory> childCategories;

  factory Subcategory.fromJson(Map<String, dynamic> json) => Subcategory(
        id: json["id"] == null ? null : json["id"],
        subCategoryName: json["sub_category_name"] == null
            ? null
            : json["sub_category_name"],
        bnSubCategoryName: json["bn_sub_category_name"] == null
            ? json["sub_category_name"]
            : json["bn_sub_category_name"],
        picture: json["picture"] == null ? null : json["picture"],
        bannerPicture:
            json["banner_picture"] == null ? null : json["banner_picture"],
        childCategories: json["child_categories"] == null
            ? null
            : List<ChildCategory>.from(
                json["child_categories"].map((x) => ChildCategory.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "sub_category_name": subCategoryName == null ? null : subCategoryName,
        "bn_sub_category_name":
            bnSubCategoryName == null ? null : bnSubCategoryName,
        "picture": picture == null ? null : picture,
        "banner_picture": bannerPicture == null ? null : bannerPicture,
        "child_categories": childCategories == null
            ? null
            : List<dynamic>.from(childCategories.map((x) => x.toJson())),
      };
}

class ChildCategory {
  ChildCategory({
    this.id,
    this.childCategoryName,
    this.bnChildCategoryName,
    this.picture,
    this.bannerPicture,
  });

  int id;
  String childCategoryName;
  String bnChildCategoryName;
  String picture;
  String bannerPicture;

  factory ChildCategory.fromJson(Map<String, dynamic> json) => ChildCategory(
        id: json["id"] == null ? null : json["id"],
        childCategoryName: json["child_category_name"] == null
            ? null
            : json["child_category_name"],
        bnChildCategoryName: json["bn_child_category_name"] == null
            ? json["child_category_name"]
            : json["bn_child_category_name"],
        picture: json["picture"] == null ? null : json["picture"],
        bannerPicture:
            json["banner_picture"] == null ? null : json["banner_picture"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "child_category_name":
            childCategoryName == null ? null : childCategoryName,
        "bn_child_category_name":
            bnChildCategoryName == null ? null : bnChildCategoryName,
        "picture": picture == null ? null : picture,
        "banner_picture": bannerPicture == null ? null : bannerPicture,
      };
}
