// To parse this JSON data, do
//
//     final rpShopSearchModel = rpShopSearchModelFromJson(jsonString);

import 'dart:convert';

RpShopSearchModel rpShopSearchModelFromJson(String str) =>
    RpShopSearchModel.fromJson(json.decode(str));

String rpShopSearchModelToJson(RpShopSearchModel data) =>
    json.encode(data.toJson());

class RpShopSearchModel {
  RpShopSearchModel({
    this.data,
    this.links,
    this.meta,
  });

  List<Datum> data;
  Links links;
  Meta meta;

  factory RpShopSearchModel.fromJson(Map<String, dynamic> json) =>
      RpShopSearchModel(
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        links: json["links"] == null ? null : Links.fromJson(json["links"]),
        meta: json["meta"] == null ? null : Meta.fromJson(json["meta"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
        "links": links == null ? null : links.toJson(),
        "meta": meta == null ? null : meta.toJson(),
      };
}

class Datum {
  Datum({
    this.id,
    this.name,
    this.bnName,
    this.thumbnailImg,
    this.description,
    this.price,
    this.bnPrice,
    this.attributes,
    this.colors,
    this.currentStock,
    this.unit,
    this.bnUnit,
    this.discount,
    this.discountAmount,
    this.bnDiscountAmount,
    this.bnDiscount,
    this.discountType,
    this.totalReview,
    this.totalStar,
  });

  int id;
  String name;
  String bnName;
  String thumbnailImg;
  String description;
  int price;
  String bnPrice;
  List<Attribute> attributes;
  List<String> colors;
  int currentStock;
  Unit unit;
  dynamic bnUnit;
  int discount;
  int discountAmount;
  String bnDiscountAmount;
  String bnDiscount;
  DiscountType discountType;
  int totalReview;
  int totalStar;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        bnName: json["bn_name"] == null ? null : json["bn_name"],
        thumbnailImg:
            json["thumbnail_img"] == null ? null : json["thumbnail_img"],
        description: json["description"] == null ? null : json["description"],
        price: json["price"] == null ? null : json["price"].round(),
        bnPrice: json["bn_price"] == null ? null : json["bn_price"],
        attributes: json["attributes"] == null
            ? null
            : List<Attribute>.from(
                json["attributes"].map((x) => Attribute.fromJson(x))),
        colors: json["colors"] == null
            ? null
            : List<String>.from(json["colors"].map((x) => x)),
        currentStock: json["current_stock"] == null ? 0 : json["current_stock"],
        unit: json["unit"] == null ? null : unitValues.map[json["unit"]],
        bnUnit: json["bn_unit"],
        discount: json["discount"] == null ? null : json["discount"],
        discountAmount: json["discount_amount"] == null
            ? null
            : json["discount_amount"].round(),
        bnDiscountAmount: json["bn_discount_amount"] == null
            ? null
            : json["bn_discount_amount"],
        bnDiscount: json["bn_discount"] == null ? null : json["bn_discount"],
        discountType: json["discount_type"] == null
            ? null
            : discountTypeValues.map[json["discount_type"]],
        totalReview: json["total_review"] == null ? null : json["total_review"],
        totalStar: json["total_star"] == null ? null : json["total_star"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "bn_name": bnName == null ? null : bnName,
        "thumbnail_img": thumbnailImg == null ? null : thumbnailImg,
        "description": description == null ? null : description,
        "price": price == null ? null : price,
        "bn_price": bnPrice == null ? null : bnPrice,
        "attributes": attributes == null
            ? null
            : List<dynamic>.from(attributes.map((x) => x.toJson())),
        "colors":
            colors == null ? null : List<dynamic>.from(colors.map((x) => x)),
        "current_stock": currentStock == null ? 0 : currentStock,
        "unit": unit == null ? null : unitValues.reverse[unit],
        "bn_unit": bnUnit,
        "discount": discount == null ? null : discount,
        "discount_amount": discountAmount == null ? null : discountAmount,
        "bn_discount_amount":
            bnDiscountAmount == null ? null : bnDiscountAmount,
        "bn_discount": bnDiscount == null ? null : bnDiscount,
        "discount_type": discountType == null
            ? null
            : discountTypeValues.reverse[discountType],
        "total_review": totalReview == null ? null : totalReview,
        "total_star": totalStar == null ? null : totalStar,
      };
}

class Attribute {
  Attribute({
    this.attributeName,
    this.values,
  });

  AttributeName attributeName;
  List<String> values;

  factory Attribute.fromJson(Map<String, dynamic> json) => Attribute(
        attributeName: json["attribute_name"] == null
            ? null
            : attributeNameValues.map[json["attribute_name"]],
        values: json["values"] == null
            ? null
            : List<String>.from(json["values"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "attribute_name": attributeName == null
            ? null
            : attributeNameValues.reverse[attributeName],
        "values":
            values == null ? null : List<dynamic>.from(values.map((x) => x)),
      };
}

enum AttributeName { SIZE }

final attributeNameValues = EnumValues({"Size": AttributeName.SIZE});

enum DiscountType { AMOUNT, PERCENT }

final discountTypeValues = EnumValues(
    {"amount": DiscountType.AMOUNT, "percent": DiscountType.PERCENT});

enum Unit { PCE, THE_1_PCS, THE_1_PC }

final unitValues = EnumValues(
    {"pce": Unit.PCE, "1pc": Unit.THE_1_PC, "1 pcs": Unit.THE_1_PCS});

class Links {
  Links({
    this.first,
    this.last,
    this.prev,
    this.next,
  });

  String first;
  String last;
  dynamic prev;
  dynamic next;

  factory Links.fromJson(Map<String, dynamic> json) => Links(
        first: json["first"] == null ? null : json["first"],
        last: json["last"] == null ? null : json["last"],
        prev: json["prev"],
        next: json["next"],
      );

  Map<String, dynamic> toJson() => {
        "first": first == null ? null : first,
        "last": last == null ? null : last,
        "prev": prev,
        "next": next,
      };
}

class Meta {
  Meta({
    this.currentPage,
    this.from,
    this.lastPage,
    this.links,
    this.path,
    this.perPage,
    this.to,
    this.total,
  });

  int currentPage;
  int from;
  int lastPage;
  List<Link> links;
  String path;
  int perPage;
  int to;
  int total;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        currentPage: json["current_page"] == null ? null : json["current_page"],
        from: json["from"] == null ? null : json["from"],
        lastPage: json["last_page"] == null ? null : json["last_page"],
        links: json["links"] == null
            ? null
            : List<Link>.from(json["links"].map((x) => Link.fromJson(x))),
        path: json["path"] == null ? null : json["path"],
        perPage: json["per_page"] == null ? null : json["per_page"],
        to: json["to"] == null ? null : json["to"],
        total: json["total"] == null ? null : json["total"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage == null ? null : currentPage,
        "from": from == null ? null : from,
        "last_page": lastPage == null ? null : lastPage,
        "links": links == null
            ? null
            : List<dynamic>.from(links.map((x) => x.toJson())),
        "path": path == null ? null : path,
        "per_page": perPage == null ? null : perPage,
        "to": to == null ? null : to,
        "total": total == null ? null : total,
      };
}

class Link {
  Link({
    this.url,
    this.label,
    this.active,
  });

  String url;
  String label;
  bool active;

  factory Link.fromJson(Map<String, dynamic> json) => Link(
        url: json["url"] == null ? null : json["url"],
        label: json["label"] == null ? null : json["label"],
        active: json["active"] == null ? null : json["active"],
      );

  Map<String, dynamic> toJson() => {
        "url": url == null ? null : url,
        "label": label == null ? null : label,
        "active": active == null ? null : active,
      };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
