// To parse this JSON data, do
//
//     final rpShopCatBrandsModel = rpShopCatBrandsModelFromJson(jsonString);

import 'dart:convert';

RpShopCatBrandsModel rpShopCatBrandsModelFromJson(String str) =>
    RpShopCatBrandsModel.fromJson(json.decode(str));

String rpShopCatBrandsModelToJson(RpShopCatBrandsModel data) =>
    json.encode(data.toJson());

class RpShopCatBrandsModel {
  RpShopCatBrandsModel({
    this.data,
  });

  List<Datum> data;

  factory RpShopCatBrandsModel.fromJson(Map<String, dynamic> json) =>
      RpShopCatBrandsModel(
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.categoryName,
    this.bnCategoryName,
    this.categoryPicture,
    this.brands,
  });

  int id;
  String categoryName;
  String bnCategoryName;
  String categoryPicture;
  List<Brand> brands;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        categoryName:
            json["category_name"] == null ? null : json["category_name"],
        bnCategoryName:
            json["bn_category_name"] == null ? null : json["bn_category_name"],
        categoryPicture:
            json["category_picture"] == null ? null : json["category_picture"],
        brands: json["brands"] == null
            ? null
            : List<Brand>.from(json["brands"].map((x) => Brand.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "category_name": categoryName == null ? null : categoryName,
        "bn_category_name": bnCategoryName == null ? null : bnCategoryName,
        "category_picture": categoryPicture == null ? null : categoryPicture,
        "brands": brands == null
            ? null
            : List<dynamic>.from(brands.map((x) => x.toJson())),
      };
}

class Brand {
  Brand({
    this.id,
    this.brandName,
    this.bnBrandName,
    this.brandPicture,
  });

  int id;
  String brandName;
  dynamic bnBrandName;
  String brandPicture;

  factory Brand.fromJson(Map<String, dynamic> json) => Brand(
        id: json["id"] == null ? null : json["id"],
        brandName: json["brand_name"] == null ? 'null' : json["brand_name"],
        bnBrandName: json["bn_brand_name"] == null
            ? json["brand_name"]
            : json["bn_brand_name"],
        brandPicture:
            json["brand_picture"] == null ? null : json["brand_picture"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "brand_name": brandName == null ? null : brandName,
        "bn_brand_name": bnBrandName == null ? brandName : bnBrandName,
        "brand_picture": brandPicture == null ? null : brandPicture,
      };
}
