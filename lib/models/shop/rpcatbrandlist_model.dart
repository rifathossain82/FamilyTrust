// To parse this JSON data, do
//
//     final rpCatBrandListModel = rpCatBrandListModelFromJson(jsonString);

import 'dart:convert';

RpCatBrandListModel rpCatBrandListModelFromJson(String str) =>
    RpCatBrandListModel.fromJson(json.decode(str));

String rpCatBrandListModelToJson(RpCatBrandListModel data) =>
    json.encode(data.toJson());

class RpCatBrandListModel {
  RpCatBrandListModel({
    this.data,
  });

  List<Datum> data;

  factory RpCatBrandListModel.fromJson(Map<String, dynamic> json) =>
      RpCatBrandListModel(
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.brandName,
    this.bnBrandName,
  });

  int id;
  String brandName;
  dynamic bnBrandName;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        brandName: json["brand_name"] == null ? null : json["brand_name"],
        bnBrandName: json["bn_brand_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "brand_name": brandName == null ? null : brandName,
        "bn_brand_name": bnBrandName,
      };
}
