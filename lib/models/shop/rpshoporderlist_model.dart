// To parse this JSON data, do
//
//     final rpShopOrderListModel = rpShopOrderListModelFromJson(jsonString);

import 'dart:convert';

RpShopOrderListModel rpShopOrderListModelFromJson(String str) =>
    RpShopOrderListModel.fromJson(json.decode(str));

String rpShopOrderListModelToJson(RpShopOrderListModel data) =>
    json.encode(data.toJson());

class RpShopOrderListModel {
  RpShopOrderListModel({
    this.data,
  });

  List<Datum> data;

  factory RpShopOrderListModel.fromJson(Map<String, dynamic> json) =>
      RpShopOrderListModel(
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.orderId,
    this.user,
    this.userPhone,
    this.paymentType,
    this.paymentStatus,
    this.subtotal,
    this.shippingCharge,
    this.total,
    this.status,
    this.deliveryNote,
    this.returnNote,
    this.orderDate,
    this.orderTime,
    this.orderName,
    this.address,
    this.city,
    this.postcode,
    this.district,
    this.area,
    this.orderProducts,
  });

  String orderId;
  String user;
  String userPhone;
  String paymentType;
  String paymentStatus;
  int subtotal;
  String shippingCharge;
  int total;
  int status;
  dynamic deliveryNote;
  dynamic returnNote;
  DateTime orderDate;
  String orderTime;
  String orderName;
  String address;
  String city;
  String postcode;
  String district;
  String area;
  List<OrderProduct> orderProducts;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        orderId: json["order_id"] == null ? null : json["order_id"],
        user: json["user"] == null ? null : json["user"],
        userPhone: json["user_phone"] == null ? null : json["user_phone"],
        paymentType: json["payment_type"] == null ? null : json["payment_type"],
        paymentStatus:
            json["payment_status"] == null ? null : json["payment_status"],
        subtotal: json["subtotal"] == null ? null : json["subtotal"],
        shippingCharge:
            json["shipping_charge"] == null ? null : json["shipping_charge"],
        total: json["total"] == null ? null : json["total"],
        status: json["status"] == null ? null : json["status"],
        deliveryNote: json["delivery_note"],
        returnNote: json["return_note"],
        orderDate: json["order_date"] == null
            ? null
            : DateTime.parse(json["order_date"]),
        orderTime: json["order_time"] == null ? null : json["order_time"],
        orderName: json["order_name"] == null ? null : json["order_name"],
        address: json["address"] == null ? null : json["address"],
        city: json["city"] == null ? null : json["city"],
        postcode: json["postcode"] == null ? null : json["postcode"],
        district: json["district"] == null ? null : json["district"],
        area: json["area"] == null ? null : json["area"],
        orderProducts: json["order_products"] == null
            ? null
            : List<OrderProduct>.from(
                json["order_products"].map((x) => OrderProduct.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "order_id": orderId == null ? null : orderId,
        "user": user == null ? null : user,
        "user_phone": userPhone == null ? null : userPhone,
        "payment_type": paymentType == null ? null : paymentType,
        "payment_status": paymentStatus == null ? null : paymentStatus,
        "subtotal": subtotal == null ? null : subtotal,
        "shipping_charge": shippingCharge == null ? null : shippingCharge,
        "total": total == null ? null : total,
        "status": status == null ? null : status,
        "delivery_note": deliveryNote,
        "return_note": returnNote,
        "order_date": orderDate == null
            ? null
            : "${orderDate.year.toString().padLeft(4, '0')}-${orderDate.month.toString().padLeft(2, '0')}-${orderDate.day.toString().padLeft(2, '0')}",
        "order_time": orderTime == null ? null : orderTime,
        "order_name": orderName == null ? null : orderName,
        "address": address == null ? null : address,
        "city": city == null ? null : city,
        "postcode": postcode == null ? null : postcode,
        "district": district == null ? null : district,
        "area": area == null ? null : area,
        "order_products": orderProducts == null
            ? null
            : List<dynamic>.from(orderProducts.map((x) => x.toJson())),
      };
}

class OrderProduct {
  OrderProduct({
    this.name,
    this.bnName,
    this.sku,
    this.thumbnailImg,
    this.price,
    this.qty,
    this.total,
    this.color,
    this.attribute,
  });

  String name;
  String bnName;
  String sku;
  String thumbnailImg;
  int price;
  int qty;
  int total;
  String color;
  dynamic attribute;

  factory OrderProduct.fromJson(Map<String, dynamic> json) => OrderProduct(
        name: json["name"] == null ? null : json["name"],
        bnName: json["bn_name"] == null ? null : json["bn_name"],
        sku: json["sku"] == null ? null : json["sku"],
        thumbnailImg:
            json["thumbnail_img"] == null ? null : json["thumbnail_img"],
        price: json["price"] == null ? null : json["price"],
        qty: json["qty"] == null ? null : json["qty"],
        total: json["total"] == null ? null : json["total"],
        color: json["color"] == null ? null : json["color"],
        attribute: json["attribute"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "bn_name": bnName == null ? null : bnName,
        "sku": sku == null ? null : sku,
        "thumbnail_img": thumbnailImg == null ? null : thumbnailImg,
        "price": price == null ? null : price,
        "qty": qty == null ? null : qty,
        "total": total == null ? null : total,
        "color": color == null ? null : color,
        "attribute": attribute,
      };
}
