// To parse this JSON data, do
//
//     final rpShopPopular = rpShopPopularFromJson(jsonString);

import 'dart:convert';

RpShopPopular rpShopPopularFromJson(String str) =>
    RpShopPopular.fromJson(json.decode(str));

String rpShopPopularToJson(RpShopPopular data) => json.encode(data.toJson());

class RpShopPopular {
  RpShopPopular({
    this.data,
  });

  List<Datum> data;

  factory RpShopPopular.fromJson(Map<String, dynamic> json) => RpShopPopular(
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.name,
    this.bnName,
    this.thumbnailImg,
    this.description,
    this.price,
    this.bnPrice,
    this.attributes,
    this.colors,
    this.currentStock,
    this.unit,
    this.bnUnit,
    this.discount,
    this.discountAmount,
    this.bnDiscountAmount,
    this.bnDiscount,
    this.discountType,
    this.insideDhaka,
    this.outsideDhaka,
    this.aroundDhaka,
    this.totalReview,
    this.totalStar,
  });

  int id;
  String name;
  String bnName;
  String thumbnailImg;
  String description;
  int price;
  String bnPrice;
  dynamic attributes;
  List<String> colors;
  int currentStock;
  String unit;
  dynamic bnUnit;
  dynamic discount;
  int discountAmount;
  String bnDiscountAmount;
  dynamic bnDiscount;
  dynamic discountType;
  Dhaka insideDhaka;
  Dhaka outsideDhaka;
  Dhaka aroundDhaka;
  int totalReview;
  int totalStar;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        bnName: json["bn_name"] == null ? null : json["bn_name"],
        thumbnailImg:
            json["thumbnail_img"] == null ? null : json["thumbnail_img"],
        description: json["description"] == null ? null : json["description"],
        price: json["price"] == null ? null : json["price"].round(),
        bnPrice: json["bn_price"] == null ? null : json["bn_price"],
        attributes: json["attributes"],
        colors: json["colors"] == null
            ? null
            : List<String>.from(json["colors"].map((x) => x)),
        currentStock: json["current_stock"] == null ? 0 : json["current_stock"],
        unit: json["unit"] == null ? null : json["unit"],
        bnUnit: json["bn_unit"],
        discount: json["discount"],
        discountAmount: json["discount_amount"] == null
            ? null
            : json["discount_amount"].round(),
        bnDiscountAmount: json["bn_discount_amount"] == null
            ? null
            : json["bn_discount_amount"],
        bnDiscount: json["bn_discount"],
        discountType: json["discount_type"],
        insideDhaka: json["inside_dhaka"] == null
            ? null
            : Dhaka.fromJson(json["inside_dhaka"]),
        outsideDhaka: json["outside_dhaka"] == null
            ? null
            : Dhaka.fromJson(json["outside_dhaka"]),
        aroundDhaka: json["around_dhaka"] == null
            ? null
            : Dhaka.fromJson(json["around_dhaka"]),
        totalReview: json["total_review"] == null ? null : json["total_review"],
        totalStar: json["total_star"] == null ? null : json["total_star"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "bn_name": bnName == null ? null : bnName,
        "thumbnail_img": thumbnailImg == null ? null : thumbnailImg,
        "description": description == null ? null : description,
        "price": price == null ? null : price,
        "bn_price": bnPrice == null ? null : bnPrice,
        "attributes": attributes,
        "colors":
            colors == null ? null : List<dynamic>.from(colors.map((x) => x)),
        "current_stock": currentStock == null ? 0 : currentStock,
        "unit": unit == null ? null : unit,
        "bn_unit": bnUnit,
        "discount": discount,
        "discount_amount": discountAmount == null ? null : discountAmount,
        "bn_discount_amount":
            bnDiscountAmount == null ? null : bnDiscountAmount,
        "bn_discount": bnDiscount,
        "discount_type": discountType,
        "inside_dhaka": insideDhaka == null ? null : insideDhaka.toJson(),
        "outside_dhaka": outsideDhaka == null ? null : outsideDhaka.toJson(),
        "around_dhaka": aroundDhaka == null ? null : aroundDhaka.toJson(),
        "total_review": totalReview == null ? null : totalReview,
        "total_star": totalStar == null ? null : totalStar,
      };
}

class Dhaka {
  Dhaka({
    this.maxQuantity,
    this.minQuantity,
    this.shippingCharge,
  });

  List<String> maxQuantity;
  List<String> minQuantity;
  List<String> shippingCharge;

  factory Dhaka.fromJson(Map<String, dynamic> json) => Dhaka(
        maxQuantity: json["max_quantity"] == null
            ? null
            : List<String>.from(json["max_quantity"].map((x) => x)),
        minQuantity: json["min_quantity"] == null
            ? null
            : List<String>.from(json["min_quantity"].map((x) => x)),
        shippingCharge: json["shipping_charge"] == null
            ? null
            : List<String>.from(json["shipping_charge"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "max_quantity": maxQuantity == null
            ? null
            : List<dynamic>.from(maxQuantity.map((x) => x)),
        "min_quantity": minQuantity == null
            ? null
            : List<dynamic>.from(minQuantity.map((x) => x)),
        "shipping_charge": shippingCharge == null
            ? null
            : List<dynamic>.from(shippingCharge.map((x) => x)),
      };
}
