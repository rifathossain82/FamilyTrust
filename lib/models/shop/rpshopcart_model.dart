// To parse this JSON data, do
//
//     final rpCartModel = rpCartModelFromJson(jsonString);

import 'dart:convert';

RpShopCartModel rpShopCartModelFromJson(String str) =>
    RpShopCartModel.fromJson(json.decode(str));

String rpCartModelToJson(RpShopCartModel data) => json.encode(data.toJson());

class RpShopCartModel {
  RpShopCartModel({
    this.id,
    this.productName,
    this.bnProductName,
    this.attribute,
    this.color,
    this.picture,
    this.price,
    this.bnPrice,
    this.maxQty,
    this.area,
    this.qty,
    this.cityId,
    this.shippingCharge,
    this.deliveryCharge,
  });

  int id;
  int cityId;
  int deliveryCharge;
  String productName;
  String bnProductName;
  String attribute;
  String color;
  String picture;
  String area;
  int price;
  String maxQty;
  String shippingCharge;
  String bnPrice;
  int qty = 0;

  factory RpShopCartModel.fromJson(Map<String, dynamic> json) =>
      RpShopCartModel(
        id: json["id"] == null ? null : json["id"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        deliveryCharge:
            json["deliveryCharge"] == null ? null : json["deliveryCharge"],
        productName: json["name"] == null ? null : json["name"],
        shippingCharge:
            json["shipping_charge"] == null ? null : json["shipping_charge"],
        bnProductName: json["bn_name"] == null ? null : json["bn_name"],
        attribute: json["attributes"] == null ? '' : json["attributes"],
        color: json["colors"] == null ? '' : json["colors"],
        picture: json["thumbnail_img"] == null ? null : json["thumbnail_img"],
        area: json["area"] == null ? null : json["area"],
        price: json["price"] == null ? null : json["price"],
        bnPrice: json["bn_price"] == null ? null : json["bn_price"],
        maxQty: json["maxQty"] == null ? null : json["maxQty"],
        qty: json["qty"] == null ? null : json["qty"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "deliveryCharge": deliveryCharge == null ? null : deliveryCharge,
        "shipping_charge": shippingCharge == null ? null : shippingCharge,
        "city_id": cityId == null ? null : cityId,
        "name": productName == null ? null : productName,
        "bn_name": bnProductName == null ? null : bnProductName,
        "area": area == null ? null : area,
        "attributes": attribute == null ? null : attribute,
        "colors": color == null ? null : color,
        "thumbnail_img": picture == null ? null : picture,
        "price": price == null ? null : price,
        "bn_price": bnPrice == null ? null : bnPrice,
        "maxQty": maxQty == null ? null : maxQty,
        "qty": qty == null ? null : qty,
      };
}

// class Shipping {
//   Shipping({
//     this.maxQuantity,
//     this.minQuantity,
//     this.shippingCharge,
//   });
//
//   List<String> maxQuantity;
//   List<String> minQuantity;
//   List<String> shippingCharge;
//
//   factory Shipping.fromJson(Map<String, dynamic> json) => Shipping(
//         maxQuantity: json["max_quantity"] == null
//             ? null
//             : List<String>.from(json["max_quantity"].map((x) => x)),
//         minQuantity: json["min_quantity"] == null
//             ? null
//             : List<String>.from(json["min_quantity"].map((x) => x)),
//         shippingCharge: json["shipping_charge"] == null
//             ? null
//             : List<String>.from(json["shipping_charge"].map((x) => x)),
//       );
//
//   Map<String, dynamic> toJson() => {
//         "max_quantity": maxQuantity == null
//             ? null
//             : List<dynamic>.from(maxQuantity.map((x) => x)),
//         "min_quantity": minQuantity == null
//             ? null
//             : List<dynamic>.from(minQuantity.map((x) => x)),
//         "shipping_charge": shippingCharge == null
//             ? null
//             : List<dynamic>.from(shippingCharge.map((x) => x)),
//       };
// }
