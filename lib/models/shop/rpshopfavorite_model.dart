// To parse this JSON data, do
//
//     final rpCartModel = rpCartModelFromJson(jsonString);

import 'dart:convert';

RpShopFavoriteModel rpShopFavoriteModelFromJson(String str) =>
    RpShopFavoriteModel.fromJson(json.decode(str));

String rpCartModelToJson(RpShopFavoriteModel data) =>
    json.encode(data.toJson());

class RpShopFavoriteModel {
  RpShopFavoriteModel({
    this.id,
    this.productName,
    this.bnProductName,
    this.picture,
    this.price,
    this.bnPrice,
  });

  int id;
  String productName;
  String bnProductName;
  String picture;
  int price;
  String bnPrice;

  factory RpShopFavoriteModel.fromJson(Map<String, dynamic> json) =>
      RpShopFavoriteModel(
        id: json["id"] == null ? null : json["id"],
        productName: json["name"] == null ? null : json["name"],
        bnProductName: json["bn_name"] == null ? null : json["bn_name"],
        picture: json["thumbnail_img"] == null ? null : json["thumbnail_img"],
        price: json["price"] == null ? null : json["price"],
        bnPrice: json["bn_price"] == null ? null : json["bn_price"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": productName == null ? null : productName,
        "bn_name": bnProductName == null ? null : bnProductName,
        "thumbnail_img": picture == null ? null : picture,
        "price": price == null ? null : price,
        "bn_price": bnPrice == null ? null : bnPrice,
      };
}
