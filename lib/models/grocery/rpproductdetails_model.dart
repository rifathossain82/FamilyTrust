// To parse this JSON data, do
//
//     final rpPrdouctDetailsModel = rpPrdouctDetailsModelFromJson(jsonString);

import 'dart:convert';

RpPrdouctDetailsModel rpPrdouctDetailsModelFromJson(String str) =>
    RpPrdouctDetailsModel.fromJson(json.decode(str));

String rpPrdouctDetailsModelToJson(RpPrdouctDetailsModel data) =>
    json.encode(data.toJson());

class RpPrdouctDetailsModel {
  RpPrdouctDetailsModel({
    this.data,
  });

  Data data;

  factory RpPrdouctDetailsModel.fromJson(Map<String, dynamic> json) =>
      RpPrdouctDetailsModel(
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.id,
    this.productName,
    this.bnProductName,
    this.attribute,
    this.bnAttribute,
    this.brandId,
    this.picture,
    this.price,
    this.bnPrice,
    this.discountAmount,
    this.bnDiscountAmount,
    this.discountType,
    this.discountPrice,
    this.bnDiscountPrice,
    this.quantity,
    this.qty,
    this.description,
  });

  int id;
  String productName;
  String bnProductName;
  String attribute;
  String bnAttribute;
  dynamic brandId;
  String picture;
  int price;
  String bnPrice;
  double discountAmount;
  String bnDiscountAmount;
  int discountType;
  double discountPrice;
  String bnDiscountPrice;
  int quantity;
  int qty = 0;
  String description;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"] == null ? null : json["id"],
        productName: json["product_name"] == null ? null : json["product_name"],
        bnProductName:
            json["bn_product_name"] == null ? null : json["bn_product_name"],
        attribute: json["attribute"] == null ? null : json["attribute"],
        bnAttribute: json["bn_attribute"] == null ? null : json["bn_attribute"],
        brandId: json["brand_id"],
        picture: json["picture"] == null ? null : json["picture"],
        price: json["price"] == null ? null : json["price"],
        bnPrice: json["bn_price"] == null ? null : json["bn_price"],
        discountAmount: json["discount_amount"] == null
            ? null
            : json["discount_amount"].toDouble(),
        bnDiscountAmount: json["bn_discount_amount"] == null
            ? null
            : json["bn_discount_amount"],
        discountType:
            json["discount_type"] == null ? null : json["discount_type"],
        discountPrice: json["discount_price"] == null
            ? null
            : json["discount_price"].toDouble(),
        bnDiscountPrice: json["bn_discount_price"] == null
            ? null
            : json["bn_discount_price"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        qty: 0,
        description: json["description"] == null ? 'null' : json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "product_name": productName == null ? null : productName,
        "bn_product_name": bnProductName == null ? null : bnProductName,
        "attribute": attribute == null ? null : attribute,
        "bn_attribute": bnAttribute == null ? null : bnAttribute,
        "brand_id": brandId,
        "picture": picture == null ? null : picture,
        "price": price == null ? null : price,
        "bn_price": bnPrice == null ? null : bnPrice,
        "discount_amount": discountAmount == null ? null : discountAmount,
        "bn_discount_amount":
            bnDiscountAmount == null ? null : bnDiscountAmount,
        "discount_type": discountType == null ? null : discountType,
        "discount_price": discountPrice == null ? null : discountPrice,
        "bn_discount_price": bnDiscountPrice == null ? null : bnDiscountPrice,
        "quantity": quantity == null ? null : quantity,
        "qty": 0,
        "description": description == null ? 'null' : description,
      };
}
