// To parse this JSON data, do
//
//     final rpGroceryPopular = rpGroceryPopularFromJson(jsonString);

import 'dart:convert';

RpGroceryPopular rpGroceryPopularFromJson(String str) =>
    RpGroceryPopular.fromJson(json.decode(str));

String rpGroceryPopularToJson(RpGroceryPopular data) =>
    json.encode(data.toJson());

class RpGroceryPopular {
  RpGroceryPopular({
    this.data,
  });

  List<Datum> data;

  factory RpGroceryPopular.fromJson(Map<String, dynamic> json) =>
      RpGroceryPopular(
        data: json["data"] == null
            ? []
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.productName,
    this.bnProductName,
    this.attribute,
    this.bnAttribute,
    this.brandId,
    this.picture,
    this.price,
    this.bnPrice,
    this.discountAmount,
    this.bnDiscountAmount,
    this.discountType,
    this.discountPrice,
    this.bnDiscountPrice,
    this.quantity,
    this.description,
    this.qty,
  });

  int id;
  String productName;
  String bnProductName;
  String attribute;
  String bnAttribute;
  dynamic brandId;
  String picture;
  int price;
  String bnPrice;
  int discountAmount;
  String bnDiscountAmount;
  int discountType;
  int discountPrice;
  String bnDiscountPrice;
  int quantity;
  int qty = 0;
  dynamic description;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        productName: json["product_name"] == null ? null : json["product_name"],
        bnProductName:
            json["bn_product_name"] == null ? null : json["bn_product_name"],
        attribute: json["attribute"] == null ? null : json["attribute"],
        bnAttribute: json["bn_attribute"] == null ? null : json["bn_attribute"],
        brandId: json["brand_id"],
        picture: json["picture"] == null ? null : json["picture"],
        price: json["price"] == null ? null : json["price"],
        bnPrice: json["bn_price"] == null ? null : json["bn_price"],
        discountAmount:
            json["discount_amount"] == null ? null : json["discount_amount"],
        bnDiscountAmount: json["bn_discount_amount"] == null
            ? null
            : json["bn_discount_amount"],
        discountType:
            json["discount_type"] == null ? null : json["discount_type"],
        discountPrice:
            json["discount_price"] == null ? null : json["discount_price"],
        bnDiscountPrice: json["bn_discount_price"] == null
            ? null
            : json["bn_discount_price"],
        quantity : json['quantity'] == null ? 0 : json["quantity"],
        description: json["description"],
        qty: 0,
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "product_name": productName == null ? null : productName,
        "bn_product_name": bnProductName == null ? null : bnProductName,
        "attribute": attribute == null ? null : attribute,
        "bn_attribute": bnAttribute == null ? null : bnAttribute,
        "brand_id": brandId,
        "picture": picture == null ? null : picture,
        "price": price == null ? null : price,
        "bn_price": bnPrice == null ? null : bnPrice,
        "discount_amount": discountAmount == null ? null : discountAmount,
        "bn_discount_amount":
            bnDiscountAmount == null ? null : bnDiscountAmount,
        "discount_type": discountType == null ? null : discountType,
        "discount_price": discountPrice == null ? null : discountPrice,
        "bn_discount_price": bnDiscountPrice == null ? null : bnDiscountPrice,
        "quantity": quantity == null ? 0 : quantity,
        "description": description,
        "qty": 0,
      };
}
