// To parse this JSON data, do
//
//     final rpSubCategoriesModel = rpSubCategoriesModelFromJson(jsonString);

import 'dart:convert';

RpSubCategoriesModel rpSubCategoriesModelFromJson(String str) =>
    RpSubCategoriesModel.fromJson(json.decode(str));

String rpSubCategoriesModelToJson(RpSubCategoriesModel data) =>
    json.encode(data.toJson());

class RpSubCategoriesModel {
  RpSubCategoriesModel({
    this.status,
    this.data,
  });

  String status;
  List<Datum> data;

  factory RpSubCategoriesModel.fromJson(Map<String, dynamic> json) =>
      RpSubCategoriesModel(
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.categoryName,
    this.bnCategoryName,
    this.categoryPicture,
    this.status,
  });

  int id;
  String categoryName;
  String bnCategoryName;
  String categoryPicture;
  int status;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        categoryName:
            json["category_name"] == null ? null : json["category_name"],
        bnCategoryName:
            json["bn_category_name"] == null ? null : json["bn_category_name"],
        categoryPicture:
            json["category_picture"] == null ? null : json["category_picture"],
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "category_name": categoryName == null ? null : categoryName,
        "bn_category_name": bnCategoryName == null ? null : bnCategoryName,
        "category_picture": categoryPicture == null ? null : categoryPicture,
        "status": status == null ? null : status,
      };
}
