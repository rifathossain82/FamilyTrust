// To parse this JSON data, do
//
//     final rpSliderModel = rpSliderModelFromJson(jsonString);

import 'dart:convert';

RpSliderModel rpSliderModelFromJson(String str) =>
    RpSliderModel.fromJson(json.decode(str));

String rpSliderModelToJson(RpSliderModel data) => json.encode(data.toJson());

class RpSliderModel {
  RpSliderModel({
    this.data,
  });

  List<Datum> data;

  factory RpSliderModel.fromJson(Map<String, dynamic> json) => RpSliderModel(
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.picture,
  });

  String picture;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        picture: json["picture"] == null ? null : json["picture"],
      );

  Map<String, dynamic> toJson() => {
        "picture": picture == null ? null : picture,
      };
}
