// To parse this JSON data, do
//
//     final rpPopularBannerModel = rpPopularBannerModelFromJson(jsonString);

import 'dart:convert';

RpPopularBannerModel rpPopularBannerModelFromJson(String str) => RpPopularBannerModel.fromJson(json.decode(str));

String rpPopularBannerModelToJson(RpPopularBannerModel data) => json.encode(data.toJson());

class RpPopularBannerModel {
  RpPopularBannerModel({
    this.data,
  });

  List<String> data;

  factory RpPopularBannerModel.fromJson(Map<String, dynamic> json) => RpPopularBannerModel(
    data: List<String>.from(json["data"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x)),
  };
}
