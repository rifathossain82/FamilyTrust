// To parse this JSON data, do
//
//     final rpCouponModel = rpCouponModelFromJson(jsonString);

import 'dart:convert';

RpCouponModel rpCouponModelFromJson(String str) => RpCouponModel.fromJson(json.decode(str));

String rpCouponModelToJson(RpCouponModel data) => json.encode(data.toJson());

class RpCouponModel {
  RpCouponModel({
    this.message,
    this.coupon,
    this.status,
  });

  String message;
  Coupon coupon;
  int status;

  factory RpCouponModel.fromJson(Map<String, dynamic> json) => RpCouponModel(
    message: json["message"],
    coupon: Coupon.fromJson(json["data"]),
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "data": coupon.toJson(),
    "status": status,
  };
}

class Coupon {
  Coupon({
    this.id,
    this.cuponName,
    this.ecommerceType,
    this.type,
    this.discountTaka,
    this.discountAmount,
    this.validityTill,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });

  int id;
  String cuponName;
  int ecommerceType;
  int type;
  num discountTaka;
  num discountAmount;
  DateTime validityTill;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic deletedAt;

  factory Coupon.fromJson(Map<String, dynamic> json) => Coupon(
    id: json["id"] ?? null,
    cuponName: json["cupon_name"] ?? null ,
    ecommerceType: json["ecommerce_type"] ?? null,
    type: json["type"] ?? null,
    discountTaka: json["discount_taka"] ?? null,
    discountAmount: json["discount_amount"] ?? null,
    validityTill: json["validity_till"] == null ? null : DateTime.parse(json["validity_till"]),
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    deletedAt: json["deleted_at"] == null ? null : DateTime.parse(json["deleted_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "cupon_name": cuponName,
    "ecommerce_type": ecommerceType,
    "type": type,
    "discount_taka": discountTaka,
    "discount_amount": discountAmount,
    "validity_till": "${validityTill.year.toString().padLeft(4, '0')}-${validityTill.month.toString().padLeft(2, '0')}-${validityTill.day.toString().padLeft(2, '0')}",
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "deleted_at": deletedAt,
  };
}
