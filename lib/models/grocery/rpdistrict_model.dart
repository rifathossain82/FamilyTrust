// To parse this JSON data, do
//
//     final rpDistrictModel = rpDistrictModelFromJson(jsonString);

import 'dart:convert';

RpDistrictModel rpDistrictModelFromJson(String str) => RpDistrictModel.fromJson(json.decode(str));

String rpDistrictModelToJson(RpDistrictModel data) => json.encode(data.toJson());

class RpDistrictModel {
  RpDistrictModel({
    this.data,
  });

  List<District> data;

  factory RpDistrictModel.fromJson(Map<String, dynamic> json) => RpDistrictModel(
    data: List<District>.from(json["data"].map((x) => District.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class District {
  District({
    this.id,
    this.name,
    this.shippingType,
  });

  int id;
  String name;
  ShippingType shippingType;

  factory District.fromJson(Map<String, dynamic> json) => District(
    id: json["id"],
    name: json["name"],
    shippingType: shippingTypeValues.map[json["shipping_type"]],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "shipping_type": shippingTypeValues.reverse[shippingType],
  };
}

enum ShippingType { INSIDE_DHAKA, AROUND_DHAKA, OUTSIDE_DHAKA }

final shippingTypeValues = EnumValues({
  "Around Dhaka": ShippingType.AROUND_DHAKA,
  "Inside Dhaka": ShippingType.INSIDE_DHAKA,
  "Outside Dhaka": ShippingType.OUTSIDE_DHAKA
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
