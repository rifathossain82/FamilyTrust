// To parse this JSON data, do
//
//     final rpCartModel = rpCartModelFromJson(jsonString);

import 'dart:convert';

RpCartModel rpCartModelFromJson(String str) =>
    RpCartModel.fromJson(json.decode(str));

String rpCartModelToJson(RpCartModel data) => json.encode(data.toJson());

class RpCartModel {
  RpCartModel({
    this.id,
    this.productName,
    this.bnProductName,
    this.attribute,
    this.bnAttribute,
    this.picture,
    this.price,
    this.bnPrice,
    this.quantity,
    this.qty,
  });

  int id;
  String productName;
  String bnProductName;
  String attribute;
  String bnAttribute;
  String picture;
  int price;
  int quantity;
  String bnPrice;
  int qty = 0;

  factory RpCartModel.fromJson(Map<String, dynamic> json) => RpCartModel(
        id: json["id"] == null ? null : json["id"],
        productName: json["product_name"] == null ? null : json["product_name"],
        bnProductName:
            json["bn_product_name"] == null ? null : json["bn_product_name"],
        attribute: json["attribute"] == null ? null : json["attribute"],
        bnAttribute: json["bn_attribute"] == null ? null : json["bn_attribute"],
        picture: json["picture"] == null ? null : json["picture"],
        price: json["price"] == null ? null : json["price"],
        bnPrice: json["bn_price"] == null ? null : json["bn_price"],
        qty: json["qty"] == null ? null : json["qty"],
    quantity: json["quantity"] == null ? null : json["quantity"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "product_name": productName == null ? null : productName,
        "bn_product_name": bnProductName == null ? null : bnProductName,
        "attribute": attribute == null ? null : attribute,
        "bn_attribute": bnAttribute == null ? null : bnAttribute,
        "picture": picture == null ? null : picture,
        "price": price == null ? null : price,
        "bn_price": bnPrice == null ? null : bnPrice,
        "qty": qty == null ? null : qty,
        "quantity": quantity == null ? null : quantity,
      };
}
