// To parse this JSON data, do
//
//     final rpHomeCategoryProductModel = rpHomeCategoryProductModelFromJson(jsonString);

import 'dart:convert';

RpHomeCategoryProductModel rpHomeCategoryProductModelFromJson(String str) =>
    RpHomeCategoryProductModel.fromJson(json.decode(str));

String rpHomeCategoryProductModelToJson(RpHomeCategoryProductModel data) =>
    json.encode(data.toJson());

class RpHomeCategoryProductModel {
  RpHomeCategoryProductModel({
    this.status,
    this.data,
  });

  String status;
  List<CatProduct> data;

  factory RpHomeCategoryProductModel.fromJson(Map<String, dynamic> json) =>
      RpHomeCategoryProductModel(
        status: json["status"],
        data: List<CatProduct>.from(json["data"].map((x) => CatProduct.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class CatProduct {
  CatProduct({
    this.id,
    this.productName,
    this.bnProductName,
    this.attribute,
    this.bnAttribute,
    this.brandId,
    this.picture,
    this.price,
    this.bnPrice,
    this.discountAmount,
    this.bnDiscountAmount,
    this.discountType,
    this.discountPrice,
    this.bnDiscountPrice,
    this.quantity,
    this.description,
    this.qty,
  });

  int id;
  String productName;
  String bnProductName;
  String attribute;
  String bnAttribute;
  dynamic brandId;
  String picture;
  int price;
  String bnPrice;
  int discountAmount;
  String bnDiscountAmount;
  int discountType;
  int discountPrice;
  String bnDiscountPrice;
  int quantity;
  String description;
  int qty = 0;

  factory CatProduct.fromJson(Map<String, dynamic> json) => CatProduct(
        id: json["id"] == null ? null : json["id"],
        productName: json["product_name"] == null ? null : json["product_name"],
        bnProductName: json["bn_product_name"] == null ? null : json["bn_product_name"],
        attribute: json["attribute"] == null ? null : json["attribute"],
        bnAttribute: json["bn_attribute"] == null ? null : json["bn_attribute"],
        brandId: json["brand_id"],
        picture: json["picture"] == null ? null : json["picture"],
        price: json["price"] == null ? null : json["price"],
        bnPrice: json["bn_price"] == null ? null : json["bn_price"],
        discountAmount: json["discount_amount"] == null
            ? null
            : json["discount_amount"].round(),
        bnDiscountAmount: json["bn_discount_amount"] == null
            ? null
            : json["bn_discount_amount"],
        discountType:
            json["discount_type"] == null ? null : json["discount_type"],
        discountPrice: json["discount_price"] == null
            ? null
            : json["discount_price"].round(),
        bnDiscountPrice: json["bn_discount_price"] == null
            ? null
            : json["bn_discount_price"],
        quantity: json["quantity"] == null ? 0 : json["quantity"],
        qty: 0,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "product_name": productName,
        "bn_product_name": bnProductName,
        "attribute": attribute,
        "bn_attribute": bnAttribute,
        "brand_id": brandId,
        "picture": picture,
        "price": price,
        "bn_price": bnPrice,
        "discount_amount": discountAmount,
        "bn_discount_amount": bnDiscountAmount,
        "discount_type": discountType,
        "discount_price": discountPrice,
        "bn_discount_price": bnDiscountPrice,
        "quantity": quantity,
        "description": description,
        'qty': 0,
      };
}
