// To parse this JSON data, do
//
//     final rpCategoriesModel = rpCategoriesModelFromJson(jsonString);

import 'dart:convert';

RpCategoriesModel rpCategoriesModelFromJson(String str) =>
    RpCategoriesModel.fromJson(json.decode(str));

String rpCategoriesModelToJson(RpCategoriesModel data) =>
    json.encode(data.toJson());

class RpCategoriesModel {
  RpCategoriesModel({
    this.data,
  });

  List<Datum> data;

  factory RpCategoriesModel.fromJson(Map<String, dynamic> json) =>
      RpCategoriesModel(
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? 'null'
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.categoryName,
    this.bnCategoryName,
    this.categoryPicture,
    this.status,
    this.subcategories,
  });

  int id;
  String categoryName;
  String bnCategoryName;
  String categoryPicture;
  int status;
  List<Datum> subcategories;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        categoryName:
            json["category_name"] == null ? null : json["category_name"],
        bnCategoryName:
            json["bn_category_name"] == null ? null : json["bn_category_name"],
        categoryPicture:
            json["category_picture"] == null ? null : json["category_picture"],
        status: json["status"] == null ? null : json["status"],
        subcategories: json["subcategories"] == null
            ? null
            : List<Datum>.from(
                json["subcategories"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "category_name": categoryName == null ? null : categoryName,
        "bn_category_name": bnCategoryName == null ? null : bnCategoryName,
        "category_picture": categoryPicture == null ? null : categoryPicture,
        "status": status == null ? null : status,
        "subcategories": subcategories == null
            ? null
            : List<dynamic>.from(subcategories.map((x) => x.toJson())),
      };
}
