// To parse this JSON data, do
//
//     final rpPrdouctsModel2 = rpPrdouctsModel2FromJson(jsonString);

import 'dart:convert';

RpProductsModel2 rpProductsModel2FromJson(String str) => RpProductsModel2.fromJson(json.decode(str));

String rpProductsModel2ToJson(RpProductsModel2 data) => json.encode(data.toJson());

class RpProductsModel2 {
  RpProductsModel2({
    this.status,
    this.data,
  });

  String status;
  List<Datum> data;

  factory RpProductsModel2.fromJson(Map<String, dynamic> json) => RpProductsModel2(
    status: json["status"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.productName,
    this.bnProductName,
    this.attribute,
    this.bnAttribute,
    this.brandId,
    this.picture,
    this.price,
    this.bnPrice,
    this.discountAmount,
    this.bnDiscountAmount,
    this.discountType,
    this.discountPrice,
    this.bnDiscountPrice,
    this.quantity,
    this.description,
    this.qty,
  });

  int id;
  String productName;
  String bnProductName;
  String attribute;
  String bnAttribute;
  dynamic brandId;
  String picture;
  int price;
  String bnPrice;
  int discountAmount;
  String bnDiscountAmount;
  int discountType;
  int discountPrice;
  String bnDiscountPrice;
  int quantity;
  dynamic description;
  int qty = 0;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    productName: json["product_name"] == null ? null : json["product_name"],
    bnProductName:
    json["bn_product_name"] == null ? null : json["bn_product_name"],
    attribute: json["attribute"] == null ? null : json["attribute"],
    bnAttribute: json["bn_attribute"] == null ? null : json["bn_attribute"],
    brandId: json["brand_id"],
    picture: json["picture"] == null ? null : json["picture"],
    price: json["price"] == null ? null : json["price"],
    bnPrice: json["bn_price"] == null ? null : json["bn_price"],
    discountAmount: json["discount_amount"] == null
        ? null
        : json["discount_amount"].round(),
    bnDiscountAmount: json["bn_discount_amount"] == null
        ? null
        : json["bn_discount_amount"],
    discountType:
    json["discount_type"] == null ? null : json["discount_type"],
    discountPrice: json["discount_price"] == null
        ? null
        : json["discount_price"].round(),
    bnDiscountPrice: json["bn_discount_price"] == null
        ? null
        : json["bn_discount_price"],
    quantity: json["quantity"] == null ? 0 : json["quantity"],
    qty: 0,
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "product_name": productName == null ? null : productName,
    "bn_product_name": bnProductName == null ? null : bnProductName,
    "attribute": attribute == null ? null : attribute,
    "bn_attribute": bnAttribute == null ? null : bnAttribute,
    "brand_id": brandId,
    "picture": picture == null ? null : picture,
    "price": price == null ? null : price,
    "bn_price": bnPrice == null ? null : bnPrice,
    "discount_amount": discountAmount == null ? null : discountAmount,
    "bn_discount_amount":
    bnDiscountAmount == null ? null : bnDiscountAmount,
    "discount_type": discountType == null ? null : discountType,
    "discount_price": discountPrice == null ? null : discountPrice,
    "bn_discount_price": bnDiscountPrice == null ? null : bnDiscountPrice,
    "quantity": quantity == null ? null : quantity,
    "qty": 0,
  };
}
