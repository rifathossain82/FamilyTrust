// // To parse this JSON data, do
// //
// //     final rpPrdouctsModel = rpPrdouctsModelFromJson(jsonString);
//
// import 'dart:convert';
//
// RpPrdouctsModel rpPrdouctsModelFromJson(String str) =>
//     RpPrdouctsModel.fromJson(json.decode(str));
//
// String rpPrdouctsModelToJson(RpPrdouctsModel data) =>
//     json.encode(data.toJson());
//
// class RpPrdouctsModel {
//   RpPrdouctsModel({
//     this.status,
//     this.data,
//   });
//
//   String status;
//   List<Datum> data;
//
//   factory RpPrdouctsModel.fromJson(Map<String, dynamic> json) =>
//       RpPrdouctsModel(
//         status: json["status"] == null ? null : json["status"],
//         data: json["data"] == null
//             ? null
//             : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
//       );
//
//   Map<String, dynamic> toJson() => {
//         "status": status == null ? null : status,
//         "data": data == null
//             ? null
//             : List<dynamic>.from(data.map((x) => x.toJson())),
//       };
// }
//
// class Datum {
//   Datum({
//     this.id,
//     this.productName,
//     this.bnProductName,
//     this.attribute,
//     this.bnAttribute,
//     this.brandId,
//     this.picture,
//     this.price,
//     this.bnPrice,
//     this.discountAmount,
//     this.bnDiscountAmount,
//     this.discountType,
//     this.discountPrice,
//     this.bnDiscountPrice,
//     this.quantity,
//     this.qty,
//   });
//
//   int id;
//   String productName;
//   String bnProductName;
//   String attribute;
//   String bnAttribute;
//   dynamic brandId;
//   String picture;
//   int price;
//   String bnPrice;
//   int discountAmount;
//   String bnDiscountAmount;
//   int discountType;
//   int discountPrice;
//   String bnDiscountPrice;
//   int quantity;
//   int qty = 0;
//
//   factory Datum.fromJson(Map<String, dynamic> json) => Datum(
//         id: json["id"] == null ? null : json["id"],
//         productName: json["product_name"] == null ? null : json["product_name"],
//         bnProductName:
//             json["bn_product_name"] == null ? null : json["bn_product_name"],
//         attribute: json["attribute"] == null ? null : json["attribute"],
//         bnAttribute: json["bn_attribute"] == null ? null : json["bn_attribute"],
//         brandId: json["brand_id"],
//         picture: json["picture"] == null ? null : json["picture"],
//         price: json["price"] == null ? null : json["price"],
//         bnPrice: json["bn_price"] == null ? null : json["bn_price"],
//         discountAmount: json["discount_amount"] == null
//             ? null
//             : json["discount_amount"].round(),
//         bnDiscountAmount: json["bn_discount_amount"] == null
//             ? null
//             : json["bn_discount_amount"],
//         discountType:
//             json["discount_type"] == null ? null : json["discount_type"],
//         discountPrice: json["discount_price"] == null
//             ? null
//             : json["discount_price"].round(),
//         bnDiscountPrice: json["bn_discount_price"] == null
//             ? null
//             : json["bn_discount_price"],
//         quantity: json["quantity"] == null ? 0 : json["quantity"],
//         qty: 0,
//       );
//
//   Map<String, dynamic> toJson() => {
//         "id": id == null ? null : id,
//         "product_name": productName == null ? null : productName,
//         "bn_product_name": bnProductName == null ? null : bnProductName,
//         "attribute": attribute == null ? null : attribute,
//         "bn_attribute": bnAttribute == null ? null : bnAttribute,
//         "brand_id": brandId,
//         "picture": picture == null ? null : picture,
//         "price": price == null ? null : price,
//         "bn_price": bnPrice == null ? null : bnPrice,
//         "discount_amount": discountAmount == null ? null : discountAmount,
//         "bn_discount_amount":
//             bnDiscountAmount == null ? null : bnDiscountAmount,
//         "discount_type": discountType == null ? null : discountType,
//         "discount_price": discountPrice == null ? null : discountPrice,
//         "bn_discount_price": bnDiscountPrice == null ? null : bnDiscountPrice,
//         "quantity": quantity == null ? null : quantity,
//         "qty": 0,
//       };
// }


// To parse this JSON data, do
//
//     final rpPrdouctsModel = rpPrdouctsModelFromJson(jsonString);

import 'dart:convert';

RpPrdouctsModel rpPrdouctsModelFromJson(String str) => RpPrdouctsModel.fromJson(json.decode(str));

String rpPrdouctsModelToJson(RpPrdouctsModel data) => json.encode(data.toJson());

class RpPrdouctsModel {
  RpPrdouctsModel({
    this.data,
    this.links,
    this.meta,
  });

  List<Datum> data;
  Links links;
  Meta meta;

  factory RpPrdouctsModel.fromJson(Map<String, dynamic> json) => RpPrdouctsModel(
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    links: Links.fromJson(json["links"]),
    meta: Meta.fromJson(json["meta"]),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "links": links.toJson(),
    "meta": meta.toJson(),
  };
}

class Datum {
  Datum({
    this.id,
    this.productName,
    this.bnProductName,
    this.attribute,
    this.bnAttribute,
    this.brandId,
    this.picture,
    this.price,
    this.bnPrice,
    this.discountAmount,
    this.bnDiscountAmount,
    this.discountType,
    this.discountPrice,
    this.bnDiscountPrice,
    this.quantity,
    this.description,
    this.qty,
  });

  int id;
  String productName;
  String bnProductName;
  String attribute;
  String bnAttribute;
  dynamic brandId;
  String picture;
  int price;
  String bnPrice;
  int discountAmount;
  String bnDiscountAmount;
  int discountType;
  int discountPrice;
  String bnDiscountPrice;
  int quantity;
  dynamic description;
  int qty = 0;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    productName: json["product_name"] == null ? null : json["product_name"],
    bnProductName:
    json["bn_product_name"] == null ? null : json["bn_product_name"],
    attribute: json["attribute"] == null ? null : json["attribute"],
    bnAttribute: json["bn_attribute"] == null ? null : json["bn_attribute"],
    brandId: json["brand_id"],
    picture: json["picture"] == null ? null : json["picture"],
    price: json["price"] == null ? null : json["price"],
    bnPrice: json["bn_price"] == null ? null : json["bn_price"],
    discountAmount: json["discount_amount"] == null
        ? null
        : json["discount_amount"].round(),
    bnDiscountAmount: json["bn_discount_amount"] == null
        ? null
        : json["bn_discount_amount"],
    discountType:
    json["discount_type"] == null ? null : json["discount_type"],
    discountPrice: json["discount_price"] == null
        ? null
        : json["discount_price"].round(),
    bnDiscountPrice: json["bn_discount_price"] == null
        ? null
        : json["bn_discount_price"],
    quantity: json["quantity"] == null ? 0 : json["quantity"],
    qty: 0,
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "product_name": productName == null ? null : productName,
    "bn_product_name": bnProductName == null ? null : bnProductName,
    "attribute": attribute == null ? null : attribute,
    "bn_attribute": bnAttribute == null ? null : bnAttribute,
    "brand_id": brandId,
    "picture": picture == null ? null : picture,
    "price": price == null ? null : price,
    "bn_price": bnPrice == null ? null : bnPrice,
    "discount_amount": discountAmount == null ? null : discountAmount,
    "bn_discount_amount":
    bnDiscountAmount == null ? null : bnDiscountAmount,
    "discount_type": discountType == null ? null : discountType,
    "discount_price": discountPrice == null ? null : discountPrice,
    "bn_discount_price": bnDiscountPrice == null ? null : bnDiscountPrice,
    "quantity": quantity == null ? null : quantity,
    "qty": 0,
  };
}

class Links {
  Links({
    this.first,
    this.last,
    this.prev,
    this.next,
  });

  String first;
  String last;
  dynamic prev;
  String next;

  factory Links.fromJson(Map<String, dynamic> json) => Links(
    first: json["first"],
    last: json["last"],
    prev: json["prev"],
    next: json["next"],
  );

  Map<String, dynamic> toJson() => {
    "first": first,
    "last": last,
    "prev": prev,
    "next": next,
  };
}

class Meta {
  Meta({
    this.currentPage,
    this.from,
    this.lastPage,
    this.links,
    this.path,
    this.perPage,
    this.to,
    this.total,
  });

  int currentPage;
  int from;
  int lastPage;
  List<Link> links;
  String path;
  int perPage;
  int to;
  int total;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    currentPage: json["current_page"],
    from: json["from"],
    lastPage: json["last_page"],
    links: List<Link>.from(json["links"].map((x) => Link.fromJson(x))),
    path: json["path"],
    perPage: json["per_page"],
    to: json["to"],
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "current_page": currentPage,
    "from": from,
    "last_page": lastPage,
    "links": List<dynamic>.from(links.map((x) => x.toJson())),
    "path": path,
    "per_page": perPage,
    "to": to,
    "total": total,
  };
}

class Link {
  Link({
    this.url,
    this.label,
    this.active,
  });

  String url;
  String label;
  bool active;

  factory Link.fromJson(Map<String, dynamic> json) => Link(
    url: json["url"] == null ? null : json["url"],
    label: json["label"],
    active: json["active"],
  );

  Map<String, dynamic> toJson() => {
    "url": url == null ? null : url,
    "label": label,
    "active": active,
  };
}
