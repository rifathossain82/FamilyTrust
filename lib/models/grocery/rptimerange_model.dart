// To parse this JSON data, do
//
//     final rpTimeRangeModel = rpTimeRangeModelFromJson(jsonString);

import 'dart:convert';

RpTimeRangeModel rpTimeRangeModelFromJson(String str) =>
    RpTimeRangeModel.fromJson(json.decode(str));

String rpTimeRangeModelToJson(RpTimeRangeModel data) =>
    json.encode(data.toJson());

class RpTimeRangeModel {
  RpTimeRangeModel({
    this.data,
  });

  List<String> data;

  factory RpTimeRangeModel.fromJson(Map<String, dynamic> json) =>
      RpTimeRangeModel(
        data: json["data"] == null
            ? []
            : List<String>.from(json["data"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x)),
      };
}
