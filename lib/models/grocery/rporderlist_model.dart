// To parse this JSON data, do
//
//     final rpOrderListModel = rpOrderListModelFromJson(jsonString);

import 'dart:convert';

RpOrderListModel rpOrderListModelFromJson(String str) =>
    RpOrderListModel.fromJson(json.decode(str));

String rpOrderListModelToJson(RpOrderListModel data) =>
    json.encode(data.toJson());

class RpOrderListModel {
  RpOrderListModel({
    this.data,
  });

  List<Datum> data;

  factory RpOrderListModel.fromJson(Map<String, dynamic> json) =>
      RpOrderListModel(
        data: json["data"] == null
            ? []
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.user,
    this.storeId,
    this.storeName,
    this.location,
    this.name,
    this.phone,
    this.address,
    this.paymentType,
    this.paymentStatus,
    this.subTotal,
    this.shippingCharge,
    this.discount,
    this.total,
    this.status,
    this.orderDate,
    this.orderTime,
    this.orderProducts,
  });

  int id;
  String user;
  int storeId;
  String storeName;
  String location;
  String name;
  String phone;
  String address;
  String paymentType;
  String paymentStatus;
  double subTotal;
  int shippingCharge;
  int discount;
  double total;
  int status;
  DateTime orderDate;
  String orderTime;
  List<OrderProduct> orderProducts;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        user: json["user"] == null ? null : json["user"],
        storeId: json["store_id"] == null ? null : json["store_id"],
        storeName: json["store_name"] == null ? null : json["store_name"],
        location: json["location"] == null ? null : json["location"],
        name: json["name"] == null ? null : json["name"],
        phone: json["phone"] == null ? null : json["phone"],
        address: json["address"] == null ? null : json["address"],
        paymentType: json["payment_type"] == null ? null : json["payment_type"],
        paymentStatus:
            json["payment_status"] == null ? null : json["payment_status"],
        subTotal:
            json["sub_total"] == null ? null : json["sub_total"].toDouble(),
        shippingCharge:
            json["shipping_charge"] == null ? null : json["shipping_charge"],
        discount: json["discount"] == null ? null : json["discount"],
        total: json["total"] == null ? null : json["total"].toDouble(),
        status: json["status"] == null ? null : json["status"],
        orderDate: json["order_date"] == null
            ? null
            : DateTime.parse(json["order_date"]),
        orderTime: json["order_time"] == null ? null : json["order_time"],
        orderProducts: json["order_products"] == null
            ? null
            : List<OrderProduct>.from(
                json["order_products"].map((x) => OrderProduct.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "user": user == null ? null : user,
        "store_id": storeId == null ? null : storeId,
        "store_name": storeName == null ? null : storeName,
        "location": location == null ? null : location,
        "name": name == null ? null : name,
        "phone": phone == null ? null : phone,
        "address": address == null ? null : address,
        "payment_type": paymentType == null ? null : paymentType,
        "payment_status": paymentStatus == null ? null : paymentStatus,
        "sub_total": subTotal == null ? null : subTotal,
        "shipping_charge": shippingCharge == null ? null : shippingCharge,
        "discount": discount == null ? null : discount,
        "total": total == null ? null : total,
        "status": status == null ? null : status,
        "order_date": orderDate == null
            ? null
            : "${orderDate.year.toString().padLeft(4, '0')}-${orderDate.month.toString().padLeft(2, '0')}-${orderDate.day.toString().padLeft(2, '0')}",
        "order_time": orderTime == null ? null : orderTime,
        "order_products": orderProducts == null
            ? null
            : List<dynamic>.from(orderProducts.map((x) => x.toJson())),
      };
}

class OrderProduct {
  OrderProduct({
    this.productName,
    this.bnProductName,
    this.attribute,
    this.bnAttribute,
    this.price,
    this.quantity,
    this.total,
    this.picture,
  });

  String productName;
  String bnProductName;
  String attribute;
  String bnAttribute;
  int price;
  int quantity;
  int total;
  String picture;

  factory OrderProduct.fromJson(Map<String, dynamic> json) => OrderProduct(
        productName: json["product_name"] == null ? null : json["product_name"],
        bnProductName:
            json["bn_product_name"] == null ? null : json["bn_product_name"],
        attribute: json["attribute"] == null ? null : json["attribute"],
        bnAttribute: json["bn_attribute"] == null ? null : json["bn_attribute"],
        price: json["price"] == null ? null : json["price"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        total: json["total"] == null ? null : json["total"],
        picture: json["picture"] == null ? null : json["picture"],
      );

  Map<String, dynamic> toJson() => {
        "product_name": productName == null ? null : productName,
        "bn_product_name": bnProductName == null ? null : bnProductName,
        "attribute": attribute == null ? null : attribute,
        "bn_attribute": bnAttribute == null ? null : bnAttribute,
        "price": price == null ? null : price,
        "quantity": quantity == null ? null : quantity,
        "total": total == null ? null : total,
        "picture": picture == null ? null : picture,
      };
}
