// To parse this JSON data, do
//
//     final rpAreaModel = rpAreaModelFromJson(jsonString);

import 'dart:convert';

RpAreaModel rpAreaModelFromJson(String str) => RpAreaModel.fromJson(json.decode(str));

String rpAreaModelToJson(RpAreaModel data) => json.encode(data.toJson());

class RpAreaModel {
  RpAreaModel({
    this.data,
  });

  List<Area> data;

  factory RpAreaModel.fromJson(Map<String, dynamic> json) => RpAreaModel(
    data: List<Area>.from(json["data"].map((x) => Area.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Area {
  Area({
    this.id,
    this.areaName,
  });

  int id;
  String areaName;

  factory Area.fromJson(Map<String, dynamic> json) => Area(
    id: json["id"],
    areaName: json["area_name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "area_name": areaName,
  };
}
