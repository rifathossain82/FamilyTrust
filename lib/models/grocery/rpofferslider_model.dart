// To parse this JSON data, do
//
//     final rpOfferSliderModel = rpOfferSliderModelFromJson(jsonString);

import 'dart:convert';

RpOfferSliderModel rpOfferSliderModelFromJson(String str) =>
    RpOfferSliderModel.fromJson(json.decode(str));

String rpOfferSliderModelToJson(RpOfferSliderModel data) =>
    json.encode(data.toJson());

class RpOfferSliderModel {
  RpOfferSliderModel({
    this.data,
  });

  List<Datum> data;

  factory RpOfferSliderModel.fromJson(Map<String, dynamic> json) =>
      RpOfferSliderModel(
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.picture,
    this.productId,
  });

  String picture;
  int productId;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        picture: json["picture"] == null ? null : json["picture"],
        productId: json["product_id"] == null ? null : json["product_id"],
      );

  Map<String, dynamic> toJson() => {
        "picture": picture == null ? null : picture,
        "product_id": productId == null ? null : productId,
      };
}
