import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

extension BuildContextExtension on BuildContext {
  ///Return height
  double get height => MediaQuery.of(this).size.height;

  ///Return width
  double get width => MediaQuery.of(this).size.width;

  ///Navigation
  Future push(Widget nextScreen) {
    return Navigator.push(
      this,
      CupertinoPageRoute(
        builder: (context) => nextScreen,
      ),
    );
  }

  void pop() {
    return Navigator.pop(this);
  }

  Future popAndPush(Widget nextScreen) {
    Navigator.pop(this);
    return Navigator.push(
        this, MaterialPageRoute(builder: (context) => nextScreen),
    );
  }

  /// persistence page route
  Future pushWithoutRoute(Widget nextScreen) {
    return Navigator.pushAndRemoveUntil(
      this,
      CupertinoPageRoute(
        builder: (context) => nextScreen,
      ),
      (Route<dynamic> route) => false,
    );
  }

  void showSnackBar(SnackBar snackBar) {
    ScaffoldMessenger.of(this).showSnackBar(snackBar);
  }
}
