import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:familytrust/helper/helper_method.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/models/common/rpwallet_model.dart';
import 'package:familytrust/models/grocery/rp_home_category_product_model.dart';
import 'package:familytrust/models/grocery/rp_popular_banner_model.dart';
import 'package:familytrust/models/grocery/rparea_model.dart';
import 'package:familytrust/models/grocery/rpcategories_model.dart';
import 'package:familytrust/models/grocery/rpcoupon_model.dart';
import 'package:familytrust/models/grocery/rpdistrict_model.dart';
import 'package:familytrust/models/grocery/rpgrocerypopular_model.dart';
import 'package:familytrust/models/grocery/rpoffer_product_model.dart';
import 'package:familytrust/models/grocery/rpofferslider_model.dart';
import 'package:familytrust/models/grocery/rporderlist_model.dart';
import 'package:familytrust/models/grocery/rpproductdetails_model.dart';
import 'package:familytrust/models/grocery/rpproducts_model.dart';
import 'package:familytrust/models/grocery/rpproducts_model2.dart';
import 'package:familytrust/models/grocery/rpsearch_model.dart';
import 'package:familytrust/models/grocery/rpslider_model.dart';
import 'package:familytrust/models/grocery/rpsubcategories_model.dart';
import 'package:familytrust/models/grocery/rptimerange_model.dart';
import 'package:familytrust/models/shop/rparea_model.dart';
import 'package:familytrust/models/shop/rpcatbrandlist_model.dart';
import 'package:familytrust/models/shop/rpshobrandproducts_model.dart';
import 'package:familytrust/models/shop/rpshodelivercharge_model.dart';
import 'package:familytrust/models/shop/rpshopcat_model.dart';
import 'package:familytrust/models/shop/rpshopcatbrands_model.dart';
import 'package:familytrust/models/shop/rpshopchildcatproducts_model.dart';
import 'package:familytrust/models/shop/rpshoporderlist_model.dart';
import 'package:familytrust/models/shop/rpshoppopular_model.dart';
import 'package:familytrust/models/shop/rpshopproductdetails_model.dart';
import 'package:familytrust/models/shop/rpshopsearch_model.dart';
import 'package:familytrust/models/shop/rpshopsubcatproducts_model.dart';
import 'package:familytrust/view/screens/dashboard/dashboard_screen.dart';
import 'package:familytrust/view/screens/no_internet/no_internet_screen.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class NetworkServices {
  static const String baseUrl = "https://familytrust.com.bd/api/";
  static var client = http.Client();

  void _checkInternet()async{
    if(!await hasInternet){
      Get.offAll(() => NoInternetScreen());
    }
  }

  ///common user api start here///

  Future userLogin(
      {BuildContext context, String number, String password}) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => CustomLoader());
    var url = Uri.parse(baseUrl + 'login');
    final http.Response response = await client.post(
      url,
      body: {
        'phone': number,
        'password': password,
      },
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return posts;
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future regOTP({String number, String otpType}) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl + otpType);
    final http.Response response = await client.post(
      url,
      body: {
        'phone': number,
      },
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return posts;
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future userRegistration({
    BuildContext context,
    String number,
    String password,
    String otp,
  }) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => CustomLoader());
    var url = Uri.parse(baseUrl + 'otp/submit');
    final http.Response response = await client.post(
      url,
      body: {
        'phone': number,
        'password': password,
        'otp': otp,
      },
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return posts;
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future userForgetPassword({
    BuildContext context,
    String number,
    String password,
    String otp,
  }) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => CustomLoader());
    var url = Uri.parse(baseUrl + 'forget/password');
    final http.Response response = await client.post(
      url,
      body: {
        'phone': number,
        'password': password,
        'otp': otp,
      },
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return posts;
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future userPasswordChange({
    BuildContext context,
    String oldPassword,
    String newPassword,
  }) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => CustomLoader());
    var url = Uri.parse(baseUrl + 'password/change');
    final http.Response response = await client.post(
      url,
      body: {
        'old_password': oldPassword,
        'new_password': newPassword,
      },
      headers: {
        'Authorization': 'Bearer ' + prefs.getString('token'),
      },
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return posts;
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future userProfileUpdate({
    BuildContext context,
    String name,
    String email,
    // String address,
  }) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => CustomLoader());
    var url = Uri.parse(baseUrl + 'profile/update');
    // print('You hit: $url');
    final http.Response response = await client.post(
      url,
      body: {
        'name': name,
        'email': email,
        'address': prefs.getString('address'),
        // 'address': address,
      },
      headers: {
        'Authorization': 'Bearer ' + prefs.getString('token'),
      },
    );

    // print('Status code: ${response.statusCode}');
    // print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return posts;
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpWalletModel> fetchWalletData() async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl + 'wallet');
    http.Response response = await client.get(
      url,
      headers: {
        'Authorization': 'Bearer ' + prefs.getString('token'),
      },
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      log(response.body.toString());
      return RpWalletModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  ///common user api end here///

  ///Groceries api start here///
  Future<RpGroceryPopular> fetchGroceryPopular() async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(
        baseUrl + 'grocery/popular/products/${prefs.get('store_id')}');
    http.Response response = await client.get(
      url,
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpGroceryPopular.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpHomeCategoryProductModel> fetchGroceryHomeCategoryProduct(String catId) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    Map<String, dynamic> params = {
      'store_id': prefs.get('store_id'),
    };
    var url = Uri.parse(baseUrl + 'grocery/category/product/$catId').replace(
      queryParameters: params,
    );
    http.Response response = await client.post(
      url,
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      // log(response.body.toString());
      return RpHomeCategoryProductModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpOfferProductModel> fetchGroceryOfferProducts(int pageNumber) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl + 'grocery/offer/products?page=$pageNumber');
    print('You hit: $url');
    Map<String, dynamic> body = {};
    body['store_id'] = prefs.get('store_id');
    http.Response response = await client.post(
      url,
      body: body,
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      // log(response.body.toString());
      return RpOfferProductModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpSliderModel> fetchSliders() async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl + 'grocery/sliders');
    http.Response response = await client.get(
      url,
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpSliderModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpPopularBannerModel> fetchBanners() async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl + 'grocery/popular/banner');
    http.Response response = await client.post(
      url,
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpPopularBannerModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpOfferSliderModel> fetchOfferSliders() async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl + 'grocery/offer/sliders');
    http.Response response = await client.post(
      url,
      body: {
        'store_id': prefs.getString('store_id'),
      },
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log('offer slider=========>' + response.body.toString());
      return RpOfferSliderModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpDistrictModel> fetchDistrict() async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl + 'district');
    http.Response response = await client.get(url);
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log('District list=========>' + response.body.toString());
      return RpDistrictModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpAreaModel> fetchArea(String districtId) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl + 'district-wise-delivery-area/$districtId');
    http.Response response = await client.get(url);
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log('Area list=========>' + response.body.toString());
      return RpAreaModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<int> fetchDeliveryCharge(String areaId, String totalAmount) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl +
        'devlivery-charge-by-area?id=$areaId&total_amount=$totalAmount');
    print('You hit: $url');
    http.Response response = await client.get(url);
    if (response.statusCode == 200) {
      var deliveryCharge = jsonDecode(response.body)['delivery_charge'];
      log(response.statusCode.toString());
      log('Delivery Charge=========>' + response.body.toString());
      return deliveryCharge;
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future fetchShop(BuildContext context) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => CustomLoader());
    try {
      var url = Uri.parse(baseUrl + 'grocery/location');
      print('You Hit: $url');
      print('Your Selected location: ${prefs.getString('address').toString()}');

      http.Response response = await client.post(url, body: {
        'address': prefs.getString('address').toString(),
      });
      if (response.statusCode == 200) {
        var posts = jsonDecode(response.body);
        log(response.statusCode.toString());
        log(response.body.toString());
        return posts;
      } else {
        log(response.statusCode.toString());
        log(response.body.toString());
        throw Exception('Fail to load');
      }
    } catch (e) {
      throw e;
    }
  }

  Future<RpCategoriesModel> fetchCategories() async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl +
        'grocery/categories/?store_id=${prefs.getString('store_id')}');

    http.Response response = await client.get(
      url,
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      print(posts);
      return RpCategoriesModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpSubCategoriesModel> fetchSubCategories(String catId) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl + 'grocery/category');
    http.Response response = await client.post(url, body: {
      'category': catId,
      'store_id': prefs.get('store_id'),
    });
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpSubCategoriesModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpPrdouctsModel> fetchProductsWithPagination(
      String id, int pageNumber) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    Map<String, dynamic> params = {
      'page': pageNumber.toString(),
      'store_id': prefs.get('store_id')
    };
    var url = Uri.parse(baseUrl + 'grocery/category/product/$id').replace(
      queryParameters: params,
    );
    print('You hit: $url');

    http.Response response = await client.post(url);

    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      return RpPrdouctsModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpProductsModel2> fetchProductsList(String subCatId) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    Map<String, dynamic> params = {
      'store_id': prefs.get('store_id')
    };
    var url = Uri.parse(baseUrl + 'grocery/category/product/$subCatId').replace(
      queryParameters: params,
    );
    print('You hit: $url');
    http.Response response = await client.post(url);
    print('\n\nStatusCode: ${response.statusCode}\n\n');

    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      return RpProductsModel2.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpPrdouctDetailsModel> fetchProductDetails(String proId) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl + 'grocery/product');
    http.Response response = await client.post(url, body: {
      'product_id': proId,
      'store_id': prefs.get('store_id'),
    });
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpPrdouctDetailsModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpSearchModel> fetchAllSearch(var value, String page) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    log('search =====>' + value);
    var url = Uri.parse(baseUrl + 'grocery/search');
    http.Response response = await client.post(
      url,
      body: {
        'store_id': prefs.getString('store_id'),
        'value': value,
        'page': page,
      },
      headers: {
        HttpHeaders.acceptHeader: 'application/json',
      },
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpSearchModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpTimeRangeModel> fetchTimeRange(var value) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl + value);
    http.Response response = await client.get(
      url,
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpTimeRangeModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpCouponModel> fetchCoupon(String couponName) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl + 'coupon');
    Map<String, dynamic> body = {};
    body['code'] = couponName;
    http.Response response = await client.post(
      url,
      body: body,
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpCouponModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      String errorMsg = 'Coupon Apply Failed!';
      if(response.body != null){
        errorMsg = jsonDecode(response.body)['message'];
      }
      throw errorMsg;
    }
  }

  Future submitOrder(var body) async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl + 'grocery/order');
    http.Response response =
        await client.post(url, body: jsonEncode(body), headers: {
      'Authorization': 'Bearer ' + prefs.getString('token'),
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.acceptHeader: 'application/json',
    });
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return posts;
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpOrderListModel> fetchGroceryOrderList() async {
    /// if no internet go to no_internet_screen
    _checkInternet();

    var url = Uri.parse(baseUrl + 'grocery/order/list');
    http.Response response = await client.get(url, headers: {
      'Authorization': 'Bearer ' + prefs.getString('token'),
    });
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpOrderListModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  ///Groceries api end here///

  ///Shop api start here///

  Future<RpShopPopular> fetchShopPopular() async {
    var url = Uri.parse(baseUrl + 'popular/products');
    http.Response response = await client.get(
      url,
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpShopPopular.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpShopCatModel> fetchShopCategories() async {
    var url = Uri.parse(baseUrl + 'categories');
    http.Response response = await client.get(
      url,
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      print(posts);
      return RpShopCatModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpShopCatBrandsModel> fetchShopCatBrands() async {
    var url = Uri.parse(baseUrl + 'category-brand');
    http.Response response = await client.get(url);
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpShopCatBrandsModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpCatBrandListModel> fetchBrandsList(String catName, String id) async {
    var url =
        Uri.parse(baseUrl + '${catName}category/brand/list?category_id=$id');
    http.Response response = await client.post(
      url,
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      log(response.body.toString());
      return RpCatBrandListModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpShopSubCatProductsModel> fetchShopSubCatProducts(
      {String id,
      String filter,
      String min,
      String max,
      String brand,
      String page}) async {
    var url = Uri.parse(baseUrl +
        'subcategory/products?$filter&minprice=$min&maxprice=$max&brand=$brand&page=$page');
    http.Response response = await client.post(url, body: {
      'sub_category_id': id,
    });
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpShopSubCatProductsModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpShopChildCatProductsModel> fetchShopChildCatProducts(
      {String id,
      String filter,
      String min,
      String max,
      String brand,
      String page}) async {
    var url = Uri.parse(baseUrl +
        'childcategory/products?$filter&minprice=$min&maxprice=$max&brand=$brand&page=$page');
    http.Response response = await client.post(url, body: {
      'child_category_id': id,
    });
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpShopChildCatProductsModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpShopBrandProducts> fetchShopBrandProducts(
      {String id, String filter, String min, String max, String page}) async {
    var url = Uri.parse(baseUrl +
        'brand/products?$filter&minprice=$min&maxprice=$max&page=$page');
    http.Response response = await client.post(url, body: {
      'brand_id': id,
    });
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      log(response.body.toString());
      return RpShopBrandProducts.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpShopSearchModel> fetchShopSearch(String value, String page) async {
    var url = Uri.parse(baseUrl + 'product/search?page=$page');
    http.Response response = await client.post(url, body: {
      'key': value,
    });
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpShopSearchModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpShopProductDetailsModel> fetchShopProductDetails(
      String proId) async {
    var url = Uri.parse(baseUrl + 'product/details/$proId');
    http.Response response = await client.get(url);
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      print(posts);
      return RpShopProductDetailsModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpAllDistrictModel> fetchAllDistrictForShop() async {
    var url = Uri.parse(baseUrl + 'district');
    http.Response response = await client.get(url);
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      print(posts);
      return RpAllDistrictModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpAreaModelShop> fetchAreaShop(String id) async {
    var url = Uri.parse(baseUrl + 'district-wise-delivery-area/$id');
    http.Response response = await client.get(url);
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      print(posts);
      return RpAreaModelShop.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future submitShopOrder(var body) async {
    var url = Uri.parse(baseUrl + 'product/order');
    http.Response response =
        await client.post(url, body: jsonEncode(body), headers: {
      'Authorization': 'Bearer ' + prefs.getString('token'),
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.acceptHeader: 'application/json',
    });
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      log(response.body);
      return posts;
    } else if (response.statusCode == 403) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      log(response.body.toString());
      return posts;
    } else {
      log(response.body.toString());
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  Future<RpShopOrderListModel> fetchShopOrderList() async {
    var url = Uri.parse(baseUrl + 'shop/order/list');
    http.Response response = await client.get(url, headers: {
      'Authorization': 'Bearer ' + prefs.getString('token'),
    });
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      log(response.statusCode.toString());
      log(response.body.toString());
      return RpShopOrderListModel.fromJson(posts);
    } else {
      log(response.statusCode.toString());
      log(response.body.toString());
      throw Exception('Fail to load');
    }
  }

  ///shop end here///
}
