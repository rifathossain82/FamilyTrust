import 'package:familytrust/controllers/grocery_controllers/grocerycart_controller.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/helper/get_di.dart';
import 'package:familytrust/routes.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/screens/splash/splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

SharedPreferences prefs;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await init();
  await Firebase.initializeApp();
  await GetStorage.init();
  Get.put(GroceryCartController());
  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      translations: LocalizationService(), // your translations
      locale: LocalizationService().getCurrentLocale(),
      fallbackLocale: Locale(
        'en',
        'US',
      ),
      title: 'Family Trust',
      theme: ThemeData(
        fontFamily: GoogleFonts.openSans().fontFamily,
        primaryColor: kPrimaryColor,
        scaffoldBackgroundColor: kWhiteColor,
        appBarTheme: AppBarTheme(
          color: kPrimaryColor,
          elevation: 0,
        ),
        progressIndicatorTheme: ProgressIndicatorThemeData(
          color: kPrimaryColor,
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: SplashScreen.routeName,
      routes: routes,
      transitionDuration: Duration(
        milliseconds: 300,
      ),
      defaultTransition: Transition.rightToLeft,
    );
  }
}
