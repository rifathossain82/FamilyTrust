import 'package:familytrust/models/shop/rpshopsubcatproducts_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class ShopSubCatProductsController extends GetxController {
  RpShopSubCatProductsModel subCatProducts;
  bool noData = false;
  var isLoading = false.obs;

  void fetchShopSubCatProducts(String id, String page) async {
    noData = false;
    try {
      isLoading(true);
      var subCatProduct = await NetworkServices().fetchShopSubCatProducts(
          id: id, page: page, filter: '', max: '', min: '', brand: '');
      if (subCatProduct != null) {
        subCatProducts = subCatProduct;
      }
    } finally {
      isLoading(false);
    }
  }

  void fetchShopSubCatProductsWithFilter(
      {String id, String filter, String min, String max, String brand}) async {
    try {
      isLoading(true);
      var subCatProduct = await NetworkServices().fetchShopSubCatProducts(
          id: id, filter: filter, min: min, max: max, brand: brand);
      if (subCatProduct != null) {
        subCatProducts = subCatProduct;
      }
    } finally {
      isLoading(false);
    }
  }

  Future<void> fetchShopSubCatProductsAdd(
      {String id,
      String page,
      String filter,
      String min,
      String max,
      String brand}) async {
    var add = await NetworkServices().fetchShopSubCatProducts(
        id: id, page: page, filter: filter, min: min, max: max, brand: brand);
    if (add != null) {
      if (add.data.isNotEmpty) {
        try {
          isLoading(true);
          noData = false;
          for (int i = 0; i < add.data.length; i++) {
            subCatProducts.data.add(add.data[i]);
          }
        } finally {
          isLoading(false);
        }
      } else {
        noData = true;
      }
    }
  }
}
