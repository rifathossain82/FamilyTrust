import 'package:familytrust/models/shop/rpshopchildcatproducts_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class ShopChildCatProductsController extends GetxController {
  RpShopChildCatProductsModel childCatProducts;
  bool noData = false;
  var isLoading = false.obs;

  void fetchShopChildCatProducts(String id, String page) async {
    noData = false;
    try {
      isLoading(true);
      var childCatProduct = await NetworkServices().fetchShopChildCatProducts(
          id: id, page: page, filter: '', max: '', min: '', brand: '');
      if (childCatProduct != null) {
        childCatProducts = childCatProduct;
      }
    } finally {
      isLoading(false);
    }
  }

  void fetchShopChildCatProductsWithFilter(
      {String id, String filter, String min, String max, String brand}) async {
    try {
      isLoading(true);
      var childCatProduct = await NetworkServices().fetchShopChildCatProducts(
          id: id, filter: filter, min: min, max: max, brand: brand);
      if (childCatProduct != null) {
        childCatProducts = childCatProduct;
      }
    } finally {
      isLoading(false);
    }
  }

  Future<void> fetchShopChildCatProductsAdd(
      {String id,
      String page,
      String filter,
      String min,
      String max,
      String brand}) async {
    var add = await NetworkServices().fetchShopChildCatProducts(
        id: id, filter: filter, min: min, max: max, brand: brand, page: page);
    if (add != null) {
      if (add.data.isNotEmpty) {
        try {
          isLoading(true);
          noData = false;
          for (int i = 0; i < add.data.length; i++) {
            childCatProducts.data.add(add.data[i]);
          }
        } finally {
          isLoading(false);
        }
      } else {
        noData = true;
      }
    }
  }
}
