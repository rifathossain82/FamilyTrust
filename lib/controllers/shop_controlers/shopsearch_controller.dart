import 'package:familytrust/models/shop/rpshopsearch_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class ShopSearchController extends GetxController {
  RpShopSearchModel shopSearchAll;
  bool noData = false;
  var isLoading = false.obs;

  void fetchShopSearch(var value, String page) async {
    noData = false;
    try {
      isLoading(true);

      var search = await NetworkServices().fetchShopSearch(value, page);
      if (search != null) {
        shopSearchAll = search;
      }
    } finally {
      isLoading(false);
    }
  }

  Future<void> fetchShopSearchAdd(var value, String page) async {
    var add = await NetworkServices().fetchShopSearch(value, page);
    if (add != null) {
      if (add.data.isNotEmpty) {
        try {
          isLoading(true);
          noData = false;
          for (int i = 0; i < add.data.length; i++) {
            shopSearchAll.data.add(add.data[i]);
          }
        } finally {
          isLoading(false);
        }
      } else {
        noData = true;
      }
    }
  }
}
