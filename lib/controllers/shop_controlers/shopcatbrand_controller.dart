import 'package:familytrust/models/shop/rpshopcatbrands_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class ShopCatBrandController extends GetxController {
  RpShopCatBrandsModel catBrands;
  var isLoading = false.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    fetchCategories();
    super.onInit();
  }

  void fetchCategories() async {
    try {
      isLoading(true);
      var catBrand = await NetworkServices().fetchShopCatBrands();
      if (catBrand != null) {
        catBrands = catBrand;
      }
    } finally {
      isLoading(false);
    }
  }
}
