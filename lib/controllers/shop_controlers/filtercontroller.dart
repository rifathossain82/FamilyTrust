import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:familytrust/models/shop/rpcatbrandlist_model.dart';

class FilterController extends GetxController {
  RangeValues priceRange = const RangeValues(1, 100000);
  List<Datum> filterBrands;
  List<String> brandId;
  var isLoading = false.obs;

  void changeValue(RangeValues value) async {
    try {
      isLoading(true);
      priceRange = value;
    } finally {
      isLoading(false);
    }
  }

  void changeBrands(List<Datum> value) async {
    try {
      isLoading(true);
      filterBrands = value;
    } finally {
      isLoading(false);
    }
  }

  void getId() {
    try {
      isLoading(true);
      for (int i = 0; i < filterBrands.length; i++) {
        brandId[i] = (filterBrands[i].id.toString());
      }
    } finally {
      isLoading(false);
    }
  }
}
