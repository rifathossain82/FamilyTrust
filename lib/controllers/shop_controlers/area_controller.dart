import 'package:familytrust/models/shop/rparea_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class AreaController extends GetxController {
  RpAreaModelShop allArea;
  // List<String> areaNameList = [];
  // List<String> tempName = [];
  String areaName;
  var isLoading = false.obs;

  void fetchArea(String id) async {
    try {
      isLoading(true);
      var area = await NetworkServices().fetchAreaShop(id);
      if (area != null) {
        allArea = area;
        areaName = allArea.data[0].areaName;
        // if (areaNameList.isEmpty) {
        //   for (int i = 0; i < allArea.data.length; i++) {
        //     tempName.add(allArea.data[i].areaName);
        //   }
        //   print('Names of District $areaName');
        // }
      }
    } finally {
      isLoading(false);
    }
  }

  void changeName(String val) {
    try {
      isLoading(true);
      areaName = val;
    } finally {
      isLoading(false);
    }
  }
}
