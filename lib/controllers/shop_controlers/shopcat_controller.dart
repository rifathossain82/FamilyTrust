import 'package:familytrust/models/shop/rpshopcat_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class ShopCatController extends GetxController {
  RpShopCatModel shopCategories;
  var isLoading = false.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    fetchShopCategories();
    super.onInit();
  }

  void fetchShopCategories() async {
    try {
      isLoading(true);
      var shopCategory = await NetworkServices().fetchShopCategories();
      if (shopCategory != null) {
        shopCategories = shopCategory;
      }
    } finally {
      isLoading(false);
    }
  }
}
