import 'package:familytrust/models/shop/rpshoppopular_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class ShopPopularController extends GetxController {
  RpShopPopular shopPopular;

  var isLoading = false.obs;

  void fetchShopPopular() async {
    try {
      isLoading(true);
      var popular = await NetworkServices().fetchShopPopular();
      if (popular != null) {
        shopPopular = popular;
      }
    } finally {
      isLoading(false);
    }
  }
}
