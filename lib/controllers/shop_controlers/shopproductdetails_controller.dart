import 'package:familytrust/models/shop/rpshopproductdetails_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class ShopProductDetailsController extends GetxController {
  RpShopProductDetailsModel shopProductDetails;
  var isLoading = false.obs;

  Future<void> fetchShopProductDetails(String proId) async {
    try {
      isLoading(true);
      var details = await NetworkServices().fetchShopProductDetails(proId);
      if (details != null) {
        shopProductDetails = details;
      }
    } finally {
      isLoading(false);
    }
  }
}
