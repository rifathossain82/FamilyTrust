import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/helper/db_helper.dart';
import 'package:familytrust/models/shop/rpshopcart_model.dart';
import 'package:get/get.dart';

class ShopCartController extends GetxController {
  var shopCart = [].obs();
  RpShopCartModel tempCart;
  var isLoading = false.obs;

  final DatabaseHelper _databaseHelper = DatabaseHelper.instance;

  @override
  void onInit() async {
    // TODO: implement onInit
    await queryAllCart();
    super.onInit();
  }

  void addToCart({
    int id,
    int cityId,
    String shippingCharge,
    String productName,
    String bnProductName,
    String attributes,
    String color,
    String picture,
    String area,
    int price,
    int deliveryCharge,
    String maxQty,
    String bnPrice,
    int qty,
    String discountType,
    int discountAmount,
    String bnDiscountAmount,
    String thumbnailImg,
  }) async {
    //for adding existing items to the cart and increase quantity
    var isExist = shopCart.indexWhere((element) => element.id == id);
    print('Exist : $isExist');
    int qty = 0;
    for (int i = 0; i < shopCart.length; i++) {
      if (shopCart[i].id == id) {
        qty = shopCart[i].qty + 1;
      }
    }
    if (isExist >= 0) {
      updateCart(id, qty, price);
      Get.snackbar('Cart Updated', '', colorText: kWhiteColor);
    } else {
      //for adding new items to the cart
      RpShopCartModel rpShopCartModel = RpShopCartModel(
        id: id,
        cityId: cityId,
        deliveryCharge: deliveryCharge,
        shippingCharge: shippingCharge,
        productName: productName,
        bnProductName: bnProductName,
        area: area,
        price: discountType == null ? price : discountAmount,
        maxQty: maxQty,
        bnPrice: discountType == null ? bnPrice : bnDiscountAmount,
        attribute: attributes.toString(),
        color: color == null ? '' : color,
        qty: qty + 1,
        picture: thumbnailImg,
      );
      _databaseHelper.insertCartShop(rpShopCartModel);
      Get.snackbar('Added to Cart', '', colorText: kWhiteColor);
      var result = await _databaseHelper.queryAllShopCart();
      if (result.isNotEmpty) {
        shopCart.assignAll(result);
        await queryAllCart();
      }
    }
  }

  Future<void> removeFromCart(int id) async {
    try {
      isLoading(true);
      await _databaseHelper.deleteCartShop(id);
      await queryAllCart();
    } finally {
      isLoading(false);
    }
  }

  void updateCart(int id, int qty, int price) async {
    try {
      isLoading(true);
      await _databaseHelper.updateShop(id, qty, price);
      await queryAllCart();
    } finally {
      isLoading(false);
    }
  }

  Future<void> queryAllCart() async {
    try {
      isLoading(true);
      var product = await _databaseHelper.queryAllShopCart();
      shopCart.assignAll(product);
    } finally {
      isLoading(false);
    }
  }

  void emptyCart() async {
    try {
      isLoading(true);
      await _databaseHelper.emptyShopCart();
      await queryAllCart();
    } finally {
      isLoading(false);
    }
  }

  void addToTempCart({
    int id,
    int cityId,
    String productName,
    String bnProductName,
    int deliveryCharge,
    String attributes,
    String color,
    String picture,
    int price,
    String bnPrice,
    int qty,
    String discountType,
    int discountAmount,
    String bnDiscountAmount,
    String thumbnailImg,
  }) async {
    try {
      isLoading(true);
      tempCart = RpShopCartModel(
        id: id,
        cityId: cityId,
        productName: productName,
        deliveryCharge: deliveryCharge,
        bnProductName: bnProductName,
        price: discountType == null ? price : discountAmount,
        bnPrice: discountType == null ? bnPrice : bnDiscountAmount,
        attribute: attributes.toString(),
        color: color == null ? '' : color,
        qty: qty + 1,
        picture: thumbnailImg,
      );
    } finally {
      isLoading(false);
    }
  }
}
