import 'package:familytrust/models/shop/rpshodelivercharge_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class AllDistrictController extends GetxController {
  RpAllDistrictModel allDistrict;
  List<String> districtName = [];
  List<String> tempName = [];
  String cityName = 'Dhaka';
  var isLoading = false.obs;

  // @override
  // void onInit() {
  //   // TODO: implement onInit
  //   fetchDeliveryCharge();
  //   super.onInit();
  // }
  //
  // void fetchDeliveryCharge() async {
  //   try {
  //     isLoading(true);
  //     var charge = await NetworkServices().fetchAllDistrict();
  //     if (charge != null) {
  //       allDistrict = charge;
  //       if (districtName.isEmpty) {
  //         for (int i = 0; i < allDistrict.data.length; i++) {
  //           districtName.add(allDistrict.data[i].name);
  //         }
  //         print('Names of District $districtName');
  //       }
  //     }
  //   } finally {
  //     isLoading(false);
  //   }
  // }

  void changeName(String val) {
    try {
      isLoading(true);
      cityName = val;
    } finally {
      isLoading(false);
    }
  }
}
