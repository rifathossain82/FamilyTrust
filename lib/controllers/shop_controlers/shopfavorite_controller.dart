import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/helper/db_helper.dart';
import 'package:familytrust/models/shop/rpshopfavorite_model.dart';
import 'package:get/get.dart';

class ShopFavoriteController extends GetxController {
  var shopFavorite = [].obs();
  var isLoading = false.obs;

  final DatabaseHelper _databaseHelper = DatabaseHelper.instance;

  @override
  void onInit() async {
    // TODO: implement onInit
    await queryAllCart();
    super.onInit();
  }

  void addToFavorite(var rpSingleFavoriteModel) async {
    //for adding existing items to the cart and increase quantity
    var isExist = shopFavorite
        .indexWhere((element) => element.id == rpSingleFavoriteModel.id);
    print('Exist : $isExist');
    // for (int i = 0; i < shopFavorite.length; i++) {
    //   if (shopFavorite[i].id == rpSingleFavoriteModel.id) {
    //     print('already exist');
    //   }
    // }
    if (isExist >= 0) {
      Get.snackbar('Already added to Favorite', '', colorText: kWhiteColor);
    } else {
      //for adding new items to the cart
      RpShopFavoriteModel rpShopFavoriteModel = RpShopFavoriteModel(
        id: rpSingleFavoriteModel.id,
        productName: rpSingleFavoriteModel.name,
        bnProductName: rpSingleFavoriteModel.bnName,
        price: rpSingleFavoriteModel.discountType == null
            ? rpSingleFavoriteModel.price
            : rpSingleFavoriteModel.discountAmount,
        bnPrice: rpSingleFavoriteModel.discountType == null
            ? rpSingleFavoriteModel.bnPrice
            : rpSingleFavoriteModel.bnDiscountAmount,
        picture: rpSingleFavoriteModel.thumbnailImg,
      );
      _databaseHelper.insertShopFavorite(rpShopFavoriteModel);

      var result = await _databaseHelper.queryAllShopFavorite();
      if (result.isNotEmpty) {
        shopFavorite.assignAll(result);
        await queryAllCart();
      }
      Get.snackbar('Added to Favorite', '', colorText: kWhiteColor);
    }
  }

  Future<void> removeFromFavorite(int id) async {
    try {
      isLoading(true);
      await _databaseHelper.deleteShopFavorite(id);
      await queryAllCart();
    } finally {
      isLoading(false);
    }
  }

  Future<void> queryAllCart() async {
    try {
      isLoading(true);
      var product = await _databaseHelper.queryAllShopFavorite();
      shopFavorite.assignAll(product);
    } finally {
      isLoading(false);
    }
  }
}
