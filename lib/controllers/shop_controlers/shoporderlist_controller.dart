import 'package:familytrust/models/shop/rpshoporderlist_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class ShopOrderController extends GetxController {
  RpShopOrderListModel shopOrderList;
  var isLoading = false.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    fetchOrderList();
    super.onInit();
  }

  void fetchOrderList() async {
    try {
      isLoading(true);
      var orders = await NetworkServices().fetchShopOrderList();
      if (orders != null) {
        shopOrderList = orders;
      }
    } finally {
      isLoading(false);
    }
  }
}
