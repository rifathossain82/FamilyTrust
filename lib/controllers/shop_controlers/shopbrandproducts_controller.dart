import 'package:familytrust/models/shop/rpshobrandproducts_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class ShopBrandProductsController extends GetxController {
  RpShopBrandProducts brandProducts;
  bool noData = false;
  var isLoading = false.obs;

  void fetchShopBrandProducts(String id, String page) async {
    noData = false;
    try {
      isLoading(true);
      var product = await NetworkServices().fetchShopBrandProducts(
          id: id, page: page, filter: '', max: '', min: '');
      if (product != null) {
        brandProducts = product;
      }
    } finally {
      isLoading(false);
    }
  }

  void fetchShopBrandProductsWithFilter(
      {String id, String filter, String min, String max}) async {
    try {
      isLoading(true);
      var product = await NetworkServices()
          .fetchShopBrandProducts(id: id, filter: filter, min: min, max: max);
      if (product != null) {
        brandProducts = product;
      }
    } finally {
      isLoading(false);
    }
  }

  Future<void> fetchShopBrandProductsAdd(
      {String id, String page, String filter, String min, String max}) async {
    var add = await NetworkServices().fetchShopBrandProducts(
        id: id, page: page, filter: filter, min: min, max: max);
    if (add != null) {
      if (add.data.isNotEmpty) {
        try {
          isLoading(true);
          noData = false;
          for (int i = 0; i < add.data.length; i++) {
            brandProducts.data.add(add.data[i]);
          }
        } finally {
          isLoading(false);
        }
      } else {
        noData = true;
      }
    }
  }
}
