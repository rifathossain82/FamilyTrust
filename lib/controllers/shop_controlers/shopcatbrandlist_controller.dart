import 'package:familytrust/models/shop/rpcatbrandlist_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class ShopCatBrandListController extends GetxController {
  RpCatBrandListModel catBrandList;
  var isLoading = false.obs;

  void fetchBrandsList(String catName, String id) async {
    try {
      isLoading(true);
      var list = await NetworkServices().fetchBrandsList(catName, id);
      if (list != null) {
        catBrandList = list;
      }
    } finally {
      isLoading(false);
    }
  }
}
