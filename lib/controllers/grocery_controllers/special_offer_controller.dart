import 'package:familytrust/models/grocery/rpgrocerypopular_model.dart';
import 'package:familytrust/models/grocery/rpoffer_product_model.dart';
import 'package:familytrust/models/grocery/rpproducts_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class SpecialOfferController extends GetxController {
  RpOfferProductModel offerProducts = RpOfferProductModel(data: []);
  var offerProductList = [].obs;
  var isLoading = false.obs;
  var pageNumber = 1.obs;
  var loadedCompleted = false.obs;

  void fetchOfferProductsWithPagination() async {
    try {
      if (pageNumber.value == 1) {
        offerProductList.value = [];
        isLoading(true);
        loadedCompleted(false);
      }

      var responseBody = await NetworkServices().fetchGroceryOfferProducts(pageNumber.value);
      if (responseBody != null) {
        if (responseBody.meta.currentPage == responseBody.meta.lastPage) {
          loadedCompleted(true);
        } else {
          loadedCompleted(false);
        }
        offerProductList.addAll(responseBody.data);
      }
    } finally {
      isLoading(false);
    }
  }

  void fetchGrocerySpecialOffer() async {
    try {
      isLoading(true);
      var products = await NetworkServices().fetchGroceryOfferProducts(1);
      if (products != null) {
        offerProducts = products;
      }
    } finally {
      isLoading(false);
    }
  }
}
