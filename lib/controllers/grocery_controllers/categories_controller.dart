import 'package:familytrust/models/grocery/rpcategories_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class CategoriesController extends GetxController {
  RpCategoriesModel categories = RpCategoriesModel(data: []);
  var isLoading = false.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    fetchCategories();
    super.onInit();
  }

  void fetchCategories() async {
    try {
      isLoading(true);
      var category = await NetworkServices().fetchCategories();
      if (category != null) {
        categories = category;
      }
    } catch (e) {
      print('Exception: $e');
    } finally {
      isLoading(false);
    }
  }
}
