import 'package:familytrust/models/grocery/rptimerange_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class TimeRangeController extends GetxController {
  RpTimeRangeModel timeRangeToday = RpTimeRangeModel(data: []);
  RpTimeRangeModel timeRangeNextDay = RpTimeRangeModel(data: []);
  var isLoading = false.obs;

  void fetchTodayTime() async {
    try {
      isLoading(true);
      var today = await NetworkServices().fetchTimeRange('time-range');
      if (today != null) {
        timeRangeToday = today;
      }
    } finally {
      isLoading(false);
    }
  }

  void fetchNextDayTime() async {
    try {
      isLoading(true);
      var nextDay = await NetworkServices().fetchTimeRange('time-range?date=1');
      if (nextDay != null) {
        timeRangeNextDay = nextDay;
      }
    } finally {
      isLoading(false);
    }
  }
}
