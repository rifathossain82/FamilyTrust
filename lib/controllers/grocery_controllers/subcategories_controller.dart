import 'package:familytrust/models/grocery/rpsubcategories_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class SubCategoriesController extends GetxController {
  RpSubCategoriesModel subCategories = RpSubCategoriesModel(data: []);
  var isLoading = false.obs;

  void fetchSubCategories(String catId) async {
    try {
      isLoading(true);
      var subCategory = await NetworkServices().fetchSubCategories(catId);
      if (subCategory != null) {
        subCategories = subCategory;
      }
    } finally {
      isLoading(false);
    }
  }
}
