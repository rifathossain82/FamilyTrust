import 'package:familytrust/models/grocery/rpsubcategories_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class ChildCategoriesController extends GetxController {
  RpSubCategoriesModel childCategories = RpSubCategoriesModel(data: []);
  var isLoading = false.obs;

  Future<void> fetchChildCategories(String subCatId) async {
    try {
      isLoading(true);
      var childCategory = await NetworkServices().fetchSubCategories(subCatId);
      if (childCategory != null) {
        childCategories = childCategory;
      }
    } finally {
      isLoading(false);
    }
  }
}
