import 'package:familytrust/models/grocery/rp_popular_banner_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class PopularBannerController extends GetxController {
  RpPopularBannerModel bannerList = RpPopularBannerModel(data: []);
  var isLoading = false.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    fetchBanners();
    super.onInit();
  }

  void fetchBanners() async {
    try {
      isLoading(true);
      var banners = await NetworkServices().fetchBanners();
      if (banners != null) {
        bannerList = banners;
      }
    } finally {
      isLoading(false);
    }
  }
}
