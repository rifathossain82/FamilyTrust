import 'package:familytrust/models/grocery/rparea_model.dart';
import 'package:familytrust/models/grocery/rpdistrict_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class LocationController extends GetxController{
  RpDistrictModel districtList = RpDistrictModel(data: []);
  RpAreaModel areaList;
  var isLoading = false.obs;
  var deliveryCharge = 0.obs;

  void fetchDistrict() async {
    try {
      isLoading(true);
      var districts = await NetworkServices().fetchDistrict();
      if (districts != null) {
        districtList = districts;
      }
    } finally {
      isLoading(false);
    }
  }

  void fetchArea(String districtId) async {
    try {
      isLoading(true);
      var area = await NetworkServices().fetchArea(districtId);
      if (area != null) {
        areaList = area;
      }
    } finally {
      isLoading(false);
    }
  }

  Future fetchDeliveryCharge(String areaId, String totalAmount) async {
    try {
      isLoading(true);
      var charge = await NetworkServices().fetchDeliveryCharge(areaId, totalAmount);
      if (charge != null) {
        deliveryCharge.value = charge;
      }
    } finally {
      isLoading(false);
    }
  }
}