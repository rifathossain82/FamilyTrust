import 'package:familytrust/models/grocery/rpproducts_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class ProductsListController extends GetxController {
  var productsList = [].obs;
  var isLoading = false.obs;
  var pageNumber = 1.obs;
  var loadedCompleted = false.obs;

  Future<void> fetchProductWithPagination(String id) async {
    try {
      if (pageNumber.value == 1) {
        productsList.value = [];
        isLoading(true);
        loadedCompleted(false);
      }

      var responseBody = await NetworkServices()
          .fetchProductsWithPagination(id, pageNumber.value);
      if (responseBody != null) {
        if (responseBody.meta.currentPage == responseBody.meta.lastPage) {
          loadedCompleted(true);
        } else {
          loadedCompleted(false);
        }
        productsList.addAll(responseBody.data);
      }
    } finally {
      isLoading(false);
    }
  }

  Future<void> fetchProductList(String childCatId) async {
    try {
      isLoading(true);
      productsList.value = [];
      var productList = await NetworkServices().fetchProductsList(childCatId);
      if (productList != null) {
        productsList.value = productList.data;
      }
    } finally {
      isLoading(false);
    }
  }
}
