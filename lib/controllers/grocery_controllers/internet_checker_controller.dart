import 'package:familytrust/helper/helper_method.dart';
import 'package:get/get.dart';

class InternetCheckerController extends GetxController{
  var noInternet = false.obs;

  @override
  void onInit() {
    checkInternet();
    super.onInit();
  }

  void checkInternet()async{
    if(await hasInternet == false){
      noInternet(true);
    } else{
      noInternet(false);
    }
  }
}