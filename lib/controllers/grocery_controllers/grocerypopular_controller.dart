import 'package:familytrust/models/grocery/rpgrocerypopular_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class GroceryPopularController extends GetxController {
  RpGroceryPopular groceryPopular = RpGroceryPopular(data: []);

  var isLoading = false.obs;

  void fetchGroceryPopular() async {
    try {
      isLoading(true);
      var popular = await NetworkServices().fetchGroceryPopular();
      if (popular != null) {
        groceryPopular = popular;
      }
    } finally {
      isLoading(false);
    }
  }
}
