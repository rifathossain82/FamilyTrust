import 'package:familytrust/models/grocery/rporderlist_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class GroceryOrderController extends GetxController {
  RpOrderListModel groceryOrderList = RpOrderListModel(data: []);
  var isLoading = false.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    fetchOrderList();
    super.onInit();
  }

  void fetchOrderList() async {
    try {
      isLoading(true);
      var orders = await NetworkServices().fetchGroceryOrderList();
      if (orders != null) {
        groceryOrderList = orders;
      }
    } finally {
      isLoading(false);
    }
  }
}
