import 'package:familytrust/models/grocery/rpproductdetails_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class ProductDetailsController extends GetxController {
  RpPrdouctDetailsModel productDetails = RpPrdouctDetailsModel(data: Data());
  var isLoading = false.obs;

  Future<void> fetchProductDetails(String proId) async {
    try {
      isLoading(true);
      var details = await NetworkServices().fetchProductDetails(proId);
      if (details != null) {
        productDetails = details;
      }
    } finally {
      isLoading(false);
    }
  }
}
