import 'package:familytrust/models/grocery/rp_home_category_product_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class GroceryHomeCategoryProductController extends GetxController {
  // RpHomeCategoryProductModel categoryProducts = RpHomeCategoryProductModel(data: []);

  var isLoading = false.obs;

  Future fetchGroceryHomeCategoryProduct(String catId) async {
    try {
      isLoading(true);
      var products = await NetworkServices().fetchGroceryHomeCategoryProduct(catId);
      if (products != null) {
        return products;
      }
    } finally {
      isLoading(false);
    }
  }
}
