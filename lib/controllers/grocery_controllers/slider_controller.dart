import 'package:familytrust/models/grocery/rpslider_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class SlidersController extends GetxController {
  RpSliderModel sliders = RpSliderModel(data: []);
  var isLoading = false.obs;

  @override
  void onInit() {
    fetchSliders();
    super.onInit();
  }

  void fetchSliders() async {
    try {
      isLoading(true);
      var slider = await NetworkServices().fetchSliders();
      if (slider != null) {
        sliders = slider;
      }
    } finally {
      isLoading(false);
    }
  }
}
