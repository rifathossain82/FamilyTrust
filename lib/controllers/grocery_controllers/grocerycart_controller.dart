import 'package:familytrust/helper/db_helper.dart';
import 'package:familytrust/models/grocery/rpcart_model.dart';
import 'package:get/get.dart';

class GroceryCartController extends GetxController {
  var groceryCart = [].obs();
  var isLoading = false.obs;

  final DatabaseHelper _databaseHelper = DatabaseHelper.instance;

  @override
  void onInit() async {
    // TODO: implement onInit
    await queryAllCart();
    super.onInit();
  }

  void addToCart(var rpSingleGroceryModel) async {
    //for adding existing items to the cart and increase quantity
    var isExist = groceryCart
        .indexWhere((element) => element.id == rpSingleGroceryModel.id);
    print('Exist : $isExist');
    int qty;
    for (int i = 0; i < groceryCart.length; i++) {
      if (groceryCart[i].id == rpSingleGroceryModel.id) {
        qty = groceryCart[i].qty + 1;
      }
    }
    if (isExist >= 0) {
      updateCart(rpSingleGroceryModel.id, qty, rpSingleGroceryModel.price);
    } else {
      //for adding new items to the cart
      RpCartModel rpCartModel = RpCartModel(
        id: rpSingleGroceryModel.id,
        productName: rpSingleGroceryModel.productName,
        bnProductName: rpSingleGroceryModel.bnProductName,
        price: rpSingleGroceryModel.discountPrice > 0
            ? rpSingleGroceryModel.discountPrice.round()
            : rpSingleGroceryModel.price,
        bnPrice: rpSingleGroceryModel.discountPrice > 0
            ? rpSingleGroceryModel.bnDiscountPrice
            : rpSingleGroceryModel.bnPrice,
        attribute: rpSingleGroceryModel.attribute,
        bnAttribute: rpSingleGroceryModel.bnAttribute,
        qty: rpSingleGroceryModel.qty,
        picture: rpSingleGroceryModel.picture,
        quantity: rpSingleGroceryModel.quantity,
      );
      _databaseHelper.insertCartGrocery(rpCartModel);

      var result = await _databaseHelper.queryAllGroceryCart();
      if (result.isNotEmpty) {
        groceryCart.assignAll(result);
        await queryAllCart();
      }
    }
  }

  Future<void> removeFromCart(int id) async {
    try {
      isLoading(true);
      await _databaseHelper.deleteCartGrocery(id);
      await queryAllCart();
    } finally {
      isLoading(false);
    }
  }

  void qtyDecrease(int id, int qty, int price) async {
    try {
      isLoading(true);
      await _databaseHelper.update(id, qty, price);
      await queryAllCart();
    } finally {
      isLoading(false);
    }
  }

  void updateCart(int id, int qty, int price) async {
    try {
      isLoading(true);
      await _databaseHelper.update(id, qty, price);
      await queryAllCart();
    } finally {
      isLoading(false);
    }
  }

  Future<void> queryAllCart() async {
    try {
      isLoading(true);
      var product = await _databaseHelper.queryAllGroceryCart();
      groceryCart.assignAll(product);
    } finally {
      isLoading(false);
    }
  }

  void emptyCart() async {
    try {
      isLoading(true);
      await _databaseHelper.emptyGroceryCart();
      await queryAllCart();
    } finally {
      isLoading(false);
    }
  }
}
