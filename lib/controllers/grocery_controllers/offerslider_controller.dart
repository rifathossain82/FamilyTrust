import 'package:familytrust/models/grocery/rpofferslider_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class OfferSlidersController extends GetxController {
  RpOfferSliderModel offerSliders = RpOfferSliderModel(data: []);
  var isLoading = false.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    fetchOfferSliders();
    super.onInit();
  }

  void fetchOfferSliders() async {
    try {
      isLoading(true);
      var offerSlider = await NetworkServices().fetchOfferSliders();
      if (offerSlider != null) {
        offerSliders = offerSlider;
      }
    } finally {
      isLoading(false);
    }
  }
}
