import 'package:familytrust/models/grocery/rpsearch_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class SearchController extends GetxController {
  RpSearchModel searchAll = RpSearchModel(data: []);
  var isLoading = false.obs;
  bool noData = false;

  Future<void> fetchSearch(var value, String page) async {
    try {
      isLoading(true);
      var search = await NetworkServices().fetchAllSearch(value, page);
      if (search != null) {
        searchAll = search;
        update();
      }
    } finally {
      isLoading(false);
    }
  }

  Future<void> fetchSearchAdd(var value, String page) async {
    var add = await NetworkServices().fetchAllSearch(value, page);
    if (add != null) {
      if (add.data.isNotEmpty) {
        try {
          isLoading(true);
          noData = false;
          for (int i = 0; i < add.data.length; i++) {
            searchAll.data.add(add.data[i]);
          }
        } finally {
          isLoading(false);
        }
      } else {
        noData = true;
      }
    }
  }
}
