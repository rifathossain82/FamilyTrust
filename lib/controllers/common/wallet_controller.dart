import 'package:familytrust/models/common/rpwallet_model.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:get/get.dart';

class WalletDataController extends GetxController {
  RpWalletModel walletModel;
  var isLoading = false.obs;

  Future<void> fetchWalletData() async {
    try {
      isLoading(true);
      var details = await NetworkServices().fetchWalletData();
      if (details != null) {
        walletModel = details;
      }
    } finally {
      isLoading(false);
    }
  }
}
