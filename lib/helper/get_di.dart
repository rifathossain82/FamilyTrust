import 'package:familytrust/controllers/grocery_controllers/coupon_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/home_category_product_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/internet_checker_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/location_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/popular_banner_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/slider_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/special_offer_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shoppopular_controller.dart';
import 'package:get/get.dart';
import 'package:familytrust/controllers/grocery_controllers/categories_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/childcategories_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/grocerycart_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/grocerypopular_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/offerslider_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/orderlist_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/productdetails_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/productslist_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/search_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/subcategories_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/timerange_controller.dart';

Future<void> init() async {
  Get.lazyPut(
    () => InternetCheckerController(),
    fenix: true,
  );
  Get.lazyPut(
    () => LocationController()..fetchDistrict(),
    fenix: true,
  );
  Get.lazyPut(
    () => SlidersController(),
    fenix: true,
  );
  Get.lazyPut(
    () => PopularBannerController(),
    fenix: true,
  );
  Get.lazyPut(
    () => CategoriesController(),
    fenix: true,
  );
  Get.lazyPut(
    () => CouponController(),
    fenix: true,
  );
  Get.lazyPut(
    () => GroceryHomeCategoryProductController(),
    fenix: true,
  );
  Get.lazyPut(
    () => ChildCategoriesController(),
    fenix: true,
  );
  Get.lazyPut(
    () => OfferSlidersController(),
    fenix: true,
  );
  Get.lazyPut(
    () => GroceryCartController(),
    fenix: true,
  );
  Get.lazyPut(
    () => GroceryPopularController(),
    fenix: true,
  );
  Get.lazyPut(
    () => SpecialOfferController(),
    fenix: true,
  );
  Get.lazyPut(
    () => GroceryOrderController(),
    fenix: true,
  );
  Get.lazyPut(
    () => ProductDetailsController(),
    fenix: true,
  );
  Get.lazyPut(
    () => ProductsListController(),
    fenix: true,
  );
  Get.lazyPut(
    () => SearchController(),
    fenix: true,
  );
  Get.lazyPut(
    () => SubCategoriesController(),
    fenix: true,
  );
  Get.lazyPut(
    () => TimeRangeController()..fetchTodayTime()..fetchNextDayTime(),
    fenix: true,
  );
  Get.lazyPut(
    () => ShopPopularController(),
    fenix: true,
  );
}
