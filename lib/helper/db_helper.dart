import 'dart:io';
import 'package:familytrust/models/grocery/rpcart_model.dart';
import 'package:familytrust/models/shop/rpshopcart_model.dart';
import 'package:familytrust/models/shop/rpshopfavorite_model.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final _dbName = 'familyTrust.db';
  static final _tableName = 'groceryCart';
  static final _tableName2 = 'shopCart';
  static final _tableName3 = 'shopFavorite';
  static final _dbVersion = 1;
  static final columnId = 'id';
  static final columnName = 'product_name';
  static final columnBnName = 'bn_product_name';
  static final columnPrice = 'price';
  static final columnBnPrice = 'bn_price';
  static final columnAttribute = 'attribute';
  static final columnBnAttribute = 'bn_attribute';
  static final columnQty = 'qty';
  static final columnQuantity = 'quantity';
  static final columnImg = 'picture';
  //extra column for shop part//
  static final columnImgShop = 'thumbnail_img';
  static final columnColorShop = 'colors';
  static final columnNameShop = 'name';
  static final columnBnNameShop = 'bn_name';
  static final columnAttributeShop = 'attributes';
  static final columnMaxQtyShop = 'maxQty';
  static final columnShippingChargeShop = 'shipping_charge';
  static final columnDeliveryChargeShop = 'deliveryCharge';
  static final columnAreaShop = 'area';
  static final columnCityIdShop = 'city_id';

  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await _initiateDatabase();
    return _database;
  }

  Future<Database> _initiateDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, _dbName);
    var database = await openDatabase(path, version: _dbVersion,
        onCreate: (Database db, int version) async {
      //grocery cart table
      await db.execute('''
            CREATE TABLE $_tableName(
            $columnId INTEGER,
            $columnName TEXT,
            $columnBnName TEXT,
            $columnPrice INTEGER,
            $columnBnPrice TEXT,
            $columnAttribute TEXT,
            $columnBnAttribute TEXT,
            $columnQuantity INTEGER,
            $columnQty INTEGER,
            $columnImg TEXT
                      
            )
            
        ''');
      //shop cart table
      await db.execute('''
            CREATE TABLE $_tableName2(
            $columnId INTEGER,
            $columnNameShop TEXT,
            $columnBnNameShop TEXT,
            $columnPrice INTEGER,
            $columnBnPrice TEXT,
            $columnImgShop TEXT, 
            $columnAttributeShop TEXT,
            $columnQty INTEGER,
            $columnDeliveryChargeShop INTEGER,
            $columnShippingChargeShop INTEGER,
            $columnCityIdShop INTEGER,
            $columnMaxQtyShop TEXT,
            $columnAreaShop TEXT,
            $columnColorShop TEXT
                        
            )
            
        ''');
      //shop favorite table
      await db.execute('''
            CREATE TABLE $_tableName3(
            $columnId INTEGER,
            $columnNameShop TEXT,
            $columnBnNameShop TEXT,
            $columnPrice INTEGER,
            $columnBnPrice TEXT,
            $columnImgShop TEXT
            
            )
            
        ''');
    });
    return database;
  }

  //grocery start here//

  //Query all row
  Future<List<RpCartModel>> queryAllGroceryCart() async {
    List<RpCartModel> _products = [];
    Database db = await instance.database;
    var results = await db.query(_tableName);
    results.forEach(
      (element) {
        var products = RpCartModel.fromJson(element);
        _products.add(products);
      },
    );
    return _products;
  }

  //Insert row
  Future insertCartGrocery(RpCartModel rpCartModel) async {
    Database db = await instance.database;
    var result = await db.insert(
      _tableName,
      rpCartModel.toJson(),
    );
    print('result : $result');
  }

  //remove table
  Future<void> emptyGroceryCart() async {
    Database db = await instance.database;
    db.delete(_tableName);
  }

  //update row
  Future update(int id, int qty, int price) async {
    Database db = await instance.database;
    Map<String, dynamic> row = {
      DatabaseHelper.columnQty: qty,
    };
    return await db
        .update(_tableName, row, where: '$columnId = ?', whereArgs: [id]);
  }

  //Delete row
  Future<int> deleteCartGrocery(int id) async {
    Database db = await instance.database;
    return await db.delete(_tableName, where: '$columnId = ?', whereArgs: [id]);
  }
  //grocery end here//

  //shop start here//

  //Query all row
  Future<List<RpShopCartModel>> queryAllShopCart() async {
    List<RpShopCartModel> _products = [];
    Database db = await instance.database;
    var results = await db.query(_tableName2);
    results.forEach(
      (element) {
        var products = RpShopCartModel.fromJson(element);
        _products.add(products);
      },
    );
    return _products;
  }

  //Insert row
  Future insertCartShop(RpShopCartModel rpShopCartModel) async {
    Database db = await instance.database;
    var result = await db.insert(
      _tableName2,
      rpShopCartModel.toJson(),
    );
    print('result : $result');
  }

  //remove table
  Future<void> emptyShopCart() async {
    Database db = await instance.database;
    db.delete(_tableName2);
  }

  //update row
  Future updateShop(int id, int qty, int price) async {
    Database db = await instance.database;
    Map<String, dynamic> row = {
      DatabaseHelper.columnQty: qty,
    };
    return await db
        .update(_tableName2, row, where: '$columnId = ?', whereArgs: [id]);
  }

  //Delete row
  Future<int> deleteCartShop(int id) async {
    Database db = await instance.database;
    return await db
        .delete(_tableName2, where: '$columnId = ?', whereArgs: [id]);
  }

//shop end here//

//shop favorite start here//

  //Query all row
  Future<List<RpShopFavoriteModel>> queryAllShopFavorite() async {
    List<RpShopFavoriteModel> _products = [];
    Database db = await instance.database;
    var results = await db.query(_tableName3);
    results.forEach(
      (element) {
        var products = RpShopFavoriteModel.fromJson(element);
        _products.add(products);
      },
    );
    return _products;
  }

  //Insert row
  Future insertShopFavorite(RpShopFavoriteModel rpShopFavoriteModel) async {
    Database db = await instance.database;
    var result = await db.insert(
      _tableName3,
      rpShopFavoriteModel.toJson(),
    );
    print('result : $result');
  }

  //Delete row
  Future<int> deleteShopFavorite(int id) async {
    Database db = await instance.database;
    return await db
        .delete(_tableName3, where: '$columnId = ?', whereArgs: [id]);
  }

//shop favorite end here//
}
