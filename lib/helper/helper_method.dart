import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

void launchMessengerChat({String msg}) async {
  var messengerURL = "https://m.me/$facebookId?text=$msg";
  if (await canLaunchUrl(Uri.parse(messengerURL))) {
    await launchUrl(
      Uri.parse(messengerURL),
      mode: LaunchMode.externalApplication,
    );
  } else {
    throw 'Could not launch $messengerURL';
  }
}

void createCall(String number) async {
  var url = Uri.parse('tel: $number');

  if (await canLaunchUrl(url)) {
    await launchUrl(url);
  }
}

void kSnackBar(String msg) {
  Get.showSnackbar(
    GetSnackBar(
      backgroundColor: kSecondaryColor,
      message: msg,
      maxWidth: 1170,
      duration: const Duration(milliseconds: 500),
      snackStyle: SnackStyle.FLOATING,
      margin: const EdgeInsets.all(10),
      borderRadius: 5,
      isDismissible: true,
      dismissDirection: DismissDirection.horizontal,
      snackPosition: SnackPosition.TOP,
    ),
  );
}

void kSnackBarWithDuration({String msg, Duration duration = const Duration(seconds: 1), Color bgColor = kDarkColor}) {
  Get.showSnackbar(
    GetSnackBar(
      backgroundColor: bgColor,
      message: msg,
      maxWidth: 1170,
      duration: duration,
      snackStyle: SnackStyle.FLOATING,
      margin: const EdgeInsets.all(10),
      borderRadius: 5,
      isDismissible: true,
      dismissDirection: DismissDirection.horizontal,
      snackPosition: SnackPosition.BOTTOM,
    ),
  );
}

Future customAlert({
  String title,
  String body,
  String confirmTitle = 'Confirm',
  String cancelTitle = 'Cancel',
  Color color,
  BuildContext context,
  Function onConfirm,
  Function onCancel,
  bool isOneButton = false,
  bool barrierDismissible = true,
}) {
  return showDialog(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        titlePadding: EdgeInsets.zero,
        contentPadding: EdgeInsets.zero,
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
              ),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10),
                  topLeft: Radius.circular(10),
                ),
              ),
              child: Padding(
                padding:
                const EdgeInsets.only(left: 10.0, top: 10, right: 10),
                child: Center(
                  child: Text(
                    title ?? '',
                    style: Theme.of(context).textTheme.headline2.copyWith(
                      color: kBlackColor,
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: 10.0, vertical: 30),
              child: Text(
                body ?? '',
                textAlign: TextAlign.left,
              ),
            ),
            if(!isOneButton)
              Row(
                children: [
                  kWidthBox10,
                  Expanded(
                    child: InkWell(
                      onTap: onCancel,
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                          color: Color(0xFFF3F3F3),
                          borderRadius: BorderRadius.all(
                            Radius.circular(5),
                          ),
                        ),
                        child: Text(
                          cancelTitle,
                          style:
                          Theme.of(context).textTheme.headline2.copyWith(
                            color: kBlackColor,
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                  ),
                  kWidthBox10,
                  Expanded(
                    child: InkWell(
                      onTap: onConfirm,
                      child: Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        decoration: const BoxDecoration(
                          color: kPrimaryColor,
                          borderRadius: BorderRadius.all(
                            Radius.circular(5),
                          ),
                        ),
                        child: Text(
                          confirmTitle,
                          style:
                          Theme.of(context).textTheme.headline2.copyWith(
                            color: kWhiteColor,
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                  ),
                  kWidthBox10,
                ],
              ),
            kHeightBox5,
            if(isOneButton)
              InkWell(
                onTap: onConfirm,
                child: Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  margin: const EdgeInsets.symmetric(horizontal: 15),
                  decoration: const BoxDecoration(
                    color: kErrorColor,
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                  ),
                  child: Text(
                    confirmTitle,
                    style:
                    Theme.of(context).textTheme.headline2.copyWith(
                      color: kWhiteColor,
                      fontSize: 14,
                    ),
                  ),
                ),
              ),
            kHeightBox5,
          ],
        ),
      );
    },
  );
}

Future<bool> get hasInternet async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.none) {
    return false;
  } else {
    return true;
  }
}
