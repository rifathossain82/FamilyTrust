class EquationHelper {
  String amountDiscount(int price, int discountPrice) {
    int total = 0;
    total = price - discountPrice;
    return '৳${total.toStringAsFixed(0)}';
  }

  String amountPercentage(int price, int percentage) {
    int total = 0;
    total = price - (percentage * 100);
    return '৳${total.toStringAsFixed(0)}';
  }

  String removeAllHtmlTags(String htmlText) {
    RegExp exp = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);

    return htmlText.replaceAll(exp, '');
  }
}
