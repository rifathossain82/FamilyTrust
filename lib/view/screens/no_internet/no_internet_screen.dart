import 'package:familytrust/helper/helper_method.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/dashboard/dashboard_screen.dart';
import 'package:familytrust/view/widgets/default_btn.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class NoInternetScreen extends StatelessWidget {
  const NoInternetScreen({Key key}) : super(key: key);
  static const routeName = 'no-internet-screen';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.cloud_off,
              size: 150,
              color: kDiamondColor,
            ),
            kHeightBox20,
            Text(
              'Ooops',
              style: kHeadLine.copyWith(
                color: kDiamondColor,
              ),
            ),
            kHeightBox20,
            Text(
              'Slow or no internet connection.',
              style: kRegularText.copyWith(
                color: kDiamondColor,
              ),
            ),
            Text(
              'Check your internet settings.',
              style: kRegularText.copyWith(
                color: kDiamondColor,
              ),
            ),
            kHeightBox40,
            SizedBox(
              width: SizeConfig.screenWidth * 0.6,
              child: DefaultBtn(
                title: 'TRY AGAIN',
                isChange: true,
                radius: 30,
                color: kPrimaryColor,
                onPress: () => navigation(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void navigation(BuildContext context) async {
    if (await hasInternet) {
      Navigator.pushNamedAndRemoveUntil(
          context, DashboardScreen.routeName, (route) => false);
    } else {
      Navigator.pushNamedAndRemoveUntil(
          context, NoInternetScreen.routeName, (route) => false);
    }
  }
}
