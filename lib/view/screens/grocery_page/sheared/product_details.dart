import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/controllers/grocery_controllers/productdetails_controller.dart';
import 'package:familytrust/helper/helper_method.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/grocery_controllers/grocerycart_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/widgets/custom_shimmer.dart';
import 'package:familytrust/view/widgets/default_btn.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';

class ProductDetails extends StatefulWidget {
  final String prodId;
  static const routeName = 'product_details';

  const ProductDetails({
    Key key,
    this.prodId,
  }) : super(key: key);

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  final ProductDetailsController productDetailsController = Get.find();
  final GroceryCartController groceryCartController = Get.find();
  final lng = LocalizationService().getCurrentLang();

  @override
  void initState() {
    getDetails();
    super.initState();
  }

  getDetails() async {
    productDetailsController.fetchProductDetails(
      widget.prodId.toString(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () {
          if (productDetailsController.isLoading.isTrue) {
            return productDetails();
          } else {
            return Column(
              children: [
                Expanded(
                  flex: 12,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Stack(
                          children: [
                            Container(
                              color: Color(0xFFEFEFEF),
                              height: SizeConfig.screenWidth,
                              child: Hero(
                                tag: productDetailsController
                                    .productDetails.data.id
                                    .toString(),
                                child: CachedNetworkImage(
                                  imageUrl: kImageUrl +
                                      productDetailsController
                                          .productDetails.data.picture,
                                  fit: BoxFit.fill,
                                  placeholder: (context, url) => CustomLoader(),
                                  errorWidget: (context, url, error) => Center(
                                    child: Icon(
                                      Icons.error,
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              top: getProportionateScreenHeight(65),
                              right: 5,
                              child: InkWell(
                                onTap: () {
                                  Get.back();
                                },
                                child: Container(
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                    color: kSecondaryColor,
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  child: Icon(
                                    Icons.close,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Text(
                            lng == 'Bangla'
                                ? productDetailsController
                                    .productDetails.data.bnProductName
                                : productDetailsController
                                    .productDetails.data.productName,
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w800),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Row(
                            children: [
                              Text(
                                productDetailsController.productDetails.data
                                            .discountPrice ==
                                        0
                                    ? (lng == 'Bangla'
                                        ? '৳${productDetailsController.productDetails.data.bnPrice}'
                                        : '৳${productDetailsController.productDetails.data.price}')
                                    : (lng == 'Bangla'
                                        ? '৳${productDetailsController.productDetails.data.bnDiscountPrice}'
                                        : '৳${productDetailsController.productDetails.data.discountPrice.round()}'),
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: kPrimaryColor,
                                  fontSize: 16,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              productDetailsController
                                          .productDetails.data.discountPrice ==
                                      0
                                  ? SizedBox()
                                  : Text(
                                      '৳${lng == 'Bangla' ? productDetailsController.productDetails.data.bnPrice : productDetailsController.productDetails.data.price.toString()}',
                                      style: TextStyle(
                                          fontSize: 16,
                                          color: kSaleColor,
                                          decoration:
                                              TextDecoration.lineThrough),
                                    ),
                              SizedBox(
                                width: 10,
                              ),
                              productDetailsController
                                          .productDetails.data.discountPrice ==
                                      0
                                  ? SizedBox()
                                  : Container(
                                      decoration: BoxDecoration(
                                          color: kSuccessColor,
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      child: Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: Text(
                                          '-৳${lng == 'Bangla' ? productDetailsController.productDetails.data.bnDiscountAmount : productDetailsController.productDetails.data.discountAmount.round()} ' +
                                              'OFF'.tr,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14,
                                              color: kWhiteColor),
                                        ),
                                      ),
                                    ),
                              Spacer(),
                              Text(
                                lng == 'Bangla'
                                    ? productDetailsController
                                        .productDetails.data.bnAttribute
                                    : productDetailsController
                                        .productDetails.data.attribute,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Divider(),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Text(
                            'Details'.tr,
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 8),
                          child: Html(
                            data: productDetailsController
                                    .productDetails.data.description ??
                                '',
                            style: {
                              // tables will have the below background color
                              "table": Style(
                                backgroundColor:
                                    const Color.fromARGB(0x50, 0xee, 0xee, 0xee),
                              ),
                              // some other granular customizations are also possible
                              "tr": Style(
                                border: const Border(
                                    bottom: BorderSide(color: Colors.grey)),
                              ),
                              "th": Style(
                                padding: const EdgeInsets.all(6),
                                backgroundColor: Colors.grey,
                              ),
                              "td": Style(
                                padding: const EdgeInsets.all(6),
                                alignment: Alignment.topLeft,
                              ),
                              // text that renders h1 elements will be red
                              "h1": Style(
                                  color: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      ?.color,
                                  fontSize: FontSize.large),
                              "h2": Style(
                                  color: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      ?.color,
                                  fontSize: FontSize.large),
                              "h3": Style(
                                  color: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      ?.color,
                                  fontSize: FontSize.large),
                              "h4": Style(
                                  color: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      ?.color,
                                  fontSize: FontSize.large),
                              "h5": Style(
                                  color: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      ?.color,
                                  fontSize: FontSize.large),
                              "h6": Style(
                                  color: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      ?.color,
                                  fontSize: FontSize.large),
                              "p": Style(
                                  color: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      ?.color,
                                  fontSize: FontSize.large),
                              "span": Style(
                                  color: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      ?.color,
                                  fontSize: FontSize.medium),
                            },
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                ),
                productDetailsController.productDetails.data.quantity == 0
                    ? Expanded(
                        flex: 1,
                        child: Container(
                          width: double.infinity,
                          child: DefaultBtn(
                            isChange: true,
                            radius: 0,
                            color: kPrimaryColor,
                            title: 'Stock Out',
                            onPress: () {
                              kSnackBar('Stock Out');
                            },
                          ),
                        ),
                      )
                    : Expanded(
                        flex: 1,
                        child: Container(
                          color: kPrimaryColor,
                          width: double.infinity,
                          child: productDetailsController
                                      .productDetails.data.qty >
                                  0
                              ? Container(
                                  margin: EdgeInsets.all(2),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          setState(() {
                                            productDetailsController
                                                .productDetails.data.qty -= 1;
                                          });
                                          if (productDetailsController
                                                  .productDetails.data.qty !=
                                              0) {
                                            groceryCartController.qtyDecrease(
                                                productDetailsController
                                                    .productDetails.data.id,
                                                productDetailsController
                                                    .productDetails.data.qty,
                                                productDetailsController
                                                    .productDetails.data.price);
                                          } else {
                                            groceryCartController
                                                .removeFromCart(
                                                    productDetailsController
                                                        .productDetails
                                                        .data
                                                        .id);
                                            kSnackBar('Remove From Cart');
                                          }
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            color: kSecondaryColor,
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey,
                                                offset:
                                                    Offset(0.0, 1.0), //(x,y)
                                                blurRadius: 6.0,
                                              ),
                                            ],
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.all(5.0),
                                            child: Icon(Icons.remove),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        productDetailsController
                                            .productDetails.data.qty
                                            .toString(),
                                        style: TextStyle(
                                            color: kWhiteColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 18),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      InkWell(
                                        onTap: () {
                                          if (productDetailsController
                                                  .productDetails
                                                  .data
                                                  .quantity >
                                              productDetailsController
                                                  .productDetails.data.qty) {
                                            setState(() {
                                              productDetailsController
                                                  .productDetails.data.qty += 1;
                                            });
                                            groceryCartController.addToCart(
                                                productDetailsController
                                                    .productDetails.data);
                                          } else {
                                            kSnackBar('Stock Out');
                                          }
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            color: kSecondaryColor,
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey,
                                                offset:
                                                    Offset(0.0, 1.0), //(x,y)
                                                blurRadius: 6.0,
                                              ),
                                            ],
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.all(5.0),
                                            child: Icon(Icons.add),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : DefaultBtn(
                                  isChange: true,
                                  radius: 0,
                                  color: kPrimaryColor,
                                  title: 'Add to Cart'.tr,
                                  onPress: () {
                                    setState(() {
                                      productDetailsController
                                          .productDetails.data.qty += 1;
                                    });
                                    groceryCartController.addToCart(
                                        productDetailsController
                                            .productDetails.data);
                                    kSnackBar('Added to Cart');
                                  },
                                ),
                        ),
                      ),
              ],
            );
          }
        },
      ),
    );
  }
}
