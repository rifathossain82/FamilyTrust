import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/grocery_controllers/grocerycart_controller.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/auth/login.dart';
import 'package:familytrust/view/screens/grocery_page/checkout/chekout_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GroceryCartScreen extends StatefulWidget {
  static const routeName = 'grocery_cart_screen';

  @override
  _GroceryCartScreenState createState() => _GroceryCartScreenState();
}

class _GroceryCartScreenState extends State<GroceryCartScreen> {
  final String lng = LocalizationService().getCurrentLang();
  final GroceryCartController groceryCartController = Get.find();
  int total = 0;

  @override
  void initState() {
    if (groceryCartController.isClosed) {
      groceryCartController.onStart();
    }
    for (int i = 0; i < groceryCartController.groceryCart.length; i++) {
      total += groceryCartController.groceryCart[i].price *
          groceryCartController.groceryCart[i].qty;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cart'.tr),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 9,
            child: Obx(
              () {
                if (groceryCartController.isLoading.isTrue) {
                  return Center(
                    child: CustomLoader(),
                  );
                } else {
                  if (groceryCartController.groceryCart.isEmpty) {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.remove_shopping_cart,
                            size: getProportionateScreenHeight(50),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'Empty Cart'.tr,
                            style: TextStyle(
                              fontSize: getProportionateScreenHeight(30),
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return ListView.builder(
                      physics: ScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: groceryCartController.groceryCart.length,
                      itemBuilder: (context, int index) {
                        return Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Container(
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Container(
                                          child: CachedNetworkImage(
                                            imageUrl: kImageUrl +
                                                groceryCartController
                                                    .groceryCart[index].picture,
                                            fit: BoxFit.fill,
                                            placeholder: (context, url) =>
                                                CustomLoader(),
                                            errorWidget:
                                                (context, url, error) =>
                                                    Image.asset(
                                              kImageDir + 'placeholder.png',
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 5,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            lng == 'Bangla'
                                                ? groceryCartController
                                                    .groceryCart[index]
                                                    .bnProductName
                                                : groceryCartController
                                                    .groceryCart[index]
                                                    .productName,
                                            style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                            ),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                lng == 'Bangla'
                                                    ? '৳${groceryCartController.groceryCart[index].bnPrice} x ${groceryCartController.groceryCart[index].qty}'
                                                    : '৳${groceryCartController.groceryCart[index].price} x ${groceryCartController.groceryCart[index].qty}',
                                              ),
                                              Spacer(),
                                              Text(
                                                lng == 'Bangla'
                                                    ? groceryCartController
                                                        .groceryCart[index]
                                                        .bnAttribute
                                                    : groceryCartController
                                                        .groceryCart[index]
                                                        .attribute,
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                '৳${groceryCartController.groceryCart[index].price * groceryCartController.groceryCart[index].qty}',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  color: kPrimaryColor,
                                                  fontSize: 16,
                                                ),
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Spacer(),
                                              IconButton(
                                                icon: Icon(
                                                  Icons.delete,
                                                  color: Colors.red,
                                                ),
                                                onPressed: () {
                                                  Get.defaultDialog(
                                                    title:
                                                        'Delete this item?'.tr,
                                                    middleText:
                                                        'this action will delete this item from your cart'
                                                            .tr,
                                                    onConfirm: () async {
                                                      groceryCartController
                                                          .removeFromCart(
                                                              groceryCartController
                                                                  .groceryCart[
                                                                      index]
                                                                  .id);
                                                      setState(() {
                                                        total -= groceryCartController
                                                                .groceryCart[
                                                                    index]
                                                                .price *
                                                            groceryCartController
                                                                .groceryCart[
                                                                    index]
                                                                .qty;
                                                      });
                                                      Get.back();
                                                    },
                                                    buttonColor: kPrimaryColor,
                                                    confirmTextColor:
                                                        kWhiteColor,
                                                    onCancel: () {},
                                                  );
                                                },
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Spacer(),
                                              Row(
                                                children: [
                                                  InkWell(
                                                    onTap: () {
                                                      setState(() {
                                                        if (groceryCartController
                                                                .groceryCart[
                                                                    index]
                                                                .qty >
                                                            1) {
                                                          groceryCartController
                                                                  .groceryCart[
                                                                      index]
                                                                  .qty =
                                                              groceryCartController
                                                                      .groceryCart[
                                                                          index]
                                                                      .qty -
                                                                  1;

                                                          total -=
                                                              groceryCartController
                                                                  .groceryCart[
                                                                      index]
                                                                  .price;
                                                        }
                                                      });

                                                      print(
                                                          groceryCartController
                                                              .groceryCart[
                                                                  index]
                                                              .qty
                                                              .toString());
                                                    },
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(15.0),
                                                        color: kOrdinaryColor,
                                                      ),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(5.0),
                                                        child:
                                                            Icon(Icons.remove),
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Text(
                                                    groceryCartController
                                                        .groceryCart[index].qty
                                                        .toString(),
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 18),
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  InkWell(
                                                    onTap: () {
                                                      if (groceryCartController
                                                              .groceryCart[
                                                                  index]
                                                              .qty <
                                                          groceryCartController
                                                              .groceryCart[
                                                                  index]
                                                              .quantity) {
                                                        setState(() {
                                                          groceryCartController
                                                                  .groceryCart[
                                                                      index]
                                                                  .qty =
                                                              groceryCartController
                                                                      .groceryCart[
                                                                          index]
                                                                      .qty +
                                                                  1;

                                                          total +=
                                                              groceryCartController
                                                                  .groceryCart[
                                                                      index]
                                                                  .price;
                                                        });
                                                        print(
                                                            groceryCartController
                                                                .groceryCart[
                                                                    index]
                                                                .qty
                                                                .toString());
                                                      } else {
                                                        Get.snackbar(
                                                          'Stock Out',
                                                          '',
                                                          colorText:
                                                              kWhiteColor,
                                                        );
                                                      }
                                                    },
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(15.0),
                                                        color: kOrdinaryColor,
                                                      ),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(5.0),
                                                        child: Icon(Icons.add),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }
                }
              },
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              decoration: BoxDecoration(
                color: kPrimaryColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15),
                  topRight: Radius.circular(15),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      width: SizeConfig.screenWidth / 3,
                      child: RichText(
                        text: TextSpan(
                          text: 'Total'.tr,
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w500),
                          children: [
                            TextSpan(
                              text: '৳' + total.toString(),
                              style: TextStyle(
                                  color: kSecondaryColor,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        if (prefs.containsKey('uid')) {
                          if (groceryCartController.groceryCart.length < 1) {
                            Get.snackbar('Empty', 'you have an empty cart',
                                colorText: kWhiteColor);
                          } else {
                            prefs.setInt('total', total);
                            log(prefs.getInt('total').toString());
                            Get.to(() => CheckOutScreen());
                          }
                        } else {
                          Get.to(
                            () => LogIn(
                              isOff: true,
                              address: prefs.getString('address'),
                            ),
                          );
                        }
                      },
                      child: Container(
                        width: SizeConfig.screenWidth / 2.2,
                        padding: EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: kSecondaryColor,
                        ),
                        child: Text(
                          'Confirm Order'.tr,
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                            color: kWhiteColor,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
