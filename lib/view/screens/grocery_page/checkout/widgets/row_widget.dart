import 'package:flutter/material.dart';

class RowWidget extends StatelessWidget {
  final String title;
  final String amount;
  final TextStyle titleStyle;
  final TextStyle amountStyle;

  RowWidget({
    this.titleStyle,
    this.amountStyle,
    @required this.title,
    @required this.amount,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: titleStyle,
        ),
        Text(
          '৳ $amount',
          style: amountStyle,
        ),
      ],
    );
  }
}
