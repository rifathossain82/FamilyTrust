import 'dart:developer';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:date_picker_timeline/date_picker_widget.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:familytrust/controllers/grocery_controllers/location_controller.dart';
import 'package:familytrust/helper/helper_method.dart';
import 'package:familytrust/models/grocery/rparea_model.dart';
import 'package:familytrust/models/grocery/rpcoupon_model.dart';
import 'package:familytrust/models/grocery/rpdistrict_model.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/grocery_controllers/grocerycart_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/timerange_controller.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:familytrust/view/screens/dashboard/dashboard_screen.dart';
import 'package:familytrust/view/widgets/custom_alert_dialog.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/widgets/default_btn.dart';
import 'package:familytrust/view/widgets/input_form_widget.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/grocery_page/checkout/widgets/promo_widget.dart';
import 'package:familytrust/view/screens/grocery_page/checkout/widgets/row_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sslcommerz/model/SSLCAdditionalInitializer.dart';
import 'package:flutter_sslcommerz/model/SSLCCustomerInfoInitializer.dart';
import 'package:flutter_sslcommerz/model/SSLCEMITransactionInitializer.dart';
import 'package:flutter_sslcommerz/model/SSLCSdkType.dart';
import 'package:flutter_sslcommerz/model/SSLCShipmentInfoInitializer.dart';
import 'package:flutter_sslcommerz/model/SSLCTransactionInfoModel.dart';
import 'package:flutter_sslcommerz/model/SSLCommerzInitialization.dart';
import 'package:flutter_sslcommerz/model/SSLCurrencyType.dart';
import 'package:flutter_sslcommerz/model/sslproductinitilizer/General.dart';
import 'package:flutter_sslcommerz/model/sslproductinitilizer/SSLCProductInitializer.dart';
import 'package:flutter_sslcommerz/sslcommerz.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class CheckOutScreen extends StatefulWidget {
  static const routeName = 'order_confirm_screen';

  const CheckOutScreen({Key key}) : super(key: key);

  @override
  _CheckOutScreenState createState() => _CheckOutScreenState();
}

class _CheckOutScreenState extends State<CheckOutScreen> {
  final formKey = GlobalKey<FormState>();
  final String lng = LocalizationService().getCurrentLang();
  final groceryCartController = Get.put(GroceryCartController());
  final timeRangeController = Get.find<TimeRangeController>();
  final locationController = Get.find<LocationController>();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController couponTextController = TextEditingController();
  int tranId = DateTime.now().millisecondsSinceEpoch;
  dynamic formData = {};
  int selected = 1;
  num deliveryCharge = 0;
  var discount = 0;
  num couponDiscount = 0;
  var voucher = 0;
  bool isToday = true;
  String selectedTime;
  String _selectedValue = DateFormat('yyyy-MM-dd').format(DateTime.now());

  District _district;
  Area _area;

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    addressController.dispose();
    phoneController.dispose();
    couponTextController.dispose();
    super.dispose();
  }

  void getData() async {
    if (prefs.getString('name') != 'null') {
      nameController = TextEditingController(text: prefs.getString('name'));
    }
  }

  double get _totalAmount =>
      prefs.getInt('total').toDouble() +
      deliveryCharge -
      discount -
      voucher -
      couponDiscount;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 0,
        title: Text(
          "Checkout".tr,
          style: TextStyle(color: Colors.white),
        ),
        /* iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),*/
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 9,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ListView.builder(
                            physics: ScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: groceryCartController.groceryCart.length,
                            itemBuilder: (context, int index) {
                              return Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Container(
                                        child: Row(
                                          children: [
                                            Expanded(
                                              flex: 2,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(10.0),
                                                child: Container(
                                                  child: CachedNetworkImage(
                                                    imageUrl: kImageUrl +
                                                        groceryCartController
                                                            .groceryCart[index]
                                                            .picture,
                                                    fit: BoxFit.fill,
                                                    placeholder:
                                                        (context, url) =>
                                                            CustomLoader(),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Icon(
                                                      Icons.error,
                                                      color: Colors.red,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 5,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    lng == 'Bangla'
                                                        ? groceryCartController
                                                            .groceryCart[index]
                                                            .bnProductName
                                                        : groceryCartController
                                                            .groceryCart[index]
                                                            .productName,
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                    ),
                                                    maxLines: 2,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Row(
                                                    children: [
                                                      Text(
                                                        lng == 'Bangla'
                                                            ? '৳${groceryCartController.groceryCart[index].bnPrice} x ${groceryCartController.groceryCart[index].qty}'
                                                            : '৳${groceryCartController.groceryCart[index].price} x ${groceryCartController.groceryCart[index].qty}',
                                                      ),
                                                      Spacer(),
                                                      Text(
                                                        lng == 'Bangla'
                                                            ? groceryCartController
                                                                .groceryCart[
                                                                    index]
                                                                .bnAttribute
                                                            : groceryCartController
                                                                .groceryCart[
                                                                    index]
                                                                .attribute,
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Row(
                                                    children: [
                                                      Text(
                                                        '৳${groceryCartController.groceryCart[index].price * groceryCartController.groceryCart[index].qty}',
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          color: kPrimaryColor,
                                                          fontSize: 16,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 10,
                                                      ),
                                                      Spacer(),
                                                      SizedBox(
                                                        width: 10,
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      )),
                                ),
                              );
                            }),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Divider(
                    thickness: 5,
                    color: Color(0xFFF6F6F6),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Form(
                      key: formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'User Details'.tr,
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          InputFormWidget(
                            fieldController: nameController,
                            labelText: 'Name',
                            hintText:
                                prefs.getString('name').toString() == 'null'
                                    ? 'Enter Your Name'
                                    : prefs.getString('name'),
                            keyType: TextInputType.name,
                            validation: (value) {
                              if (prefs.getString('name').toString() ==
                                      'null' &&
                                  value.toString().isEmpty) {
                                return 'Please Enter Your Name!';
                              }
                            },
                          ),
                          InputFormWidget(
                            fieldController: phoneController,
                            labelText: 'Phone',
                            hintText:
                                prefs.getString('phone').toString() == 'null'
                                    ? 'Enter Your Phone No.'
                                    : prefs.getString('phone'),
                            keyType: TextInputType.number,
                            validation: (value) {
                              if (prefs.getString('phone').toString() ==
                                      'null' &&
                                  value.toString().isEmpty) {
                                return 'Please Enter Phone No.!';
                              }
                            },
                          ),
                          _buildDistrictDropdown(),
                          SizedBox(height: 10),
                          _district != null
                              ? _buildAreaDropdown()
                              : Container(),
                          SizedBox(height: 10),
                          InputFormWidget(
                            fieldController: addressController,
                            labelText: 'Delivery Location',
                            hintText: 'Road no: , House no: ',
                            keyType: TextInputType.streetAddress,
                            validation: (value) {
                              if (value.toString().isEmpty) {
                                return 'Please Enter Your Address!';
                              }
                            },
                          ),
                          // InputFormWidget(
                          //   isEditable: false,
                          //   labelText: 'Shop Address',
                          //   hintText: prefs.getString('address'),
                          //   keyType: TextInputType.name,
                          // ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Divider(
                    thickness: 5,
                    color: Color(0xFFF6F6F6),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Text(
                      'Preferred Delivery Timings'.tr,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                    ),
                  ),
                  DatePicker(
                    DateTime.now(),
                    height: 90,
                    daysCount: 7,
                    initialSelectedDate: DateTime.now(),
                    selectionColor: kPrimaryColor,
                    selectedTextColor: kWhiteColor,
                    onDateChange: (date) {
                      // New date selected
                      if (date.day == DateTime.now().day) {
                        setState(() {
                          isToday = true;
                          _selectedValue =
                              DateFormat('yyyy-MM-dd').format(date);
                          selectedTime = null;
                        });
                      } else {
                        setState(() {
                          isToday = false;
                          _selectedValue =
                              DateFormat('yyyy-MM-dd').format(date);
                          selectedTime = null;
                        });
                      }
                      print(_selectedValue.toString());
                    },
                  ),
                  SizedBox(height: 5),
                  _buildTimeSelectorDropDown(),
                  Divider(
                    thickness: 5,
                    color: Color(0xFFF6F6F6),
                  ),
                  _buildPriceDetailsWidget(),
                  // Divider(
                  //   thickness: 5,
                  //   color: Color(0xFFF6F6F6),
                  // ),
                  // PromoWidget(
                  //   title: 'ADD PROMO'.tr,
                  //   icon: Icons.pages_rounded,
                  // ),
                  Divider(
                    thickness: 5,
                    color: Color(0xFFF6F6F6),
                  ),
                  PromoWidget(
                    title: 'ADD COUPON'.tr,
                    icon: Icons.verified_rounded,
                    onPressed: () {
                      CustomAlertDialog.addCouponDialog(
                        context: context,
                        controller: couponTextController,
                        couponMethod: _couponMethod,
                      );
                    },
                  ),
                  Divider(
                    thickness: 5,
                    color: Color(0xFFF6F6F6),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Payment Methods'.tr,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600),
                        ),
                        RadioListTile(
                          activeColor: kPrimaryColor,
                          title: Text('Cash On Delivery'.tr),
                          value: 1,
                          groupValue: selected,
                          onChanged: (value) {
                            setState(() {
                              selected = value;
                              deliveryCharge = prefs.getInt('delivery_charge');
                            });
                          },
                        ),
                        // RadioListTile(
                        //   activeColor: kPrimaryColor,
                        //   title: Text('Shop Pickup'.tr),
                        //   groupValue: selected,
                        //   value: 2,
                        //   onChanged: (value) {
                        //     setState(() {
                        //       selected = value;
                        //       deliveryCharge = 0;
                        //     });
                        //   },
                        // ),
                        RadioListTile(
                          activeColor: kPrimaryColor,
                          title: Text('Online Payment'.tr),
                          value: 3,
                          groupValue: selected,
                          onChanged: (value) {
                            setState(() {
                              selected = value;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.all(5),
              width: SizeConfig.screenWidth,
              child: DefaultBtn(
                title: 'Confirm Order'.tr,
                onPress: () async {
                  if (formKey.currentState.validate()) {
                    if (selected == 1) {
                      completeOrder(payType: 'Cash On Delivery');
                    } else if (selected == 2) {
                      completeOrder(payType: 'Pickup Delivery');
                    } else if (selected == 3) {
                      sslCommercePayment();
                    }
                  } else {
                    kSnackBarWithDuration(
                      msg: 'Please complete all details!',
                      bgColor: kErrorColor,
                    );
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDistrictDropdown() {
    return Obx(
      () {
        if (locationController.isLoading.value) {
          return Center(child: CircularProgressIndicator());
        } else if (locationController.districtList.data.isEmpty) {
          return noData(
            title: 'No District is available!',
            refresh: locationController.fetchDistrict,
          );
        } else {
          return Container(
              width: MediaQuery.of(context).size.width,
              child: locationController.districtList.data != null
                  ? DropdownSearch<District>(
                      popupBarrierColor: Colors.transparent,
                      popupTitle: Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(10),
                        alignment: Alignment.center,
                        child: Text(
                          'Select District'.tr.toString(),
                          style: TextStyle(
                              color: Colors.green,
                              fontSize: 16,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                      dropdownSearchDecoration: InputDecoration(
                        hintText: 'Select District'.tr.toString(),
                        prefixIcon: Icon(Icons.search),
                        border: OutlineInputBorder(
                            borderSide: new BorderSide(color: Colors.teal),
                        ),
                      ),
                      showSearchBox: true,
                      mode: Mode.BOTTOM_SHEET,
                      showSelectedItems: false,
                      items: locationController.districtList.data,
                      itemAsString: (District u) => u.name,
                      onChanged: (value) {
                        setState(() {
                          _district = value;
                          _area = null;
                          locationController.fetchArea(_district.id.toString());
                          FocusScope.of(context).unfocus();
                        });
                      },
                      selectedItem: _district)
                  : Container());
        }
      },
    );
  }

  Widget _buildAreaDropdown() {
    return Obx(
      () {
        if (locationController.isLoading.value) {
          return Container();
        } else if (locationController.areaList.data.isEmpty) {
          return noData(
            title: 'No Area is available!',
            refresh: () {
              locationController.fetchArea(_district.id.toString());
            },
          );
        } else {
          return Container(
              width: MediaQuery.of(context).size.width,
              child: locationController.areaList.data != null
                  ? DropdownSearch<Area>(
                      popupBarrierColor: Colors.transparent,
                      popupTitle: Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(10),
                        alignment: Alignment.center,
                        child: Text(
                          'Select Area'.tr.toString(),
                          style: TextStyle(
                              color: Colors.green,
                              fontSize: 16,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                      dropdownSearchDecoration: InputDecoration(
                        hintText: 'Select Area'.tr.toString(),
                        prefixIcon: Icon(Icons.search),
                        border: OutlineInputBorder(
                            borderSide: new BorderSide(color: Colors.teal)),
                      ),
                      showSearchBox: true,
                      mode: Mode.BOTTOM_SHEET,
                      showSelectedItems: false,
                      items: locationController.areaList.data,
                      itemAsString: (Area u) => u.areaName,
                      onChanged: (value) async {
                        await locationController.fetchDeliveryCharge(
                          value.id.toString(),
                          prefs.getInt('total').toString(),
                        );

                        setState(() {
                          _area = value;
                          deliveryCharge =
                              locationController.deliveryCharge.value;
                          FocusScope.of(context).unfocus();
                        });
                      },
                      selectedItem: _area,
                    )
                  : Container());
        }
      },
    );
  }

  Widget _buildTimeSelectorDropDown() {
    return Obx(
      () {
        if (timeRangeController.isLoading.isTrue) {
          return Center(child: CircularProgressIndicator());
        } else if (timeRangeController.timeRangeToday.data.isEmpty) {
          return noData(
            title: 'No Time-rang is available!',
            refresh: (){
              timeRangeController..fetchNextDayTime()..fetchTodayTime();
            }
          );
        } else {
          return Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 5,
            ),
            child: DropdownSearch<String>(
              popupBarrierColor: Colors.transparent,
              popupTitle: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(10),
                alignment: Alignment.center,
                child: Text(
                  'Select Preferred Time'.tr.toString(),
                  style: TextStyle(
                      color: Colors.green,
                      fontSize: 16,
                      fontWeight: FontWeight.w700),
                ),
              ),
              dropdownSearchDecoration: InputDecoration(
                hintText: 'Select Preferred Time'.tr.toString(),
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                    borderSide: new BorderSide(color: Colors.teal)),
              ),
              showSearchBox: true,
              mode: Mode.BOTTOM_SHEET,
              showSelectedItems: false,
              items: isToday == true
                  ? timeRangeController.timeRangeToday.data ?? []
                  : timeRangeController.timeRangeNextDay.data ?? [],
              onChanged: (value) {
                setState(
                  () {
                    selectedTime = value;
                    print(selectedTime);
                  },
                );
              },
              selectedItem: selectedTime,
            ),
          );
        }
      },
    );
  }

  Widget noData({String title, Function refresh}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          title,
        ),
        const SizedBox(width: 12),
        TextButton(onPressed: refresh, child: Text('Refresh')),
      ],
    );
  }

  /// these widget from dropdown_search: ^5.0.5
  /// ignore now
  // Widget buildDistrictDropdown() {
  //   return Obx(
  //     () {
  //       if (locationController.isLoading.value) {
  //         return Center(child: CircularProgressIndicator());
  //       } else if (locationController.districtList.data.isEmpty) {
  //         return Container();
  //       } else {
  //         return Container(
  //           //height: MediaQuery.of(context).size.height*0.09,
  //           width: MediaQuery.of(context).size.width,
  //           height: MediaQuery.of(context).size.height * 0.08,
  //           decoration: BoxDecoration(
  //             color: Colors.transparent,
  //             border: Border.all(color: Colors.grey, width: 0.9),
  //             borderRadius: BorderRadius.circular(5),
  //           ),
  //           //height: 50,
  //           child: Row(
  //             children: <Widget>[
  //               SizedBox(
  //                 width: 10,
  //               ),
  //               locationController.districtList.data != null
  //                   ? Expanded(
  //                       child: DropdownButton<District>(
  //                         value: _district,
  //                         isExpanded: true,
  //                         underline: Container(),
  //                         hint: Text('Select District'.tr.toString()),
  //                         items: locationController.districtList.data.map((e) {
  //                           return DropdownMenuItem<District>(
  //                             child: Text(e.name),
  //                             value: e,
  //                           );
  //                         }).toList(),
  //                         onChanged: (value) {
  //                           setState(() {
  //                             _district = value;
  //                             _area = null;
  //                             locationController
  //                                 .fetchArea(_district.id.toString());
  //                             FocusScope.of(context).unfocus();
  //                           });
  //                         },
  //                       ),
  //                     )
  //                   : Container(),
  //             ],
  //           ),
  //         );
  //       }
  //     },
  //   );
  // }
  //
  // Widget buildAreaDropdown() {
  //   return Obx(() {
  //     if (locationController.isLoading.value) {
  //       return Container();
  //     } else if (locationController.areaList.data.isEmpty) {
  //       return Container();
  //     } else {
  //       return Container(
  //           width: MediaQuery.of(context).size.width,
  //           height: MediaQuery.of(context).size.height * 0.08,
  //           decoration: BoxDecoration(
  //             color: Colors.transparent,
  //             border: Border.all(color: Colors.grey, width: 0.9),
  //             borderRadius: BorderRadius.circular(5),
  //           ),
  //           //height: 50,
  //           child: Row(
  //             children: <Widget>[
  //               SizedBox(
  //                 width: 10,
  //               ),
  //               locationController.areaList.data != null
  //                   ? Expanded(
  //                       child: DropdownButton<Area>(
  //                         value: _area,
  //                         isExpanded: true,
  //                         underline: Container(),
  //                         hint: Text('Select Area'.tr.toString()),
  //                         items: locationController.areaList.data.map((e) {
  //                           return DropdownMenuItem<Area>(
  //                             child: Text(e.areaName),
  //                             value: e,
  //                           );
  //                         }).toList(),
  //                         onChanged: (value) async{
  //                           await locationController.fetchDeliveryCharge(
  //                             value.id.toString(),
  //                             prefs.getInt('total').toString(),
  //                           );
  //
  //                           setState(() {
  //                             _area = value;
  //                             deliveryCharge = locationController.deliveryCharge.value;
  //                             FocusScope.of(context).unfocus();
  //                           });
  //                         },
  //                       ),
  //                     )
  //                   : Container(),
  //             ],
  //           ));
  //     }
  //   });
  // }

  Widget _buildPriceDetailsWidget() {
    return Container(
      padding: EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Price Details'.tr,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
          ),
          SizedBox(
            height: 15,
          ),
          RowWidget(
            title: 'Product Price'.tr,
            amount: prefs.getInt('total').toString(),
          ),
          SizedBox(
            height: 10,
          ),
          Obx(
            () => RowWidget(
              title: 'Delivery Charge'.tr,
              amount: locationController.deliveryCharge.value.toString(),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          RowWidget(
            title: 'Discount'.tr,
            amount: discount.toString(),
            amountStyle: TextStyle(color: Colors.red),
          ),
          SizedBox(
            height: 10,
          ),
          couponDiscount != 0
              ? RowWidget(
                  title: 'Coupon Discount'.tr,
                  amount: couponDiscount.toString(),
                  amountStyle: TextStyle(color: Colors.red),
                )
              : Container(),
          Divider(),
          RowWidget(
            title: 'Total'.tr,
            amount: _totalAmount.toString(),
            titleStyle: TextStyle(fontWeight: FontWeight.w600),
            amountStyle: TextStyle(fontWeight: FontWeight.w600),
          )
        ],
      ),
    );
  }

  void _couponMethod() async {
    try {
      couponDiscount = 0;
      RpCouponModel couponModel;
      var responseBody =
          await NetworkServices().fetchCoupon(couponTextController.text);
      if (responseBody != null) {
        couponModel = responseBody;
      }

      setState(() {
        if (couponModel != null) {
          if (couponModel.coupon.type == 1) {
            couponDiscount =
                (_totalAmount * couponModel.coupon.discountAmount) / 100;
          } else {
            couponDiscount = couponModel.coupon.discountTaka;
          }
        }
      });
    } catch (e) {
      print(e);
      kSnackBarWithDuration(
        msg: e,
        bgColor: kErrorColor,
      );
    }
  }

  Future<void> sslCommercePayment() async {
    Sslcommerz sslCommerce = Sslcommerz(
      initializer: SSLCommerzInitialization(
        multi_card_name: formData['multicard'],
        currency: SSLCurrencyType.BDT,
        product_category: "Products",

        ///SSL SandBox ///
        // ipn_url: "www.ipnurl.com",
        // sdkType: SSLCSdkType.TESTBOX,
        // store_id: 'testi6119f4717446f',
        // store_passwd: 'testi6119f4717446f@ssl',
        //
        ///SSL SandBox End///

        ///SSL LIVE ///
        ipn_url: "https://familytrust.com.bd/ipn",
        sdkType: SSLCSdkType.LIVE,
        store_id: 'familytrustshoplive',
        store_passwd: '611B8BFD7581488684',

        ///SSL LIVE End///
        total_amount: _totalAmount,
        tran_id: tranId.toString(),
      ),
    );
    sslCommerce
        .addEMITransactionInitializer(
          sslcemiTransactionInitializer: SSLCEMITransactionInitializer(
            emi_options: 1,
            emi_max_list_options: 3,
            emi_selected_inst: 2,
          ),
        )
        .addShipmentInfoInitializer(
          sslcShipmentInfoInitializer: SSLCShipmentInfoInitializer(
            shipmentMethod: "yes",
            numOfItems: groceryCartController.groceryCart.length,
            shipmentDetails: ShipmentDetails(
              shipAddress1: addressController.text,
              shipCity: prefs.getString('address'),
              shipCountry: 'Bangladesh',
              shipName: 'Ship name 1',
              shipPostCode: '1100',
            ),
          ),
        )
        .addCustomerInfoInitializer(
          customerInfoInitializer: SSLCCustomerInfoInitializer(
            customerName: nameController.text,
            customerEmail: '',
            customerAddress1: addressController.text,
            customerState: '',
            customerCity: '',
            customerPostCode: '1100',
            customerCountry: 'Bangladesh',
            customerPhone: prefs.getString('phone'),
          ),
        )
        .addProductInitializer(
          sslcProductInitializer: SSLCProductInitializer(
            productName: "Gadgets",
            productCategory: "Widgets",
            general: General(
              general: "General Purpose",
              productProfile: "Product Profile",
            ),
          ),
        )
        .addAdditionalInitializer(
          sslcAdditionalInitializer: SSLCAdditionalInitializer(
            valueA: "app",
            valueB: "value b",
            valueC: "value c",
            valueD: "value d",
          ),
        );
    var result = await sslCommerce.payNow();

    log('ssl Result ====>' + result.toString());
    if (result is PlatformException) {
      print("the response is: " + result.status);
      Get.snackbar('Transaction failed!', '', colorText: kWhiteColor);
      // Navigator.pushNamed(context, OrderDetailsToPayment.routeName);
    } else {
      SSLCTransactionInfoModel model = result;
      print("ssL json" + model.toJson().toString());
      if (model.aPIConnect == 'DONE' && model.status == 'VALID') {
        Get.snackbar('Payment success', '', colorText: kWhiteColor);
        completeOrder(payType: 'SSL Commerz');
      } else {
        Get.snackbar('Payment Failed', '', colorText: kWhiteColor);
      }
    }
  }

  void completeOrder({String payType}) async {
    FocusScope.of(context).unfocus();
    if (_area == null) {
      kSnackBarWithDuration(
        msg: 'Please Select Your Area!',
        bgColor: kErrorColor,
      );
    } else {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (_) => CustomLoader());
      List product = [];
      for (int i = 0; i < groceryCartController.groceryCart.length; i++) {
        product.add({
          "product_id": groceryCartController.groceryCart[i].id,
          "price": groceryCartController.groceryCart[i].price,
          "qty": groceryCartController.groceryCart[i].qty,
        });
      }
      print(product);
      var body = {
        "orderid": tranId,
        "store_id": prefs.getString('store_id'),
        "location": prefs.getString('address') ?? '',
        "area_id": _area.id,
        "name": nameController.text.length == 0
            ? prefs.getString('name')
            : nameController.text,
        "phone": phoneController.text.isNotEmpty
            ? phoneController.text
            : prefs.getString('phone'),
        "address": addressController.text ?? '',
        "sub_total": _totalAmount.toString(),
        "payment_type": payType,
        "subtotal": prefs.getInt('total'),
        "shipping_charge": deliveryCharge,
        "discount": discount + couponDiscount,
        "final_total": _totalAmount.toString(),
        "order_date": _selectedValue,
        "order_time": selectedTime,
        "products": product,
      };
      print('Request Body: $body');

      var p = await NetworkServices().submitOrder(body);
      Map<String, dynamic> js = p;
      if (js.containsKey('error')) {
        Navigator.of(context).pop();
        print(p['error']);
        Get.defaultDialog(
            title: 'Error',
            middleText: p['error'],
            onConfirm: () {
              Navigator.pop(context);
            });
      } else {
        Navigator.of(context).pop();
        Get.defaultDialog(
          barrierDismissible: false,
          title: 'Successful',
          middleText: p['message'],
          buttonColor: kPrimaryColor,
          confirmTextColor: kWhiteColor,
          onConfirm: () {
            groceryCartController.emptyCart();
            Get
              ..back()
              ..back();
          },
        );
      }
    }
  }
}
