import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_cart/cart_screen.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class ReviewBarWidget extends StatelessWidget {
  final String total;
  const ReviewBarWidget({
    Key key,
    this.total,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: kSecondaryColor,
      borderRadius: BorderRadius.circular(5),
      child: InkWell(
        onTap: () {
          Get.to(
            () => GroceryCartScreen(),
          );
        },
        child: Container(
          padding: EdgeInsets.all(10),
          width: SizeConfig.screenWidth / 1.1,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Icon(
                Icons.shopping_bag,
                color: kPrimaryColor,
                size: getProportionateScreenWidth(30),
              ),
              Text(
                'Review Order'.tr,
                style: kRegularText.copyWith(
                  fontWeight: FontWeight.w600,
                  color: kPrimaryColor,
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: kPrimaryColor,
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 5,
                ),
                child: Text(
                  '৳$total',
                  style: kRegularText.copyWith(
                    color: kWhiteColor,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
