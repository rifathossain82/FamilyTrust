import 'package:familytrust/controllers/grocery_controllers/grocerycart_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/productdetails_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/search_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/view/screens/grocery_page/search/widgets/review_bar_widget.dart';
import 'package:familytrust/view/screens/grocery_page/sheared/product_details.dart';
import 'package:familytrust/view/widgets/custom_shimmer.dart';
import 'package:familytrust/view/widgets/grocery_product_list_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchPage extends StatefulWidget {
  static const routeName = 'search_page';

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final _scrollController = ScrollController();
  final TextEditingController valueController = TextEditingController();
  final SearchController searchController = Get.find();
  final ProductDetailsController productDetailsController = Get.find();
  final GroceryCartController groceryCartController = Get.find();
  final String lng = LocalizationService().getCurrentLang();
  int page = 1;
  var value = '';
  int total = 0;

  @override
  void initState() {
    if (groceryCartController.isClosed) {
      groceryCartController.onStart();
    }
    for (int i = 0; i < groceryCartController.groceryCart.length; i++) {
      total += groceryCartController.groceryCart[i].price *
          groceryCartController.groceryCart[i].qty;
    }
    getData();
    _scrollController.addListener(addProducts);
    super.initState();
  }

  void getData() async {
    await searchController.fetchSearch(valueController.text ?? '', '1');
  }

  void addProducts() async {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      print('work');
      page++;
      await searchController.fetchSearchAdd(
          valueController.text ?? '', page.toString());
      if (searchController.noData == true) {
        Get.snackbar('No More Products', '', colorText: kWhiteColor);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextFormField(
          autofocus: true,
          style: TextStyle(color: kWhiteColor),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: 'Search Products'.tr,
            hintStyle: TextStyle(color: kWhiteColor),
            suffix: InkWell(
              onTap: () {
                valueController.clear();
                getData();
              },
              child: Icon(
                Icons.close,
                color: kWhiteColor,
              ),
            ),
          ),
          onChanged: (val) {
            getData();
          },
          controller: valueController,
          cursorColor: kWhiteColor,
        ),
      ),
      body: Obx(
        () {
          if (searchController.isLoading.isTrue) {
            return productListShimmer();
          } else {
            if (searchController.searchAll.data.isEmpty) {
              return Center(
                child: Text(
                  'No Products Available'.tr,
                ),
              );
            } else {
              return ListView.builder(
                controller: _scrollController,
                physics: ScrollPhysics(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: searchController.searchAll.data.length,
                itemBuilder: (context, int index) {
                  return InkWell(
                    onTap: () {
                      Get.to(() => ProductDetails(
                            prodId: searchController.searchAll.data[index].id.toString(),
                          ));
                    },
                    child: GroceryProductListCard(
                      lng: lng,
                      title: searchController.searchAll.data[index].productName,
                      bnTitle: searchController.searchAll.data[index].bnProductName,
                      image: searchController.searchAll.data[index].picture,
                      discountPrice: searchController.searchAll.data[index].discountPrice,
                      bnDiscountPrice: searchController.searchAll.data[index].bnDiscountPrice,
                      price: searchController.searchAll.data[index].price,
                      bnPrice: searchController.searchAll.data[index].bnPrice,
                      quantity: searchController.searchAll.data[index].quantity,
                      qty: searchController.searchAll.data[index].qty,
                      discountAmount: searchController.searchAll.data[index].discountAmount,
                      bnDiscountAmount: searchController.searchAll.data[index].bnDiscountAmount,
                      attribute: searchController.searchAll.data[index].attribute,
                      bnAttribute: searchController.searchAll.data[index].bnAttribute,
                      onDecPress: (){
                        setState(() {
                          searchController.searchAll.data[index].qty -= 1;
                          total -= searchController.searchAll.data[index].discountAmount > 0
                              ? searchController.searchAll.data[index].discountPrice
                              : searchController.searchAll.data[index].price;
                        });
                        if (searchController.searchAll.data[index].qty != 0) {
                          groceryCartController.qtyDecrease(
                              searchController.searchAll.data[index].id,
                              searchController.searchAll.data[index].qty,
                              searchController.searchAll.data[index].price);
                        } else {
                          groceryCartController.removeFromCart(searchController.searchAll.data[index].id);
                          Get.snackbar('Remove From Cart', '', colorText: kWhiteColor,);
                        }
                      },
                      onIncPress: (){
                        if (searchController.searchAll.data[index].quantity > searchController.searchAll.data[index].qty) {
                          setState(() {
                            searchController.searchAll.data[index].qty += 1;
                            total += searchController.searchAll.data[index].discountAmount > 0
                                ? searchController.searchAll.data[index].discountPrice
                                : searchController.searchAll.data[index].price;
                          });
                          groceryCartController.addToCart(searchController.searchAll.data[index]);
                        } else {
                          Get.snackbar('Stock Out', '', colorText: kWhiteColor,);
                        }
                      },
                      onIncPress2: (){
                        setState(() {
                          searchController.searchAll.data[index].qty += 1;
                          total += searchController.searchAll.data[index].discountAmount > 0
                              ? searchController.searchAll.data[index].discountPrice
                              : searchController.searchAll.data[index].price;
                        });
                        groceryCartController.addToCart(searchController.searchAll.data[index],
                        );
                        Get.snackbar(
                          'Added to Cart',
                          '',
                          colorText: kWhiteColor,
                        );
                      },
                    )
                  );
                },
              );
            }
          }
        },
      ),
      floatingActionButton: Obx(
        () {
          if (groceryCartController.isLoading.isTrue) {
            return ReviewBarWidget(total: total.toString());
          } else {
            if (groceryCartController.groceryCart.isEmpty) {
              return SizedBox();
            } else {
              return ReviewBarWidget(total: total.toString());
            }
          }
        },
      ),
    );
  }
}
