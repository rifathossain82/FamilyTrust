import 'package:familytrust/helper/helper_method.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/grocery_controllers/grocerycart_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/productslist_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_shimmer.dart';
import 'package:familytrust/view/screens/grocery_page/sheared/product_details.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductListWidget extends StatefulWidget {
  final String id;

  const ProductListWidget({Key key, this.id}) : super(key: key);
  @override
  _ProductListWidgetState createState() => _ProductListWidgetState();
}

class _ProductListWidgetState extends State<ProductListWidget> {
  final String lng = LocalizationService().getCurrentLang();
  final ProductsListController productsListController = Get.find();
  final GroceryCartController groceryCartController = Get.find();

  @override
  void initState() {
    getProductGrid();
    super.initState();
  }

  getProductGrid() async {
    await productsListController.fetchProductList(widget.id.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        if (productsListController.isLoading.isTrue) {
          return productListShimmer();
        } else {
          if (productsListController.productsList.isEmpty) {
            return Center(
              child: Text('No Products Available'.tr),
            );
          } else {
            return ListView.builder(
                physics: ScrollPhysics(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: productsListController.productsList.length,
                itemBuilder: (context, int index) {
                  return InkWell(
                    onTap: () {
                      Get.to(
                        () => ProductDetails(
                          prodId: productsListController
                              .productsList[index].id
                              .toString(),
                        ),
                      );
                    },
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 5,
                        vertical: 4,
                      ),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Stack(
                          children: [
                            Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Container(
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: FadeInImage.assetNetwork(
                                          placeholder:
                                              '${kImageDir}placeholder.png',
                                          image: kImageUrl +
                                              productsListController
                                                  .productsList
                                                  [index]
                                                  .picture,
                                          height: 80,
                                          width: 80,
                                          imageScale: 1,
                                          imageErrorBuilder: (context, url,
                                                  error) =>
                                              Image.asset(
                                                  '${kImageDir}placeholder.png'),
                                        ),
                                      ),
                                      kWidthBox10,
                                      Expanded(
                                        flex: 5,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              lng == 'Bangla'
                                                  ? productsListController
                                                      .productsList
                                                      [index]
                                                      .bnProductName
                                                  : productsListController
                                                      .productsList
                                                      [index]
                                                      .productName,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 15,
                                                height: 1.10,
                                                color: kBlackColor,
                                              ),
                                            ),
                                            kHeightBox10,
                                            Text(
                                              lng == 'Bangla'
                                                  ? productsListController
                                                      .productsList
                                                      [index]
                                                      .bnAttribute
                                                  : productsListController
                                                      .productsList
                                                      [index]
                                                      .attribute,
                                              style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 14,
                                                height: 1.0,
                                                color: kSilverColor,
                                              ),
                                            ),
                                            kHeightBox10,
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text(
                                                        productsListController
                                                                    .productsList
                                                                    [index]
                                                                    .discountPrice ==
                                                                0
                                                            ? (lng == 'Bangla'
                                                                ? '৳${productsListController.productsList[index].bnPrice} TK'
                                                                : '৳${productsListController.productsList[index].price} TK')
                                                            : (lng == 'Bangla'
                                                                ? '${productsListController.productsList[index].bnDiscountPrice} TK'
                                                                : '${productsListController.productsList[index].discountPrice} TK'),
                                                        style: TextStyle(
                                                            color: kBlackColor,
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            fontSize: productsListController
                                                                        .productsList
                                                                        [
                                                                            index]
                                                                        .discountPrice !=
                                                                    0
                                                                ? 13
                                                                : 15,
                                                            height: 1.0),
                                                      ),
                                                      kWidthBox10,
                                                      productsListController
                                                                  .productsList
                                                                  [index]
                                                                  .discountPrice !=
                                                              0
                                                          ? Text(
                                                              '${lng == 'Bangla' ? productsListController.productsList[index].bnPrice : productsListController.productsList[index].price} TK',
                                                              style: TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600,
                                                                  fontSize: 13,
                                                                  height: 1.0,
                                                                  color:
                                                                      kAccentColor,
                                                                  decoration:
                                                                      TextDecoration
                                                                          .lineThrough),
                                                            )
                                                          : SizedBox(),
                                                    ],
                                                  ),
                                                ),
                                                SizedBox(
                                                  child: !(productsListController
                                                              .productsList
                                                              [index]
                                                              .quantity >
                                                          productsListController
                                                              .productsList
                                                              [index]
                                                              .qty)
                                                      ? Container(
                                                          decoration: BoxDecoration(
                                                              color:
                                                                  kWhiteColor,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10),
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color:
                                                                      kAccentColor)),
                                                          child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10,
                                                                    vertical:
                                                                        4),
                                                            child: Text(
                                                              'Stock Out'.tr,
                                                              style: TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize: 12,
                                                                  height: 1.0,
                                                                  color:
                                                                      kAccentColor),
                                                            ),
                                                          ),
                                                        )
                                                      : SizedBox(
                                                          child: productsListController
                                                                      .productsList
                                                                      [
                                                                          index]
                                                                      .qty >
                                                                  0
                                                              ? Container(
                                                                  margin:
                                                                      EdgeInsets
                                                                          .all(
                                                                              2),
                                                                  child: Row(
                                                                    children: [
                                                                      InkWell(
                                                                        onTap:
                                                                            () {
                                                                          setState(
                                                                              () {
                                                                            productsListController.productsList[index].qty -=
                                                                                1;
                                                                          });
                                                                          if (productsListController.productsList[index].qty !=
                                                                              0) {
                                                                            groceryCartController.qtyDecrease(
                                                                                productsListController.productsList[index].id,
                                                                                productsListController.productsList[index].qty,
                                                                                productsListController.productsList[index].price);
                                                                          } else {
                                                                            groceryCartController.removeFromCart(productsListController.productsList[index].id);
                                                                            kSnackBar('Remove from cart');
                                                                          }
                                                                        },
                                                                        child:
                                                                            Container(
                                                                          width:
                                                                              28,
                                                                          height:
                                                                              28,
                                                                          decoration: BoxDecoration(
                                                                              color: kWhiteColor,
                                                                              borderRadius: BorderRadius.circular(5),
                                                                              border: Border.all(width: 1, color: kSecondaryColor)),
                                                                          child:
                                                                              Icon(
                                                                            Icons.remove,
                                                                            color:
                                                                                kBlackColor,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                      SizedBox(
                                                                        width:
                                                                            10,
                                                                      ),
                                                                      Text(
                                                                        productsListController
                                                                            .productsList
                                                                            [index]
                                                                            .qty
                                                                            .toString(),
                                                                        style: TextStyle(
                                                                            fontWeight:
                                                                                FontWeight.w500,
                                                                            fontSize: 18),
                                                                      ),
                                                                      SizedBox(
                                                                        width:
                                                                            10,
                                                                      ),
                                                                      InkWell(
                                                                        onTap:
                                                                            () {
                                                                          if (productsListController.productsList[index].quantity >
                                                                              productsListController.productsList[index].qty) {
                                                                            setState(() {
                                                                              productsListController.productsList[index].qty += 1;
                                                                            });
                                                                            groceryCartController.addToCart(productsListController.productsList[index]);
                                                                          } else {
                                                                            kSnackBar('Stock Out');
                                                                          }
                                                                        },
                                                                        child:
                                                                            Container(
                                                                          width:
                                                                              28,
                                                                          height:
                                                                              28,
                                                                          decoration: BoxDecoration(
                                                                              color: kWhiteColor,
                                                                              borderRadius: BorderRadius.circular(5),
                                                                              border: Border.all(width: 1, color: kSecondaryColor)),
                                                                          child:
                                                                              Icon(
                                                                            Icons.add,
                                                                            color:
                                                                                kBlackColor,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                )
                                                              : Container(
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              10),
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color:
                                                                        kSecondaryColor,
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            17),
                                                                  ),
                                                                  child:
                                                                      InkWell(
                                                                    onTap: () {
                                                                      setState(
                                                                          () {
                                                                        productsListController
                                                                            .productsList
                                                                            [index]
                                                                            .qty += 1;
                                                                      });
                                                                      groceryCartController.addToCart(productsListController
                                                                          .productsList
                                                                          [index]);

                                                                      kSnackBar(
                                                                          'Added to Cart');
                                                                    },
                                                                    child: Icon(
                                                                      Icons.add,
                                                                      color:
                                                                          kWhiteColor,
                                                                    ),
                                                                  ),
                                                                ),
                                                        ),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                )),
                            productsListController.productsList[index]
                                        .discountPrice !=
                                    0
                                ? Positioned(
                                    top: 0,
                                    right: 0,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: kAccentColor,
                                        borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(12),
                                            bottomLeft: Radius.circular(12)),
                                      ),
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8, vertical: 4),
                                      child: Center(
                                        child: Text(
                                          '-৳${lng == 'Bangla' ? productsListController.productsList[index].bnDiscountAmount : productsListController.productsList[index].discountAmount} ' +
                                              'OFF'.tr,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12,
                                              height: 1.0,
                                              color: kWhiteColor),
                                        ),
                                      ),
                                    ),
                                  )
                                : Positioned(
                                    top: 0,
                                    right: 0,
                                    child: Container(),
                                  )
                          ],
                        ),
                      ),
                    ),
                  );
                });
          }
        }
      },
    );
  }
}


