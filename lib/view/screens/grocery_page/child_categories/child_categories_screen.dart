import 'package:familytrust/services/extensions/build_context_extension.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/grocery_controllers/childcategories_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/grocerycart_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/productslist_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/widgets/custom_shimmer.dart';
import 'package:familytrust/view/screens/grocery_page/child_categories/widgets/product_grid_widget.dart';
import 'package:familytrust/view/screens/grocery_page/child_categories/widgets/product_list_widget.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_cart/cart_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChildCatScreen extends StatefulWidget {
  static const routeName = 'child_categories_screen';
  final String catId;
  final String catName;
  const ChildCatScreen({
    Key key,
    this.catId,
    this.catName,
  }) : super(key: key);

  @override
  _ChildCatScreenState createState() => _ChildCatScreenState();
}

class _ChildCatScreenState extends State<ChildCatScreen> {
  final ChildCategoriesController childCategoriesController = Get.find();
  final ProductsListController productsListController = Get.find();
  final GroceryCartController groceryCartController = Get.find();
  String lng = LocalizationService().getCurrentLang();
  bool activeGrid = false;
  int indexNo = 0;

  @override
  void initState() {
    getChildCat();
    super.initState();
  }

  getChildCat() async {
    await childCategoriesController.fetchChildCategories(widget.catId.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.catName),
        actions: [
          Stack(
            children: [
              IconButton(
                icon: Icon(Icons.add_shopping_cart),
                onPressed: () {
                  Get.to(() => GroceryCartScreen());
                },
              ),
              Positioned(
                top: 0,
                right: 8,
                child: Container(
                  padding: EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      color: kSecondaryColor,
                      borderRadius: BorderRadius.circular(10)),
                  child: Obx(
                    () {
                      if (groceryCartController.isLoading.isTrue) {
                        return Center(
                          child: CustomLoader(),
                        );
                      } else {
                        return Text(
                          groceryCartController.groceryCart.length.toString(),
                          style: TextStyle(
                              color: kPrimaryColor,
                              fontWeight: FontWeight.w800),
                        );
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      body: Obx(
        () {
          if (childCategoriesController.isLoading.isTrue) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: childCatShimmer(),
                ),
                Expanded(
                  flex: 11,
                  child: productListShimmer(),
                )
              ],
            );
          } else {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                      width: double.infinity,
                      child: Container(
                        height: 50,
                        margin: EdgeInsets.symmetric(
                          vertical: 5,
                          horizontal: 0
                        ),
                        padding: EdgeInsets.symmetric(vertical: 3),
                        child: ListView.separated(
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            itemCount: childCategoriesController
                                .childCategories.data.length,
                            itemBuilder: (context, int index) {
                              return InkWell(
                                onTap: () {
                                  setState(() {
                                    indexNo = index;
                                    productsListController.fetchProductList(
                                        childCategoriesController
                                            .childCategories
                                            .data[indexNo]
                                            .id
                                            .toString());
                                  });
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: indexNo == index
                                        ? kSecondaryColor
                                        : kWhiteColor,
                                    border: Border.all(
                                      width: 0.5,
                                      color: indexNo == index
                                          ? kSecondaryColor
                                          : const Color(0xFFA8A8A8),
                                    ),
                                    borderRadius: BorderRadius.circular(5)
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Text(
                                      lng == 'Bangla'
                                          ? childCategoriesController
                                          .childCategories
                                          .data[index]
                                          .bnCategoryName
                                          : childCategoriesController
                                          .childCategories
                                          .data[index]
                                          .categoryName
                                          .toString(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ),
                              );
                            },
                          separatorBuilder: (context, index) => const SizedBox(width: 8,),
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 5,
                        ),
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Row(
                              children: [
                                Text(
                                  'Product View Type',
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 15,
                                    height: 1.10,
                                    color: kBlackColor,
                                  ),
                                ),
                                const Spacer(),
                                InkWell(
                                  onTap: () {
                                    setState(() {
                                      activeGrid = false;
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color:
                                      activeGrid == false ? kSecondaryColor : kWhiteColor,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Icon(
                                        Icons.list,
                                        color: kPrimaryColor,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                InkWell(
                                  onTap: () {
                                    setState(() {
                                      activeGrid = true;
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color:
                                      activeGrid == true ? kSecondaryColor : kWhiteColor,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Icon(
                                        Icons.grid_on,
                                        color: kPrimaryColor,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        activeGrid == false
                            ? ProductListWidget(
                                id: childCategoriesController
                                    .childCategories.data[indexNo].id
                                    .toString(),
                              )
                            : ProductGridWidget(
                                id: childCategoriesController
                                    .childCategories.data[indexNo].id
                                    .toString(),
                              )
                      ],
                    ),
                  ),
                ),
              ],
            );
          }
        },
      ),
    );
  }
}
