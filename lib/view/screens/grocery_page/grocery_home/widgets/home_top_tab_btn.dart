import 'package:flutter/material.dart';
import '../../../../../utils/constants.dart';

class HomeTopTabButton extends StatelessWidget {
  final String image, title;
  final isSelect;

  const HomeTopTabButton({
    Key key,
    @required this.title,
    @required this.image,
    this.isSelect = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 43,
      decoration: BoxDecoration(
        color: isSelect ? kSecondaryColor : kWhiteColor,
        borderRadius: BorderRadius.circular(5),
        border: Border.all(width: isSelect ? 0 : 0.5, color: isSelect ? Colors.transparent : kSecondaryColor)
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            decoration: BoxDecoration(
              color: kWhiteColor,
              borderRadius: BorderRadius.circular(5),
            ),
            padding: EdgeInsets.all(8),
            child: Image.asset(
              image,
              fit: BoxFit.cover,
            ),
          ),
          kWidthBox15,
          Text(
            title,
            style: TextStyle(
                fontSize: 12,
                color: kBlackColor,
                fontWeight: FontWeight.w600,
                height: 1),
          ),
        ],
      ),
    );
  }
}
