import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../../utils/constants.dart';
import '../../../../widgets/custom_loader.dart';

class HomeCategoryView extends StatelessWidget {
  final String image, title;
  final int index;

  const HomeCategoryView({
    Key key,
    @required this.title,
    @required this.image,
    this.index = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90,
      decoration: BoxDecoration(
        color: index == 7
            ? const Color(0xFFFFFFFF)
            : index == 0 || index == 5
            ? const Color(0xFFCFFCFF)
            : index == 1 || index == 4
            ? const Color(0xFFDBDCFF)
            : index == 2
            ? const Color.fromRGBO(83, 177, 117, 0.1)
            : index == 3 || index == 6
            ? const Color.fromRGBO(247, 165, 147, 0.25)
            : const Color(0xFFDBDCFF),
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          width: 0.5,
          color: index == 7
              ? const Color(0xFFFE9901)
              : index == 0 || index == 5
                  ? const Color(0xFF65F6FF)
                  : index == 1 || index == 4
                      ? const Color(0xFF9B9FFF)
                      : index == 2
                          ? const Color(0xFF53B175).withOpacity(0.50)
                          : index == 3 || index == 6
                              ? const Color(0xFFF7A593).withOpacity(0.60)
                              : const Color(0xFFDBDCFF),
        ),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 1),
            blurRadius: 2,
            spreadRadius: 0,
            color: kWhiteColor.withOpacity(0.15),
          ),
        ],
      ),
      child: index == 7
          ? Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  'View All Category',
                  overflow: TextOverflow.ellipsis,
                  style: GoogleFonts.quicksand(
                    fontWeight: FontWeight.w700,
                    fontSize: 13,
                    color: kSecondaryColor,
                  ),
                ),
              ),
            )
          : Stack(
              alignment: Alignment.bottomRight,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.3,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Text(
                        title,
                        style: GoogleFonts.quicksand(
                          fontWeight: FontWeight.w700,
                          fontSize: 13,
                          color: kBlackColor,
                        ),
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: SizedBox(
                    width: 70,
                    height: 70,
                    child: ClipRRect(
                      clipBehavior: Clip.hardEdge,
                      borderRadius: BorderRadius.circular(5),
                      child: CachedNetworkImage(
                        imageUrl: '$kImageUrl$image',
                        fit: BoxFit.cover,
                        colorBlendMode: BlendMode.darken,
                        placeholder: (context, url) => CustomLoader(),
                        errorWidget: (context, url, error) => Icon(
                          Icons.error,
                          color: Colors.red,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}
