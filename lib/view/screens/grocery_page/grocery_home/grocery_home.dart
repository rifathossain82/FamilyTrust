import 'dart:developer';

import 'package:familytrust/controllers/grocery_controllers/categories_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/grocerypopular_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/internet_checker_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/special_offer_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shoppopular_controller.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:familytrust/view/screens/dashboard/dashboard_screen.dart';
import 'package:flutter_svg/svg.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_place_picker_mb/google_maps_place_picker.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/grocery_controllers/grocerycart_controller.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/view/widgets/custom_drawer.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_cart/cart_screen.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/body.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'components/header_content.dart';

class GroceryHome extends StatefulWidget {
  static final kInitialPosition = LatLng(23.93033506606703, 90.71022715220114);
  static const routeName = 'grocery_home';

  const GroceryHome({Key key}) : super(key: key);

  @override
  _GroceryHomeState createState() => _GroceryHomeState();
}

class _GroceryHomeState extends State<GroceryHome> {
  final GroceryCartController groceryCartController = Get.find();
  final CategoriesController categoriesController = Get.put(CategoriesController());
  final groceryPopularController = Get.put(GroceryPopularController());
  final specialOfferController = Get.put(SpecialOfferController());
  final shopPopularController = Get.put(ShopPopularController());
  var _locationMessage = "";
  var addresses;
  var lanLong;
  var first;
  PickResult selectedPlace;

  @override
  void initState() {
    getPermission();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      groceryCartController.onInit();
    });

    print(prefs.getString('uid'));
    print(prefs.getString('email'));
    print(prefs.getString('address'));
    print(prefs.getString('name'));
    print(prefs.getString('token'));
    print(prefs.getString('store_id'));
    // shopPopularController.fetchShopPopular();
    // if (prefs.containsKey('store_id')) {
    //   groceryPopularController.fetchGroceryPopular();
    //   specialOfferController.fetchGrocerySpecialOffer();
    // }
    super.initState();
  }

  Future<void> getPermission() async {
    final status = await Permission.locationWhenInUse.request();
    if (status == PermissionStatus.granted) {
      _getCurrentLocation();
      log('Permission Granted');
    } else if (status == PermissionStatus.denied) {
      log('Permission denied');
    } else if (status == PermissionStatus.permanentlyDenied) {
      log('Permission Permanently Denied');
      await openAppSettings();
    }
  }

  void _getCurrentLocation() async {
    final position = await Geolocator.getCurrentPosition();
    setState(() {
      lanLong = position;
    });
    List<Placemark> newPlace =
        await placemarkFromCoordinates(position.latitude, position.longitude);
    // this is all you need
    Placemark placeMark = newPlace[0];
    String streetAddress = placeMark.street;
    String locality = placeMark.locality;
    String postalCode = placeMark.postalCode;
    String country = placeMark.country;
    String countryCOde = placeMark.isoCountryCode;
    String city = placeMark.subAdministrativeArea;
    String address = "$streetAddress, $locality $postalCode, $country";
    log(address.toString());
    log(city.toString());
    log(countryCOde.toString());
    log(country.toString());
    setState(() {
      _locationMessage = address;
      prefs.setString('address', _locationMessage);
    });

    var p = await NetworkServices().fetchShop(context);
    Map<String, dynamic> js = p;
    if (js.containsKey('error')) {
      Get.back();
      print(p['error']);
      Get.defaultDialog(
          barrierDismissible: false,
          title: 'No Shop Available'.tr,
          middleText: 'Change your location to see the available shops'.tr,
          onConfirm: () {
            Get.back();
          },
          confirmTextColor: kWhiteColor,
          buttonColor: kPrimaryColor);
    } else {
      Get.back();
      store(p, context);
    }
  }

  @override
  Widget build(BuildContext context) {
    final _scaffoldKey = GlobalKey<ScaffoldState>();
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xFFFFFFFF),
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 5,
        titleSpacing: 0,
        title: HeaderContent(),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(30),
          child: InkWell(
            onTap: () async {
              Get.to(() => PlacePicker(
                    apiKey: 'AIzaSyDJu-xdHXPpyEE-MhyXUdwJIXRBO4pY2b8',
                    initialPosition: GroceryHome.kInitialPosition,
                    useCurrentLocation: true,
                    selectInitialPosition: true,
                    // pickArea: CircleArea(
                    //   center: GroceryHome.kInitialPosition,
                    //   radius: 500000,
                    //   fillColor: Colors.lightGreen.withGreen(255).withAlpha(32),
                    //   strokeColor: Colors.lightGreen.withGreen(255).withAlpha(192),
                    //   strokeWidth: 2,
                    // ),

                    //usePlaceDetailSearch: true,
                    onPlacePicked: (result) async {
                      setState(() {
                        selectedPlace = result;
                        _locationMessage = selectedPlace.formattedAddress;
                        prefs.setString('address', _locationMessage);
                        print('Rifat You just select: $_locationMessage');
                      });
                      // get_area(selectedPlace.geometry.location.lat,
                      //     selectedPlace.geometry.location.lng);
                      print('map working!!!!!!!!!!!!');

                      var p = await NetworkServices().fetchShop(context);
                      Map<String, dynamic> js = p;
                      if (js.containsKey('error')) {
                        Get.back();
                        print(p['error']);
                        Get.defaultDialog(
                            barrierDismissible: false,
                            title: 'No Shop Available'.tr,
                            middleText:
                                'Change your location to see the available shops'
                                    .tr,
                            onConfirm: () {
                              _getCurrentLocation();
                              Get..back()..back();
                            },
                            confirmTextColor: kWhiteColor,
                            buttonColor: kPrimaryColor);
                      } else {
                        Get..back()..back();
                        store(p, context);
                      }
                    },
                    forceSearchOnZoomChanged: true,
                    automaticallyImplyAppBarLeading: false,
                  ));
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: size.width * 0.72,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.location_on_outlined,
                        size: 15,
                        color: kWhiteColor,
                      ),
                      Flexible(
                        child: _locationMessage == ''
                            ? (prefs.getString('address') != null
                                ? Text(
                                    prefs.getString('address'),
                                    maxLines: 1,
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: kWhiteColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )
                                : Text(
                                    'Searching...',
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: kWhiteColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ))
                            : Text(
                                _locationMessage.toString(),
                                maxLines: 1,
                                overflow: TextOverflow.clip,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 12,
                                  color: kWhiteColor,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      SvgPicture.asset('$kIconsDir/edit.svg'),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
          ),
        ),
        actions: [
          Stack(
            children: [
              IconButton(
                icon: SvgPicture.asset('$kIconsDir/cart.svg'),
                onPressed: () {
                  Get.to(() => GroceryCartScreen());
                },
              ),
              Positioned(
                top: 0,
                right: 11,
                child: Container(
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    color: kSecondaryColor,
                    shape: BoxShape.circle,
                  ),
                  child: Obx(
                    () {
                      if (groceryCartController.isLoading.isTrue) {
                        return Center(
                          child: CustomLoader(),
                        );
                      } else {
                        return Text(
                          groceryCartController.groceryCart.length.toString(),
                          style: TextStyle(
                              color: kPrimaryColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w600),
                        );
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ],
        leading: IconButton(
          onPressed: () {
            _scaffoldKey.currentState.openDrawer();
          },
          icon: Icon(Icons.menu),
        ),
      ),
      drawer: CustomDrawer(),
      body: RefreshIndicator(
        child: Stack(
          children: [
            ListView(),
            Body(),
          ],
        ),
        color: kPrimaryColor,
        onRefresh: loadData,
      ),
    );
  }

  void store(var mat, BuildContext context) async {
    prefs = await SharedPreferences.getInstance();
    prefs.setString('store_id', mat['data']['store_id'].toString());
    prefs.setInt('delivery_charge', int.parse(mat['data']['delivery_charge']));
    print('store id ' + prefs.getString('store_id'));
    loadData();
  }

  Future<void> loadData() async{
    shopPopularController.fetchShopPopular();
    if (prefs.containsKey('store_id')) {
      categoriesController.fetchCategories();
      groceryPopularController.fetchGroceryPopular();
      specialOfferController.fetchGrocerySpecialOffer();
    }
  }
}
