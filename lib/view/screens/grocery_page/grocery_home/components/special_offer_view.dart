import 'package:familytrust/controllers/grocery_controllers/grocerypopular_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/special_offer_controller.dart';
import 'package:familytrust/services/extensions/build_context_extension.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/offer_productlist_screen.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/show_all_products.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../widgets/home_more_widgets.dart';
import '../../../../widgets/horizontal_grocery_list_view.dart';

class SpecialOfferView extends StatelessWidget {
  final String title;

  SpecialOfferView({
    Key key,
    @required this.title,
  }) : super(key: key);

  final SpecialOfferController specialOfferController =
      Get.find<SpecialOfferController>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: HomeMoreWidgets(
            title: title,
            onPressed: () {
              context.push(
                OfferProductListScreen(
                  title: title,
                ),
              );
            },
          ),
        ),
        Obx(
          () {
            if (specialOfferController.isLoading.value) {
              return Center(child: CircularProgressIndicator());
            } else if (specialOfferController.offerProducts.data.isEmpty) {
              return Center(child: Text('No Products Available!'));
            } else {
              return HorizontalGroceryListView(
                productList: specialOfferController.offerProducts.data,
              );
            }
          },
        )
      ],
    );
  }
}
