import 'package:familytrust/models/grocery/rp_home_category_product_model.dart';
import 'package:familytrust/models/grocery/rpcategories_model.dart';
import 'package:familytrust/services/extensions/build_context_extension.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/productlist_screen.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/show_all_products.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/widgets/horizontal_grocery_list_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../utils/constants.dart';
import '../../../../widgets/home_more_widgets.dart';
import '../../../../widgets/horizontal_grocery_list_child_cat.dart';

class ProductCategoryView extends StatefulWidget {
  final String title;
  final int catId;
  final List<Datum> subCategoryList;

  ProductCategoryView({
    Key key,
    @required this.title,
    @required this.catId,
    @required this.subCategoryList,
  }) : super(key: key);

  @override
  State<ProductCategoryView> createState() => _ProductCategoryViewState();
}

class _ProductCategoryViewState extends State<ProductCategoryView> {
  int selectedId;
  List<CatProduct> products = [];

  @override
  void initState() {
    selectedId = widget.subCategoryList.isNotEmpty? widget.subCategoryList.first.id : 0;
    getData();
    super.initState();
  }

  getData()async{
    products = [];
    var data =
    await fetchGroceryHomeCategoryProduct(selectedId.toString());
    products = data.data;
    if(!mounted){
      return;
    }
    setState(() {});
  }

  Future<RpHomeCategoryProductModel> fetchGroceryHomeCategoryProduct(String catId) async {
    try {
      var products = await NetworkServices().fetchGroceryHomeCategoryProduct(catId);
      if (products != null) {
        return products;
      }
    } catch(e){
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: HomeMoreWidgets(
            title: widget.title,
            onPressed: () {
              context.push(
                ProductListScreen(
                  id: widget.catId.toString(),
                  title: widget.title,
                ),
              );
            },
          ),
        ),
        SizedBox(
          height: getProportionateScreenWidth(90),
          child: ListView.separated(
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemCount: widget.subCategoryList.length,
            padding: EdgeInsets.zero,
            itemBuilder: (context, int index) {
              return GestureDetector(
                onTap: () {
                  selectedId = widget.subCategoryList[index].id;
                  print('ID: $selectedId');
                  getData();
                },
                child: GroceryChildCat(
                  title: LocalizationService().getCurrentLang() == 'Bangla'
                      ? widget.subCategoryList[index].bnCategoryName
                      : widget.subCategoryList[index].categoryName,
                  picture: widget.subCategoryList[index].categoryPicture,
                  isSelected: widget.subCategoryList[index].id == selectedId,
                ),
              );
            },
            separatorBuilder: (context, index) => const SizedBox(width: 15),
          ),
        ),
        kHeightBox15,
        products.isEmpty
            ? noData(title: 'No products found!', refresh: getData)
            : HorizontalGroceryListView(
                productList: products,
              ),
      ],
    );
  }

  Widget noData({String title, Function refresh}) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            title,
          ),
          const SizedBox(width: 12),
          TextButton(onPressed: refresh, child: Text('Refresh')),
        ],
      ),
    );
  }
  //
  // goToShowAllPage() async {
  //   showDialog(
  //       context: context,
  //       barrierDismissible: false,
  //       builder: (_) => CustomLoader());
  //
  //   List<CatProduct> productList = await fetchAllProducts();
  //
  //   /// close loader
  //   Get.back();
  //
  //   context.push(
  //     ShowAllProductScreen(
  //       title: widget.title,
  //       productList: productList,
  //     ),
  //   );
  // }
  //
  // Future<List<CatProduct>> fetchAllProducts()async{
  //   List<CatProduct> allProducts = [];
  //   for(var i in widget.subCategoryList){
  //     try {
  //       var products = await NetworkServices().fetchGroceryHomeCategoryProduct(i.id.toString());
  //       if (products != null) {
  //         allProducts.addAll(products.data);
  //       }
  //     } catch(e){
  //       print(e);
  //     }
  //   }
  //
  //   return allProducts;
  // }
}
