import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:flutter/material.dart';

class CatRowWidget extends StatelessWidget {
  final String image;
  final String titleBD;
  final String titleEN;
  final String discount;
  final String description;
  final Function onPress;

  CatRowWidget({
    this.titleBD,
    this.titleEN,
    this.description,
    this.image,
    this.onPress,
    this.discount,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
            height: getProportionateScreenHeight(120),
            width: SizeConfig.screenWidth / 2.8,
            child: CachedNetworkImage(
              imageUrl: image,
              fit: BoxFit.fill,
              placeholder: (context, url) => CustomLoader(),
              errorWidget: (context, url, error) => Icon(
                Icons.error,
                color: Colors.red,
              ),
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(discount,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: kSuccessColor)),
                SizedBox(
                  height: 10,
                ),
                Text(
                    LocalizationService().getCurrentLang() == 'Bangla'
                        ? titleBD
                        : titleEN,
                    style:
                        TextStyle(fontWeight: FontWeight.w800, fontSize: 14)),
                SizedBox(
                  height: 10,
                ),
                Text(
                  description,
                  maxLines: 3,
                  style: TextStyle(fontSize: 12),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
