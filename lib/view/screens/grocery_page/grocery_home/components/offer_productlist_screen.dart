import 'package:familytrust/controllers/grocery_controllers/grocerycart_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/productslist_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/special_offer_controller.dart';
import 'package:familytrust/helper/helper_method.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/grocery_page/sheared/product_details.dart';
import 'package:familytrust/view/widgets/cart_item_badge.dart';
import 'package:familytrust/view/widgets/custom_shimmer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

class OfferProductListScreen extends StatefulWidget {
  final String title;
  static const routeName = 'offer_products_list_screen';

  OfferProductListScreen({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  _OfferProductListScreenState createState() => _OfferProductListScreenState();
}

class _OfferProductListScreenState extends State<OfferProductListScreen> {
  bool activeGrid = false;
  final String lng = LocalizationService().getCurrentLang();
  final SpecialOfferController specialOfferController = Get.put(SpecialOfferController());
  final GroceryCartController groceryCartController = Get.find();
  ScrollController _scrollController = ScrollController();


  @override
  void initState() {
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      specialOfferController.fetchOfferProductsWithPagination();
    });
    scrollIndicator();
    super.initState();
  }

  @override
  void dispose(){
    specialOfferController.pageNumber.value = 1;
    _scrollController.dispose();
    super.dispose();
  }

  void scrollIndicator() {
    _scrollController.addListener(
          () {
        if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
            !_scrollController.position.outOfRange) {
          print('reach to bottom');
          if(!specialOfferController.loadedCompleted.value){
            ++specialOfferController.pageNumber.value;
            specialOfferController.fetchOfferProductsWithPagination();
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          CartItemBadge(),
        ],
      ),
      body: Column(
        children: [
          const SizedBox(height: 5,),
          Expanded(
            flex: 1,
            child: Card(
              margin: EdgeInsets.zero,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0.0),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 8,
                ),
                child: Row(
                  children: [
                    Text(
                      'Product View Type',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        height: 1.10,
                        color: kBlackColor,
                      ),
                    ),
                    const Spacer(),
                    InkWell(
                      onTap: () {
                        setState(() {
                          activeGrid = false;
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color:
                              activeGrid == false ? kSecondaryColor : kWhiteColor,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.list,
                            color: kPrimaryColor,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          activeGrid = true;
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color:
                              activeGrid == true ? kSecondaryColor : kWhiteColor,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.grid_on,
                            color: kPrimaryColor,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          const SizedBox(height: 5,),
          activeGrid == false
              ? Expanded(
                  flex: 11,
                  child: _buildProductListView(),
                )
              : Expanded(
                  flex: 11,
                  child: _buildProductGridView(),
                )
        ],
      ),
    );
  }

  Widget _buildProductListView(){
    return Obx(
          () {
        if (specialOfferController.isLoading.isTrue) {
          return productListShimmer();
        } else {
          if (specialOfferController.offerProductList.isEmpty) {
            return Center(
              child: Text('No Products Available'.tr),
            );
          } else {
            return ListView.builder(
                controller: _scrollController,
                physics: ScrollPhysics(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: specialOfferController.offerProductList.length+1,
                itemBuilder: (context, int index) {
                  if(index==specialOfferController.offerProductList.length &&
                      !Get.find<SpecialOfferController>().loadedCompleted.value){
                    return Center(child: CircularProgressIndicator());
                  }
                  else if(index==specialOfferController.offerProductList.length &&
                      Get.find<SpecialOfferController>().loadedCompleted.value){
                    return Container();
                  }
                  else{
                    return InkWell(
                      onTap: () {
                        Get.to(
                              () => ProductDetails(
                            prodId: specialOfferController
                                .offerProductList[index].id
                                .toString(),
                          ),
                        );
                      },
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: 5,
                          vertical: 4,
                        ),
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Stack(
                            children: [
                              Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Container(
                                    child: Row(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child:  FadeInImage.assetNetwork(
                                            placeholder: '${kImageDir}placeholder.png',
                                            image: kImageUrl + specialOfferController.offerProductList[index].picture,
                                            height: 80,
                                            width: 80,
                                            imageScale: 1,
                                            imageErrorBuilder: (context, url, error) => Image.asset('${kImageDir}placeholder.png'),
                                          ),
                                        ),
                                        kWidthBox10,
                                        Expanded(
                                          flex: 5,
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                lng == 'Bangla'
                                                    ? specialOfferController
                                                    .offerProductList
                                                [index]
                                                    .bnProductName
                                                    : specialOfferController
                                                    .offerProductList
                                                [index]
                                                    .productName,
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 15,
                                                  height: 1.10,
                                                  color: kBlackColor,
                                                ),
                                              ),
                                              kHeightBox10,
                                              Text(
                                                lng == 'Bangla'
                                                    ? specialOfferController.offerProductList[index].bnAttribute
                                                    : specialOfferController.offerProductList[index].attribute,
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14,
                                                  height: 1.0,
                                                  color: kSilverColor,
                                                ),
                                              ),
                                              kHeightBox10,
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Row(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                      children: [
                                                        Text(
                                                          specialOfferController
                                                              .offerProductList
                                                          [index]
                                                              .discountPrice ==
                                                              0
                                                              ? (lng == 'Bangla'
                                                              ? '৳${specialOfferController.offerProductList[index].bnPrice} TK'
                                                              : '৳${specialOfferController.offerProductList[index].price} TK')
                                                              : (lng == 'Bangla'
                                                              ? '${specialOfferController.offerProductList[index].bnDiscountPrice} TK'
                                                              : '${specialOfferController.offerProductList[index].discountPrice} TK'),
                                                          style: TextStyle(
                                                              color: kBlackColor,
                                                              fontWeight: FontWeight.w600,
                                                              fontSize: specialOfferController
                                                                  .offerProductList
                                                              [index]
                                                                  .discountPrice !=
                                                                  0
                                                                  ? 13
                                                                  : 15,
                                                              height: 1.0),
                                                        ),
                                                        kWidthBox10,
                                                        specialOfferController
                                                            .offerProductList
                                                        [index]
                                                            .discountPrice !=
                                                            0
                                                            ? Text(
                                                          '${lng == 'Bangla' ? specialOfferController.offerProductList[index].bnPrice : specialOfferController.offerProductList[index].price} TK',
                                                          style: TextStyle(
                                                              fontWeight: FontWeight.w600,
                                                              fontSize: 13,
                                                              height: 1.0,
                                                              color: kAccentColor,
                                                              decoration: TextDecoration
                                                                  .lineThrough),
                                                        )
                                                            : SizedBox(),
                                                      ],
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    child:  !(specialOfferController.offerProductList[index].quantity > specialOfferController.offerProductList[index].qty )
                                                        ? Container(
                                                      decoration: BoxDecoration(
                                                          color: kWhiteColor,
                                                          borderRadius: BorderRadius.circular(10),
                                                          border: Border.all(width: 1, color: kAccentColor)),
                                                      child: Padding(
                                                        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                                                        child: Text(
                                                          'Stock Out'.tr,
                                                          style: TextStyle(
                                                              fontWeight: FontWeight.w400,
                                                              fontSize: 12,
                                                              height: 1.0,
                                                              color: kAccentColor),
                                                        ),
                                                      ),
                                                    ) : SizedBox(
                                                      child: specialOfferController
                                                          .offerProductList[index].qty >
                                                          0
                                                          ? Container(
                                                        margin: EdgeInsets.all(2),
                                                        child: Row(
                                                          children: [
                                                            InkWell(
                                                              onTap: () {
                                                                setState(() {
                                                                  specialOfferController
                                                                      .offerProductList
                                                                  [index]
                                                                      .qty -= 1;
                                                                });
                                                                if (specialOfferController
                                                                    .offerProductList
                                                                [index]
                                                                    .qty !=
                                                                    0) {
                                                                  groceryCartController.qtyDecrease(
                                                                      specialOfferController
                                                                          .offerProductList
                                                                      [index]
                                                                          .id,
                                                                      specialOfferController
                                                                          .offerProductList
                                                                      [index]
                                                                          .qty,
                                                                      specialOfferController
                                                                          .offerProductList
                                                                      [index]
                                                                          .price);
                                                                } else {
                                                                  groceryCartController
                                                                      .removeFromCart(
                                                                      specialOfferController
                                                                          .offerProductList
                                                                      [index]
                                                                          .id);
                                                                  kSnackBar('Remove from cart');
                                                                }
                                                              },
                                                              child: Container(
                                                                width: 28,
                                                                height: 28,
                                                                decoration: BoxDecoration(
                                                                    color: kWhiteColor,
                                                                    borderRadius:
                                                                    BorderRadius
                                                                        .circular(5),
                                                                    border: Border.all(
                                                                        width: 1,
                                                                        color:
                                                                        kSecondaryColor)),
                                                                child: Icon(
                                                                  Icons.remove,
                                                                  color: kBlackColor,
                                                                ),
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              width: 10,
                                                            ),
                                                            Text(
                                                              specialOfferController
                                                                  .offerProductList
                                                              [index]
                                                                  .qty
                                                                  .toString(),
                                                              style: TextStyle(
                                                                  fontWeight:
                                                                  FontWeight.w500,
                                                                  fontSize: 18),
                                                            ),
                                                            SizedBox(
                                                              width: 10,
                                                            ),
                                                            InkWell(
                                                              onTap: () {
                                                                if (specialOfferController
                                                                    .offerProductList
                                                                [index]
                                                                    .quantity >
                                                                    specialOfferController
                                                                        .offerProductList
                                                                    [index]
                                                                        .qty) {
                                                                  setState(() {
                                                                    specialOfferController
                                                                        .offerProductList
                                                                    [index]
                                                                        .qty += 1;
                                                                  });
                                                                  groceryCartController
                                                                      .addToCart(
                                                                      specialOfferController
                                                                          .offerProductList
                                                                      [index]);
                                                                } else {
                                                                  kSnackBar('Stock Out');
                                                                }
                                                              },
                                                              child: Container(
                                                                width: 28,
                                                                height: 28,
                                                                decoration: BoxDecoration(
                                                                    color: kWhiteColor,
                                                                    borderRadius:
                                                                    BorderRadius
                                                                        .circular(5),
                                                                    border: Border.all(
                                                                        width: 1,
                                                                        color:
                                                                        kSecondaryColor)),
                                                                child: Icon(
                                                                  Icons.add,
                                                                  color: kBlackColor,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                          : Container(
                                                        padding: EdgeInsets.all(10),
                                                        decoration: BoxDecoration(
                                                          color: kSecondaryColor,
                                                          borderRadius:
                                                          BorderRadius.circular(17),
                                                        ),
                                                        child: InkWell(
                                                          onTap: () {
                                                            setState(() {
                                                              specialOfferController
                                                                  .offerProductList
                                                              [index]
                                                                  .qty += 1;
                                                            });
                                                            groceryCartController.addToCart(
                                                                specialOfferController
                                                                    .offerProductList
                                                                [index]);

                                                            kSnackBar('Added to Cart');
                                                          },
                                                          child: Icon(
                                                            Icons.add,
                                                            color: kWhiteColor,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  )),
                              specialOfferController.offerProductList[index]
                                  .discountPrice !=
                                  0
                                  ? Positioned(
                                top: 0,
                                right: 0,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: kAccentColor,
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(12),
                                        bottomLeft: Radius.circular(12)),
                                  ),
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 4),
                                  child: Center(
                                    child: Text(
                                      '-৳${lng == 'Bangla' ? specialOfferController.offerProductList[index].bnDiscountAmount : specialOfferController.offerProductList[index].discountAmount} ' +
                                          'OFF'.tr,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 12,
                                          height: 1.0,
                                          color: kWhiteColor),
                                    ),
                                  ),
                                ),
                              )
                                  : Positioned(
                                top: 0,
                                right: 0,
                                child: Container(),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  }
                });
          }
        }
      },
    );
  }

  Widget _buildProductGridView(){
    return Obx(
          () {
        if (specialOfferController.isLoading.isTrue) {
          return productGridShimmer();
        } else {
          if (specialOfferController.offerProductList.isEmpty) {
            return Center(
              child: Text('No Products Available'.tr),
            );
          } else {
            return StaggeredGridView.countBuilder(
              controller: _scrollController,
              crossAxisCount: 2,
              shrinkWrap: true,
              itemCount: specialOfferController.offerProductList.length+1,
              physics: ScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 15),
              staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
              mainAxisSpacing: 5.0,
              crossAxisSpacing: 5.0,
              itemBuilder: (BuildContext context, int index) {
                if(index==specialOfferController.offerProductList.length &&
                    !Get.find<SpecialOfferController>().loadedCompleted.value){
                  return Center(child: CircularProgressIndicator());
                }
                else if(index==specialOfferController.offerProductList.length &&
                    Get.find<SpecialOfferController>().loadedCompleted.value){
                  return Container();
                }
                else{
                  return InkWell(
                    onTap: () {
                      Get.to(
                            () => ProductDetails(
                          prodId: specialOfferController
                              .offerProductList[index].id
                              .toString(),
                        ),
                      );
                    },
                    child: Container(
                      height: 250,
                      width: SizeConfig.screenWidth,
                      decoration: BoxDecoration(
                          color: kWhiteColor,
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(width: 1, color: kBorderColor)),
                      child: Column(
                        children: [
                          Expanded(
                            child: SizedBox(
                              width: SizeConfig.screenWidth,
                              child: Stack(
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(2),
                                    child: Center(
                                      child: /*CachedNetworkImage(
                                      imageUrl: kImageUrl +
                                          specialOfferController
                                              .offerProductList[index].picture,
                                      fit: BoxFit.scaleDown,
                                      progressIndicatorBuilder:
                                          (context, url, downloadProgress) =>
                                              CustomLoader(),
                                      errorWidget: (context, url, error) =>
                                          Image.asset(
                                              '${kImageDir}placeholder.png'),
                                    ),*/
                                      FadeInImage.assetNetwork(
                                        placeholder: '${kImageDir}placeholder.png',
                                        image: kImageUrl + specialOfferController.offerProductList[index].picture,
                                        height: 100,
                                        width: 100,
                                        imageScale: 1,
                                        imageErrorBuilder: (context, url, error) => Image.asset('${kImageDir}placeholder.png'),
                                      ),
                                    ),
                                  ),
                                  specialOfferController.offerProductList[index]
                                      .discountPrice !=
                                      0
                                      ? Positioned(
                                    top: 0,
                                    right: 0,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: kAccentColor,
                                        borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(12),
                                            bottomLeft: Radius.circular(12)),
                                      ),
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8, vertical: 4),
                                      child: Center(
                                        child: Text(
                                          '-৳${lng == 'Bangla' ? specialOfferController.offerProductList[index].bnDiscountAmount : specialOfferController.offerProductList[index].discountAmount} ' +
                                              'OFF'.tr,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12,
                                              height: 1.0,
                                              color: kWhiteColor),
                                        ),
                                      ),
                                    ),
                                  )
                                      : Positioned(
                                    top: 0,
                                    right: 0,
                                    child: Container(),
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  lng == 'Bangla'
                                      ? specialOfferController
                                      .offerProductList[index].bnProductName
                                      : specialOfferController
                                      .offerProductList[index].productName,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 15,
                                    height: 1.10,
                                    color: kBlackColor,
                                  ),
                                ),
                                kHeightBox5,
                                Text(
                                  lng == 'Bangla'
                                      ? specialOfferController
                                      .offerProductList[index].bnAttribute
                                      : specialOfferController
                                      .offerProductList[index].attribute,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14,
                                    height: 1.0,
                                    color: kSilverColor,
                                  ),
                                ),
                                kHeightBox10,
                                Row(
                                  /*crossAxisAlignment: specialOfferController
                                    .offerProductList
                                    [index]
                                    .discountPrice !=
                                    0 ? CrossAxisAlignment.end : CrossAxisAlignment.center,*/
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            specialOfferController
                                                .offerProductList
                                            [index]
                                                .discountPrice ==
                                                0
                                                ? (lng == 'Bangla'
                                                ? '৳${specialOfferController.offerProductList[index].bnPrice} TK'
                                                : '৳${specialOfferController.offerProductList[index].price} TK')
                                                : (lng == 'Bangla'
                                                ? '${specialOfferController.offerProductList[index].bnDiscountPrice} TK'
                                                : '${specialOfferController.offerProductList[index].discountPrice} TK'),
                                            style: TextStyle(
                                                color: kBlackColor,
                                                fontWeight: FontWeight.w600,
                                                fontSize: specialOfferController
                                                    .offerProductList
                                                [index]
                                                    .discountPrice !=
                                                    0
                                                    ? 13
                                                    : 15,
                                                height: 1.0),
                                          ),
                                          specialOfferController
                                              .offerProductList
                                          [index]
                                              .discountPrice !=
                                              0
                                              ? Text(
                                            '${lng == 'Bangla' ? specialOfferController.offerProductList[index].bnPrice : specialOfferController.offerProductList[index].price} TK',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 13,
                                                height: 1.0,
                                                color: kAccentColor,
                                                decoration: TextDecoration
                                                    .lineThrough),
                                          )
                                              : SizedBox(),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      child:  !(specialOfferController.offerProductList[index].quantity > specialOfferController.offerProductList[index].qty )
                                          ? Container(
                                        decoration: BoxDecoration(
                                            color: kWhiteColor,
                                            borderRadius: BorderRadius.circular(10),
                                            border: Border.all(width: 1, color: kAccentColor)),
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                                          child: Text(
                                            'Stock Out'.tr,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 12,
                                                height: 1.0,
                                                color: kAccentColor),
                                          ),
                                        ),
                                      ) : SizedBox(
                                        child: specialOfferController
                                            .offerProductList[index].qty >
                                            0
                                            ? Container(
                                          margin: EdgeInsets.all(2),
                                          child: Row(
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    specialOfferController
                                                        .offerProductList
                                                    [index]
                                                        .qty -= 1;
                                                  });
                                                  if (specialOfferController
                                                      .offerProductList
                                                  [index]
                                                      .qty !=
                                                      0) {
                                                    groceryCartController.qtyDecrease(
                                                        specialOfferController
                                                            .offerProductList
                                                        [index]
                                                            .id,
                                                        specialOfferController
                                                            .offerProductList
                                                        [index]
                                                            .qty,
                                                        specialOfferController
                                                            .offerProductList
                                                        [index]
                                                            .price);
                                                  } else {
                                                    groceryCartController
                                                        .removeFromCart(
                                                        specialOfferController
                                                            .offerProductList
                                                        [index]
                                                            .id);
                                                    kSnackBar('Remove From Cart');
                                                  }
                                                },
                                                child: Container(
                                                  width: 28,
                                                  height: 28,
                                                  decoration: BoxDecoration(
                                                      color: kWhiteColor,
                                                      borderRadius:
                                                      BorderRadius
                                                          .circular(5),
                                                      border: Border.all(
                                                          width: 1,
                                                          color:
                                                          kSecondaryColor)),
                                                  child: Icon(
                                                    Icons.remove,
                                                    color: kBlackColor,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Text(
                                                specialOfferController
                                                    .offerProductList
                                                [index]
                                                    .qty
                                                    .toString(),
                                                style: TextStyle(
                                                    fontWeight:
                                                    FontWeight.w500,
                                                    fontSize: 18),
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  if (specialOfferController
                                                      .offerProductList
                                                  [index]
                                                      .quantity >
                                                      specialOfferController
                                                          .offerProductList
                                                      [index]
                                                          .qty) {
                                                    setState(() {
                                                      specialOfferController
                                                          .offerProductList
                                                      [index]
                                                          .qty += 1;
                                                    });
                                                    groceryCartController
                                                        .addToCart(
                                                        specialOfferController
                                                            .offerProductList
                                                        [index]);
                                                  } else {
                                                    kSnackBar('Stock Out');
                                                  }
                                                },
                                                child: Container(
                                                  width: 28,
                                                  height: 28,
                                                  decoration: BoxDecoration(
                                                      color: kWhiteColor,
                                                      borderRadius:
                                                      BorderRadius
                                                          .circular(5),
                                                      border: Border.all(
                                                          width: 1,
                                                          color:
                                                          kSecondaryColor)),
                                                  child: Icon(
                                                    Icons.add,
                                                    color: kBlackColor,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                            : Container(
                                          padding: EdgeInsets.all(10),
                                          decoration: BoxDecoration(
                                            color: kSecondaryColor,
                                            borderRadius:
                                            BorderRadius.circular(17),
                                          ),
                                          child: InkWell(
                                            onTap: () {
                                              setState(() {
                                                specialOfferController
                                                    .offerProductList
                                                [index]
                                                    .qty += 1;
                                              });
                                              groceryCartController.addToCart(
                                                  specialOfferController
                                                      .offerProductList
                                                  [index]);

                                              kSnackBar('Added to Cart');
                                            },
                                            child: Icon(
                                              Icons.add,
                                              color: kWhiteColor,
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }
              },
            );
          }
        }
      },
    );
  }
}
