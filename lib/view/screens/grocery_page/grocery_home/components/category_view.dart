import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/services/extensions/build_context_extension.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/grocery_controllers/categories_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/subcategories_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/screens/menu/widgets/sub_category_page.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/widgets/custom_shimmer.dart';
import 'package:familytrust/view/screens/grocery_page/child_categories/child_categories_screen.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/category_widget.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/productlist_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

import '../widgets/home_category_card.dart';
import 'all_categories.dart';

class CategoryView extends StatefulWidget {
  @override
  _CategoryViewState createState() => _CategoryViewState();
}

class _CategoryViewState extends State<CategoryView> {
  final CategoriesController categoriesController = Get.find();

  int selected;
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      categoriesController.onInit();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        if (categoriesController.isLoading.isTrue) {
          return mainCatShimmer();
        } else {
          if (categoriesController.categories.data.isEmpty) {
            return Center(
              child: Text('No Categories Available'.tr),
            );
          } else {
            return StaggeredGridView.countBuilder(
              crossAxisCount: 2,
              shrinkWrap: true,
              itemCount: categoriesController.categories.data.length,
              physics: const NeverScrollableScrollPhysics(),
              staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
              mainAxisSpacing: 14.0,
              crossAxisSpacing: 14.0,
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  onTap: (){
                    if(index == 7){
                      //open all category
                      context.push(AllCategories());
                    }else{
                      //open this category screen
                      context.push(SubCategories(
                        title: LocalizationService().getCurrentLang() == 'Bangla'
                            ? categoriesController.categories.data[index].bnCategoryName
                            : categoriesController.categories.data[index].categoryName,
                        index: index,
                      ),);
                    }
                  },
                  child: HomeCategoryView(
                    image: categoriesController.categories.data[index].categoryPicture ?? '',
                    title: categoriesController.categories.data[index].categoryName ?? '',
                    index: index,
                  ),
                );
              },
            );
          }
        }
      },
    );
  }
}
