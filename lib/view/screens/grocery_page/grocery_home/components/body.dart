import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/controllers/grocery_controllers/categories_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/banner_screen.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/special_offer_view.dart';
import 'package:familytrust/view/widgets/custom_shimmer.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../widgets/home_top_tab_btn.dart';
import 'best_seller_view.dart';
import 'categories_list.dart';
import 'category_view.dart';
import 'offer_slider.dart';
import 'product_category_view.dart';
import 'slider.dart';

class Body extends StatelessWidget {
  Body({Key key}) : super(key: key);
  final CategoriesController categoriesController = Get.find();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                children: [
                  Expanded(
                    child: HomeTopTabButton(
                      image: '${kImageDir}grocery.png',
                      title: 'Grocery & Gas',
                      isSelect: true,
                    ),
                  ),
                  kWidthBox10,
                  Expanded(
                    child: HomeTopTabButton(
                      image: '${kImageDir}home_lifestyle.png',
                      title: 'Upcoming',
                    ),
                  ),
                ],
              ),
            ),
            SliderScreen(),
            kHeightBox10,
            CategoryView(),
            BestSellerView(title: 'Best Selling'.tr),
            kHeightBox15,
            BannerScreen(),
            SpecialOfferView(title: 'Special Offer'.tr),
            kHeightBox15,
            Obx(
              () {
                if (categoriesController.isLoading.value) {
                  return Center(child: CircularProgressIndicator());
                } else if (categoriesController.categories.data.isEmpty) {
                  return Center(child: Text('No Categories Available!'));
                } else {
                  return ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: categoriesController.categories.data.length,
                    itemBuilder: (context, index) => ProductCategoryView(
                      title: LocalizationService().getCurrentLang() == 'Bangla'
                          ? categoriesController.categories.data[index].bnCategoryName
                          : categoriesController.categories.data[index].categoryName,
                      catId: categoriesController.categories.data[index].id,
                      subCategoryList: categoriesController.categories.data[index].subcategories,
                    ),
                    separatorBuilder: (context, index) => kHeightBox15,
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
