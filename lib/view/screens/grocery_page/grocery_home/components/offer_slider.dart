import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/grocery_controllers/offerslider_controller.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OfferSliderScreen extends StatefulWidget {
  @override
  _OfferSliderScreenState createState() => _OfferSliderScreenState();
}

class _OfferSliderScreenState extends State<OfferSliderScreen> {
  final OfferSlidersController offerSliderController = Get.find();
  int sliderIndex = 0;
  @override
  void initState() {
    offerSliderController.onInit();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        if (offerSliderController.isLoading.isTrue) {
          return Center(
            child: CustomLoader(),
          );
        } else {
          return offerSliderController.offerSliders.data.length != 0
              ? Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        'Special Offers',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(bottom: 10),
                      alignment: Alignment.centerLeft,
                      color: Colors.white,
                      width: SizeConfig.screenWidth,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CarouselSlider(
                            options: CarouselOptions(
                                aspectRatio: 2.5,
                                viewportFraction: 1,
                                initialPage: 0,
                                enableInfiniteScroll: true,
                                reverse: false,
                                autoPlay: true,
                                autoPlayInterval: Duration(seconds: 5),
                                autoPlayAnimationDuration:
                                    Duration(milliseconds: 1000),
                                autoPlayCurve: Curves.easeInCubic,
                                enlargeCenterPage: true,
                                scrollDirection: Axis.horizontal,
                                onPageChanged: (int index, reason) {
                                  setState(() {
                                    sliderIndex = index;
                                  });
                                }),
                            items: offerSliderController.offerSliders.data
                                .map(
                                  (item) => Stack(
                                    alignment: Alignment.bottomCenter,
                                    children: [
                                      Container(
                                        width: double.infinity,
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: CachedNetworkImage(
                                            imageUrl: kImageUrl + item.picture,
                                            fit: BoxFit.fill,
                                            placeholder: (context, url) =>
                                                CustomLoader(),
                                            errorWidget:
                                                (context, url, error) => Icon(
                                              Icons.error,
                                              color: Colors.red,
                                            ),
                                          ),
                                        ),
                                      ),
                                      ListView.builder(
                                          shrinkWrap: true,
                                          scrollDirection: Axis.horizontal,
                                          itemCount: offerSliderController
                                              .offerSliders.data.length,
                                          itemBuilder: (context, int index) {
                                            return Align(
                                              alignment: Alignment.bottomCenter,
                                              child: Container(
                                                width: 11,
                                                height: 11,
                                                margin: EdgeInsets.symmetric(
                                                    vertical: 10.0,
                                                    horizontal: 5.0),
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  border: Border.all(
                                                    width: 1,
                                                    color: kSecondaryColor,
                                                  ),
                                                  color: sliderIndex == index
                                                      ? kPrimaryColor
                                                      : kWhiteColor,
                                                ),
                                              ),
                                            );
                                          }),
                                    ],
                                  ),
                                )
                                .toList(),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              : SizedBox();
        }
      },
    );
  }
}
