import 'package:familytrust/view/screens/grocery_page/grocery_home/components/categories_list.dart';
import 'package:familytrust/view/widgets/cart_item_badge.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

class AllCategories extends StatelessWidget {
  static const routeName = 'categories_all';
  const AllCategories({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        title: Text('All Categories'.tr),
        actions: [
          CartItemBadge(),
        ],
      ),
      body: CategoriesList(),
    );
  }
}
