import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/all_categories.dart';
import 'package:familytrust/view/screens/grocery_page/search/search_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class HeaderContent extends StatelessWidget {
  const HeaderContent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.zero,
      color: kPrimaryColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10),
        child: Row(
          children: [
            SizedBox(
              width: 39,
              child: Image.asset('${kImageDir}logo2.png', fit: BoxFit.cover,),
            ),
            kWidthBox10,
            Expanded(
              child: InkWell(
                onTap: () {
                  Get.to(() => SearchPage());
                },
                child: Container(
                  height: 31,
                  decoration: BoxDecoration(
                    color: kWhiteColor,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Text('Search Products'.tr,  style: TextStyle(
                            fontSize: 12,
                            color: kBlackColor,
                            fontWeight: FontWeight.w400,
                            height: 1
                          ),),
                        ),
                      ),
                      Container(
                        height: 31,
                        decoration: BoxDecoration(
                          color: kSecondaryColor,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(15),
                            bottomRight: Radius.circular(15),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: SvgPicture.asset(
                              '$kIconsDir/search.svg'
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
