import 'package:familytrust/controllers/grocery_controllers/grocerycart_controller.dart';
import 'package:familytrust/helper/helper_method.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/grocery_page/sheared/product_details.dart';
import 'package:familytrust/view/widgets/cart_item_badge.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

class ShowAllProductScreen extends StatefulWidget {
  final String title;
  final List<dynamic> productList;

  ShowAllProductScreen({
    Key key,
    @required this.title,
    @required this.productList,
  }) : super(key: key);

  @override
  _ShowAllProductScreenState createState() => _ShowAllProductScreenState();
}

class _ShowAllProductScreenState extends State<ShowAllProductScreen> {
  
  bool activeGrid = false;
  final String lng = LocalizationService().getCurrentLang();
  final GroceryCartController groceryCartController = Get.find();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          CartItemBadge(),
        ],
      ),
      body: Column(
        children: [
          const SizedBox(height: 5,),
          Expanded(
            flex: 1,
            child: Card(
              margin: EdgeInsets.zero,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0.0),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 8,
                ),
                child: Row(
                  children: [
                    Text(
                      'Product View Type',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        height: 1.10,
                        color: kBlackColor,
                      ),
                    ),
                    const Spacer(),
                    InkWell(
                      onTap: () {
                        setState(() {
                          activeGrid = false;
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color:
                          activeGrid == false ? kSecondaryColor : kWhiteColor,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.list,
                            color: kPrimaryColor,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          activeGrid = true;
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color:
                          activeGrid == true ? kSecondaryColor : kWhiteColor,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.grid_on,
                            color: kPrimaryColor,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          const SizedBox(height: 5,),
          activeGrid == false
              ? Expanded(
            flex: 11,
            child: _buildListProductView(),
          )
              : Expanded(
            flex: 11,
            child: _buildGridProductView(),
          )
        ],
      ),
    );
  }
  
  Widget _buildListProductView(){
    return ListView.builder(
        physics: ScrollPhysics(),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: widget.productList.length,
        itemBuilder: (context, int index) {
          return InkWell(
            onTap: () {
              Get.to(
                    () => ProductDetails(
                  prodId: widget.productList[index].id.toString(),
                ),
              );
            },
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 5,
                vertical: 4,
              ),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Stack(
                  children: [
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child:  FadeInImage.assetNetwork(
                                  placeholder: '${kImageDir}placeholder.png',
                                  image: kImageUrl + widget.productList[index].picture,
                                  height: 80,
                                  width: 80,
                                  imageScale: 1,
                                  imageErrorBuilder: (context, url, error) => Image.asset('${kImageDir}placeholder.png'),
                                ),
                              ),
                              kWidthBox10,
                              Expanded(
                                flex: 5,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      lng == 'Bangla'
                                          ? widget.productList[index].bnProductName
                                          : widget.productList[index].productName,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 15,
                                        height: 1.10,
                                        color: kBlackColor,
                                      ),
                                    ),
                                    kHeightBox10,
                                    Text(
                                      lng == 'Bangla'
                                          ? widget.productList[index].bnAttribute
                                          : widget.productList[index].attribute,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        height: 1.0,
                                        color: kSilverColor,
                                      ),
                                    ),
                                    kHeightBox10,
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Row(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                widget.productList[index].discountPrice ==
                                                    0
                                                    ? (lng == 'Bangla'
                                                    ? '৳${widget.productList[index].bnPrice} TK'
                                                    : '৳${widget.productList[index].price} TK')
                                                    : (lng == 'Bangla'
                                                    ? '${widget.productList[index].bnDiscountPrice} TK'
                                                    : '${widget.productList[index].discountPrice} TK'),
                                                style: TextStyle(
                                                    color: kBlackColor,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: widget.productList[index].discountPrice != 0
                                                        ? 13
                                                        : 15,
                                                    height: 1.0),
                                              ),
                                              kWidthBox10,
                                              widget.productList[index].discountPrice != 0
                                                  ? Text(
                                                '${lng == 'Bangla' ? widget.productList[index].bnPrice : widget.productList[index].price} TK',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 13,
                                                    height: 1.0,
                                                    color: kAccentColor,
                                                    decoration: TextDecoration
                                                        .lineThrough),
                                              )
                                                  : SizedBox(),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          child:  !(widget.productList[index].quantity > widget.productList[index].qty )
                                              ? Container(
                                            decoration: BoxDecoration(
                                                color: kWhiteColor,
                                                borderRadius: BorderRadius.circular(10),
                                                border: Border.all(width: 1, color: kAccentColor)),
                                            child: Padding(
                                              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                                              child: Text(
                                                'Stock Out'.tr,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 12,
                                                    height: 1.0,
                                                    color: kAccentColor),
                                              ),
                                            ),
                                          ) : SizedBox(
                                            child: widget.productList[index].qty > 0
                                                ? Container(
                                              margin: EdgeInsets.all(2),
                                              child: Row(
                                                children: [
                                                  InkWell(
                                                    onTap: () {
                                                      setState(() {
                                                        widget.productList[index]
                                                            .qty -= 1;
                                                      });
                                                      if (widget.productList[index].qty != 0) {
                                                        groceryCartController.qtyDecrease(
                                                            widget.productList[index].id,
                                                            widget.productList[index].qty,
                                                            widget.productList[index].price
                                                        );
                                                      } else {
                                                        groceryCartController
                                                            .removeFromCart(
                                                            widget.productList[index].id);
                                                        kSnackBar('Remove from cart');
                                                      }
                                                    },
                                                    child: Container(
                                                      width: 28,
                                                      height: 28,
                                                      decoration: BoxDecoration(
                                                          color: kWhiteColor,
                                                          borderRadius:
                                                          BorderRadius
                                                              .circular(5),
                                                          border: Border.all(
                                                              width: 1,
                                                              color:
                                                              kSecondaryColor)),
                                                      child: Icon(
                                                        Icons.remove,
                                                        color: kBlackColor,
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Text(
                                                    widget.productList[index].qty.toString(),
                                                    style: TextStyle(
                                                        fontWeight:
                                                        FontWeight.w500,
                                                        fontSize: 18),
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  InkWell(
                                                    onTap: () {
                                                      if (widget.productList[index].quantity > widget.productList[index].qty) {
                                                        setState(() {
                                                          widget.productList[index].qty += 1;
                                                        });
                                                        groceryCartController.addToCart(widget.productList[index]);
                                                      } else {
                                                        kSnackBar('Stock Out');
                                                      }
                                                    },
                                                    child: Container(
                                                      width: 28,
                                                      height: 28,
                                                      decoration: BoxDecoration(
                                                          color: kWhiteColor,
                                                          borderRadius:
                                                          BorderRadius
                                                              .circular(5),
                                                          border: Border.all(
                                                              width: 1,
                                                              color:
                                                              kSecondaryColor)),
                                                      child: Icon(
                                                        Icons.add,
                                                        color: kBlackColor,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                                : Container(
                                              padding: EdgeInsets.all(10),
                                              decoration: BoxDecoration(
                                                color: kSecondaryColor,
                                                borderRadius:
                                                BorderRadius.circular(17),
                                              ),
                                              child: InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    widget.productList[index].qty += 1;
                                                  });
                                                  groceryCartController.addToCart(widget.productList[index]);
                                                  kSnackBar('Added to Cart');
                                                },
                                                child: Icon(
                                                  Icons.add,
                                                  color: kWhiteColor,
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )),
                    widget.productList[index]
                        .discountPrice !=
                        0
                        ? Positioned(
                      top: 0,
                      right: 0,
                      child: Container(
                        decoration: BoxDecoration(
                          color: kAccentColor,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(12),
                              bottomLeft: Radius.circular(12)),
                        ),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8, vertical: 4),
                        child: Center(
                          child: Text(
                            '-৳${lng == 'Bangla' ? widget.productList[index].bnDiscountAmount : widget.productList[index].discountAmount} ' +
                                'OFF'.tr,
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 12,
                                height: 1.0,
                                color: kWhiteColor),
                          ),
                        ),
                      ),
                    )
                        : Positioned(
                      top: 0,
                      right: 0,
                      child: Container(),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
  
  
  Widget _buildGridProductView(){
    return StaggeredGridView.countBuilder(
      crossAxisCount: 2,
      shrinkWrap: true,
      itemCount: widget.productList.length,
      physics: ScrollPhysics(),
      padding: EdgeInsets.symmetric(horizontal: 15),
      staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
      mainAxisSpacing: 5.0,
      crossAxisSpacing: 5.0,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          onTap: () {
            Get.to(
                  () => ProductDetails(
                prodId: widget.productList[index].id.toString(),
              ),
            );
          },
          child: Container(
            height: 250,
            width: SizeConfig.screenWidth,
            decoration: BoxDecoration(
                color: kWhiteColor,
                borderRadius: BorderRadius.circular(12),
                border: Border.all(width: 1, color: kBorderColor)),
            child: Column(
              children: [
                Expanded(
                  child: SizedBox(
                    width: SizeConfig.screenWidth,
                    child: Stack(
                      children: [
                        Container(
                          padding: EdgeInsets.all(2),
                          child: Center(
                            child: /*CachedNetworkImage(
                                      imageUrl: kImageUrl +
                                          productsListController
                                              .productsList.data[index].picture,
                                      fit: BoxFit.scaleDown,
                                      progressIndicatorBuilder:
                                          (context, url, downloadProgress) =>
                                              CustomLoader(),
                                      errorWidget: (context, url, error) =>
                                          Image.asset(
                                              '${kImageDir}placeholder.png'),
                                    ),*/
                            FadeInImage.assetNetwork(
                              placeholder: '${kImageDir}placeholder.png',
                              image: kImageUrl + widget.productList[index].picture,
                              height: 100,
                              width: 100,
                              imageScale: 1,
                              imageErrorBuilder: (context, url, error) => Image.asset('${kImageDir}placeholder.png'),
                            ),
                          ),
                        ),
                        widget.productList[index]
                            .discountPrice !=
                            0
                            ? Positioned(
                          top: 0,
                          right: 0,
                          child: Container(
                            decoration: BoxDecoration(
                              color: kAccentColor,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(12),
                                  bottomLeft: Radius.circular(12)),
                            ),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8, vertical: 4),
                            child: Center(
                              child: Text(
                                '-৳${lng == 'Bangla' ? widget.productList[index].bnDiscountAmount : widget.productList[index].discountAmount} ' +
                                    'OFF'.tr,
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                    height: 1.0,
                                    color: kWhiteColor),
                              ),
                            ),
                          ),
                        )
                            : Positioned(
                          top: 0,
                          right: 0,
                          child: Container(),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        lng == 'Bangla'
                            ? widget.productList[index].bnProductName
                            : widget.productList[index].productName,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 15,
                          height: 1.10,
                          color: kBlackColor,
                        ),
                      ),
                      kHeightBox5,
                      Text(
                        lng == 'Bangla'
                            ? widget.productList[index].bnAttribute
                            : widget.productList[index].attribute,
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          height: 1.0,
                          color: kSilverColor,
                        ),
                      ),
                      kHeightBox10,
                      Row(
                        /*crossAxisAlignment: productsListController
                                    .productsList
                                    .data[index]
                                    .discountPrice !=
                                    0 ? CrossAxisAlignment.end : CrossAxisAlignment.center,*/
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Text(
                                  widget.productList[index].discountPrice == 0
                                      ? (lng == 'Bangla'
                                      ? '৳${widget.productList[index].bnPrice} TK'
                                      : '৳${widget.productList[index].price} TK')
                                      : (lng == 'Bangla'
                                      ? '${widget.productList[index].bnDiscountPrice} TK'
                                      : '${widget.productList[index].discountPrice} TK'),
                                  style: TextStyle(
                                      color: kBlackColor,
                                      fontWeight: FontWeight.w600,
                                      fontSize: widget.productList[index].discountPrice != 0
                                          ? 13
                                          : 15,
                                      height: 1.0),
                                ),
                                widget.productList[index].discountPrice != 0
                                    ? Text(
                                  '${lng == 'Bangla' ? widget.productList[index].bnPrice : widget.productList[index].price} TK',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 13,
                                      height: 1.0,
                                      color: kAccentColor,
                                      decoration: TextDecoration
                                          .lineThrough),
                                )
                                    : SizedBox(),
                              ],
                            ),
                          ),
                          SizedBox(
                            child:  !(widget.productList[index].quantity > widget.productList[index].qty )
                                ? Container(
                              decoration: BoxDecoration(
                                  color: kWhiteColor,
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(width: 1, color: kAccentColor)),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                                child: Text(
                                  'Stock Out'.tr,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 12,
                                      height: 1.0,
                                      color: kAccentColor),
                                ),
                              ),
                            ) : SizedBox(
                              child: widget.productList[index].qty > 0
                                  ? Container(
                                margin: EdgeInsets.all(2),
                                child: Row(
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        setState(() {
                                          widget.productList[index].qty -= 1;
                                        });
                                        if (widget.productList[index].qty != 0) {
                                          groceryCartController.qtyDecrease(
                                              widget.productList[index].id,
                                              widget.productList[index].qty,
                                              widget.productList[index].price);
                                        } else {
                                          groceryCartController.removeFromCart(widget.productList[index].id);
                                          kSnackBar('Remove From Cart');
                                        }
                                      },
                                      child: Container(
                                        width: 28,
                                        height: 28,
                                        decoration: BoxDecoration(
                                            color: kWhiteColor,
                                            borderRadius:
                                            BorderRadius
                                                .circular(5),
                                            border: Border.all(
                                                width: 1,
                                                color:
                                                kSecondaryColor)),
                                        child: Icon(
                                          Icons.remove,
                                          color: kBlackColor,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      widget.productList[index].qty.toString(),
                                      style: TextStyle(
                                          fontWeight:
                                          FontWeight.w500,
                                          fontSize: 18),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    InkWell(
                                      onTap: () {
                                        if (widget.productList[index].quantity > widget.productList[index].qty) {
                                          setState(() {
                                            widget.productList[index].qty += 1;
                                          });
                                          groceryCartController.addToCart(widget.productList[index]);
                                        } else {
                                          kSnackBar('Stock Out');
                                        }
                                      },
                                      child: Container(
                                        width: 28,
                                        height: 28,
                                        decoration: BoxDecoration(
                                            color: kWhiteColor,
                                            borderRadius:
                                            BorderRadius
                                                .circular(5),
                                            border: Border.all(
                                                width: 1,
                                                color:
                                                kSecondaryColor)),
                                        child: Icon(
                                          Icons.add,
                                          color: kBlackColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                                  : Container(
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  color: kSecondaryColor,
                                  borderRadius:
                                  BorderRadius.circular(17),
                                ),
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      widget.productList[index].qty += 1;
                                    });
                                    groceryCartController.addToCart(widget.productList[index]);

                                    kSnackBar('Added to Cart');
                                  },
                                  child: Icon(
                                    Icons.add,
                                    color: kWhiteColor,
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
