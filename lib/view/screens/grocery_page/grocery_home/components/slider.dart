import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/grocery_controllers/slider_controller.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/widgets/custom_shimmer.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SliderScreen extends StatefulWidget {
  @override
  _SliderScreenState createState() => _SliderScreenState();
}

class _SliderScreenState extends State<SliderScreen> {
  final SlidersController sliderController = Get.find();
  int sliderIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        if (sliderController.isLoading.isTrue) {
          return sliderShimmer();
        } else {
          return Container(
            padding: EdgeInsets.only(bottom: 10),
            alignment: Alignment.centerLeft,
            color: Colors.white,
            width: SizeConfig.screenWidth,
            child: CarouselSlider(
              options: CarouselOptions(
                  aspectRatio: 2.8,
                  viewportFraction: 1,
                  initialPage: 0,
                  enableInfiniteScroll: true,
                  reverse: false,
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 5),
                  autoPlayAnimationDuration: Duration(milliseconds: 1000),
                  autoPlayCurve: Curves.easeInCubic,
                  enlargeCenterPage: true,
                  scrollDirection: Axis.horizontal,
                  onPageChanged: (int index, reason) {
                    setState(() {
                      sliderIndex = index;
                    });
                  }),
              items: sliderController.sliders.data
                  .map(
                    (item) => Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        Container(
                          width: double.infinity,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: CachedNetworkImage(
                              imageUrl: kImageUrl + item.picture,
                              fit: BoxFit.fill,
                              placeholder: (context, url) => CustomLoader(),
                              errorWidget: (context, url, error) => Icon(
                                Icons.error,
                                color: Colors.red,
                              ),
                            ),
                          ),
                        ),
                        ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemCount: sliderController.sliders.data.length,
                            itemBuilder: (context, int index) {
                              return Align(
                                alignment: Alignment.bottomCenter,
                                child: Container(
                                  width: sliderIndex == index ? 18 : 6,
                                  height: 6,
                                  margin: EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 5.0),
                                  decoration: BoxDecoration(
                                   // shape: sliderIndex == index ? BoxShape.rectangle :  BoxShape.circle,
                                    borderRadius: BorderRadius.circular(
                                        sliderIndex == index ? 15: 30
                                    ),
                                    color: sliderIndex == index
                                        ? kSliderSelectColor
                                        : kSilverColor,
                                  ),
                                ),
                              );
                            }),
                      ],
                    ),
                  )
                  .toList(),
            ),
          );
        }
      },
    );
  }
}
