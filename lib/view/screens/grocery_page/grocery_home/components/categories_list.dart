import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/services/extensions/build_context_extension.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/grocery_controllers/categories_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/subcategories_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/screens/menu/components/category_item.dart';
import 'package:familytrust/view/screens/menu/widgets/sub_category_page.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/widgets/custom_shimmer.dart';
import 'package:familytrust/view/screens/grocery_page/child_categories/child_categories_screen.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/productlist_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

class CategoriesList extends StatefulWidget {
  @override
  _CategoriesListState createState() => _CategoriesListState();
}

class _CategoriesListState extends State<CategoriesList> {
  final CategoriesController categoriesController = Get.find();
  final SubCategoriesController subCategoriesController = Get.find();

  int selected;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      categoriesController.onInit();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 15,
        right: 15,
        top: 15,
      ),
      child: Obx(
        () {
          if (categoriesController.isLoading.isTrue) {
            return mainCatShimmer();
          } else {
            if (categoriesController.categories.data.isEmpty) {
              return Center(
                child: Text('No Categories Available'.tr),
              );
            } else {
              return StaggeredGridView.countBuilder(
                crossAxisCount: 2,
                shrinkWrap: true,
                itemCount: categoriesController.categories.data.length,
                staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
                mainAxisSpacing: 14.0,
                crossAxisSpacing: 14.0,
                itemBuilder: (BuildContext context, int index) {
                  int randomNumber = 0;
                  if (index < categoryBgColor.length) {
                    randomNumber = index;
                  } else {
                    randomNumber = Random().nextInt(categoryBgColor.length);
                  }
                  return InkWell(
                    onTap: () {
                      context.push(
                        SubCategories(
                          title: LocalizationService().getCurrentLang() == 'Bangla'
                              ? categoriesController.categories.data[index].bnCategoryName
                              : categoriesController.categories.data[index].categoryName,
                          index: index,
                        ),
                      );
                    },
                    child: CategoryItem(
                      image: categoriesController.categories.data[index].categoryPicture ?? '',
                      title: categoriesController.categories.data[index].categoryName ?? '',
                      index: index,
                      bgColor: categoryBgColor[randomNumber],
                      borderColor: categoryBorderColor[randomNumber],
                    ),
                  );
                },
              );
            }
          }
        },
      ),
    );
  }

  /*
  Card(
                    elevation: 0,
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: categoriesController
                                  .categories.data[index].status ==
                              0
                          ? ExpansionTile(
                              onExpansionChanged: ((newState) {
                                if (newState)
                                  setState(() {
                                    selected = index;
                                  });
                                else
                                  setState(() {
                                    selected = -1;
                                  });
                              }),
                              key: Key(index.toString()),
                              initiallyExpanded: index == selected,
                              backgroundColor: kSecondaryColor.withOpacity(.2),
                              title: CatRowWidget(
                                titleBD: categoriesController
                                    .categories.data[index].bnCategoryName,
                                titleEN: categoriesController
                                    .categories.data[index].categoryName,
                                description:
                                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat risus ac metus commodo, ut accumsan velit facilisis. Donec condimentum lorem diam',
                                discount: 'Up to 60% OFF',
                                image: kImageUrl +
                                    categoriesController
                                        .categories.data[index].categoryPicture,
                              ),
                              children: categoriesExpandableGridBuilder(index),
                            )
                          : InkWell(
                              onTap: () => context.push(
                                ProductListScreen(
                                  id: categoriesController
                                      .categories.data[index].id
                                      .toString(),
                                ),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: CatRowWidget(
                                  titleBD: categoriesController
                                      .categories.data[index].bnCategoryName,
                                  titleEN: categoriesController
                                      .categories.data[index].categoryName,
                                  description:
                                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat risus ac metus commodo, ut accumsan velit facilisis. Donec condimentum lorem diam',
                                  discount: 'Up to 60% OFF',
                                  image: kImageUrl +
                                      categoriesController.categories
                                          .data[index].categoryPicture,
                                ),
                              ),
                            ),
                    ),
                  );
   */

  categoriesExpandableGridBuilder(int id) {
    List<Widget> columnContent = [];
    [1].forEach((product) => {
          columnContent.add(Padding(
            padding: const EdgeInsets.all(12.0),
            child: Container(
              color: kWhiteColor,
              child: GridView.builder(
                physics: ScrollPhysics(),
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: categoriesController
                    .categories.data[id].subcategories.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3),
                itemBuilder: (BuildContext context, int index2) {
                  return InkWell(
                    onTap: () async {
                      // if (categoriesController.categories.data[id].status ==
                      //     0) {
                      //   Get.to(
                      //     () => ProductListScreen(
                      //       id: categoriesController.categories.data[id]
                      //           .toString(),
                      //     ),
                      //   );
                      // } else
                      if (categoriesController.categories.data[id]
                              .subcategories[index2].status ==
                          0) {
                        context.push(
                          ProductListScreen(
                            id: categoriesController
                                .categories.data[id].subcategories[index2].id
                                .toString(),
                          ),
                        );
                      } else {
                        context.push(
                          ChildCatScreen(
                            catId: categoriesController
                                .categories.data[id].subcategories[index2].id
                                .toString(),
                            catName: LocalizationService().getCurrentLang() ==
                                    'Bangla'
                                ? categoriesController.categories.data[id]
                                    .subcategories[index2].bnCategoryName
                                : categoriesController.categories.data[id]
                                    .subcategories[index2].categoryName,
                          ),
                        );
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Color(0xFFF5F5F5),
                          width: 1.0,
                        ),
                      ),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 5,
                          ),
                          Expanded(
                            flex: 4,
                            child: CachedNetworkImage(
                              imageUrl: kImageUrl +
                                  categoriesController.categories.data[id]
                                      .subcategories[index2].categoryPicture,
                              fit: BoxFit.cover,
                              placeholder: (context, url) => CustomLoader(),
                              errorWidget: (context, url, error) => Icon(
                                Icons.error,
                                color: Colors.red,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Expanded(
                            flex: 2,
                            child: Text(
                              LocalizationService().getCurrentLang() == 'Bangla'
                                  ? categoriesController.categories.data[id]
                                      .subcategories[index2].bnCategoryName
                                  : categoriesController.categories.data[id]
                                      .subcategories[index2].categoryName,
                              textAlign: TextAlign.center,
                              maxLines: 2,
                              style: TextStyle(fontWeight: FontWeight.w500),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          )),
        });
    return columnContent;
  }
}
