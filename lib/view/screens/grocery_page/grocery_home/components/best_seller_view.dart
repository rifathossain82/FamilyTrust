import 'package:familytrust/controllers/grocery_controllers/grocerypopular_controller.dart';
import 'package:familytrust/services/extensions/build_context_extension.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/show_all_products.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../widgets/home_more_widgets.dart';
import '../../../../widgets/horizontal_grocery_list_view.dart';

class BestSellerView extends StatelessWidget {
  final String title;

  BestSellerView({
    Key key,
    @required this.title,
  }) : super(key: key);

  final GroceryPopularController groceryPopularController = Get.find<GroceryPopularController>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: HomeMoreWidgets(
            title: title,
            onPressed: () {
              context.push(
                ShowAllProductScreen(
                  title: title,
                  productList: groceryPopularController.groceryPopular.data,
                ),
              );
            },
          ),
        ),
        Obx(
          () {
            if (groceryPopularController.isLoading.value) {
              return Center(child: CircularProgressIndicator());
            } else if (groceryPopularController.groceryPopular.data.isEmpty) {
              return Center(child: Text('No Products Available!'));
            } else {
              return HorizontalGroceryListView(
                  productList: groceryPopularController.groceryPopular.data);
            }
          },
        )
      ],
    );
  }
}
