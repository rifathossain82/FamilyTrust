import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/grocery_controllers/orderlist_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/grocery_page/checkout/widgets/row_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class GroceryOrderList extends StatefulWidget {
  @override
  _GroceryOrderListState createState() => _GroceryOrderListState();
}

class _GroceryOrderListState extends State<GroceryOrderList> {
  final GroceryOrderController groceryOrderListController = Get.find();
  final String lng = LocalizationService().getCurrentLang();
  int selected;

  @override
  void initState() {
    groceryOrderListController.fetchOrderList();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () {
          if (groceryOrderListController.isLoading.isTrue) {
            return Center(
              child: CustomLoader(),
            );
          } else {
            if (groceryOrderListController.groceryOrderList.data.isEmpty) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.file_present,
                      size: getProportionateScreenHeight(50),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Empty Orders'.tr,
                      style: TextStyle(
                        fontSize: getProportionateScreenHeight(30),
                      ),
                    ),
                  ],
                ),
              );
            } else {
              print(groceryOrderListController.groceryOrderList.data.length);
              return ListView.builder(
                  key: Key('builder ${selected.toString()}'),
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                  itemCount:
                      groceryOrderListController.groceryOrderList.data.length,
                  itemBuilder: (context, index) {
                    return Card(
                      child: ExpansionTile(
                        initiallyExpanded: index == selected,
                        onExpansionChanged: ((newState) {
                          if (newState) {
                            setState(() {
                              selected = index;
                            });
                          } else {
                            setState(() {
                              selected = -1;
                            });
                          }
                        }),
                        backgroundColor: kSecondaryColor.withOpacity(.2),
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              children: [
                                Container(
                                  child: SelectableText(
                                    '#SCG${groceryOrderListController.groceryOrderList.data[index].id}',
                                    style: TextStyle(
                                        color: kPrimaryColor,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  child: Text(
                                    '৳ ${groceryOrderListController.groceryOrderList.data[index].total.round()}',
                                    style: TextStyle(
                                        color: kSaleColor,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    color: kSecondaryColor,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: Text(
                                      groceryOrderListController
                                          .groceryOrderList
                                          .data[index]
                                          .paymentType,
                                      style: TextStyle(
                                          color: kWhiteColor,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Container(
                                  child: Text(
                                    DateFormat.yMMMMd().format(
                                      groceryOrderListController
                                          .groceryOrderList
                                          .data[index]
                                          .orderDate,
                                    ),
                                    style: TextStyle(
                                        color: kPrimaryColor,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        children: orderListExpendableBuilder(index),
                      ),
                    );
                  });
            }
          }
        },
      ),
    );
  }

  orderListExpendableBuilder(int id) {
    List<Widget> columnContent = [];
    [1].forEach((product) => {
          columnContent.add(
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Text('Delivery Location: ' +
                      groceryOrderListController
                          .groceryOrderList.data[id].location.toString()),
                ),
                for (int i = 0;
                    i <
                        groceryOrderListController
                            .groceryOrderList.data[id].orderProducts.length;
                    i++)
                  Card(
                    child: ListTile(
                      leading: Container(
                        width: 50,
                        height: 50,
                        child: CachedNetworkImage(
                          imageUrl: kImageUrl +
                              groceryOrderListController.groceryOrderList
                                  .data[id].orderProducts[i].picture,
                          fit: BoxFit.fitHeight,
                          placeholder: (context, url) => CustomLoader(),
                          errorWidget: (context, url, error) => Image.asset(
                            kImageDir + 'placeholder.png',
                          ),
                        ),
                      ),
                      title: Text(
                        lng == 'Bangla'
                            ? groceryOrderListController.groceryOrderList
                                .data[id].orderProducts[i].bnProductName
                            : groceryOrderListController.groceryOrderList
                                .data[id].orderProducts[i].productName,
                        maxLines: 2,
                      ),
                      subtitle: Text(
                          '${groceryOrderListController.groceryOrderList.data[id].orderProducts[i].quantity} X ৳${groceryOrderListController.groceryOrderList.data[id].orderProducts[i].price}'),
                      trailing: Text(
                        '৳' +
                            groceryOrderListController.groceryOrderList.data[id]
                                .orderProducts[i].total
                                .toString(),
                      ),
                    ),
                  ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RowWidget(
                    title: 'Sub Total',
                    amount: groceryOrderListController
                        .groceryOrderList.data[id].subTotal
                        .round()
                        .toString(),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RowWidget(
                    title: 'Discount',
                    amount: groceryOrderListController
                        .groceryOrderList.data[id].discount
                        .round()
                        .toString(),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RowWidget(
                    title: 'Delivery Charge',
                    amount: groceryOrderListController
                        .groceryOrderList.data[id].shippingCharge
                        .toString(),
                  ),
                ),
                Divider(
                  thickness: 1,
                  color: kPrimaryColor,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RowWidget(
                    title: 'Total',
                    amount: groceryOrderListController
                        .groceryOrderList.data[id].total
                        .round()
                        .toString(),
                  ),
                ),
              ],
            ),
          ),
        });
    return columnContent;
  }
}
