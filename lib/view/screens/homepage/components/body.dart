import 'package:familytrust/main.dart';
import 'package:familytrust/view/screens/homepage/components/greetings_user.dart';
import 'package:familytrust/view/screens/homepage/components/grocery_offers.dart';
import 'package:familytrust/view/screens/homepage/components/shop_offers.dart';
import 'package:familytrust/view/screens/homepage/components/shops_card.dart';
import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  const Body({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GreetingUser(),
          ShopsCard(),
          SizedBox(
            height: 15,
          ),
          ShopOffers(),
          SizedBox(
            height: 15,
          ),
          prefs.containsKey('store_id') ? GroceryOffers() : SizedBox(),
        ],
      ),
    );
  }
}
