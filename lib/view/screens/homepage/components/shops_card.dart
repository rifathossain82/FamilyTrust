import 'package:familytrust/services/extensions/build_context_extension.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/shop_controlers/alldistrict_controller.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:familytrust/view/screens/dashboard/dashboard_screen.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/grocery_home.dart';
import 'package:familytrust/view/screens/homepage/wigets/card_widget.dart';
import 'package:familytrust/view/screens/shop_page/shop_main.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ShopsCard extends StatefulWidget {
  const ShopsCard({Key key}) : super(key: key);

  @override
  _ShopsCardState createState() => _ShopsCardState();
}

class _ShopsCardState extends State<ShopsCard> {
  final allDistrictController = Get.put(AllDistrictController());
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CardWidget(
          colors: LinearGradient(colors: [
            Color(0xFFF3B019),
            Color(0xFFF9C50D),
            Color(0xFFF9C50D),
            Color(0xFFF3B019)
          ]),
          btnColor: kPrimaryColor,
          btnTextColor: kWhiteColor,
          textColor: kPrimaryColor,
          title: 'Family Trust E-Shop',
          image: 'shop_stall.png',
          des: 'Get the best deals on brand products',
          onPress: () {
            // allDistrictController.fetchDeliveryCharge();
            // Navigator.pushNamed(context, ShopMainPage.routeName);
          },
        ),
        SizedBox(
          height: 15,
        ),
        CardWidget(
          colors: LinearGradient(colors: [
            Color(0xFF061E3F),
            Color(0xFF093F79),
            Color(0xFF093F79),
            Color(0xFF061E3F)
          ]),
          btnColor: kSecondaryColor,
          btnTextColor: kPrimaryColor,
          textColor: kWhiteColor,
          title: 'Grocery Shop',
          image: 'grocery_stall.png',
          des: 'Get groceries within 30 minutes',
          onPress: () async {
            // /// test mode store///
            // prefs.setString('store_id', 5.toString());
            // Navigator.pushNamed(context, GroceryHome.routeName);
            //
            // /// test mode store end///

            var p = await NetworkServices().fetchShop(context);
            Map<String, dynamic> js = p;
            if (js.containsKey('error')) {
              Navigator.of(context).pop();
              print(p['error']);
              Get.defaultDialog(
                  title: 'No Shop Available'.tr,
                  middleText:
                      'Change your location to see the available shops'.tr,
                  onConfirm: () {
                    Navigator.pop(context);
                  },
                  confirmTextColor: kWhiteColor,
                  buttonColor: kPrimaryColor);
            } else {
              Navigator.of(context).pop();
              store(p, context);
            }
          },
        ),
      ],
    );
  }

  void store(var mat, BuildContext context) async {
    prefs = await SharedPreferences.getInstance();
    // prefs.setString('store_id', 5.toString());
    prefs.setString('store_id', mat['data']['store_id'].toString());
    prefs.setInt('delivery_charge', int.parse(mat['data']['delivery_charge']));
    print('store id ' + prefs.getString('store_id'));
    Get.offAllNamed(DashboardScreen.routeName);
  }
}
