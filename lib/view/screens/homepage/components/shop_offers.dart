import 'package:familytrust/controllers/shop_controlers/shoppopular_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopproductdetails_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/homepage/wigets/popularproductwidget.dart';
import 'package:familytrust/view/screens/shop_page/products/shopproductdetails.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

class ShopOffers extends StatelessWidget {
  final shopPopularController = Get.put(ShopPopularController());
  final shopProductDetailsController = Get.put(ShopProductDetailsController());
  final lng = LocalizationService().getCurrentLang();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        if (shopPopularController.isLoading.isTrue) {
          return Center(
            child: CustomLoader(),
          );
        } else {
          if (shopPopularController.shopPopular.data.isEmpty) {
            return SizedBox();
          } else if (shopPopularController.shopPopular.data == null) {
            return SizedBox();
          } else {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    'Shop Popular Deals',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: getProportionateScreenWidth(130),
                  child: StaggeredGridView.countBuilder(
                    crossAxisCount: 1,
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: shopPopularController.shopPopular.data.length,
                    physics: ScrollPhysics(),
                    staggeredTileBuilder: (int index) =>
                        new StaggeredTile.fit(1),
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                    itemBuilder: (BuildContext context, int index) {
                      return PopularProductWidget(
                        onPress: () async {
                          await shopProductDetailsController
                              .fetchShopProductDetails(shopPopularController
                                  .shopPopular.data[index].id
                                  .toString());
                          Navigator.pushNamed(
                            context,
                            ShopProductDetails.routeName,
                          );
                        },
                        id: shopPopularController.shopPopular.data[index].id
                            .toString(),
                        picture: shopPopularController
                            .shopPopular.data[index].thumbnailImg,
                        title: lng == 'Bangla'
                            ? shopPopularController
                                .shopPopular.data[index].bnName
                            : shopPopularController
                                .shopPopular.data[index].name,
                        stock: shopPopularController
                            .shopPopular.data[index].currentStock
                            .toString(),
                        price: shopPopularController
                                    .shopPopular.data[index].discountType ==
                                null
                            ? (lng == 'Bangla'
                                ? '৳${shopPopularController.shopPopular.data[index].bnPrice}'
                                : '৳${shopPopularController.shopPopular.data[index].price}')
                            : (lng == 'Bangla'
                                ? '৳${shopPopularController.shopPopular.data[index].bnDiscountAmount}'
                                : '৳${shopPopularController.shopPopular.data[index].discountAmount.round()}'),
                      );
                    },
                  ),
                ),
              ],
            );
          }
        }
      },
    );
  }
}
