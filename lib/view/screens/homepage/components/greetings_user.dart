import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/main.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GreetingUser extends StatelessWidget {
  const GreetingUser({Key key}) : super(key: key);

  List greetingMessage() {
    var timeNow = DateTime.now().hour;

    if ((timeNow > 6) && (timeNow < 12)) {
      return [
        'Morning'.tr,
        'MText'.tr,
        'morning.png',
      ];
    } else if ((timeNow >= 12) && (timeNow <= 16)) {
      return [
        'Afternoon'.tr,
        'AText'.tr,
        'afternoon.png',
      ];
    } else if ((timeNow > 16) && (timeNow < 20)) {
      return [
        'Evening'.tr,
        'EText'.tr,
        'evening.png',
      ];
    } else {
      return [
        'Night'.tr,
        'NText'.tr,
        'night.png',
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: ListTile(
        contentPadding: EdgeInsets.all(5),
        title: Padding(
          padding: const EdgeInsets.symmetric(vertical: 5),
          child: RichText(
              text: TextSpan(
            children: [
              TextSpan(
                text: greetingMessage()[0],
                style: TextStyle(color: kPrimaryColor),
              ),
              TextSpan(
                text: prefs.containsKey('uid')
                    ? (prefs.getString('name') != null
                        ? prefs.getString('name')
                        : prefs.getString('phone'))
                    : 'Guest',
                style: TextStyle(color: kPrimaryColor),
              ),
            ],
          )),
        ),
        subtitle: Text(greetingMessage()[1]),
        trailing: Image.asset('$kImageDir${greetingMessage()[2]}'),
      ),
    );
  }
}
