import 'package:familytrust/controllers/grocery_controllers/grocerypopular_controller.dart';
import 'package:familytrust/controllers/grocery_controllers/productdetails_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/grocery_page/sheared/product_details.dart';
import 'package:familytrust/view/screens/homepage/wigets/popularproductwidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

class GroceryOffers extends StatelessWidget {
  final groceryPopularController = Get.put(GroceryPopularController());
  final productDetailsController = Get.put(ProductDetailsController());
  final lng = LocalizationService().getCurrentLang();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        if (groceryPopularController.isLoading.isTrue) {
          return Center(
            child: CustomLoader(),
          );
        } else {
          if (groceryPopularController.groceryPopular.data.isEmpty) {
            return SizedBox();
          }
          if (groceryPopularController.groceryPopular.data == null) {
            return SizedBox();
          } else {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    'Grocery Popular Deals',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: getProportionateScreenWidth(130),
                  child: StaggeredGridView.countBuilder(
                    crossAxisCount: 1,
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount:
                        groceryPopularController.groceryPopular.data.length,
                    physics: ScrollPhysics(),
                    staggeredTileBuilder: (int index) =>
                        new StaggeredTile.fit(1),
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                    itemBuilder: (BuildContext context, int index) {
                      return PopularProductWidget(
                        onPress: () async {
                          await productDetailsController.fetchProductDetails(
                              groceryPopularController
                                  .groceryPopular.data[index].id
                                  .toString());
                          Navigator.pushNamed(
                            context,
                            ProductDetails.routeName,
                          );
                        },
                        id: groceryPopularController
                            .groceryPopular.data[index].id
                            .toString(),
                        picture: groceryPopularController
                            .groceryPopular.data[index].picture,
                        title: lng == 'Bangla'
                            ? groceryPopularController
                                .groceryPopular.data[index].bnProductName
                            : groceryPopularController
                                .groceryPopular.data[index].productName,
                        price: groceryPopularController
                                    .groceryPopular.data[index].discountType ==
                                null
                            ? (lng == 'Bangla'
                                ? '৳${groceryPopularController.groceryPopular.data[index].bnPrice}'
                                : '৳${groceryPopularController.groceryPopular.data[index].price}')
                            : (lng == 'Bangla'
                                ? '৳${groceryPopularController.groceryPopular.data[index].bnDiscountAmount}'
                                : '৳${groceryPopularController.groceryPopular.data[index].discountAmount.round()}'),
                      );
                    },
                  ),
                ),
              ],
            );
          }
        }
      },
    );
  }
}
