import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:flutter/material.dart';

class CardWidget extends StatelessWidget {
  final String title;
  final String image;
  final String des;
  final Function onPress;
  final LinearGradient colors;
  final Color textColor;
  final Color btnColor;
  final Color btnTextColor;

  CardWidget(
      {@required this.des,
      @required this.title,
      @required this.image,
      @required this.colors,
      @required this.textColor,
      @required this.btnColor,
      @required this.btnTextColor,
      @required this.onPress});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          gradient: colors,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        height: SizeConfig.screenWidth / 2.4,
        child: Row(
          children: [
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      des,
                      style: TextStyle(color: textColor),
                      maxLines: 2,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: btnColor,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      width: SizeConfig.screenWidth / 3,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              'Visit Shop',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: btnTextColor),
                            ),
                          ),
                          Icon(
                            Icons.arrow_forward,
                            color: btnTextColor,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                  child: Image.asset(
                    kImageDir + image,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
