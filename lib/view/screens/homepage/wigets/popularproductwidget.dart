import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:flutter/material.dart';

class PopularProductWidget extends StatelessWidget {
  final String id;
  final Function onPress;
  final String title;
  final String price;
  final String picture;
  final String stock;
  PopularProductWidget({
    this.id,
    this.onPress,
    this.title,
    this.stock,
    this.picture,
    this.price,
  });
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Card(
        elevation: 2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: Container(
                width: getProportionateScreenWidth(75),
                child: Hero(
                  tag: id,
                  child: CachedNetworkImage(
                    imageUrl: kImageUrl + picture,
                    fit: BoxFit.fill,
                    placeholder: (context, url) => CustomLoader(),
                    errorWidget: (context, url, error) => Icon(
                      Icons.error,
                      color: Colors.red,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Text(
                title,
                maxLines: 1,
                style: kSmallText,
              ),
            ),
            // Padding(
            //   padding: const EdgeInsets.symmetric(horizontal: 10),
            //   child: Text(
            //     'Stock : ' + stock,
            //     style: kSmallText,
            //   ),
            // ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    price,
                    style: kSmallText.copyWith(color: kErrorColor),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
