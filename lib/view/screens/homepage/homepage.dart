import 'dart:developer';

import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/grocery_controllers/grocerypopular_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shoppopular_controller.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/view/widgets/custom_drawer.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/homepage/components/body.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_place_picker_mb/google_maps_place_picker.dart';
import 'package:new_version/new_version.dart';
import 'package:permission_handler/permission_handler.dart';

class HomePage extends StatefulWidget {
  static final kInitialPosition = LatLng(23.6850, 90.3563);
  static const routeName = 'home_page';
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final shopPopularController = Get.put(ShopPopularController());
  final groceryPopularController = Get.put(GroceryPopularController());
  var _locationMessage = "";
  var addresses;
  var first;
  var lanLong;
  PickResult selectedPlace;

  @override
  void initState() {
    _checkVersion();
    print(prefs.getString('uid'));
    print(prefs.getString('email'));
    print(prefs.getString('address'));
    print(prefs.getString('name'));
    print(prefs.getString('token'));
    shopPopularController.fetchShopPopular();
    if (prefs.containsKey('store_id')) {
      groceryPopularController.fetchGroceryPopular();
    }
    getPermission();
    super.initState();
  }

  void _checkVersion() async {
    final newVersion = NewVersion();
    final status = await newVersion.getVersionStatus();

    if (status.canUpdate) {
      newVersion.showUpdateDialog(
        context: context,
        versionStatus: status,
        dialogTitle: 'Update Available'.tr,
        updateButtonText: 'Update Now'.tr,
        dismissButtonText: 'Later'.tr,
        dialogText: 'Update Details'.tr,
        dismissAction: () {
          Get.back();
        },
      );
    }
    log("DEVICE : " + status.localVersion);
    log("STORE : " + status.storeVersion);
  }

  Future<bool> _onBackPressed() {
    return Get.defaultDialog(
      title: 'Are you sure?',
      content: Text('Do you want to exit!'),
      actions: <Widget>[
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: kPrimaryColor,
          ),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
          child: Text("Confirm"),
        ),
        OutlinedButton(
          onPressed: () {
            Navigator.of(context).pop(false);
          },
          child: Text(
            "Cancel",
            style: TextStyle(color: kPrimaryColor),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final _scaffoldKey = GlobalKey<ScaffoldState>();
    SizeConfig().init(context);
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              _scaffoldKey.currentState.openDrawer();
            },
            icon: Icon(Icons.menu),
          ),
          title: InkWell(
            onTap: () async {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return PlacePicker(
                      apiKey: 'AIzaSyDJu-xdHXPpyEE-MhyXUdwJIXRBO4pY2b8',
                      initialPosition: HomePage.kInitialPosition,
                      useCurrentLocation: true,
                      selectInitialPosition: true,

                      //usePlaceDetailSearch: true,
                      onPlacePicked: (result) {
                        setState(() {
                          selectedPlace = result;
                          _locationMessage = selectedPlace.formattedAddress;
                          prefs.setString('address', _locationMessage);
                        });
                        // get_area(selectedPlace.geometry.location.lat,
                        //     selectedPlace.geometry.location.lng);
                        print('map working!!!!!!!!!!!!');
                        Navigator.of(context).pop();
                      },
                      forceSearchOnZoomChanged: true,
                      automaticallyImplyAppBarLeading: false,
                    );
                  },
                ),
              );
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.location_on,
                      size: 16,
                    ),
                    Flexible(
                      child: prefs.getString('address') != 'null'
                          ? _locationMessage == ''
                              ? Text(
                                  'Searching...',
                                  style: TextStyle(fontSize: 15),
                                )
                              : Text(
                                  _locationMessage.toString(),
                                  style: TextStyle(fontSize: 15),
                                )
                          : Text(
                              'Searching...',
                              style: TextStyle(fontSize: 15),
                            ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      'Current Location',
                      style: TextStyle(fontSize: 12),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Icon(
                      Icons.edit,
                      size: 14,
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
        drawer: CustomDrawer(),
        body: SingleChildScrollView(
          child: Body(),
        ),
      ),
    );
  }

  Future<void> getPermission() async {
    final status = await Permission.locationWhenInUse.request();
    if (status == PermissionStatus.granted) {
      _getCurrentLocation();
      log('Permission Granted');
    } else if (status == PermissionStatus.denied) {
      log('Permission denied');
    } else if (status == PermissionStatus.permanentlyDenied) {
      log('Permission Permanently Denied');
      await openAppSettings();
    }
  }

  void _getCurrentLocation() async {
    final position = await Geolocator.getCurrentPosition();
    setState(() {
      lanLong = position;
    });
    List<Placemark> newPlace =
        await placemarkFromCoordinates(position.latitude, position.longitude);
    // this is all you need
    Placemark placeMark = newPlace[0];
    String streetAddress = placeMark.street;
    String locality = placeMark.locality;
    String postalCode = placeMark.postalCode;
    String country = placeMark.country;
    String countryCOde = placeMark.isoCountryCode;
    String city = placeMark.subAdministrativeArea;
    String address = "$streetAddress, $locality $postalCode, $country";
    log(address.toString());
    log(city.toString());
    log(countryCOde.toString());
    log(country.toString());
    setState(() {
      _locationMessage = address;
      prefs.setString('address', _locationMessage);
    });
  }
}
