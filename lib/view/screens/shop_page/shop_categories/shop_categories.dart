import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/shop_controlers/shopcart_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopcat_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/shop_page/products/childcatproducts.dart';
import 'package:familytrust/view/screens/shop_page/products/subcatproducts.dart';
import 'package:familytrust/view/screens/shop_page/search_page/shop_search.dart';
import 'package:familytrust/view/screens/shop_page/shop_cart/shop_cart_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ShopCategoriesScreen extends StatefulWidget {
  static const routeName = 'shop_cat_screen';

  @override
  _ShopCategoriesScreenState createState() => _ShopCategoriesScreenState();
}

class _ShopCategoriesScreenState extends State<ShopCategoriesScreen> {
  final shopCatController = Get.put(ShopCatController());
  final shopCartController = Get.put(ShopCartController());
  final String lng = LocalizationService().getCurrentLang();
  int catIndex;

  @override
  void initState() {
    setState(() {
      catIndex = shopCatController.shopCategories.data[0].id;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Categories'.tr),
        centerTitle: true,
        actions: [
          Stack(
            children: [
              IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  Navigator.pushNamed(context, ShopSearchScreen.routeName);
                },
              ),
            ],
          ),
          Stack(
            children: [
              IconButton(
                icon: Icon(Icons.add_shopping_cart),
                onPressed: () {
                  Navigator.pushNamed(context, ShopCartScreen.routeName);
                },
              ),
              Positioned(
                top: 0,
                right: 8,
                child: Container(
                  padding: EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      color: kSecondaryColor,
                      borderRadius: BorderRadius.circular(10)),
                  child: Obx(
                    () {
                      if (shopCartController.isLoading.isTrue) {
                        return Center(
                          child: CustomLoader(),
                        );
                      } else {
                        return Text(
                          shopCartController.shopCart.length.toString(),
                          style: TextStyle(
                              color: kPrimaryColor,
                              fontWeight: FontWeight.w800),
                        );
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      body: Container(
        width: SizeConfig.screenWidth,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 2,
              child: Obx(
                () {
                  if (shopCatController.isLoading.isTrue) {
                    return Center(
                      child: CustomLoader(),
                    );
                  } else {
                    return GridView.builder(
                      physics: ScrollPhysics(),
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: shopCatController.shopCategories.data.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 1),
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: () async {
                            setState(() {
                              catIndex = index;
                              print(catIndex);
                            });
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: catIndex == index
                                  ? kPrimaryColor.withOpacity(.2)
                                  : kOrdinaryColor,
                              border: Border(
                                left: BorderSide(
                                  width: catIndex == index ? 5 : 0,
                                  color: kPrimaryColor,
                                ),
                              ),
                            ),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 5,
                                ),
                                Expanded(
                                  flex: 4,
                                  child: CachedNetworkImage(
                                    imageUrl: kImageUrl +
                                        shopCatController
                                            .shopCategories.data[index].picture,
                                    fit: BoxFit.cover,
                                    placeholder: (context, url) =>
                                        CustomLoader(),
                                    errorWidget: (context, url, error) => Icon(
                                      Icons.error,
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Expanded(
                                  flex: 3,
                                  child: Text(
                                    lng == 'Bangla'
                                        ? shopCatController.shopCategories
                                            .data[index].bnCategoryName
                                        : shopCatController.shopCategories
                                            .data[index].categoryName,
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  }
                },
              ),
            ),
            shopCatController
                        .shopCategories.data[catIndex].subcategories.length ==
                    0
                ? Expanded(
                    flex: 7,
                    child: Center(
                      child: Text('Empty Categories!'),
                    ),
                  )
                : Expanded(
                    flex: 7,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 10.0,
                      ),
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        itemCount: shopCatController
                            .shopCategories.data[catIndex].subcategories.length,
                        itemBuilder: (context, int index) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              InkWell(
                                onTap: () async {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          SubCatProductsScreen(
                                        id: shopCatController
                                            .shopCategories
                                            .data[catIndex]
                                            .subcategories[index]
                                            .id
                                            .toString(),
                                        title: shopCatController
                                            .shopCategories
                                            .data[catIndex]
                                            .subcategories[index]
                                            .subCategoryName,
                                        bnTitle: shopCatController
                                            .shopCategories
                                            .data[catIndex]
                                            .subcategories[index]
                                            .bnSubCategoryName,
                                      ),
                                    ),
                                  );
                                },
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      lng == 'Bangla'
                                          ? shopCatController
                                              .shopCategories
                                              .data[catIndex]
                                              .subcategories[index]
                                              .bnSubCategoryName
                                          : shopCatController
                                              .shopCategories
                                              .data[catIndex]
                                              .subcategories[index]
                                              .subCategoryName,
                                      style: kHomeTitle,
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios,
                                      size: 20,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              shopCatController
                                          .shopCategories
                                          .data[catIndex]
                                          .subcategories[index]
                                          .childCategories
                                          .length !=
                                      0
                                  ? Container(
                                      height: getProportionateScreenWidth(100),
                                      child: GridView.builder(
                                        physics: ScrollPhysics(),
                                        shrinkWrap: true,
                                        scrollDirection: Axis.horizontal,
                                        itemCount: shopCatController
                                            .shopCategories
                                            .data[catIndex]
                                            .subcategories[index]
                                            .childCategories
                                            .length,
                                        gridDelegate:
                                            SliverGridDelegateWithFixedCrossAxisCount(
                                                crossAxisCount: 1),
                                        itemBuilder:
                                            (BuildContext context, int index2) {
                                          return InkWell(
                                            onTap: () async {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      ChildCatProductsScreen(
                                                    id: shopCatController
                                                        .shopCategories
                                                        .data[catIndex]
                                                        .subcategories[index]
                                                        .childCategories[index2]
                                                        .id
                                                        .toString(),
                                                    title: shopCatController
                                                        .shopCategories
                                                        .data[catIndex]
                                                        .subcategories[index]
                                                        .childCategories[index2]
                                                        .childCategoryName,
                                                    bnTitle: shopCatController
                                                        .shopCategories
                                                        .data[catIndex]
                                                        .subcategories[index]
                                                        .childCategories[index2]
                                                        .bnChildCategoryName,
                                                  ),
                                                ),
                                              );
                                            },
                                            child: Card(
                                              elevation: 1,
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                              ),
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                    getProportionateScreenWidth(
                                                        15),
                                                  ),
                                                ),
                                                child: Column(
                                                  children: [
                                                    Expanded(
                                                      flex: 3,
                                                      child: CachedNetworkImage(
                                                        imageUrl: kImageUrl +
                                                            shopCatController
                                                                .shopCategories
                                                                .data[catIndex]
                                                                .subcategories[
                                                                    index]
                                                                .childCategories[
                                                                    index2]
                                                                .picture,
                                                        fit: BoxFit.cover,
                                                        placeholder:
                                                            (context, url) =>
                                                                CustomLoader(),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            Icon(
                                                          Icons.error,
                                                          color: Colors.red,
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    Expanded(
                                                      flex: 2,
                                                      child: Text(
                                                        lng == 'Bangla'
                                                            ? shopCatController
                                                                .shopCategories
                                                                .data[catIndex]
                                                                .subcategories[
                                                                    index]
                                                                .childCategories[
                                                                    index2]
                                                                .bnChildCategoryName
                                                            : shopCatController
                                                                .shopCategories
                                                                .data[catIndex]
                                                                .subcategories[
                                                                    index]
                                                                .childCategories[
                                                                    index2]
                                                                .childCategoryName,
                                                        textAlign:
                                                            TextAlign.center,
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    )
                                  : SizedBox(),
                              SizedBox(
                                height: 10,
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
