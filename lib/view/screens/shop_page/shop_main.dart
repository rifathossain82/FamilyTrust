import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/view/screens/profile/profile_screen.dart';
import 'package:familytrust/view/screens/shop_page/home_page/home_page.dart';
import 'package:familytrust/view/screens/shop_page/shop_categories/shop_categories.dart';
import 'package:familytrust/view/screens/shop_page/wislist/wish_page_screen.dart';
import 'package:flutter/material.dart';

class ShopMainPage extends StatefulWidget {
  static const routeName = 'shop_main_page';
  @override
  _ShopMainPageState createState() => _ShopMainPageState();
}

class _ShopMainPageState extends State<ShopMainPage> {
  int _selectedIndex = 0;
  final List<Widget> _taps = [
    ShopHomeScreen(),
    WishScreen(),
    ShopCategoriesScreen(),
    ProfileScreen(
      backActive: false,
      centerTitle: true,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (_selectedIndex == 0) {
          Navigator.pop(context);
        } else {
          setState(() {
            _selectedIndex = 0;
          });
        }
        return false;
      },
      child: Scaffold(
        body: _taps[_selectedIndex],
        bottomNavigationBar: BottomNavigationBar(
          elevation: 5,
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              label: 'Favorite',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.category),
              label: 'Categories',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Profile',
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: kPrimaryColor,
          unselectedItemColor: Colors.grey,
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}
