import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/shop_controlers/shopfavorite_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopproductdetails_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/shop_page/products/shopproductdetails.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WishScreen extends StatefulWidget {
  static const routeName = 'wish_screen';

  @override
  _WishScreenState createState() => _WishScreenState();
}

class _WishScreenState extends State<WishScreen> {
  final shopFavoriteController = Get.put(ShopFavoriteController());

  final shopProductDetailsController = Get.put(ShopProductDetailsController());

  final String lng = LocalizationService().getCurrentLang();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Favorite'.tr),
        centerTitle: true,
      ),
      body: Obx(
        () {
          if (shopFavoriteController.isLoading.isTrue) {
            return Center(
              child: CustomLoader(),
            );
          } else {
            if (shopFavoriteController.shopFavorite.isEmpty) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.favorite_border,
                      size: getProportionateScreenHeight(50),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Empty Favorite'.tr,
                      style: TextStyle(
                        fontSize: getProportionateScreenHeight(30),
                      ),
                    ),
                  ],
                ),
              );
            } else {
              return ListView.builder(
                  physics: ScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: shopFavoriteController.shopFavorite.length,
                  itemBuilder: (context, int index) {
                    return InkWell(
                      onTap: () async {
                        await shopProductDetailsController
                            .fetchShopProductDetails(shopFavoriteController
                                .shopFavorite[index].id
                                .toString());
                        Navigator.pushNamed(
                          context,
                          ShopProductDetails.routeName,
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Container(
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Container(
                                          child: Hero(
                                            tag: shopFavoriteController
                                                .shopFavorite[index].id
                                                .toString(),
                                            child: CachedNetworkImage(
                                              imageUrl: kImageUrl +
                                                  shopFavoriteController
                                                      .shopFavorite[index]
                                                      .picture,
                                              fit: BoxFit.fill,
                                              placeholder: (context, url) =>
                                                  CustomLoader(),
                                              errorWidget:
                                                  (context, url, error) => Icon(
                                                Icons.error,
                                                color: Colors.red,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 5,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            lng == 'Bangla'
                                                ? shopFavoriteController
                                                    .shopFavorite[index]
                                                    .bnProductName
                                                : shopFavoriteController
                                                    .shopFavorite[index]
                                                    .productName,
                                            style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                            ),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                lng == 'Bangla'
                                                    ? '৳${shopFavoriteController.shopFavorite[index].bnPrice}  '
                                                    : '৳${shopFavoriteController.shopFavorite[index].price}  ',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  color: kPrimaryColor,
                                                  fontSize: 16,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.delete,
                                          color: Colors.red,
                                        ),
                                        onPressed: () {
                                          Get.defaultDialog(
                                            title: 'Delete this item?'.tr,
                                            middleText:
                                                'this action will delete this item from your favorite'
                                                    .tr,
                                            onConfirm: () async {
                                              shopFavoriteController
                                                  .removeFromFavorite(
                                                      shopFavoriteController
                                                          .shopFavorite[index]
                                                          .id);

                                              Navigator.pop(context);
                                            },
                                            buttonColor: kPrimaryColor,
                                            confirmTextColor: kWhiteColor,
                                            onCancel: () {},
                                          );
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                        ),
                      ),
                    );
                  });
            }
          }
        },
      ),
    );
  }
}
