import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/shop_controlers/shopcart_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopproductdetails_controller.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/auth/login.dart';
import 'package:familytrust/view/screens/shop_page/checkout/shop_checkout.dart';
import 'package:familytrust/view/screens/shop_page/products/shopproductdetails.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ShopCartScreen extends StatefulWidget {
  static const routeName = 'shop_cart_screen';

  @override
  _ShopCartScreenState createState() => _ShopCartScreenState();
}

class _ShopCartScreenState extends State<ShopCartScreen> {
  final String lng = LocalizationService().getCurrentLang();
  final shopCartController = Get.put(ShopCartController());
  final shopProductDetailsController = Get.put(ShopProductDetailsController());
  int total = 0;

  void initState() {
    if (shopCartController.isClosed) {
      shopCartController.onStart();
    }
    for (int i = 0; i < shopCartController.shopCart.length; i++) {
      total += shopCartController.shopCart[i].price *
          shopCartController.shopCart[i].qty;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cart'.tr),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 9,
            child: Obx(
              () {
                if (shopCartController.isLoading.isTrue) {
                  return Center(
                    child: CustomLoader(),
                  );
                } else {
                  if (shopCartController.shopCart.isEmpty) {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.remove_shopping_cart,
                            size: getProportionateScreenHeight(50),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'Empty Cart'.tr,
                            style: TextStyle(
                              fontSize: getProportionateScreenHeight(30),
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return ListView.builder(
                        physics: ScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: shopCartController.shopCart.length,
                        itemBuilder: (context, int index) {
                          return InkWell(
                            onTap: () async {
                              await shopProductDetailsController
                                  .fetchShopProductDetails(shopCartController
                                      .shopCart[index].id
                                      .toString());
                              Navigator.pushNamed(
                                context,
                                ShopProductDetails.routeName,
                              );
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Container(
                                      child: Row(
                                        children: [
                                          Expanded(
                                            flex: 2,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(10.0),
                                              child: Container(
                                                child: Hero(
                                                  tag: shopCartController
                                                      .shopCart[index].id
                                                      .toString(),
                                                  child: CachedNetworkImage(
                                                    imageUrl: kImageUrl +
                                                        shopCartController
                                                            .shopCart[index]
                                                            .picture,
                                                    fit: BoxFit.fill,
                                                    placeholder:
                                                        (context, url) =>
                                                            CustomLoader(),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Icon(
                                                      Icons.error,
                                                      color: Colors.red,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 5,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Flexible(
                                                      child: Text(
                                                        lng == 'Bangla'
                                                            ? shopCartController
                                                                .shopCart[index]
                                                                .bnProductName
                                                            : shopCartController
                                                                .shopCart[index]
                                                                .productName,
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w500,
                                                        ),
                                                        maxLines: 2,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                      ),
                                                    ),
                                                    shopCartController
                                                            .shopCart[index]
                                                            .color
                                                            .isEmpty
                                                        ? SizedBox()
                                                        : Text('Color: ' +
                                                            shopCartController
                                                                .shopCart[index]
                                                                .color),
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Row(
                                                  children: [
                                                    Text(
                                                      lng == 'Bangla'
                                                          ? '৳${shopCartController.shopCart[index].bnPrice} x ${shopCartController.shopCart[index].qty}'
                                                          : '৳${shopCartController.shopCart[index].price} x ${shopCartController.shopCart[index].qty}',
                                                    ),
                                                    Spacer(),
                                                    shopCartController
                                                                .shopCart[index]
                                                                .attribute ==
                                                            '{}'
                                                        ? SizedBox()
                                                        : Text(
                                                            shopCartController
                                                                .shopCart[index]
                                                                .attribute
                                                                .replaceAll(
                                                                    '{', '')
                                                                .replaceAll(
                                                                    '}', '')
                                                                .replaceAll(
                                                                    '"', ''),
                                                          ),
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Row(
                                                  children: [
                                                    Text(
                                                      '৳${shopCartController.shopCart[index].price * shopCartController.shopCart[index].qty}',
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        color: kPrimaryColor,
                                                        fontSize: 16,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Spacer(),
                                                    IconButton(
                                                      icon: Icon(
                                                        Icons.delete,
                                                        color: Colors.red,
                                                      ),
                                                      onPressed: () {
                                                        Get.defaultDialog(
                                                          title:
                                                              'Delete this item?'
                                                                  .tr,
                                                          middleText:
                                                              'this action will delete this item from your cart'
                                                                  .tr,
                                                          onConfirm: () async {
                                                            shopCartController
                                                                .removeFromCart(
                                                                    shopCartController
                                                                        .shopCart[
                                                                            index]
                                                                        .id);
                                                            setState(() {
                                                              total -= shopCartController
                                                                      .shopCart[
                                                                          index]
                                                                      .price *
                                                                  shopCartController
                                                                      .shopCart[
                                                                          index]
                                                                      .qty;
                                                            });
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                          buttonColor:
                                                              kPrimaryColor,
                                                          confirmTextColor:
                                                              kWhiteColor,
                                                          onCancel: () {},
                                                        );
                                                      },
                                                    ),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Spacer(),
                                                    Row(
                                                      children: [
                                                        InkWell(
                                                          onTap: () {
                                                            setState(() {
                                                              if (shopCartController
                                                                      .shopCart[
                                                                          index]
                                                                      .qty >
                                                                  1) {
                                                                shopCartController
                                                                    .shopCart[
                                                                        index]
                                                                    .qty = shopCartController
                                                                        .shopCart[
                                                                            index]
                                                                        .qty -
                                                                    1;

                                                                total -=
                                                                    shopCartController
                                                                        .shopCart[
                                                                            index]
                                                                        .price;
                                                              }
                                                            });
                                                          },
                                                          child: Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          15.0),
                                                              color:
                                                                  kOrdinaryColor,
                                                            ),
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(5.0),
                                                              child: Icon(
                                                                  Icons.remove),
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: 10,
                                                        ),
                                                        Text(
                                                          shopCartController
                                                              .shopCart[index]
                                                              .qty
                                                              .toString(),
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500,
                                                              fontSize: 18),
                                                        ),
                                                        SizedBox(
                                                          width: 10,
                                                        ),
                                                        InkWell(
                                                          onTap: () {
                                                            int length = jsonDecode(
                                                                    shopCartController
                                                                        .shopCart[
                                                                            index]
                                                                        .maxQty)
                                                                .length;

                                                            if (shopCartController
                                                                    .shopCart[
                                                                        index]
                                                                    .qty <
                                                                jsonDecode(shopCartController
                                                                        .shopCart[
                                                                            index]
                                                                        .maxQty)[
                                                                    length -
                                                                        1]) {
                                                              setState(() {
                                                                shopCartController
                                                                    .shopCart[
                                                                        index]
                                                                    .qty = shopCartController
                                                                        .shopCart[
                                                                            index]
                                                                        .qty +
                                                                    1;

                                                                total +=
                                                                    shopCartController
                                                                        .shopCart[
                                                                            index]
                                                                        .price;
                                                              });
                                                            } else {
                                                              Get.snackbar(
                                                                  'Maximum Quantity Crossed',
                                                                  '',
                                                                  colorText:
                                                                      kWhiteColor);
                                                            }
                                                          },
                                                          child: Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          15.0),
                                                              color:
                                                                  kOrdinaryColor,
                                                            ),
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(5.0),
                                                              child: Icon(
                                                                  Icons.add),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    )),
                              ),
                            ),
                          );
                        });
                  }
                }
              },
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              decoration: BoxDecoration(
                color: kPrimaryColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15),
                  topRight: Radius.circular(15),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      width: SizeConfig.screenWidth / 3,
                      child: RichText(
                        text: TextSpan(
                          text: 'Total'.tr,
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w500),
                          children: [
                            TextSpan(
                              text: '৳' + total.toString(),
                              style: TextStyle(
                                  color: kSecondaryColor,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        if (prefs.containsKey('uid')) {
                          if (shopCartController.shopCart.length < 1) {
                            Get.snackbar('Empty', 'you have an empty cart',
                                colorText: kWhiteColor);
                          } else {
                            prefs.setInt('total_shop', total);
                            Navigator.pushNamed(
                                context, ShopCheckOut.routeName);
                          }
                        } else {
                          Get.to(
                            () => LogIn(
                              isOff: true,
                              address: prefs.getString('address'),
                            ),
                          );
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          color: kSecondaryColor,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        width: SizeConfig.screenWidth / 2.2,
                        child: Text(
                          'Confirm Order'.tr,
                          style: TextStyle(
                              color: kPrimaryColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
