import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/shop_controlers/shoporderlist_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/screens/grocery_page/checkout/widgets/row_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class ShopOrderList extends StatefulWidget {
  @override
  _ShopOrderListState createState() => _ShopOrderListState();
}

class _ShopOrderListState extends State<ShopOrderList> {
  final shopOrderController = Get.put(ShopOrderController());
  final String lng = LocalizationService().getCurrentLang();
  int selected;
  @override
  void initState() {
    shopOrderController.fetchOrderList();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () {
          if (shopOrderController.isLoading.isTrue) {
            return Center(
              child: CustomLoader(),
            );
          } else {
            return ListView.builder(
                key: Key('builder ${selected.toString()}'),
                physics: ScrollPhysics(),
                shrinkWrap: true,
                itemCount: shopOrderController.shopOrderList.data.length,
                itemBuilder: (context, index) {
                  return Card(
                    child: ExpansionTile(
                      key: Key(index.toString()),
                      initiallyExpanded: index == selected,
                      onExpansionChanged: ((newState) {
                        if (newState)
                          setState(() {
                            selected = index;
                          });
                        else
                          setState(() {
                            selected = -1;
                          });
                      }),
                      backgroundColor: kSecondaryColor.withOpacity(.2),
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Container(
                                child: SelectableText(
                                  '#FTS${shopOrderController.shopOrderList.data[index].total}',
                                  style: TextStyle(
                                      color: kPrimaryColor,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                child: Text(
                                  '৳ ${shopOrderController.shopOrderList.data[index].total}',
                                  style: TextStyle(
                                      color: kSaleColor,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: kSecondaryColor,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    shopOrderController.shopOrderList
                                            .data[index].paymentType ??
                                        '',
                                    style: TextStyle(
                                        color: kWhiteColor,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                child: Text(
                                  DateFormat.yMMMMd().format(
                                    shopOrderController
                                        .shopOrderList.data[index].orderDate,
                                  ),
                                  style: TextStyle(
                                      color: kPrimaryColor,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      children: orderListExpendableBuilder(index),
                    ),
                  );
                });
          }
        },
      ),
    );
  }

  orderListExpendableBuilder(int id) {
    List<Widget> columnContent = [];
    [1].forEach((product) => {
          columnContent.add(
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Text(
                      'Delivery Location: ${shopOrderController.shopOrderList.data[id].address}, ${shopOrderController.shopOrderList.data[id].city}, ${shopOrderController.shopOrderList.data[id].postcode}'),
                ),
                for (int i = 0;
                    i <
                        shopOrderController
                            .shopOrderList.data[id].orderProducts.length;
                    i++)
                  Card(
                    child: ListTile(
                      leading: Container(
                        width: 50,
                        height: 50,
                        child: CachedNetworkImage(
                          imageUrl: kImageUrl +
                              shopOrderController.shopOrderList.data[id]
                                  .orderProducts[i].thumbnailImg,
                          fit: BoxFit.fitHeight,
                          placeholder: (context, url) => CustomLoader(),
                          errorWidget: (context, url, error) => Image.asset(
                            kImageDir + 'placeholder.png',
                          ),
                        ),
                      ),
                      title: Text(
                        lng == 'Bangla'
                            ? shopOrderController
                                .shopOrderList.data[id].orderProducts[i].bnName
                            : shopOrderController
                                .shopOrderList.data[id].orderProducts[i].name,
                        maxLines: 2,
                      ),
                      subtitle: Text(
                          '${shopOrderController.shopOrderList.data[id].orderProducts[i].qty} X ৳${shopOrderController.shopOrderList.data[id].orderProducts[i].price}'),
                      trailing: Text(
                        '৳' +
                            shopOrderController
                                .shopOrderList.data[id].orderProducts[i].total
                                .toString(),
                      ),
                    ),
                  ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RowWidget(
                    title: 'Sub Total',
                    amount: shopOrderController.shopOrderList.data[id].subtotal
                        .toString(),
                  ),
                ),
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: RowWidget(
                //     title: 'Discount',
                //     amount: shopOrderController
                //         .shopOrderList.data[id].discount
                //         .round()
                //         .toString(),
                //   ),
                // ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RowWidget(
                    title: 'Delivery Charge',
                    amount: shopOrderController
                        .shopOrderList.data[id].shippingCharge
                        .toString(),
                  ),
                ),
                Divider(
                  thickness: 1,
                  color: kPrimaryColor,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RowWidget(
                    title: 'Total',
                    amount: shopOrderController.shopOrderList.data[id].total
                        .round()
                        .toString(),
                  ),
                ),
              ],
            ),
          ),
        });
    return columnContent;
  }
}
