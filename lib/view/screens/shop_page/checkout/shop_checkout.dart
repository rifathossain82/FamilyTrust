import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/shop_controlers/alldistrict_controller.dart';
import 'package:familytrust/controllers/shop_controlers/area_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopcart_controller.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/widgets/default_btn.dart';
import 'package:familytrust/view/widgets/input_form_widget.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/grocery_page/checkout/widgets/row_widget.dart';
import 'package:familytrust/view/screens/shop_page/shop_main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sslcommerz/model/SSLCAdditionalInitializer.dart';
import 'package:flutter_sslcommerz/model/SSLCCustomerInfoInitializer.dart';
import 'package:flutter_sslcommerz/model/SSLCEMITransactionInitializer.dart';
import 'package:flutter_sslcommerz/model/SSLCSdkType.dart';
import 'package:flutter_sslcommerz/model/SSLCShipmentInfoInitializer.dart';
import 'package:flutter_sslcommerz/model/SSLCTransactionInfoModel.dart';
import 'package:flutter_sslcommerz/model/SSLCommerzInitialization.dart';
import 'package:flutter_sslcommerz/model/SSLCurrencyType.dart';
import 'package:flutter_sslcommerz/sslcommerz.dart';
import 'package:get/get.dart';

class ShopCheckOut extends StatefulWidget {
  static const routeName = 'shop_checkout';
  final bool buyNow;
  ShopCheckOut({this.buyNow = false});
  @override
  _ShopCheckOutState createState() => _ShopCheckOutState();
}

class _ShopCheckOutState extends State<ShopCheckOut> {
  final _formKey = GlobalKey<FormState>();
  final String lng = LocalizationService().getCurrentLang();
  final shopCartController = Get.put(ShopCartController());
  final shopDeliveryController = Get.put(AllDistrictController());
  final allDistrictController = Get.put(AllDistrictController());
  final areaController = Get.put(AreaController());
  TextEditingController nameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController numberController = TextEditingController();
  int tranId = DateTime.now().millisecondsSinceEpoch;
  dynamic formData = {};
  int selected = 1;
  String city;
  int cityId;
  String area;
  int areaId;
  var discount = 0;
  var voucher = 0;
  int deliveryCharge = 0;

  @override
  void dispose() {
    nameController.dispose();
    addressController.dispose();
    numberController.dispose();
    cityController.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void initState() {
    allDistrictController.tempName = allDistrictController.districtName;
    if (widget.buyNow == true) {
      deliveryCharge = shopCartController.tempCart.deliveryCharge;
    } else {
      totalDeliveryCharge();
    }
    area = areaController.areaName;
    for (int i = 0; i < areaController.allArea.data.length; i++) {
      if (area == areaController.allArea.data[i].areaName) {
        setState(() {
          areaId = areaController.allArea.data[i].id;
        });
      }
    }
    print('areaID: $areaId');
    super.initState();
  }

  void totalDeliveryCharge() {
    List maxQty = [];
    List shipping = [];
    int val;
    for (int i = 0; i < shopCartController.shopCart.length; i++) {
      setState(() {
        cityId = shopCartController.shopCart[i].cityId;
      });
      maxQty = jsonDecode(shopCartController.shopCart[i].maxQty);
      shipping = jsonDecode(shopCartController.shopCart[i].shippingCharge);
      for (int j = 0; j < maxQty.length; j++) {
        if (shopCartController.shopCart[i].qty <= maxQty[j]) {
          print('index: $j');
          val = j;
          break;
        }
      }
      setState(() {
        deliveryCharge += shipping[val];
      });
    }
    print(deliveryCharge);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text(
            "Checkout",
            style: TextStyle(color: Colors.white),
          ),
          /* iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),*/
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 9,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          widget.buyNow == false
                              ? ListView.builder(
                                  physics: ScrollPhysics(),
                                  scrollDirection: Axis.vertical,
                                  shrinkWrap: true,
                                  itemCount: shopCartController.shopCart.length,
                                  itemBuilder: (context, int index) {
                                    return Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Card(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                        child: Padding(
                                            padding: const EdgeInsets.all(10.0),
                                            child: Container(
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    flex: 2,
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              10.0),
                                                      child: Container(
                                                        child: Hero(
                                                          tag:
                                                              shopCartController
                                                                  .shopCart[
                                                                      index]
                                                                  .id
                                                                  .toString(),
                                                          child:
                                                              CachedNetworkImage(
                                                            imageUrl: kImageUrl +
                                                                shopCartController
                                                                    .shopCart[
                                                                        index]
                                                                    .picture,
                                                            fit: BoxFit.fill,
                                                            placeholder: (context,
                                                                    url) =>
                                                                CustomLoader(),
                                                            errorWidget:
                                                                (context, url,
                                                                        error) =>
                                                                    Icon(
                                                              Icons.error,
                                                              color: Colors.red,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 5,
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            Flexible(
                                                              child: Text(
                                                                lng == 'Bangla'
                                                                    ? shopCartController
                                                                        .shopCart[
                                                                            index]
                                                                        .bnProductName
                                                                    : shopCartController
                                                                        .shopCart[
                                                                            index]
                                                                        .productName,
                                                                style:
                                                                    TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                ),
                                                                maxLines: 2,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                              ),
                                                            ),
                                                            shopCartController
                                                                    .shopCart[
                                                                        index]
                                                                    .color
                                                                    .isEmpty
                                                                ? SizedBox()
                                                                : Text('Color: ' +
                                                                    shopCartController
                                                                        .shopCart[
                                                                            index]
                                                                        .color),
                                                          ],
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Row(
                                                          children: [
                                                            Text(
                                                              lng == 'Bangla'
                                                                  ? '৳${shopCartController.shopCart[index].bnPrice} x ${shopCartController.shopCart[index].qty}'
                                                                  : '৳${shopCartController.shopCart[index].price} x ${shopCartController.shopCart[index].qty}',
                                                            ),
                                                            Spacer(),
                                                            shopCartController
                                                                        .shopCart[
                                                                            index]
                                                                        .attribute ==
                                                                    '{}'
                                                                ? SizedBox()
                                                                : Text(
                                                                    shopCartController
                                                                        .shopCart[
                                                                            index]
                                                                        .attribute
                                                                        .replaceAll(
                                                                            '{', '')
                                                                        .replaceAll(
                                                                            '}',
                                                                            '')
                                                                        .replaceAll(
                                                                            '"',
                                                                            ''),
                                                                  ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )),
                                      ),
                                    );
                                  })
                              : Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Card(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    child: Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Container(
                                          child: Row(
                                            children: [
                                              Expanded(
                                                flex: 2,
                                                child: Padding(
                                                  padding: const EdgeInsets.all(
                                                      10.0),
                                                  child: Container(
                                                    child: Hero(
                                                      tag: shopCartController
                                                          .tempCart.id
                                                          .toString(),
                                                      child: CachedNetworkImage(
                                                        imageUrl: kImageUrl +
                                                            shopCartController
                                                                .tempCart
                                                                .picture,
                                                        fit: BoxFit.fill,
                                                        placeholder:
                                                            (context, url) =>
                                                                CustomLoader(),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            Icon(
                                                          Icons.error,
                                                          color: Colors.red,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 5,
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Flexible(
                                                          child: Text(
                                                            lng == 'Bangla'
                                                                ? shopCartController
                                                                    .tempCart
                                                                    .bnProductName
                                                                : shopCartController
                                                                    .tempCart
                                                                    .productName,
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500,
                                                            ),
                                                            maxLines: 2,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                          ),
                                                        ),
                                                        shopCartController
                                                                .tempCart
                                                                .color
                                                                .isEmpty
                                                            ? SizedBox()
                                                            : Text('Color: ' +
                                                                shopCartController
                                                                    .tempCart
                                                                    .color),
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    Row(
                                                      children: [
                                                        Text(
                                                          lng == 'Bangla'
                                                              ? '৳${shopCartController.tempCart.bnPrice} x ${shopCartController.tempCart.qty}'
                                                              : '৳${shopCartController.tempCart.price} x ${shopCartController.tempCart.qty}',
                                                        ),
                                                        Spacer(),
                                                        shopCartController
                                                                    .tempCart
                                                                    .attribute ==
                                                                '{}'
                                                            ? SizedBox()
                                                            : Text(
                                                                shopCartController
                                                                    .tempCart
                                                                    .attribute
                                                                    .replaceAll(
                                                                        '{', '')
                                                                    .replaceAll(
                                                                        '}', '')
                                                                    .replaceAll(
                                                                        '"',
                                                                        ''),
                                                              ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        )),
                                  ),
                                ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            'Price Details'.tr,
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          RowWidget(
                            title: 'Product Price'.tr,
                            amount: widget.buyNow == false
                                ? prefs.getInt('total_shop').toString()
                                : shopCartController.tempCart.price.toString(),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          RowWidget(
                            title: 'Delivery Charge'.tr,
                            amount: deliveryCharge.toString(),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          RowWidget(
                            title: 'Discount'.tr,
                            amount: discount.toString(),
                            amountStyle: TextStyle(color: Colors.red),
                          ),
                          Divider(),
                          widget.buyNow == false
                              ? RowWidget(
                                  title: 'Total'.tr,
                                  amount:
                                      '${prefs.getInt('total_shop') + deliveryCharge + discount + voucher}',
                                  titleStyle:
                                      TextStyle(fontWeight: FontWeight.w600),
                                  amountStyle:
                                      TextStyle(fontWeight: FontWeight.w600),
                                )
                              : RowWidget(
                                  title: 'Total'.tr,
                                  amount:
                                      '${shopCartController.tempCart.price + deliveryCharge + discount + voucher}',
                                  titleStyle:
                                      TextStyle(fontWeight: FontWeight.w600),
                                  amountStyle:
                                      TextStyle(fontWeight: FontWeight.w600),
                                )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Divider(
                      thickness: 5,
                      color: Color(0xFFF6F6F6),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              'User Details'.tr,
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w600),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            InputFormWidget(
                              fieldController: numberController,
                              labelText: 'Mobile Number',
                              hintText: '01XXXXXXXXX',
                              keyType: TextInputType.number,
                              validation: (value) {
                                if (value.isEmpty) {
                                  return kInvalidNumberError;
                                } else if (value.length < 11) {
                                  return kInvalidNumberError;
                                }
                                return null;
                              },
                            ),
                            InputFormWidget(
                              fieldController: nameController,
                              labelText: 'Full Name',
                              hintText: 'Enter your full name',
                              keyType: TextInputType.name,
                              validation: (value) {
                                if (value.isEmpty) {
                                  return kNameNullError;
                                }
                                return null;
                              },
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text('District'),
                            InkWell(
                              onTap: () {
                                Get.bottomSheet(
                                  StatefulBuilder(builder:
                                      (BuildContext context2,
                                          StateSetter setState) {
                                    return Container(
                                      decoration: BoxDecoration(
                                          color: kWhiteColor,
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(30),
                                            topRight: Radius.circular(30),
                                          )),
                                      child: SingleChildScrollView(
                                        child: Column(
                                          children: [
                                            Container(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 0),
                                              child: Text(
                                                'Choose your District',
                                                style: kRegularText,
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 10),
                                              child: InputFormWidget(
                                                labelText: 'Search',
                                                hintText: 'Enter District Name',
                                                keyType: TextInputType.text,
                                                onSaved: (val) {
                                                  val = val.toLowerCase();
                                                  setState(() {
                                                    allDistrictController
                                                            .tempName =
                                                        allDistrictController
                                                            .districtName
                                                            .where((element) {
                                                      var temp =
                                                          element.toLowerCase();
                                                      return temp.contains(val);
                                                    }).toList();
                                                  });
                                                },
                                              ),
                                            ),
                                            ListView.builder(
                                              shrinkWrap: true,
                                              physics: ScrollPhysics(),
                                              itemCount: allDistrictController
                                                  .tempName.length,
                                              itemBuilder:
                                                  (context, int index) {
                                                return InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      city =
                                                          allDistrictController
                                                              .tempName[index];
                                                      allDistrictController
                                                          .changeName(
                                                              allDistrictController
                                                                      .tempName[
                                                                  index]);
                                                    });
                                                    print(city);
                                                    Navigator.pop(context2);
                                                    for (int i = 0;
                                                        i <
                                                            allDistrictController
                                                                .allDistrict
                                                                .data
                                                                .length;
                                                        i++) {
                                                      if (city ==
                                                          allDistrictController
                                                              .allDistrict
                                                              .data[i]
                                                              .name) {
                                                        setState(() {
                                                          cityId =
                                                              allDistrictController
                                                                  .allDistrict
                                                                  .data[i]
                                                                  .id;
                                                          areaController.fetchArea(
                                                              allDistrictController
                                                                  .allDistrict
                                                                  .data[i]
                                                                  .id
                                                                  .toString());
                                                        });
                                                      }
                                                    }

                                                    setState(() {
                                                      allDistrictController
                                                              .tempName =
                                                          allDistrictController
                                                              .districtName;

                                                      // areaController
                                                      //     .tempName = [];
                                                    });
                                                  },
                                                  child: ListTile(
                                                    title: Text(
                                                      allDistrictController
                                                          .tempName[index],
                                                    ),
                                                  ),
                                                );
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  }),
                                );
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5),
                                child: Container(
                                  height: getProportionateScreenWidth(50),
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    border: Border.all(color: kPrimaryColor),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10),
                                        child: Obx(
                                          () {
                                            if (allDistrictController
                                                .isLoading.isTrue) {
                                              return Center(
                                                child: CustomLoader(),
                                              );
                                            } else {
                                              return Text(
                                                allDistrictController.cityName,
                                                style: kRegularText,
                                                textAlign: TextAlign.center,
                                              );
                                            }
                                          },
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10),
                                        child: Icon(Icons.arrow_drop_down),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text('Upazila/City'),
                            InkWell(
                              onTap: () {
                                Get.bottomSheet(
                                  StatefulBuilder(builder:
                                      (BuildContext context2,
                                          StateSetter setState) {
                                    return Container(
                                      decoration: BoxDecoration(
                                          color: kWhiteColor,
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(30),
                                            topRight: Radius.circular(30),
                                          )),
                                      child: SingleChildScrollView(
                                        child: Column(
                                          children: [
                                            Container(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 0),
                                              child: Text(
                                                'Choose your Area',
                                                style: kRegularText,
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                            // Padding(
                                            //   padding: EdgeInsets.symmetric(
                                            //       horizontal: 10),
                                            //   child: InputFormWidget(
                                            //     labelText: 'Search',
                                            //     hintText: 'Enter Area Name',
                                            //     keyType: TextInputType.text,
                                            //     onSaved: (val) {
                                            //       val = val.toLowerCase();
                                            //       setState(() {
                                            //         areaController.tempName =
                                            //             areaController
                                            //                 .areaNameList
                                            //                 .where((element) {
                                            //           var temp =
                                            //               element.toLowerCase();
                                            //           return temp.contains(val);
                                            //         }).toList();
                                            //       });
                                            //     },
                                            //   ),
                                            // ),
                                            ListView.builder(
                                              shrinkWrap: true,
                                              physics: ScrollPhysics(),
                                              itemCount: areaController
                                                  .allArea.data.length,
                                              itemBuilder:
                                                  (context, int index) {
                                                return InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      area = areaController
                                                          .allArea
                                                          .data[index]
                                                          .areaName;
                                                      areaId = areaController
                                                          .allArea
                                                          .data[index]
                                                          .id;
                                                      areaController.changeName(
                                                          areaController
                                                              .allArea
                                                              .data[index]
                                                              .areaName);
                                                      // areaController.changeName(
                                                      //     allDistrictController
                                                      //         .tempName[index]);
                                                    });
                                                    print(area);
                                                    print(areaId);
                                                    Navigator.pop(context2);
                                                    // for (int i = 0;
                                                    //     i <
                                                    //         areaController.allArea
                                                    //             .data.length;
                                                    //     i++) {
                                                    //   if (area ==
                                                    //       areaController.allArea
                                                    //           .data[i].areaName) {
                                                    //     setState(() {
                                                    //       areaId = areaController
                                                    //           .allArea.data[i].id;
                                                    //     });
                                                    //   }
                                                    // }
                                                    //
                                                    // print(areaId);
                                                    // setState(() {
                                                    //   allDistrictController
                                                    //           .tempName =
                                                    //       allDistrictController
                                                    //           .districtName;
                                                    // });
                                                  },
                                                  child: ListTile(
                                                    title: Text(
                                                      areaController.allArea
                                                          .data[index].areaName,
                                                    ),
                                                  ),
                                                );
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  }),
                                );
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5),
                                child: Container(
                                  height: getProportionateScreenWidth(50),
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    border: Border.all(color: kPrimaryColor),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 8,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 10),
                                            child: Obx(
                                              () {
                                                if (areaController
                                                    .isLoading.isTrue) {
                                                  return Center(
                                                    child: CustomLoader(),
                                                  );
                                                } else {
                                                  return Text(
                                                    area,
                                                    style: kRegularText,
                                                    textAlign: TextAlign.center,
                                                  );
                                                }
                                              },
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10),
                                          child: Icon(Icons.arrow_drop_down),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            InputFormWidget(
                              fieldController: cityController,
                              labelText: 'Area',
                              hintText: 'Enter Area',
                              keyType: TextInputType.streetAddress,
                              validation: (value) {
                                if (value.isEmpty) {
                                  return kAreaNullError;
                                }
                                return null;
                              },
                            ),
                            InputFormWidget(
                              fieldController: addressController,
                              labelText: 'Full Address',
                              hintText: 'Road no: , House no: ',
                              keyType: TextInputType.streetAddress,
                              validation: (value) {
                                if (value.isEmpty) {
                                  return kAddressNullError;
                                }
                                return null;
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    // Divider(
                    //   thickness: 5,
                    //   color: Color(0xFFF6F6F6),
                    // ),
                    // SizedBox(
                    //   height: 10,
                    // ),
                    // PromoWidget(
                    //   title: 'ADD PROMO'.tr,
                    //   icon: Icons.pages_rounded,
                    // ),
                    // SizedBox(
                    //   height: 10,
                    // ),
                    // Divider(
                    //   thickness: 5,
                    //   color: Color(0xFFF6F6F6),
                    // ),
                    // SizedBox(
                    //   height: 10,
                    // ),
                    // PromoWidget(
                    //   title: 'ADD VOUCHER'.tr,
                    //   icon: Icons.verified_rounded,
                    // ),
                    // SizedBox(
                    //   height: 10,
                    // ),
                    // Divider(
                    //   thickness: 5,
                    //   color: Color(0xFFF6F6F6),
                    // ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 15),
                            child: Text(
                              'Payment Methods'.tr,
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w600),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    selected = 1;
                                  });
                                },
                                child: Container(
                                  width: SizeConfig.screenWidth / 2.4,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: selected == 1
                                        ? kSecondaryColor
                                        : kWhiteColor,
                                    border: Border.all(
                                      color: kPrimaryColor,
                                      width: 2,
                                    ),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                      vertical: 15, horizontal: 10),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'COD',
                                        style: kRegularText.copyWith(
                                          fontWeight: FontWeight.w600,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                      Radio(
                                        materialTapTargetSize:
                                            MaterialTapTargetSize.shrinkWrap,
                                        activeColor: kPrimaryColor,
                                        value: 1,
                                        groupValue: selected,
                                        onChanged: (val) {},
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    selected = 2;
                                  });
                                },
                                child: Container(
                                  width: SizeConfig.screenWidth / 2.4,
                                  decoration: BoxDecoration(
                                    color: selected == 2
                                        ? kSecondaryColor
                                        : kWhiteColor,
                                    borderRadius: BorderRadius.circular(5),
                                    border: Border.all(
                                      color: kPrimaryColor,
                                      width: 2,
                                    ),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                      vertical: 15, horizontal: 10),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Online Pay',
                                        style: kRegularText.copyWith(
                                          fontWeight: FontWeight.w600,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                      Radio(
                                        materialTapTargetSize:
                                            MaterialTapTargetSize.shrinkWrap,
                                        activeColor: kPrimaryColor,
                                        value: 2,
                                        groupValue: selected,
                                        onChanged: (val) {},
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(5),
                width: SizeConfig.screenWidth,
                child: DefaultBtn(
                  title: 'Confirm Order'.tr,
                  onPress: () async {
                    if (_formKey.currentState.validate()) {
                      if (selected == 1) {
                        completeOrder(payType: 'Cash On Delivery');
                      } else {
                        print('online payment system');
                        sslCommercePayment();
                      }
                    }
                  },
                ),
              ),
            ),
          ],
        ));
  }

  Future<void> sslCommercePayment() async {
    Sslcommerz sslCommerce = Sslcommerz(
      initializer: SSLCommerzInitialization(
        ipn_url: "https://familytrust.com.bd/ipn",
        multi_card_name: formData['multicard'],
        currency: SSLCurrencyType.BDT,
        product_category: "Products",

        ///SSL SandBox ///
        // sdkType: SSLCSdkType.TESTBOX,
        // store_id: 'testi6119f4717446f',
        // store_passwd: 'testi6119f4717446f@ssl',
        ///SSL SandBox End///

        ///SSL LIVE ///
        sdkType: SSLCSdkType.LIVE,
        store_id: 'familytrustshoplive',
        store_passwd: '611B8BFD7581488684',

        ///SSL LIVE End///

        total_amount: widget.buyNow == false
            ? double.parse(
                '${prefs.getInt('total_shop') + deliveryCharge - discount - voucher}')
            : double.parse(
                '${shopCartController.tempCart.price + deliveryCharge - discount - voucher}'),
        tran_id: tranId.toString(),
      ),
    );
    sslCommerce
        .addEMITransactionInitializer(
          sslcemiTransactionInitializer: SSLCEMITransactionInitializer(
            emi_options: 1,
            emi_max_list_options: 3,
            emi_selected_inst: 2,
          ),
        )
        .addShipmentInfoInitializer(
          sslcShipmentInfoInitializer: SSLCShipmentInfoInitializer(
            shipmentMethod: "yes",
            numOfItems:
                widget.buyNow == false ? shopCartController.shopCart.length : 1,
            shipmentDetails: ShipmentDetails(
              shipAddress1: addressController.text,
              shipCity: city,
              shipCountry: 'Bangladesh',
              shipName: 'Ship name 1',
              shipPostCode: '1100',
            ),
          ),
        )
        .addCustomerInfoInitializer(
          customerInfoInitializer: SSLCCustomerInfoInitializer(
            customerName: nameController.text,
            customerEmail: '',
            customerAddress1: addressController.text,
            customerState: city,
            customerCity: city,
            customerPostCode: '1100',
            customerCountry: 'Bangladesh',
            customerPhone: numberController.text,
          ),
        )
        .addAdditionalInitializer(
          sslcAdditionalInitializer: SSLCAdditionalInitializer(
            valueA: "app",
            valueB: "value b",
            valueC: "value c",
            valueD: "value d",
          ),
        );
    var result = await sslCommerce.payNow();
    if (result is PlatformException) {
      print("the response is: " + result.status);
      Get.snackbar('Transaction failed!', '', colorText: kWhiteColor);
    } else {
      SSLCTransactionInfoModel model = result;
      print("ssL json" + model.toJson().toString());
      if (model.aPIConnect == 'DONE' && model.status == 'VALID') {
        Get.snackbar('Payment success', '', colorText: kWhiteColor);
        completeOrder(payType: 'SSL Commerz');
      } else {
        Get.snackbar('Payment Failed', '', colorText: kWhiteColor);
      }
    }
  }

  void completeOrder({String payType}) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => CustomLoader());
    List product = [];
    List tempProduct = [];
    if (widget.buyNow == false) {
      for (int i = 0; i < shopCartController.shopCart.length; i++) {
        product.add({
          "product_id": shopCartController.shopCart[i].id,
          "price": shopCartController.shopCart[i].price,
          "qty": shopCartController.shopCart[i].qty,
          "color": shopCartController.shopCart[i].color,
          "total_price": shopCartController.shopCart[i].price *
              shopCartController.shopCart[i].qty,
          "attribute": [shopCartController.shopCart[i].attribute],
        });
        setState(() {
          cityId = shopCartController.shopCart[i].cityId;
        });
      }
    } else {
      tempProduct.add({
        "product_id": shopCartController.tempCart.id,
        "price": shopCartController.tempCart.price,
        "qty": shopCartController.tempCart.qty,
        "color": shopCartController.tempCart.color,
        "total_price":
            shopCartController.tempCart.price * shopCartController.tempCart.qty,
        "attribute": [shopCartController.tempCart.attribute],
      });
    }
    print('products print: ' + product.toString());
    print('total price: ${prefs.getInt('total_shop')}');
    print('Temp products print: ' + tempProduct.toString());
    var body = {
      "user_id": prefs.getString('uid'),
      // "location": prefs.getString('address'),
      "tran_id": tranId,
      "firstname": nameController.text.length == 0
          ? prefs.getString('name')
          : nameController.text,
      "lastname": "",
      "contact_number": nameController.text.length == 0
          ? prefs.getString('phone')
          : numberController.text,
      "address": addressController.text,
      "total": widget.buyNow == false
          ? prefs.getInt('total_shop') + deliveryCharge - discount - voucher
          : shopCartController.tempCart.price +
              deliveryCharge -
              discount -
              voucher,
      "payment_type": payType,
      "currency": "BDT",
      "shipping_type": 1,
      "subtotal": widget.buyNow == false
          ? prefs.getInt('total_shop')
          : shopCartController.tempCart.price,
      "shipping_charge": deliveryCharge,
      "discount": discount,
      "city": cityController.text,
      "area_id": areaId,
      "postcode": "",
      "district_id":
          widget.buyNow == false ? cityId : shopCartController.tempCart.cityId,
      "order_products": widget.buyNow == false ? product : tempProduct,
    };
    print(body);
    var p = await NetworkServices().submitShopOrder(body);
    Map<String, dynamic> js = p;
    if (js.containsKey('error')) {
      Navigator.of(context).pop();
      print(p['error']);
      Get.defaultDialog(
          title: 'Error',
          middleText: p['error'],
          confirmTextColor: kWhiteColor,
          onConfirm: () {
            Navigator.pop(context);
          });
    } else {
      Navigator.of(context).pop();
      Get.defaultDialog(
          barrierDismissible: false,
          title: 'Successful',
          middleText: p['message'],
          buttonColor: kPrimaryColor,
          confirmTextColor: kWhiteColor,
          onConfirm: () {
            if (widget.buyNow == false) {
              shopCartController.emptyCart();
            } else {
              shopCartController.tempCart = null;
            }
            Navigator.pushNamedAndRemoveUntil(
                context, ShopMainPage.routeName, (route) => false);
          });
    }
  }
}
