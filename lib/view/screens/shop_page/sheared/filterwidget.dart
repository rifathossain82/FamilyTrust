import 'package:familytrust/utils/constants.dart';
import 'package:flutter/material.dart';

class FilterWidgets extends StatelessWidget {
  final String title;
  final Function onPress;
  final IconData icon;
  FilterWidgets({this.title, this.onPress, this.icon});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: kWhiteColor,
        ),
        padding: EdgeInsets.all(8),
        child: Row(
          children: [
            Text(
              title,
              style: kDescriptionText.copyWith(color: kPrimaryColor),
            ),
            Icon(icon),
          ],
        ),
      ),
    );
  }
}
