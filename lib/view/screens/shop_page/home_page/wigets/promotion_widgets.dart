import 'package:familytrust/utils/size_config.dart';
import 'package:flutter/material.dart';

class PromotionWidgets extends StatelessWidget {
  final IconData icon;
  final String title;
  final Color color;
  PromotionWidgets({
    this.title,
    this.color,
    this.icon,
  });
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          alignment: Alignment.center,
          decoration:
              BoxDecoration(color: Colors.black12, shape: BoxShape.circle),
          child: Padding(
            padding: EdgeInsets.all(
              getProportionateScreenWidth(15.0),
            ),
            child: Icon(
              icon,
              color: color,
              size: getProportionateScreenWidth(30.0),
            ),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(title),
      ],
    );
  }
}
