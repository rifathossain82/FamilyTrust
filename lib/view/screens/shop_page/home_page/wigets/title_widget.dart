import 'package:familytrust/utils/constants.dart';
import 'package:flutter/material.dart';

class TitleWidgets extends StatelessWidget {
  final String title;
  final String seeMore;
  final Function onPress;

  TitleWidgets({
    @required this.title,
    this.onPress,
    this.seeMore,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: kHomeTitle,
          ),
          InkWell(
            onTap: onPress,
            child: Text(
              seeMore,
              style: TextStyle(
                  color: kPrimaryColor,
                  fontWeight: FontWeight.w600,
                  fontSize: 16),
            ),
          ),
        ],
      ),
    );
  }
}
