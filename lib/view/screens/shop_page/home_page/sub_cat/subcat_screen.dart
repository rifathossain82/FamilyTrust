import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/shop_controlers/shopcart_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopcat_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopcatbrandlist_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/shop_page/products/childcatproducts.dart';
import 'package:familytrust/view/screens/shop_page/products/subcatproducts.dart';
import 'package:familytrust/view/screens/shop_page/search_page/shop_search.dart';
import 'package:familytrust/view/screens/shop_page/shop_cart/shop_cart_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SubCatScreen extends StatelessWidget {
  static const routeName = 'sub_cat_screen';
  final shopCatController = Get.put(ShopCatController());
  final shopCartController = Get.put(ShopCartController());
  final shopCatBrandListController = Get.put(ShopCatBrandListController());
  final String lng = LocalizationService().getCurrentLang();
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text(lng == 'Bangla'
            ? shopCatController.shopCategories.data[args].bnCategoryName
            : shopCatController.shopCategories.data[args].categoryName),
        actions: [
          Stack(
            children: [
              IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  Navigator.pushNamed(context, ShopSearchScreen.routeName);
                },
              ),
            ],
          ),
          Stack(
            children: [
              IconButton(
                icon: Icon(Icons.add_shopping_cart),
                onPressed: () {
                  Navigator.pushNamed(context, ShopCartScreen.routeName);
                },
              ),
              Positioned(
                top: 0,
                right: 8,
                child: Container(
                  padding: EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      color: kSecondaryColor,
                      borderRadius: BorderRadius.circular(10)),
                  child: Obx(
                    () {
                      if (shopCartController.isLoading.isTrue) {
                        return Center(
                          child: CustomLoader(),
                        );
                      } else {
                        return Text(
                          shopCartController.shopCart.length.toString(),
                          style: TextStyle(
                              color: kPrimaryColor,
                              fontWeight: FontWeight.w800),
                        );
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      body: Obx(
        () {
          if (shopCatController.isLoading.isTrue) {
            return Center(
              child: CustomLoader(),
            );
          } else {
            return shopCatController
                        .shopCategories.data[args].subcategories.length ==
                    0
                ? SizedBox()
                : SingleChildScrollView(
                    child: Column(
                      children: [
                        Stack(
                          clipBehavior: Clip.none,
                          fit: StackFit.passthrough,
                          children: [
                            // Container(
                            //   width: double.infinity,
                            //   child: Image.asset(
                            //     kImageDir + 'banner.png',
                            //     fit: BoxFit.cover,
                            //   ),
                            // ),
                            AspectRatio(
                              aspectRatio: 3,
                              child: CachedNetworkImage(
                                imageUrl: kImageUrl +
                                    shopCatController
                                        .shopCategories.data[args].banerPicture,
                                fit: BoxFit.fill,
                                placeholder: (context, url) => CustomLoader(),
                                errorWidget: (context, url, error) =>
                                    AspectRatio(
                                  aspectRatio: 3,
                                  child: Image.network(
                                    'https://familytrust.com.bd/uploads/slider/6069b1f42633e.png',
                                    fit: BoxFit.fill,
                                    width: double.infinity,
                                  ),
                                ),
                              ),
                            ),

                            Positioned(
                              bottom: getProportionateScreenWidth(-60),
                              left: SizeConfig.screenWidth / 3.3,
                              child: Container(
                                decoration: BoxDecoration(
                                  color: kWhiteColor,
                                  borderRadius: BorderRadius.circular(
                                      getProportionateScreenWidth(30)),
                                  border: Border.all(
                                    color: kWhiteColor,
                                    width: 5,
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 2,
                                      blurRadius: 7,
                                      offset: Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ],
                                ),
                                width: getProportionateScreenWidth(120),
                                height: getProportionateScreenWidth(120),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(
                                    getProportionateScreenWidth(30),
                                  ),
                                  child: CachedNetworkImage(
                                    imageUrl: kImageUrl +
                                        shopCatController
                                            .shopCategories.data[args].picture,
                                    placeholder: (context, url) =>
                                        CustomLoader(),
                                    errorWidget: (context, url, error) => Icon(
                                      Icons.error,
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: SizeConfig.screenWidth / 4,
                        ),
                        Text(
                          lng == 'Bangla'
                              ? shopCatController
                                  .shopCategories.data[args].bnCategoryName
                              : shopCatController
                                  .shopCategories.data[args].categoryName,
                          style: kHomeTitle,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          height: getProportionateScreenWidth(120),
                          margin: EdgeInsets.only(top: 5, left: 5, right: 5),
                          child: GridView.builder(
                            physics: ScrollPhysics(),
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemCount: shopCatController
                                .shopCategories.data[args].subcategories.length,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 1),
                            itemBuilder: (BuildContext context, int index) {
                              return InkWell(
                                onTap: () async {
                                  shopCatBrandListController.fetchBrandsList(
                                    'sub',
                                    shopCatController.shopCategories.data[args]
                                        .subcategories[index].id
                                        .toString(),
                                  );
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          SubCatProductsScreen(
                                        id: shopCatController.shopCategories
                                            .data[args].subcategories[index].id
                                            .toString(),
                                        title: shopCatController
                                            .shopCategories
                                            .data[args]
                                            .subcategories[index]
                                            .subCategoryName,
                                        bnTitle: shopCatController
                                            .shopCategories
                                            .data[args]
                                            .subcategories[index]
                                            .bnSubCategoryName,
                                      ),
                                    ),
                                  );
                                },
                                child: Card(
                                  elevation: 1,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(
                                        getProportionateScreenWidth(15),
                                      ),
                                    ),
                                    child: Column(
                                      children: [
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Expanded(
                                          flex: 4,
                                          child: CachedNetworkImage(
                                            imageUrl: kImageUrl +
                                                shopCatController
                                                    .shopCategories
                                                    .data[args]
                                                    .subcategories[index]
                                                    .picture,
                                            fit: BoxFit.cover,
                                            placeholder: (context, url) =>
                                                CustomLoader(),
                                            errorWidget:
                                                (context, url, error) => Icon(
                                              Icons.error,
                                              color: Colors.red,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Text(
                                            lng == 'Bangla'
                                                ? shopCatController
                                                    .shopCategories
                                                    .data[args]
                                                    .subcategories[index]
                                                    .bnSubCategoryName
                                                : shopCatController
                                                    .shopCategories
                                                    .data[args]
                                                    .subcategories[index]
                                                    .subCategoryName,
                                            textAlign: TextAlign.center,
                                            maxLines: 2,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        // Container(
                        //   width: double.infinity,
                        //   child: Image.asset(
                        //     kImageDir + 'banner.png',
                        //     fit: BoxFit.cover,
                        //   ),
                        // ),
                        CachedNetworkImage(
                          imageUrl: kImageUrl +
                              shopCatController.shopCategories.data[args]
                                  .subcategories[0].bannerPicture,
                          fit: BoxFit.cover,
                          placeholder: (context, url) => CustomLoader(),
                          errorWidget: (context, url, error) => Image.network(
                            'https://familytrust.com.bd/uploads/slider/6069b1f42633e.png',
                            fit: BoxFit.cover,
                            width: double.infinity,
                          ),
                        ),

                        // for(int i=0;i<shopCatController
                        //     .shopCategories.data[args].subcategories.length;i++){
                        //
                        // }

                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 20),
                          child: ListView.builder(
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            itemCount: shopCatController
                                .shopCategories.data[args].subcategories.length,
                            itemBuilder: (context, int index) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  InkWell(
                                    onTap: () async {
                                      shopCatBrandListController
                                          .fetchBrandsList(
                                        'sub',
                                        shopCatController.shopCategories
                                            .data[args].subcategories[index].id
                                            .toString(),
                                      );
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              SubCatProductsScreen(
                                            id: shopCatController
                                                .shopCategories
                                                .data[args]
                                                .subcategories[index]
                                                .id
                                                .toString(),
                                            title: shopCatController
                                                .shopCategories
                                                .data[args]
                                                .subcategories[index]
                                                .subCategoryName,
                                            bnTitle: shopCatController
                                                .shopCategories
                                                .data[args]
                                                .subcategories[index]
                                                .bnSubCategoryName,
                                          ),
                                        ),
                                      );
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          lng == 'Bangla'
                                              ? shopCatController
                                                  .shopCategories
                                                  .data[args]
                                                  .subcategories[index]
                                                  .bnSubCategoryName
                                              : shopCatController
                                                  .shopCategories
                                                  .data[args]
                                                  .subcategories[index]
                                                  .subCategoryName,
                                          style: kHomeTitle,
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios,
                                          size: 20,
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  shopCatController
                                              .shopCategories
                                              .data[args]
                                              .subcategories[index]
                                              .childCategories
                                              .length !=
                                          0
                                      ? Container(
                                          height:
                                              getProportionateScreenWidth(120),
                                          child: GridView.builder(
                                            physics: ScrollPhysics(),
                                            shrinkWrap: true,
                                            scrollDirection: Axis.horizontal,
                                            itemCount: shopCatController
                                                .shopCategories
                                                .data[args]
                                                .subcategories[index]
                                                .childCategories
                                                .length,
                                            gridDelegate:
                                                SliverGridDelegateWithFixedCrossAxisCount(
                                                    crossAxisCount: 1),
                                            itemBuilder: (BuildContext context,
                                                int index2) {
                                              return InkWell(
                                                onTap: () async {
                                                  shopCatBrandListController
                                                      .fetchBrandsList(
                                                    'child',
                                                    shopCatController
                                                        .shopCategories
                                                        .data[args]
                                                        .subcategories[index]
                                                        .childCategories[index2]
                                                        .id
                                                        .toString(),
                                                  );
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          ChildCatProductsScreen(
                                                        id: shopCatController
                                                            .shopCategories
                                                            .data[args]
                                                            .subcategories[
                                                                index]
                                                            .childCategories[
                                                                index2]
                                                            .id
                                                            .toString(),
                                                        title: shopCatController
                                                            .shopCategories
                                                            .data[args]
                                                            .subcategories[
                                                                index]
                                                            .childCategories[
                                                                index2]
                                                            .childCategoryName,
                                                        bnTitle: shopCatController
                                                            .shopCategories
                                                            .data[args]
                                                            .subcategories[
                                                                index]
                                                            .childCategories[
                                                                index2]
                                                            .bnChildCategoryName,
                                                      ),
                                                    ),
                                                  );
                                                },
                                                child: Card(
                                                  elevation: 1,
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10.0),
                                                  ),
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                        getProportionateScreenWidth(
                                                            15),
                                                      ),
                                                    ),
                                                    child: Column(
                                                      children: [
                                                        SizedBox(
                                                          height: 5,
                                                        ),
                                                        Expanded(
                                                          flex: 4,
                                                          child:
                                                              CachedNetworkImage(
                                                            imageUrl: kImageUrl +
                                                                shopCatController
                                                                    .shopCategories
                                                                    .data[args]
                                                                    .subcategories[
                                                                        index]
                                                                    .childCategories[
                                                                        index2]
                                                                    .picture,
                                                            fit: BoxFit.cover,
                                                            placeholder: (context,
                                                                    url) =>
                                                                CustomLoader(),
                                                            errorWidget:
                                                                (context, url,
                                                                        error) =>
                                                                    Icon(
                                                              Icons.error,
                                                              color: Colors.red,
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Expanded(
                                                          flex: 2,
                                                          child: Text(
                                                            lng == 'Bangla'
                                                                ? shopCatController
                                                                    .shopCategories
                                                                    .data[args]
                                                                    .subcategories[
                                                                        index]
                                                                    .childCategories[
                                                                        index2]
                                                                    .bnChildCategoryName
                                                                : shopCatController
                                                                    .shopCategories
                                                                    .data[args]
                                                                    .subcategories[
                                                                        index]
                                                                    .childCategories[
                                                                        index2]
                                                                    .childCategoryName,
                                                            textAlign: TextAlign
                                                                .center,
                                                            maxLines: 2,
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                        )
                                      : SizedBox(),
                                  SizedBox(
                                    height: 20,
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  );
          }
        },
      ),
    );
  }
}
