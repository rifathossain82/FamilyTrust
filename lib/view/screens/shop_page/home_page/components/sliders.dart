import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/grocery_controllers/slider_controller.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Sliders extends StatefulWidget {
  @override
  _SlidersState createState() => _SlidersState();
}

class _SlidersState extends State<Sliders> {
  final sliderController = Get.put(SlidersController());
  int sliderIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        if (sliderController.isLoading.isTrue) {
          return Center(
            child: CustomLoader(),
          );
        } else {
          return Container(
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white,
            ),
            alignment: Alignment.centerLeft,
            width: SizeConfig.screenWidth,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CarouselSlider(
                  options: CarouselOptions(
                      aspectRatio: 2.5,
                      viewportFraction: 1,
                      initialPage: 0,
                      enableInfiniteScroll: true,
                      reverse: false,
                      autoPlay: true,
                      autoPlayInterval: Duration(seconds: 5),
                      autoPlayAnimationDuration: Duration(milliseconds: 1000),
                      autoPlayCurve: Curves.easeInCubic,
                      enlargeCenterPage: true,
                      scrollDirection: Axis.horizontal,
                      onPageChanged: (int index, reason) {
                        setState(() {
                          sliderIndex = index;
                        });
                      }),
                  items: sliderController.sliders.data
                      .map(
                        (item) => Stack(
                          alignment: Alignment.bottomCenter,
                          children: [
                            Container(
                              width: double.infinity,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: CachedNetworkImage(
                                  imageUrl: kImageUrl + item.picture,
                                  fit: BoxFit.fill,
                                  placeholder: (context, url) => CustomLoader(),
                                  errorWidget: (context, url, error) => Icon(
                                    Icons.error,
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                            ),
                            ListView.builder(
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                itemCount: sliderController.sliders.data.length,
                                itemBuilder: (context, int index) {
                                  return Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                      width: 11,
                                      height: 11,
                                      margin: EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 5.0),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                          width: 1,
                                          color: kSecondaryColor,
                                        ),
                                        color: sliderIndex == index
                                            ? kPrimaryColor
                                            : kWhiteColor,
                                      ),
                                    ),
                                  );
                                }),
                          ],
                        ),
                      )
                      .toList(),
                ),
              ],
            ),
          );
        }
      },
    );
  }
}
