import 'package:familytrust/view/screens/shop_page/home_page/components/brands.dart';
import 'package:familytrust/view/screens/shop_page/home_page/components/categories.dart';
import 'package:familytrust/view/screens/shop_page/home_page/components/promotions.dart';
import 'package:familytrust/view/screens/shop_page/home_page/components/sliders.dart';
import 'package:familytrust/view/screens/shop_page/home_page/wigets/title_widget.dart';
import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Sliders(),
          Promotions(),
          TitleWidgets(
            title: 'Categories',
            seeMore: '',
          ),
          Categories(),
          SizedBox(
            height: 10,
          ),
          Brands(),
        ],
      ),
    );
  }
}
