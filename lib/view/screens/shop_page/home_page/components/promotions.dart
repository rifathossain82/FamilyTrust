import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/view/screens/shop_page/home_page/wigets/promotion_widgets.dart';
import 'package:flutter/material.dart';

class Promotions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: kOrdinaryColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            PromotionWidgets(
              icon: Icons.local_shipping,
              title: 'Fast Delivery',
              color: kPrimaryColor,
            ),
            PromotionWidgets(
              icon: Icons.monetization_on,
              title: 'Low Price',
              color: kSecondaryColor,
            ),
            PromotionWidgets(
              icon: Icons.branding_watermark,
              title: 'Brands',
              color: kSaleColor,
            ),
            PromotionWidgets(
              icon: Icons.verified,
              title: 'Authentic',
              color: kSuccessColor,
            ),
          ],
        ),
      ),
    );
  }
}
