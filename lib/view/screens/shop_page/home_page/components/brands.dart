import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/shop_controlers/shopbrandproducts_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopcatbrand_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/screens/shop_page/products/brandproducts.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Brands extends StatelessWidget {
  final shopCatBrandController = Get.put(ShopCatBrandController());
  final shopBrandProductsController = Get.put(ShopBrandProductsController());
  final lng = LocalizationService().getCurrentLang();
  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        if (shopCatBrandController.isLoading.isTrue) {
          return Center(
            child: CustomLoader(),
          );
        } else {
          return ListView.builder(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemCount: shopCatBrandController.catBrands.data.length,
            itemBuilder: (context, int index) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Divider(
                    color: kOrdinaryColor,
                    thickness: 5,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      shopCatBrandController.catBrands.data[index].categoryName,
                      style: kHomeTitle,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: GridView.builder(
                      physics: ScrollPhysics(),
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: shopCatBrandController
                          .catBrands.data[index].brands.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3),
                      itemBuilder: (BuildContext context, int index2) {
                        return InkWell(
                          onTap: () async {
                            shopBrandProductsController.fetchShopBrandProducts(
                                shopCatBrandController
                                    .catBrands.data[index].brands[index2].id
                                    .toString(),
                                '1');
                            print(shopCatBrandController
                                .catBrands.data[index].brands[index2].id);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => BrandProducts(
                                  id: shopCatBrandController
                                      .catBrands.data[index].brands[index2].id
                                      .toString(),
                                  name: lng == 'Bangla'
                                      ? shopCatBrandController
                                          .catBrands
                                          .data[index]
                                          .brands[index2]
                                          .bnBrandName
                                      : shopCatBrandController.catBrands
                                          .data[index].brands[index2].brandName,
                                ),
                              ),
                            );
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Color(0xFFF5F5F5),
                                width: 1.0,
                              ),
                            ),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 5,
                                ),
                                Expanded(
                                  flex: 4,
                                  child: CachedNetworkImage(
                                    imageUrl: kImageUrl +
                                        shopCatBrandController
                                            .catBrands
                                            .data[index]
                                            .brands[index2]
                                            .brandPicture,
                                    fit: BoxFit.cover,
                                    placeholder: (context, url) =>
                                        CustomLoader(),
                                    errorWidget: (context, url, error) => Icon(
                                      Icons.error,
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Text(
                                    lng == 'Bangla'
                                        ? shopCatBrandController
                                            .catBrands
                                            .data[index]
                                            .brands[index2]
                                            .bnBrandName
                                        : shopCatBrandController
                                            .catBrands
                                            .data[index]
                                            .brands[index2]
                                            .brandName,
                                    textAlign: TextAlign.center,
                                    maxLines: 2,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              );
            },
          );
        }
      },
    );
  }
}
