import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/shop_controlers/shopcat_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/screens/shop_page/home_page/sub_cat/subcat_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Categories extends StatelessWidget {
  final shopCatController = Get.put(ShopCatController());
  final String lng = LocalizationService().getCurrentLang();
  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        if (shopCatController.isLoading.isTrue) {
          return Center(
            child: CustomLoader(),
          );
        } else {
          return GridView.builder(
            physics: ScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemCount: shopCatController.shopCategories.data.length,
            gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                onTap: () async {
                  Navigator.pushNamed(context, SubCatScreen.routeName, arguments: index);
                },
                child: Container(
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xFFF5F5F5),
                      width: 1.0,
                    ),
                  ),
                  child: CachedNetworkImage(
                    imageUrl: kImageUrl +
                        shopCatController.shopCategories.data[index].picture,
                    fit: BoxFit.cover,
                    placeholder: (context, url) => CustomLoader(),
                    errorWidget: (context, url, error) => Icon(
                      Icons.error,
                      color: Colors.red,
                    ),
                  ),
                  // Column(
                  //   children: [
                  //     SizedBox(
                  //       height: 5,
                  //     ),
                  //     Expanded(
                  //       flex: 4,
                  //       child: CachedNetworkImage(
                  //         imageUrl: kImageUrl +
                  //             shopCatController
                  //                 .shopCategories.data[index].picture,
                  //         fit: BoxFit.cover,
                  //         placeholder: (context, url) => CustomLoader(),
                  //         errorWidget: (context, url, error) => Icon(
                  //           Icons.error,
                  //           color: Colors.red,
                  //         ),
                  //       ),
                  //     ),
                  //     SizedBox(
                  //       height: 10,
                  //     ),
                  //     Expanded(
                  //       flex: 2,
                  //       child: Text(
                  //         lng == 'Bangla'
                  //             ? shopCatController
                  //                 .shopCategories.data[index].bnCategoryName
                  //             : shopCatController
                  //                 .shopCategories.data[index].categoryName,
                  //         textAlign: TextAlign.center,
                  //         maxLines: 2,
                  //         style: TextStyle(fontWeight: FontWeight.w500),
                  //       ),
                  //     ),
                  //   ],
                  // ),
                ),
              );
            },
          );
        }
      },
    );
  }
}
