import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/shop_controlers/shopcart_controller.dart';
import 'package:familytrust/view/widgets/custom_drawer.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/shop_page/search_page/shop_search.dart';
import 'package:familytrust/view/screens/shop_page/shop_cart/shop_cart_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'components/body.dart';

class ShopHomeScreen extends StatelessWidget {
  static const routeName = 'shop_home_screen';
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final shopCartController = Get.put(ShopCartController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: InkWell(
          onTap: () {
            Navigator.pushNamed(context, ShopSearchScreen.routeName);
          },
          child: Container(
            decoration: BoxDecoration(
              color: kWhiteColor,
              borderRadius: BorderRadius.circular(5),
            ),
            padding: EdgeInsets.all(10),
            child: Row(
              children: [
                Icon(
                  Icons.search,
                  size: getProportionateScreenWidth(20),
                  color: kPrimaryColor,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Search Products'.tr,
                  style: TextStyle(
                    color: kPrimaryColor,
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
        ),
        actions: [
          Stack(
            children: [
              IconButton(
                icon: Icon(Icons.add_shopping_cart),
                onPressed: () {
                  Navigator.pushNamed(context, ShopCartScreen.routeName);
                },
              ),
              Positioned(
                top: 0,
                right: 8,
                child: Container(
                  padding: EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      color: kSecondaryColor,
                      borderRadius: BorderRadius.circular(10)),
                  child: Obx(
                    () {
                      if (shopCartController.isLoading.isTrue) {
                        return Center(
                          child: CustomLoader(),
                        );
                      } else {
                        return Text(
                          shopCartController.shopCart.length.toString(),
                          style: TextStyle(
                              color: kPrimaryColor,
                              fontWeight: FontWeight.w800),
                        );
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ],
        leading: IconButton(
          onPressed: () {
            _scaffoldKey.currentState.openDrawer();
          },
          icon: Icon(Icons.menu),
        ),
      ),
      body: Body(),
      drawer: CustomDrawer(),
    );
  }
}
