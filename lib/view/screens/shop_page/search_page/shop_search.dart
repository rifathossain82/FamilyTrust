import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/shop_controlers/shopproductdetails_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopsearch_controller.dart';
import 'package:familytrust/models/shop/rpshopsearch_model.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/screens/shop_page/products/shopproductdetails.dart';
import 'package:familytrust/view/screens/shop_page/products/widgets/product_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

class ShopSearchScreen extends StatefulWidget {
  static const routeName = 'shop_search_screen';

  @override
  _ShopSearchScreenState createState() => _ShopSearchScreenState();
}

class _ShopSearchScreenState extends State<ShopSearchScreen> {
  final _scrollController = ScrollController();
  final TextEditingController valueController = TextEditingController();
  final String lng = LocalizationService().getCurrentLang();
  final shopSearchController = Get.put(ShopSearchController());
  final shopProductDetailsController = Get.put(ShopProductDetailsController());
  var value = '';
  int page = 1;

  @override
  void initState() {
    if (shopSearchController.isClosed) {
      shopSearchController.onStart();
    }
    shopSearchController.fetchShopSearch(value, 1.toString());
    _scrollController.addListener(addProducts);
    super.initState();
  }

  void addProducts() async {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      print('work');
      page++;
      await shopSearchController.fetchShopSearchAdd(value, page.toString());
      if (shopSearchController.noData == true) {
        Get.snackbar('No More Products', '', colorText: kWhiteColor);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        title: TextFormField(
          autofocus: true,
          style: TextStyle(color: kWhiteColor),
          decoration: InputDecoration(
            hintText: 'Search Products'.tr,
            hintStyle: TextStyle(color: kWhiteColor),
            suffix: InkWell(
              onTap: () {
                setState(() {
                  valueController.clear();
                  value = '';
                  shopSearchController.fetchShopSearch(value, '1');
                });
              },
              child: Icon(
                Icons.close,
                color: kWhiteColor,
              ),
            ),
          ),
          onChanged: (val) {
            setState(() {
              shopSearchController.fetchShopSearch(value, '1');
              value = val;
            });
          },
          controller: valueController,
          cursorColor: kWhiteColor,
        ),
      ),
      body: Obx(
        () {
          if (shopSearchController.isLoading.isTrue) {
            return Center(
              child: CustomLoader(),
            );
          } else {
            if (shopSearchController.shopSearchAll.data.isEmpty) {
              return Center(
                child: Text('No Products Available'),
              );
            } else {
              return Container(
                margin: EdgeInsets.only(top: 5, left: 5, right: 5),
                child: StaggeredGridView.countBuilder(
                  controller: _scrollController,
                  crossAxisCount: 2,
                  shrinkWrap: true,
                  itemCount: shopSearchController.shopSearchAll.data.length,
                  physics: ScrollPhysics(),
                  staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
                  mainAxisSpacing: 4.0,
                  crossAxisSpacing: 4.0,
                  itemBuilder: (BuildContext context, int index) {
                    return ProductWidget(
                      onPress: () async {
                        await shopProductDetailsController
                            .fetchShopProductDetails(shopSearchController
                                .shopSearchAll.data[index].id
                                .toString());
                        Navigator.pushNamed(
                          context,
                          ShopProductDetails.routeName,
                        );
                      },
                      id: shopSearchController.shopSearchAll.data[index].id
                          .toString(),
                      picture: shopSearchController
                          .shopSearchAll.data[index].thumbnailImg,
                      title: lng == 'Bangla'
                          ? shopSearchController
                              .shopSearchAll.data[index].bnName
                          : shopSearchController.shopSearchAll.data[index].name,
                      stock: shopSearchController
                          .shopSearchAll.data[index].currentStock
                          .toString(),
                      price: shopSearchController
                                  .shopSearchAll.data[index].discountType ==
                              null
                          ? (lng == 'Bangla'
                              ? '৳${shopSearchController.shopSearchAll.data[index].bnPrice}'
                              : '৳${shopSearchController.shopSearchAll.data[index].price}')
                          : (lng == 'Bangla'
                              ? '৳${shopSearchController.shopSearchAll.data[index].bnDiscountAmount}'
                              : '৳${shopSearchController.shopSearchAll.data[index].discountAmount}'),
                      isDiscount: shopSearchController
                                  .shopSearchAll.data[index].discountType ==
                              null
                          ? false
                          : true,
                      discountTitle: shopSearchController
                                  .shopSearchAll.data[index].discountType ==
                              DiscountType.PERCENT
                          ? (lng == 'Bangla'
                              ? '-${shopSearchController.shopSearchAll.data[index].bnDiscount}%'
                              : '-${shopSearchController.shopSearchAll.data[index].discount}%')
                          : (lng == 'Bangla'
                              ? '-৳${shopSearchController.shopSearchAll.data[index].bnDiscount}'
                              : '-৳${shopSearchController.shopSearchAll.data[index].discount}'),
                      discountPrice: shopSearchController
                                  .shopSearchAll.data[index].discountType ==
                              DiscountType.PERCENT
                          ? (lng == 'Bangla'
                              ? '৳${shopSearchController.shopSearchAll.data[index].bnPrice}'
                              : '৳${shopSearchController.shopSearchAll.data[index].price}')
                          : (lng == 'Bangla'
                              ? '৳${shopSearchController.shopSearchAll.data[index].bnPrice}'
                              : '৳${shopSearchController.shopSearchAll.data[index].price}'),
                    );
                  },
                ),
              );
            }
          }
        },
      ),
    );
  }
}
