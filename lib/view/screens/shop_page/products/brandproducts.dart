import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/shop_controlers/filtercontroller.dart';
import 'package:familytrust/controllers/shop_controlers/shopbrandproducts_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopproductdetails_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/shop_page/products/shopproductdetails.dart';
import 'package:familytrust/view/screens/shop_page/products/widgets/product_widget.dart';
import 'package:familytrust/view/screens/shop_page/search_page/shop_search.dart';
import 'package:familytrust/view/screens/shop_page/sheared/filterwidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

class BrandProducts extends StatefulWidget {
  final String id;
  final String name;
  BrandProducts({this.id, this.name});
  static const routeName = 'brand_products';

  @override
  _BrandProductsState createState() => _BrandProductsState();
}

class _BrandProductsState extends State<BrandProducts> {
  final shopBrandProductsController = Get.put(ShopBrandProductsController());
  final shopProductDetailsController = Get.put(ShopProductDetailsController());
  final filterController = Get.put(FilterController());
  final lng = LocalizationService().getCurrentLang();
  String filterProduct = '';
  RangeValues priceRange;
  bool show = false;
  int page = 1;
  final _scrollController = ScrollController();

  @override
  void initState() {
    _scrollController.addListener(addProducts);
    super.initState();
  }

  void addProducts() async {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      print('work');
      page++;
      await shopBrandProductsController.fetchShopBrandProductsAdd(
        id: widget.id,
        page: page.toString(),
        filter: filterProduct,
        max: priceRange != null ? priceRange.end.round().toString() : '',
        min: priceRange != null ? priceRange.start.round().toString() : '',
      );
      if (shopBrandProductsController.noData == true) {
        Get.snackbar('No More Products', '', colorText: kWhiteColor);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        title: Text(widget.name),
        bottom: PreferredSize(
          preferredSize: show == false
              ? Size.fromHeight(getProportionateScreenWidth(50))
              : Size.fromHeight(getProportionateScreenWidth(135)),
          child: Container(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Column(
                children: [
                  Row(
                    children: [
                      FilterWidgets(
                        title: 'Price Range',
                        icon: Icons.attach_money,
                        onPress: () {
                          setState(() {
                            show = !show;
                          });
                        },
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      FilterWidgets(
                        title: 'Filter',
                        icon: Icons.filter_alt,
                        onPress: () {
                          if (show == true) {
                            setState(() {
                              show = false;
                            });
                          }
                          _filteredByLowHigh();
                        },
                      ),
                    ],
                  ),
                  show == false
                      ? SizedBox()
                      : Container(
                          height: getProportionateScreenWidth(85),
                          child: Obx(
                            () {
                              if (filterController.isLoading.isTrue) {
                                return Center(
                                  child: CustomLoader(),
                                );
                              } else {
                                return Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    RangeSlider(
                                      values: filterController.priceRange,
                                      min: 1,
                                      max: 100000,
                                      divisions: 100000,
                                      activeColor: kWhiteColor,
                                      inactiveColor: kSecondaryColor,
                                      labels: RangeLabels(
                                        '৳${filterController.priceRange.start.round().toString()}',
                                        '৳${filterController.priceRange.end.round().toString()}',
                                      ),
                                      onChanged: (RangeValues values) {
                                        filterController.changeValue(values);
                                      },
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        InkWell(
                                          onTap: () {
                                            setState(
                                              () {
                                                show = false;
                                              },
                                            );
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: kWhiteColor,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(10.0),
                                              child: Text(
                                                "Cancel",
                                                style: TextStyle(
                                                    color: kPrimaryColor),
                                              ),
                                            ),
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            setState(
                                              () {
                                                priceRange =
                                                    filterController.priceRange;
                                                print('price range' +
                                                    priceRange.toString());
                                                shopBrandProductsController
                                                    .fetchShopBrandProductsWithFilter(
                                                        id: widget.id,
                                                        filter: filterProduct !=
                                                                null
                                                            ? filterProduct
                                                            : '',
                                                        min: priceRange != null
                                                            ? priceRange.start
                                                                .round()
                                                                .toString()
                                                            : '',
                                                        max: priceRange != null
                                                            ? priceRange.end
                                                                .round()
                                                                .toString()
                                                            : '');
                                                show = false;
                                              },
                                            );
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: kSecondaryColor,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            child: Padding(
                                              padding: const EdgeInsets.all(10),
                                              child: Text(
                                                "Confirm",
                                                style: TextStyle(
                                                    color: kPrimaryColor),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                );
                              }
                            },
                          ),
                        )
                ],
              ),
            ),
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              Navigator.pushNamed(context, ShopSearchScreen.routeName);
            },
          ),
        ],
      ),
      body: Obx(
        () {
          if (shopBrandProductsController.isLoading.isTrue) {
            return Center(
              child: CustomLoader(),
            );
          } else {
            return Container(
              margin: EdgeInsets.only(top: 5, left: 5, right: 5),
              child: StaggeredGridView.countBuilder(
                controller: _scrollController,
                crossAxisCount: 2,
                shrinkWrap: true,
                itemCount:
                    shopBrandProductsController.brandProducts.data.length,
                physics: ScrollPhysics(),
                staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
                mainAxisSpacing: 4.0,
                crossAxisSpacing: 4.0,
                itemBuilder: (BuildContext context, int index) {
                  return ProductWidget(
                    onPress: () async {
                      await shopProductDetailsController
                          .fetchShopProductDetails(shopBrandProductsController
                              .brandProducts.data[index].id
                              .toString());
                      Navigator.pushNamed(
                        context,
                        ShopProductDetails.routeName,
                      );
                    },
                    id: shopBrandProductsController.brandProducts.data[index].id
                        .toString(),
                    picture: shopBrandProductsController
                        .brandProducts.data[index].thumbnailImg,
                    title: lng == 'Bangla'
                        ? shopBrandProductsController
                            .brandProducts.data[index].bnName
                        : shopBrandProductsController
                            .brandProducts.data[index].name,
                    stock: shopBrandProductsController
                        .brandProducts.data[index].currentStock
                        .toString(),
                    price: shopBrandProductsController
                                .brandProducts.data[index].discountType ==
                            null
                        ? (lng == 'Bangla'
                            ? '৳${shopBrandProductsController.brandProducts.data[index].bnPrice}'
                            : '৳${shopBrandProductsController.brandProducts.data[index].price}')
                        : (lng == 'Bangla'
                            ? '৳${shopBrandProductsController.brandProducts.data[index].bnDiscountAmount}'
                            : '৳${shopBrandProductsController.brandProducts.data[index].discountAmount}'),
                    isDiscount: shopBrandProductsController
                                .brandProducts.data[index].discountType ==
                            null
                        ? false
                        : true,
                    discountTitle: shopBrandProductsController
                                .brandProducts.data[index].discountType ==
                            'percent'
                        ? (lng == 'Bangla'
                            ? '-${shopBrandProductsController.brandProducts.data[index].bnDiscount}%'
                            : '-${shopBrandProductsController.brandProducts.data[index].discount}%')
                        : (lng == 'Bangla'
                            ? '-৳${shopBrandProductsController.brandProducts.data[index].bnDiscount}'
                            : '-৳${shopBrandProductsController.brandProducts.data[index].discount}'),
                    discountPrice: shopBrandProductsController
                                .brandProducts.data[index].discountType ==
                            'percent'
                        ? (lng == 'Bangla'
                            ? '৳${shopBrandProductsController.brandProducts.data[index].bnPrice}'
                            : '৳${shopBrandProductsController.brandProducts.data[index].price}')
                        : (lng == 'Bangla'
                            ? '৳${shopBrandProductsController.brandProducts.data[index].bnPrice}'
                            : '৳${shopBrandProductsController.brandProducts.data[index].price}'),
                  );
                },
              ),
            );
          }
        },
      ),
    );
  }

  _filteredByLowHigh() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        int temp;
        return AlertDialog(
          title: Text('Filter Products'.tr),
          content: StatefulBuilder(
            // You need this, notice the parameters below:
            builder: (BuildContext context, StateSetter setState) {
              return Column(mainAxisSize: MainAxisSize.min, children: [
                RadioListTile(
                  activeColor: kPrimaryColor,
                  title: Text('Latest'),
                  value: 1,
                  groupValue: temp,
                  onChanged: (value) {
                    setState(() {
                      temp = value;
                      filterProduct = 'latest=';
                    });
                  },
                ),
                RadioListTile(
                  activeColor: kPrimaryColor,
                  title: Text('High to low'),
                  value: 2,
                  groupValue: temp,
                  onChanged: (value) {
                    setState(() {
                      temp = value;
                      filterProduct = 'sortprice=high';
                    });
                  },
                ),
                RadioListTile(
                  activeColor: kPrimaryColor,
                  title: Text('Low to High'),
                  value: 3,
                  groupValue: temp,
                  onChanged: (value) {
                    setState(() {
                      temp = value;
                      filterProduct = 'sortprice=low';
                    });
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      color: kPrimaryColor,
                      child: TextButton(
                        onPressed: () {
                          setState(
                            () {
                              Navigator.pop(context);
                            },
                          );
                        },
                        child: Text(
                          "Cancel",
                          style: TextStyle(color: kWhiteColor),
                        ),
                      ),
                    ),
                    Container(
                      color: kPrimaryColor,
                      child: TextButton(
                        onPressed: () {
                          setState(
                            () {
                              shopBrandProductsController
                                  .fetchShopBrandProductsWithFilter(
                                      id: widget.id,
                                      filter: filterProduct,
                                      min: priceRange != null
                                          ? priceRange.start.round().toString()
                                          : '',
                                      max: priceRange != null
                                          ? priceRange.end.round().toString()
                                          : '');
                              Navigator.pop(context);
                            },
                          );
                        },
                        child: Text(
                          "Ok",
                          style: TextStyle(color: kWhiteColor),
                        ),
                      ),
                    ),
                  ],
                ),
              ]);
            },
          ),
        );
      },
    );
  }
}
