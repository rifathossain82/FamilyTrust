import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/shop_controlers/alldistrict_controller.dart';
import 'package:familytrust/controllers/shop_controlers/area_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopcart_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopfavorite_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopproductdetails_controller.dart';
import 'package:familytrust/helper/equation_helper.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/widgets/default_btn.dart';
import 'package:familytrust/view/widgets/input_form_widget.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/auth/login.dart';
import 'package:familytrust/view/screens/shop_page/checkout/shop_checkout.dart';
import 'package:familytrust/view/screens/shop_page/shop_cart/shop_cart_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ShopProductDetails extends StatefulWidget {
  static const routeName = 'shop_product_details';

  @override
  _ShopProductDetailsState createState() => _ShopProductDetailsState();
}

class _ShopProductDetailsState extends State<ShopProductDetails> {
  final shopProductDetailsController = Get.put(ShopProductDetailsController());
  final allDistrictController = Get.put(AllDistrictController());
  final areaController = Get.put(AreaController());
  final shopFavoriteController = Get.put(ShopFavoriteController());
  final shopCartController = Get.put(ShopCartController());
  final String lng = LocalizationService().getCurrentLang();
  List maxQty = [];
  List shippingCharge = [];
  Map attr = {};
  // List<String> newDistrict = [];
  int colorSelectedIndex;
  int cityId = 1;
  String city = 'Dhaka';
  String deliveryLocation = 'Inside Dhaka';
  String selectedColor;

  @override
  void initState() {
    maxQty = shopProductDetailsController
        .shopProductDetails.data.shipping.insideDhaka.maxQuantity;
    shippingCharge = shopProductDetailsController
        .shopProductDetails.data.shipping.insideDhaka.shippingCharge;
    allDistrictController.tempName = allDistrictController.districtName;
    areaController.fetchArea(cityId.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kWhiteColor,
        appBar: AppBar(
          title: Text(lng == 'Bangla'
              ? shopProductDetailsController.shopProductDetails.data.bnName
              : shopProductDetailsController.shopProductDetails.data.name),
          actions: [
            Stack(
              children: [
                IconButton(
                  icon: Icon(Icons.add_shopping_cart),
                  onPressed: () {
                    Navigator.pushNamed(context, ShopCartScreen.routeName);
                  },
                ),
                Positioned(
                  top: 0,
                  right: 8,
                  child: Container(
                    padding: EdgeInsets.all(2),
                    decoration: BoxDecoration(
                        color: kSecondaryColor,
                        borderRadius: BorderRadius.circular(10)),
                    child: Obx(
                      () {
                        if (shopCartController.isLoading.isTrue) {
                          return Center(
                            child: CustomLoader(),
                          );
                        } else {
                          return Text(
                            shopCartController.shopCart.length.toString(),
                            style: TextStyle(
                                color: kPrimaryColor,
                                fontWeight: FontWeight.w800),
                          );
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        body: Obx(
          () {
            if (shopProductDetailsController.isLoading.isTrue) {
              return Center(
                child: CustomLoader(),
              );
            } else {
              return Column(
                children: [
                  Expanded(
                    flex: 9,
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Stack(
                            children: [
                              Hero(
                                tag: shopProductDetailsController
                                    .shopProductDetails.data.id
                                    .toString(),
                                child: CachedNetworkImage(
                                  imageUrl: kImageUrl +
                                      shopProductDetailsController
                                          .shopProductDetails.data.thumbnailImg,
                                  fit: BoxFit.fitHeight,
                                  placeholder: (context, url) => CustomLoader(),
                                  errorWidget: (context, url, error) => Icon(
                                    Icons.error,
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                              shopProductDetailsController.shopProductDetails
                                          .data.discountType !=
                                      null
                                  ? (shopProductDetailsController
                                              .shopProductDetails
                                              .data
                                              .discountType ==
                                          'amount'
                                      ? (lng == 'Bangla'
                                          ? Positioned(
                                              top: 10,
                                              right: 10,
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: kSaleColor,
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                ),
                                                padding: EdgeInsets.all(10),
                                                child: Text(
                                                  '-৳${shopProductDetailsController.shopProductDetails.data.bnDiscount}',
                                                  style: TextStyle(
                                                      color: kWhiteColor,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                            )
                                          : Positioned(
                                              top: 10,
                                              right: 10,
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: kSaleColor,
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                ),
                                                padding: EdgeInsets.all(10),
                                                child: Text(
                                                  '-৳${shopProductDetailsController.shopProductDetails.data.discount}',
                                                  style: TextStyle(
                                                      color: kWhiteColor,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                            ))
                                      : (lng == 'Bangla'
                                          ? Positioned(
                                              top: 10,
                                              right: 10,
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: kSaleColor,
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                ),
                                                padding: EdgeInsets.all(10),
                                                child: Text(
                                                  '-${shopProductDetailsController.shopProductDetails.data.bnDiscount}%',
                                                  style: TextStyle(
                                                      color: kWhiteColor,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                            )
                                          : Positioned(
                                              top: 10,
                                              right: 10,
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: kSaleColor,
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                ),
                                                padding: EdgeInsets.all(10),
                                                child: Text(
                                                  '-${shopProductDetailsController.shopProductDetails.data.discount}%',
                                                  style: TextStyle(
                                                      color: kWhiteColor,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                            )))
                                  : SizedBox(),
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 20),
                            child: Text(
                              lng == 'Bangla'
                                  ? shopProductDetailsController
                                      .shopProductDetails.data.bnName
                                  : shopProductDetailsController
                                      .shopProductDetails.data.name,
                              style: kHeadLine3,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 20),
                            child: Row(
                              children: [
                                Text(
                                  shopProductDetailsController
                                              .shopProductDetails
                                              .data
                                              .discountType ==
                                          null
                                      ? (lng == 'Bangla'
                                          ? '৳${shopProductDetailsController.shopProductDetails.data.bnPrice}'
                                          : '৳${shopProductDetailsController.shopProductDetails.data.price}')
                                      : (lng == 'Bangla'
                                          ? '৳${shopProductDetailsController.shopProductDetails.data.bnDiscountAmount}'
                                          : '৳${shopProductDetailsController.shopProductDetails.data.discountAmount}'),
                                  style:
                                      kRegularText.copyWith(color: kSaleColor),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                shopProductDetailsController.shopProductDetails
                                            .data.discountType ==
                                        null
                                    ? SizedBox()
                                    : Text(
                                        shopProductDetailsController
                                                    .shopProductDetails
                                                    .data
                                                    .discountType ==
                                                'percent'
                                            ? (lng == 'Bangla'
                                                ? '৳${shopProductDetailsController.shopProductDetails.data.bnPrice}'
                                                : '৳${shopProductDetailsController.shopProductDetails.data.price}')
                                            : (lng == 'Bangla'
                                                ? '৳${shopProductDetailsController.shopProductDetails.data.bnPrice}'
                                                : '৳${shopProductDetailsController.shopProductDetails.data.price}'),
                                        style: kRegularText.copyWith(
                                            color: kSaleColor,
                                            decoration:
                                                TextDecoration.lineThrough),
                                      ),
                                Spacer(),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: kPrimaryColor,
                                  ),
                                  child: IconButton(
                                      icon: Icon(
                                        Icons.favorite,
                                        color: kErrorColor,
                                      ),
                                      onPressed: () {
                                        shopFavoriteController.addToFavorite(
                                            shopProductDetailsController
                                                .shopProductDetails.data);
                                      }),
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            thickness: 5,
                            color: kOrdinaryColor,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 25),
                            child: RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                  text: 'Stock Available : ',
                                  style: kRegularText.copyWith(
                                      color: kPrimaryColor),
                                ),
                                TextSpan(
                                  text: shopProductDetailsController
                                      .shopProductDetails.data.currentStock
                                      .toString(),
                                  style: kRegularText.copyWith(
                                      color: kPrimaryColor),
                                ),
                              ]),
                            ),
                          ),
                          Divider(
                            thickness: 5,
                            color: kOrdinaryColor,
                          ),
                          shopProductDetailsController
                                      .shopProductDetails.data.colors.length ==
                                  0
                              ? SizedBox()
                              : Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 10, bottom: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                          left: 15,
                                        ),
                                        child: Text(
                                          'Color',
                                          style: kRegularText,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(
                                          left: 15,
                                        ),
                                        height:
                                            getProportionateScreenHeight(40),
                                        child: ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            shrinkWrap: true,
                                            itemCount:
                                                shopProductDetailsController
                                                    .shopProductDetails
                                                    .data
                                                    .colors
                                                    .length,
                                            itemBuilder: (context, int index) {
                                              return InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    selectedColor =
                                                        shopProductDetailsController
                                                            .shopProductDetails
                                                            .data
                                                            .colors[index];
                                                    colorSelectedIndex = index;
                                                    print(selectedColor);
                                                  });
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Color(0xFFEEEEEE),
                                                    border: Border.all(
                                                      color:
                                                          colorSelectedIndex ==
                                                                  index
                                                              ? kPrimaryColor
                                                              : Color(
                                                                  0xFFEEEEEE),
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                  ),
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10),
                                                    child: Text(
                                                      shopProductDetailsController
                                                          .shopProductDetails
                                                          .data
                                                          .colors[index],
                                                    ),
                                                  ),
                                                ),
                                              );
                                            }),
                                      ),
                                    ],
                                  ),
                                ),
                          shopProductDetailsController
                                      .shopProductDetails.data.colors.length ==
                                  0
                              ? SizedBox()
                              : Divider(
                                  thickness: 5,
                                  color: kOrdinaryColor,
                                ),
                          shopProductDetailsController
                                      .shopProductDetails.data.attributes !=
                                  null
                              ? ListView.builder(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.vertical,
                                  physics: ScrollPhysics(),
                                  itemCount: shopProductDetailsController
                                      .shopProductDetails
                                      .data
                                      .attributes
                                      .length,
                                  itemBuilder: (context, int index) {
                                    // attribute.addAll(other);
                                    return Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 25),
                                          child: Text(
                                            shopProductDetailsController
                                                .shopProductDetails
                                                .data
                                                .attributes[index]
                                                .attributeName,
                                            style: kRegularText,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                          margin: const EdgeInsets.symmetric(
                                              vertical: 10, horizontal: 25),
                                          height:
                                              getProportionateScreenHeight(40),
                                          child: ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            shrinkWrap: true,
                                            itemCount:
                                                shopProductDetailsController
                                                    .shopProductDetails
                                                    .data
                                                    .attributes[index]
                                                    .values
                                                    .length,
                                            itemBuilder: (context, int index2) {
                                              return InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    attr[shopProductDetailsController
                                                            .shopProductDetails
                                                            .data
                                                            .attributes[index]
                                                            .attributeName] =
                                                        shopProductDetailsController
                                                            .shopProductDetails
                                                            .data
                                                            .attributes[index]
                                                            .values[index2];
                                                  });
                                                  print(jsonEncode(attr));
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Color(0xFFEEEEEE),
                                                    border: Border.all(
                                                      color: attr[shopProductDetailsController
                                                                  .shopProductDetails
                                                                  .data
                                                                  .attributes[
                                                                      index]
                                                                  .attributeName] ==
                                                              shopProductDetailsController
                                                                  .shopProductDetails
                                                                  .data
                                                                  .attributes[
                                                                      index]
                                                                  .values[index2]
                                                          ? kPrimaryColor
                                                          : Color(0xFFEEEEEE),
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                  ),
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10),
                                                    child: Text(
                                                      shopProductDetailsController
                                                          .shopProductDetails
                                                          .data
                                                          .attributes[index]
                                                          .values[index2],
                                                    ),
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                        Divider(
                                          thickness: 5,
                                          color: kOrdinaryColor,
                                        ),
                                      ],
                                    );
                                  })
                              : SizedBox(),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 25),
                                child: Text(
                                  'Description',
                                  style: kRegularText,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 25),
                                child: Text(
                                  EquationHelper().removeAllHtmlTags(
                                      shopProductDetailsController
                                          .shopProductDetails.data.description),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                          Divider(
                            thickness: 5,
                            color: kOrdinaryColor,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 25),
                                child: Text(
                                  'Select Your District',
                                  style: kRegularText,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              InkWell(
                                onTap: () {
                                  Get.bottomSheet(
                                    StatefulBuilder(builder:
                                        (BuildContext context2,
                                            StateSetter setState) {
                                      return Container(
                                        decoration: BoxDecoration(
                                            color: kWhiteColor,
                                            borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(30),
                                              topRight: Radius.circular(30),
                                            )),
                                        child: SingleChildScrollView(
                                          child: Column(
                                            children: [
                                              Container(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 10),
                                                child: Text(
                                                  'Choose your District',
                                                  style: kRegularText,
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 10),
                                                child: InputFormWidget(
                                                  labelText: 'Search',
                                                  hintText:
                                                      'Enter District Name',
                                                  keyType: TextInputType.text,
                                                  onSaved: (val) {
                                                    val = val.toLowerCase();
                                                    setState(() {
                                                      allDistrictController
                                                              .tempName =
                                                          allDistrictController
                                                              .districtName
                                                              .where((element) {
                                                        var temp = element
                                                            .toLowerCase();
                                                        return temp
                                                            .contains(val);
                                                      }).toList();
                                                    });
                                                  },
                                                ),
                                              ),
                                              ListView.builder(
                                                shrinkWrap: true,
                                                physics: ScrollPhysics(),
                                                itemCount: allDistrictController
                                                    .tempName.length,
                                                itemBuilder:
                                                    (context, int index) {
                                                  return InkWell(
                                                    onTap: () {
                                                      setState(() {
                                                        city =
                                                            allDistrictController
                                                                    .tempName[
                                                                index];
                                                        allDistrictController
                                                            .changeName(
                                                                allDistrictController
                                                                        .tempName[
                                                                    index]);
                                                      });
                                                      print(city);
                                                      Navigator.pop(context2);
                                                      for (int i = 0;
                                                          i <
                                                              allDistrictController
                                                                  .allDistrict
                                                                  .data
                                                                  .length;
                                                          i++) {
                                                        if (city ==
                                                            allDistrictController
                                                                .allDistrict
                                                                .data[i]
                                                                .name) {
                                                          setState(() {
                                                            deliveryLocation =
                                                                allDistrictController
                                                                    .allDistrict
                                                                    .data[i]
                                                                    .shippingType;
                                                            cityId =
                                                                allDistrictController
                                                                    .allDistrict
                                                                    .data[i]
                                                                    .id;

                                                            if (deliveryLocation ==
                                                                'Inside Dhaka') {
                                                              maxQty = shopProductDetailsController
                                                                  .shopProductDetails
                                                                  .data
                                                                  .shipping
                                                                  .insideDhaka
                                                                  .maxQuantity;
                                                              shippingCharge =
                                                                  shopProductDetailsController
                                                                      .shopProductDetails
                                                                      .data
                                                                      .shipping
                                                                      .insideDhaka
                                                                      .shippingCharge;
                                                            } else {
                                                              if (deliveryLocation ==
                                                                  'Around Dhaka') {
                                                                maxQty = shopProductDetailsController
                                                                    .shopProductDetails
                                                                    .data
                                                                    .shipping
                                                                    .aroundDhaka
                                                                    .maxQuantity;
                                                                shippingCharge = shopProductDetailsController
                                                                    .shopProductDetails
                                                                    .data
                                                                    .shipping
                                                                    .aroundDhaka
                                                                    .shippingCharge;
                                                              } else {
                                                                maxQty = shopProductDetailsController
                                                                    .shopProductDetails
                                                                    .data
                                                                    .shipping
                                                                    .outsideDhaka
                                                                    .maxQuantity;
                                                                shippingCharge = shopProductDetailsController
                                                                    .shopProductDetails
                                                                    .data
                                                                    .shipping
                                                                    .outsideDhaka
                                                                    .shippingCharge;
                                                              }
                                                            }
                                                          });
                                                        }
                                                      }
                                                      print(maxQty);
                                                      print(deliveryLocation);
                                                      areaController.fetchArea(
                                                          cityId.toString());
                                                      setState(() {
                                                        allDistrictController
                                                                .tempName =
                                                            allDistrictController
                                                                .districtName;
                                                      });
                                                    },
                                                    child: ListTile(
                                                      title: Text(
                                                        allDistrictController
                                                            .tempName[index],
                                                      ),
                                                    ),
                                                  );
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    }),
                                  );
                                },
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 25),
                                  child: Container(
                                    height: getProportionateScreenWidth(50),
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      border: Border.all(color: kPrimaryColor),
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10),
                                          child: Obx(
                                            () {
                                              if (allDistrictController
                                                  .isLoading.isTrue) {
                                                return Center(
                                                  child: CustomLoader(),
                                                );
                                              } else {
                                                return Text(
                                                  allDistrictController
                                                      .cityName,
                                                  style: kRegularText,
                                                  textAlign: TextAlign.center,
                                                );
                                              }
                                            },
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10),
                                          child: Icon(Icons.arrow_drop_down),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          width: SizeConfig.screenWidth / 2,
                          child: DefaultBtn(
                            isChange: true,
                            radius: 10,
                            color: kPrimaryColor,
                            title: 'Add to Cart'.tr,
                            onPress: () {
                              if (shopProductDetailsController
                                      .shopProductDetails.data.currentStock >
                                  0) {
                                if (shopProductDetailsController
                                            .shopProductDetails.data.colors !=
                                        null &&
                                    selectedColor != null &&
                                    shopProductDetailsController
                                            .shopProductDetails
                                            .data
                                            .attributes !=
                                        null &&
                                    attr.isNotEmpty) {
                                  shopCartController.addToCart(
                                    id: shopProductDetailsController
                                        .shopProductDetails.data.id,
                                    productName: shopProductDetailsController
                                        .shopProductDetails.data.name,
                                    bnProductName: shopProductDetailsController
                                        .shopProductDetails.data.bnName,
                                    price: shopProductDetailsController
                                        .shopProductDetails.data.price,
                                    bnPrice: shopProductDetailsController
                                        .shopProductDetails.data.bnPrice,
                                    discountType: shopProductDetailsController
                                        .shopProductDetails.data.discountType,
                                    discountAmount: shopProductDetailsController
                                        .shopProductDetails.data.discountAmount,
                                    bnDiscountAmount:
                                        shopProductDetailsController
                                            .shopProductDetails
                                            .data
                                            .bnDiscountAmount,
                                    thumbnailImg: shopProductDetailsController
                                        .shopProductDetails.data.thumbnailImg,
                                    qty: shopProductDetailsController
                                        .shopProductDetails.data.qty,
                                    color: selectedColor,
                                    attributes: jsonEncode(attr),
                                    area: deliveryLocation,
                                    cityId: cityId,
                                    deliveryCharge:
                                        int.parse(shippingCharge[0]),
                                    maxQty: maxQty.toString(),
                                    shippingCharge: shippingCharge.toString(),
                                  );
                                } else if (shopProductDetailsController
                                        .shopProductDetails
                                        .data
                                        .colors
                                        .isNotEmpty &&
                                    selectedColor == null) {
                                  Get.snackbar('Choose color first', '',
                                      colorText: kWhiteColor);
                                } else if (shopProductDetailsController
                                            .shopProductDetails
                                            .data
                                            .attributes !=
                                        null &&
                                    attr.isEmpty) {
                                  Get.snackbar('Choose variant first', '',
                                      colorText: kWhiteColor);
                                } else if (shopProductDetailsController
                                        .shopProductDetails
                                        .data
                                        .colors
                                        .isEmpty &&
                                    selectedColor == null &&
                                    attr != null) {
                                  shopCartController.addToCart(
                                    id: shopProductDetailsController
                                        .shopProductDetails.data.id,
                                    productName: shopProductDetailsController
                                        .shopProductDetails.data.name,
                                    bnProductName: shopProductDetailsController
                                        .shopProductDetails.data.bnName,
                                    price: shopProductDetailsController
                                        .shopProductDetails.data.price,
                                    bnPrice: shopProductDetailsController
                                        .shopProductDetails.data.bnPrice,
                                    discountType: shopProductDetailsController
                                        .shopProductDetails.data.discountType,
                                    discountAmount: shopProductDetailsController
                                        .shopProductDetails.data.discountAmount,
                                    bnDiscountAmount:
                                        shopProductDetailsController
                                            .shopProductDetails
                                            .data
                                            .bnDiscountAmount,
                                    thumbnailImg: shopProductDetailsController
                                        .shopProductDetails.data.thumbnailImg,
                                    qty: shopProductDetailsController
                                        .shopProductDetails.data.qty,
                                    color: selectedColor,
                                    attributes: jsonEncode(attr),
                                    area: deliveryLocation,
                                    cityId: cityId,
                                    deliveryCharge:
                                        int.parse(shippingCharge[0]),
                                    maxQty: maxQty.toString(),
                                    shippingCharge: shippingCharge.toString(),
                                  );
                                } else if (shopProductDetailsController
                                            .shopProductDetails
                                            .data
                                            .attributes ==
                                        null &&
                                    attr.isEmpty &&
                                    selectedColor != null) {
                                  shopCartController.addToCart(
                                    id: shopProductDetailsController
                                        .shopProductDetails.data.id,
                                    productName: shopProductDetailsController
                                        .shopProductDetails.data.name,
                                    bnProductName: shopProductDetailsController
                                        .shopProductDetails.data.bnName,
                                    price: shopProductDetailsController
                                        .shopProductDetails.data.price,
                                    bnPrice: shopProductDetailsController
                                        .shopProductDetails.data.bnPrice,
                                    discountType: shopProductDetailsController
                                        .shopProductDetails.data.discountType,
                                    discountAmount: shopProductDetailsController
                                        .shopProductDetails.data.discountAmount,
                                    bnDiscountAmount:
                                        shopProductDetailsController
                                            .shopProductDetails
                                            .data
                                            .bnDiscountAmount,
                                    thumbnailImg: shopProductDetailsController
                                        .shopProductDetails.data.thumbnailImg,
                                    qty: shopProductDetailsController
                                        .shopProductDetails.data.qty,
                                    color: selectedColor,
                                    attributes: jsonEncode(attr),
                                    area: deliveryLocation,
                                    cityId: cityId,
                                    deliveryCharge:
                                        int.parse(shippingCharge[0]),
                                    maxQty: maxQty.toString(),
                                    shippingCharge: shippingCharge.toString(),
                                  );
                                }
                              } else {
                                Get.snackbar('Stock Out', '',
                                    colorText: kWhiteColor);
                              }
                            },
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          width: SizeConfig.screenWidth / 2,
                          child: DefaultBtn(
                            isChange: true,
                            radius: 10,
                            color: kSaleColor,
                            title: 'Order Now'.tr,
                            onPress: () {
                              if (prefs.containsKey('uid')) {
                                if (shopProductDetailsController
                                        .shopProductDetails.data.currentStock >
                                    0) {
                                  if (shopProductDetailsController.shopProductDetails
                                              .data.colors !=
                                          null &&
                                      selectedColor != null &&
                                      shopProductDetailsController
                                              .shopProductDetails
                                              .data
                                              .attributes !=
                                          null &&
                                      attr.isNotEmpty) {
                                    shopCartController.addToTempCart(
                                      id: shopProductDetailsController
                                          .shopProductDetails.data.id,
                                      productName: shopProductDetailsController
                                          .shopProductDetails.data.name,
                                      bnProductName:
                                          shopProductDetailsController
                                              .shopProductDetails.data.bnName,
                                      price: shopProductDetailsController
                                          .shopProductDetails.data.price,
                                      bnPrice: shopProductDetailsController
                                          .shopProductDetails.data.bnPrice,
                                      discountType: shopProductDetailsController
                                          .shopProductDetails.data.discountType,
                                      discountAmount:
                                          shopProductDetailsController
                                              .shopProductDetails
                                              .data
                                              .discountAmount,
                                      bnDiscountAmount:
                                          shopProductDetailsController
                                              .shopProductDetails
                                              .data
                                              .bnDiscountAmount,
                                      thumbnailImg: shopProductDetailsController
                                          .shopProductDetails.data.thumbnailImg,
                                      qty: shopProductDetailsController
                                          .shopProductDetails.data.qty,
                                      color: selectedColor,
                                      attributes: jsonEncode(attr),
                                      cityId: cityId,
                                      deliveryCharge: deliveryLocation ==
                                              'Inside Dhaka'
                                          ? int.parse(
                                              shopProductDetailsController
                                                  .shopProductDetails
                                                  .data
                                                  .shipping
                                                  .insideDhaka
                                                  .shippingCharge[0])
                                          : (deliveryLocation == 'Outside Dhaka'
                                              ? int.parse(
                                                  shopProductDetailsController
                                                      .shopProductDetails
                                                      .data
                                                      .shipping
                                                      .outsideDhaka
                                                      .shippingCharge[0])
                                              : int.parse(
                                                  shopProductDetailsController
                                                      .shopProductDetails
                                                      .data
                                                      .shipping
                                                      .aroundDhaka
                                                      .shippingCharge[0])),
                                    );
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => ShopCheckOut(
                                          buyNow: true,
                                        ),
                                      ),
                                    );
                                  } else if (shopProductDetailsController
                                          .shopProductDetails
                                          .data
                                          .colors
                                          .isNotEmpty &&
                                      selectedColor == null) {
                                    Get.snackbar('Choose color first', '',
                                        colorText: kWhiteColor);
                                  } else if (shopProductDetailsController
                                              .shopProductDetails
                                              .data
                                              .attributes !=
                                          null &&
                                      attr.isEmpty) {
                                    Get.snackbar('Choose variant first', '',
                                        colorText: kWhiteColor);
                                  } else if (shopProductDetailsController
                                          .shopProductDetails
                                          .data
                                          .colors
                                          .isEmpty &&
                                      selectedColor == null &&
                                      attr != null) {
                                    shopCartController.addToTempCart(
                                      id: shopProductDetailsController
                                          .shopProductDetails.data.id,
                                      productName: shopProductDetailsController
                                          .shopProductDetails.data.name,
                                      bnProductName:
                                          shopProductDetailsController
                                              .shopProductDetails.data.bnName,
                                      price: shopProductDetailsController
                                          .shopProductDetails.data.price,
                                      bnPrice: shopProductDetailsController
                                          .shopProductDetails.data.bnPrice,
                                      discountType: shopProductDetailsController
                                          .shopProductDetails.data.discountType,
                                      discountAmount:
                                          shopProductDetailsController
                                              .shopProductDetails
                                              .data
                                              .discountAmount,
                                      bnDiscountAmount:
                                          shopProductDetailsController
                                              .shopProductDetails
                                              .data
                                              .bnDiscountAmount,
                                      thumbnailImg: shopProductDetailsController
                                          .shopProductDetails.data.thumbnailImg,
                                      qty: shopProductDetailsController
                                          .shopProductDetails.data.qty,
                                      color: selectedColor,
                                      attributes: jsonEncode(attr),
                                      cityId: cityId,
                                      deliveryCharge: deliveryLocation ==
                                              'Inside Dhaka'
                                          ? int.parse(
                                              shopProductDetailsController
                                                  .shopProductDetails
                                                  .data
                                                  .shipping
                                                  .insideDhaka
                                                  .shippingCharge[0])
                                          : (deliveryLocation == 'Outside Dhaka'
                                              ? int.parse(
                                                  shopProductDetailsController
                                                      .shopProductDetails
                                                      .data
                                                      .shipping
                                                      .outsideDhaka
                                                      .shippingCharge[0])
                                              : int.parse(
                                                  shopProductDetailsController
                                                      .shopProductDetails
                                                      .data
                                                      .shipping
                                                      .aroundDhaka
                                                      .shippingCharge[0])),
                                    );
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => ShopCheckOut(
                                          buyNow: true,
                                        ),
                                      ),
                                    );
                                  } else if (shopProductDetailsController
                                              .shopProductDetails
                                              .data
                                              .attributes ==
                                          null &&
                                      attr.isEmpty &&
                                      selectedColor != null) {
                                    shopCartController.addToTempCart(
                                      id: shopProductDetailsController
                                          .shopProductDetails.data.id,
                                      productName: shopProductDetailsController
                                          .shopProductDetails.data.name,
                                      bnProductName:
                                          shopProductDetailsController
                                              .shopProductDetails.data.bnName,
                                      price: shopProductDetailsController
                                          .shopProductDetails.data.price,
                                      bnPrice: shopProductDetailsController
                                          .shopProductDetails.data.bnPrice,
                                      discountType: shopProductDetailsController
                                          .shopProductDetails.data.discountType,
                                      discountAmount:
                                          shopProductDetailsController
                                              .shopProductDetails
                                              .data
                                              .discountAmount,
                                      bnDiscountAmount:
                                          shopProductDetailsController
                                              .shopProductDetails
                                              .data
                                              .bnDiscountAmount,
                                      thumbnailImg: shopProductDetailsController
                                          .shopProductDetails.data.thumbnailImg,
                                      qty: shopProductDetailsController
                                          .shopProductDetails.data.qty,
                                      color: selectedColor,
                                      attributes: jsonEncode(attr),
                                      cityId: cityId,
                                      deliveryCharge: deliveryLocation ==
                                              'Inside Dhaka'
                                          ? int.parse(
                                              shopProductDetailsController
                                                  .shopProductDetails
                                                  .data
                                                  .shipping
                                                  .insideDhaka
                                                  .shippingCharge[0])
                                          : (deliveryLocation == 'Outside Dhaka'
                                              ? int.parse(
                                                  shopProductDetailsController
                                                      .shopProductDetails
                                                      .data
                                                      .shipping
                                                      .outsideDhaka
                                                      .shippingCharge[0])
                                              : int.parse(
                                                  shopProductDetailsController
                                                      .shopProductDetails
                                                      .data
                                                      .shipping
                                                      .aroundDhaka
                                                      .shippingCharge[0])),
                                    );
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => ShopCheckOut(
                                          buyNow: true,
                                        ),
                                      ),
                                    );
                                  }
                                } else {
                                  Get.snackbar('Stock Out', '',
                                      colorText: kWhiteColor);
                                }
                              } else {
                                Get.to(
                                  () => LogIn(
                                    isOff: true,
                                    address: prefs.getString('address'),
                                  ),
                                );
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            }
          },
        ));
  }
}
