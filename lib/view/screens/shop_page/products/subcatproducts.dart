import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/shop_controlers/filtercontroller.dart';
import 'package:familytrust/controllers/shop_controlers/shopcatbrandlist_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopproductdetails_controller.dart';
import 'package:familytrust/controllers/shop_controlers/shopsubcatproducts_controller.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/shop_page/products/shopproductdetails.dart';
import 'package:familytrust/view/screens/shop_page/products/widgets/product_widget.dart';
import 'package:familytrust/view/screens/shop_page/search_page/shop_search.dart';
import 'package:familytrust/view/screens/shop_page/sheared/filterwidget.dart';
import 'package:familytrust/view/screens/shop_page/sheared/multiple_select_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

class SubCatProductsScreen extends StatefulWidget {
  final String id;
  final String bnTitle;
  final String title;
  SubCatProductsScreen({this.id, this.title, this.bnTitle});
  static const routeName = 'sub_cat_products_screen';

  @override
  _SubCatProductsScreenState createState() => _SubCatProductsScreenState();
}

class _SubCatProductsScreenState extends State<SubCatProductsScreen> {
  final _scrollController = ScrollController();
  final shopSubCatProductsController = Get.put(ShopSubCatProductsController());
  final shopProductDetailsController = Get.put(ShopProductDetailsController());
  final shopCatBrandListController = Get.put(ShopCatBrandListController());
  final filterController = Get.put(FilterController());
  final lng = LocalizationService().getCurrentLang();
  RangeValues priceRange;
  String filterProduct = '';
  bool show = false;
  int page = 1;
  List<String> selectedBrand = [];

  @override
  void initState() {
    shopSubCatProductsController.fetchShopSubCatProducts(
        widget.id, page.toString());
    _scrollController.addListener(addProducts);
    super.initState();
  }

  void addProducts() async {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      print('work');
      page++;
      await shopSubCatProductsController.fetchShopSubCatProductsAdd(
          id: widget.id,
          page: page.toString(),
          filter: filterProduct,
          max: priceRange != null ? priceRange.end.round().toString() : '',
          min: priceRange != null ? priceRange.start.round().toString() : '',
          brand: selectedBrand != []
              ? selectedBrand.toString().replaceAll('[', '').replaceAll(']', '')
              : '');
      if (shopSubCatProductsController.noData == true) {
        Get.snackbar('No More Products', '', colorText: kWhiteColor);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    print(widget.id);
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        title: Text(lng == 'Bangla' ? widget.bnTitle : widget.title),
        bottom: PreferredSize(
          preferredSize: show == false
              ? Size.fromHeight(getProportionateScreenWidth(50))
              : Size.fromHeight(getProportionateScreenWidth(135)),
          child: Container(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      FilterWidgets(
                        title: 'Sorted By',
                        icon: Icons.arrow_drop_down,
                        onPress: () {
                          if (show == true) {
                            setState(() {
                              show = false;
                            });
                          }
                          selectedBrand = [];
                          _filteredByBrand();
                        },
                      ),
                      FilterWidgets(
                        title: 'Price Range',
                        icon: Icons.attach_money,
                        onPress: () {
                          setState(() {
                            show = !show;
                          });
                        },
                      ),
                      FilterWidgets(
                        title: 'Filter',
                        icon: Icons.filter_alt,
                        onPress: () {
                          if (show == true) {
                            setState(() {
                              show = false;
                            });
                          }
                          _filteredByLowHigh();
                        },
                      ),
                    ],
                  ),
                  show == false
                      ? SizedBox()
                      : Container(
                          height: getProportionateScreenWidth(85),
                          child: Obx(
                            () {
                              if (filterController.isLoading.isTrue) {
                                return Center(
                                  child: CustomLoader(),
                                );
                              } else {
                                return Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    RangeSlider(
                                      values: filterController.priceRange,
                                      min: 1,
                                      max: 100000,
                                      divisions: 100000,
                                      activeColor: kWhiteColor,
                                      inactiveColor: kSecondaryColor,
                                      labels: RangeLabels(
                                        '৳${filterController.priceRange.start.round().toString()}',
                                        '৳${filterController.priceRange.end.round().toString()}',
                                      ),
                                      onChanged: (RangeValues values) {
                                        filterController.changeValue(values);
                                      },
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        InkWell(
                                          onTap: () {
                                            setState(
                                              () {
                                                show = false;
                                              },
                                            );
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: kWhiteColor,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(10.0),
                                              child: Text(
                                                "Cancel",
                                                style: TextStyle(
                                                    color: kPrimaryColor),
                                              ),
                                            ),
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            setState(
                                              () {
                                                priceRange =
                                                    filterController.priceRange;
                                                print('price range' +
                                                    priceRange.toString());
                                                shopSubCatProductsController
                                                    .fetchShopSubCatProductsWithFilter(
                                                        id: widget.id,
                                                        filter: filterProduct !=
                                                                null
                                                            ? filterProduct
                                                            : '',
                                                        min: priceRange != null
                                                            ? priceRange.start
                                                                .round()
                                                                .toString()
                                                            : '',
                                                        max: priceRange != null
                                                            ? priceRange.end
                                                                .round()
                                                                .toString()
                                                            : '',
                                                        brand:
                                                            selectedBrand != []
                                                                ? selectedBrand
                                                                    .toString()
                                                                    .replaceAll(
                                                                        '[', '')
                                                                    .replaceAll(
                                                                        ']', '')
                                                                : '');
                                                show = false;
                                              },
                                            );
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: kSecondaryColor,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            child: Padding(
                                              padding: const EdgeInsets.all(10),
                                              child: Text(
                                                "Confirm",
                                                style: TextStyle(
                                                    color: kPrimaryColor),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                );
                              }
                            },
                          ),
                        )
                ],
              ),
            ),
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              Navigator.pushNamed(context, ShopSearchScreen.routeName);
            },
          ),
        ],
      ),
      body: Obx(
        () {
          if (shopSubCatProductsController.isLoading.isTrue) {
            return Center(
              child: CustomLoader(),
            );
          } else {
            if (shopSubCatProductsController.subCatProducts.data.isEmpty) {
              return Center(
                child: Text(
                  'No Products Available',
                  style: kRegularText,
                ),
              );
            } else {
              return Container(
                margin: EdgeInsets.only(top: 5, left: 5, right: 5),
                child: StaggeredGridView.countBuilder(
                  controller: _scrollController,
                  crossAxisCount: 2,
                  shrinkWrap: true,
                  itemCount:
                      shopSubCatProductsController.subCatProducts.data.length,
                  physics: ScrollPhysics(),
                  staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
                  mainAxisSpacing: 4.0,
                  crossAxisSpacing: 4.0,
                  itemBuilder: (BuildContext context, int index) {
                    return ProductWidget(
                      onPress: () async {
                        await shopProductDetailsController
                            .fetchShopProductDetails(
                                shopSubCatProductsController
                                    .subCatProducts.data[index].id
                                    .toString());
                        Navigator.pushNamed(
                          context,
                          ShopProductDetails.routeName,
                        );
                      },
                      id: shopSubCatProductsController
                          .subCatProducts.data[index].id
                          .toString(),
                      picture: shopSubCatProductsController
                          .subCatProducts.data[index].thumbnailImg,
                      title: lng == 'Bangla'
                          ? shopSubCatProductsController
                              .subCatProducts.data[index].bnName
                          : shopSubCatProductsController
                              .subCatProducts.data[index].name,
                      stock: shopSubCatProductsController
                          .subCatProducts.data[index].currentStock
                          .toString(),
                      price: shopSubCatProductsController
                                  .subCatProducts.data[index].discountType ==
                              null
                          ? (lng == 'Bangla'
                              ? '৳${shopSubCatProductsController.subCatProducts.data[index].bnPrice}'
                              : '৳${shopSubCatProductsController.subCatProducts.data[index].price}')
                          : (lng == 'Bangla'
                              ? '৳${shopSubCatProductsController.subCatProducts.data[index].bnDiscountAmount}'
                              : '৳${shopSubCatProductsController.subCatProducts.data[index].discountAmount}'),
                      isDiscount: shopSubCatProductsController
                                  .subCatProducts.data[index].discountType ==
                              null
                          ? false
                          : true,
                      discountTitle: shopSubCatProductsController
                                  .subCatProducts.data[index].discountType ==
                              'percent'
                          ? (lng == 'Bangla'
                              ? '-${shopSubCatProductsController.subCatProducts.data[index].bnDiscount}%'
                              : '-${shopSubCatProductsController.subCatProducts.data[index].discount}%')
                          : (lng == 'Bangla'
                              ? '-৳${shopSubCatProductsController.subCatProducts.data[index].bnDiscount}'
                              : '-৳${shopSubCatProductsController.subCatProducts.data[index].discount}'),
                      discountPrice: shopSubCatProductsController
                                  .subCatProducts.data[index].discountType ==
                              'percent'
                          ? (lng == 'Bangla'
                              ? '৳${shopSubCatProductsController.subCatProducts.data[index].bnPrice}'
                              : '৳${shopSubCatProductsController.subCatProducts.data[index].price}')
                          : (lng == 'Bangla'
                              ? '৳${shopSubCatProductsController.subCatProducts.data[index].bnPrice}'
                              : '৳${shopSubCatProductsController.subCatProducts.data[index].price}'),
                    );
                  },
                ),
              );
            }
          }
        },
      ),
    );
  }

  _filteredByBrand() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          //Here we will build the content of the dialog
          return AlertDialog(
            title: Text("Select Brands"),
            content: MultiSelectChip(
              shopCatBrandListController.catBrandList.data,
              onSelectionChanged: (selectedList) {
                filterController.changeBrands(selectedList);
              },
            ),
            actions: <Widget>[
              TextButton(
                  child: Text("Cancel"),
                  style: TextButton.styleFrom(
                    primary: kWhiteColor,
                    backgroundColor: kPrimaryColor,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              TextButton(
                  child: Text("Submit"),
                  style: TextButton.styleFrom(
                    primary: kWhiteColor,
                    backgroundColor: kPrimaryColor,
                  ),
                  onPressed: () async {
                    for (int i = 0;
                        i < filterController.filterBrands.length;
                        i++) {
                      print(filterController.filterBrands[i].brandName);
                      setState(() {
                        selectedBrand.add(
                            filterController.filterBrands[i].id.toString());
                      });
                    }
                    shopSubCatProductsController
                        .fetchShopSubCatProductsWithFilter(
                            id: widget.id,
                            filter: filterProduct,
                            min: priceRange != null
                                ? priceRange.start.round().toString()
                                : '',
                            max: priceRange != null
                                ? priceRange.end.round().toString()
                                : '',
                            brand: selectedBrand != []
                                ? selectedBrand
                                    .toString()
                                    .replaceAll('[', '')
                                    .replaceAll(']', '')
                                : '');

                    Navigator.pop(context);
                  })
            ],
          );
        });
  }

  _filteredByLowHigh() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        int temp;
        return AlertDialog(
          title: Text('Filter Products'.tr),
          content: StatefulBuilder(
            // You need this, notice the parameters below:
            builder: (BuildContext context, StateSetter setState) {
              return Column(mainAxisSize: MainAxisSize.min, children: [
                RadioListTile(
                  activeColor: kPrimaryColor,
                  title: Text('Latest'),
                  value: 1,
                  groupValue: temp,
                  onChanged: (value) {
                    setState(() {
                      temp = value;
                      filterProduct = 'latest=';
                    });
                  },
                ),
                RadioListTile(
                  activeColor: kPrimaryColor,
                  title: Text('High to low'),
                  value: 2,
                  groupValue: temp,
                  onChanged: (value) {
                    setState(() {
                      temp = value;
                      filterProduct = 'sortprice=high';
                    });
                  },
                ),
                RadioListTile(
                  activeColor: kPrimaryColor,
                  title: Text('Low to High'),
                  value: 3,
                  groupValue: temp,
                  onChanged: (value) {
                    setState(() {
                      temp = value;
                      filterProduct = 'sortprice=low';
                    });
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      color: kPrimaryColor,
                      child: TextButton(
                        onPressed: () {
                          setState(
                            () {
                              Navigator.pop(context);
                            },
                          );
                        },
                        child: Text(
                          "Cancel",
                          style: TextStyle(color: kWhiteColor),
                        ),
                      ),
                    ),
                    Container(
                      color: kPrimaryColor,
                      child: TextButton(
                        onPressed: () {
                          setState(
                            () {
                              shopSubCatProductsController
                                  .fetchShopSubCatProductsWithFilter(
                                      id: widget.id,
                                      filter: filterProduct,
                                      min: priceRange != null
                                          ? priceRange.start.round().toString()
                                          : '',
                                      max: priceRange != null
                                          ? priceRange.end.round().toString()
                                          : '',
                                      brand: selectedBrand != []
                                          ? selectedBrand
                                              .toString()
                                              .replaceAll('[', '')
                                              .replaceAll(']', '')
                                          : '');
                              Navigator.pop(context);
                            },
                          );
                        },
                        child: Text(
                          "Ok",
                          style: TextStyle(color: kWhiteColor),
                        ),
                      ),
                    ),
                  ],
                ),
              ]);
            },
          ),
        );
      },
    );
  }
}
