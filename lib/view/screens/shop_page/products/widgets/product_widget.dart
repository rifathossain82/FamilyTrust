import 'package:cached_network_image/cached_network_image.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:flutter/material.dart';

class ProductWidget extends StatelessWidget {
  final String id;
  final Function onPress;
  final String title;
  final String price;
  final String picture;
  final String stock;
  final String discountPrice;
  final String discountTitle;
  final bool isDiscount;
  ProductWidget({
    this.id,
    this.onPress,
    this.title,
    this.stock,
    this.picture,
    this.price,
    this.discountPrice,
    this.discountTitle,
    this.isDiscount = false,
  });
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Card(
        elevation: 2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Container(
                    height: getProportionateScreenHeight(200),
                    child: Hero(
                      tag: id,
                      child: CachedNetworkImage(
                        imageUrl: kImageUrl + picture,
                        fit: BoxFit.fitWidth,
                        placeholder: (context, url) => CustomLoader(),
                        errorWidget: (context, url, error) => Icon(
                          Icons.error,
                          color: Colors.red,
                        ),
                      ),
                    ),
                  ),
                ),
                isDiscount == false
                    ? SizedBox()
                    : Positioned(
                        top: 10,
                        right: 10,
                        child: Container(
                          decoration: BoxDecoration(
                            color: kSaleColor,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          padding: EdgeInsets.all(10),
                          child: Text(
                            discountTitle,
                            style: TextStyle(
                                color: kWhiteColor,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Text(
                title,
                maxLines: 2,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text('Stock : ' + stock),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Row(
                children: [
                  Text(
                    price,
                    style: TextStyle(color: kErrorColor),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  isDiscount == false
                      ? SizedBox()
                      : Text(
                          discountPrice,
                          style: TextStyle(
                            color: kErrorColor,
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
