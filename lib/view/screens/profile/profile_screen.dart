import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/controllers/common/wallet_controller.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:familytrust/view/screens/dashboard/dashboard_screen.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/grocery_home.dart';
import 'package:familytrust/view/widgets/custom_alert_dialog.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:familytrust/view/widgets/default_btn.dart';
import 'package:familytrust/view/widgets/input_form_widget.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/auth/login.dart';
import 'package:familytrust/view/screens/homepage/homepage.dart';
import 'package:familytrust/view/screens/order_page/order_screens.dart';
import 'package:familytrust/view/screens/profile/widgets/list_widget.dart';
import 'package:familytrust/view/screens/profile/widgets/row_widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'widgets/wallet_status_widget.dart';

class ProfileScreen extends StatefulWidget {
  static const routeName = 'profile_screen';
  final bool backActive;
  final bool centerTitle;
  ProfileScreen({
    this.backActive = true,
    this.centerTitle = false,
  });
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final _passFormKey = GlobalKey<FormState>();
  final _profileFormKey = GlobalKey<FormState>();
  final TextEditingController oldPasswordController = TextEditingController();
  final TextEditingController newPasswordController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final WalletDataController walletDataController =
      Get.put(WalletDataController());

  @override
  void initState() {
    if (prefs.containsKey('uid')) {
      getWalletData();
    }
    super.initState();
  }

  void getWalletData() async {
    await walletDataController.fetchWalletData();
  }

  @override
  void dispose() {
    oldPasswordController.dispose();
    newPasswordController.dispose();
    nameController.dispose();
    emailController.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: widget.backActive,
        centerTitle: widget.centerTitle,
        title: Text('Profile'.tr),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: getProportionateScreenHeight(20),
            ),
            Container(
              alignment: Alignment.center,
              height: getProportionateScreenHeight(140),
              width: getProportionateScreenWidth(140),
              decoration: BoxDecoration(
                border: Border.all(
                  color: kPrimaryColor,
                  width: 2,
                ),
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage(
                    kImageDir + 'people.png',
                  ),
                ),
                shape: BoxShape.circle,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            prefs.getString('name') != null
                ? Container(
                    alignment: Alignment.center,
                    child: Text(
                      prefs.getString('name'),
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w600),
                    ),
                  )
                : SizedBox(),
            SizedBox(
              height: 10,
            ),
            Container(
              alignment: Alignment.center,
              child: Text(
                prefs.containsKey('uid')
                    ? prefs.getString('phone')
                    : 'Guest'.tr,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.w600),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            prefs.containsKey('uid')
                ? Obx(
                    () {
                      if (walletDataController.isLoading.isTrue) {
                        return Container(
                          height: 150,
                          margin: EdgeInsets.all(10),
                          padding: const EdgeInsets.symmetric(
                            vertical: 20,
                            horizontal: 15,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: kPrimaryColor,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          child: Center(
                            child: CustomLoader(),
                          ),
                        );
                      } else {
                        return WalletStatusWidget(
                          percentage: double.parse(
                              '${walletDataController.walletModel.progress / 100}'),
                          statusName:
                              walletDataController.walletModel.package == null
                                  ? 'Bronze'
                                  : walletDataController
                                      .walletModel.package.name,
                          point:
                              walletDataController.walletModel.point.toString(),
                          statusColor:
                              walletDataController.walletModel.package != null
                                  ? (walletDataController
                                              .walletModel.package.id ==
                                          1
                                      ? kBronzeColor
                                      : (walletDataController
                                                  .walletModel.package.id ==
                                              2
                                          ? kSilverColor
                                          : (walletDataController
                                                      .walletModel.package.id ==
                                                  3
                                              ? kGoldColor
                                              : (walletDataController
                                                          .walletModel
                                                          .package
                                                          .id ==
                                                      4
                                                  ? kDiamondColor
                                                  : kPlatinumColor))))
                                  : kBronzeColor,
                        );
                      }
                    },
                  )
                : WalletStatusWidget(
                    percentage: 0,
                    statusName: 'Not Available',
                    point: '0',
                    statusColor: kBronzeColor,
                  ),
            Divider(
              thickness: 5,
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  RowWidgets(
                    title: 'Orders'.tr,
                    color: Colors.green,
                    icon: Icons.article,
                    onPress: () {
                      if (prefs.containsKey('uid')) {
                        Navigator.pushNamed(context, OrderScreens.routeName);
                      } else {
                        Navigator.pushNamed(context, LogIn.routeName);
                      }
                    },
                  ),
                  RowWidgets(
                    title: 'Profile'.tr,
                    color: Colors.blue,
                    icon: Icons.person,
                    onPress: () {
                      if (prefs.containsKey('uid')) {
                        Get.bottomSheet(
                          Container(
                            decoration: BoxDecoration(
                              color: kWhiteColor,
                              borderRadius: new BorderRadius.only(
                                topRight: const Radius.circular(15.0),
                                topLeft: const Radius.circular(15.0),
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: SingleChildScrollView(
                                child: Form(
                                  key: _profileFormKey,
                                  child: Column(
                                    children: [
                                      InputFormWidget(
                                        fieldController: nameController,
                                        labelText: 'Name',
                                        hintText:
                                            prefs.getString('name') != null
                                                ? prefs.getString('name')
                                                : 'Enter Your Name',
                                        keyType: TextInputType.name,
                                        validation: (value) {
                                          if (value.isEmpty) {
                                            return 'Name can\'t be empty';
                                          } else if (value.length < 4) {
                                            return 'too short';
                                          }
                                          return null;
                                        },
                                      ),
                                      InputFormWidget(
                                        keyType: TextInputType.number,
                                        isEditable: false,
                                        labelText: 'Number',
                                        hintText: prefs.getString('phone'),
                                      ),
                                      InputFormWidget(
                                        fieldController: emailController,
                                        labelText: 'Email',
                                        hintText:
                                            prefs.getString('email') != null
                                                ? prefs.getString('email')
                                                : 'Enter your email',
                                        keyType: TextInputType.emailAddress,
                                        validation: (value) {
                                          if (value.isEmpty) {
                                            return kEmailNullError;
                                          } else if (!emailValidatorRegExp
                                              .hasMatch(value)) {
                                            return kInvalidEmailError;
                                          }
                                          return null;
                                        },
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Container(
                                        width: double.infinity,
                                        child: DefaultBtn(
                                          title: 'Submit'.tr,
                                          onPress: () async {
                                            if (_profileFormKey.currentState
                                                .validate()) {
                                              var p = await NetworkServices()
                                                  .userProfileUpdate(
                                                context: context,
                                                name: prefs.getString('name') !=
                                                        null
                                                    ? prefs.getString('name')
                                                    : nameController.text,
                                                email: prefs.getString(
                                                            'email') !=
                                                        null
                                                    ? prefs.getString('email')
                                                    : emailController.text,
                                                // address: prefs.getString(
                                                //             'address') !=
                                                //         null
                                                //     ? prefs.getString('address')
                                                //     : addressController.text,
                                              );
                                              Map<String, dynamic> js = p;
                                              if (js.containsKey('error')) {
                                                Navigator.of(context).pop();
                                                print(p['error']);
                                                Get.snackbar(
                                                  'Error',
                                                  p['error'],
                                                  colorText: kWhiteColor,
                                                );
                                              } else {
                                                Navigator.pop(context);
                                                Navigator.pop(context);
                                                Navigator.pushReplacementNamed(
                                                    context,
                                                    ProfileScreen.routeName);
                                                prefs.setString('email',
                                                    emailController.text);
                                                prefs.setString('name',
                                                    nameController.text);
                                                // prefs.setString('address',
                                                //     addressController.text);
                                                Get.snackbar(
                                                  'Profile Update Successfully',
                                                  '',
                                                  colorText: kWhiteColor,
                                                );
                                              }
                                              print('ok');
                                            }
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      } else {
                        Get.snackbar('You are in Guest Mode',
                            'Log in for unlock more feature',
                            colorText: kWhiteColor);
                      }
                    },
                  ),
                  RowWidgets(
                    title: 'Notification'.tr,
                    color: Colors.orange,
                    icon: Icons.notifications_none,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Divider(
              thickness: 5,
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: Container(
                child: Column(
                  children: [
                    ListWidget(
                      title: 'Payment History'.tr,
                      icon: Icons.payment,
                      color: Colors.pink,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ListWidget(
                      title: 'Change Language'.tr,
                      icon: Icons.language,
                      color: kPrimaryColor,
                      onPress: () {
                        CustomAlertDialog().changeLanguage(context);
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ListWidget(
                      title: 'Change Password'.tr,
                      icon: Icons.lock_open_rounded,
                      color: Colors.blue,
                      onPress: () {
                        if (prefs.containsKey('uid')) {
                          Get.bottomSheet(
                            Container(
                              decoration: BoxDecoration(
                                color: kWhiteColor,
                                borderRadius: new BorderRadius.only(
                                  topRight: const Radius.circular(15.0),
                                  topLeft: const Radius.circular(15.0),
                                ),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: SingleChildScrollView(
                                  child: Form(
                                    key: _passFormKey,
                                    child: Column(
                                      children: [
                                        InputFormWidget(
                                          fieldController:
                                              oldPasswordController,
                                          labelText: 'Old Password',
                                          hintText: 'Enter Old Password',
                                          keyType:
                                              TextInputType.visiblePassword,
                                          isProtected: false,
                                          validation: (value) {
                                            if (value.isEmpty) {
                                              return kPassNullError;
                                            } else if (value.length < 4) {
                                              return kShortPassError;
                                            }
                                            return null;
                                          },
                                        ),
                                        InputFormWidget(
                                          fieldController:
                                              newPasswordController,
                                          labelText: 'New Password',
                                          hintText: 'Enter New Password',
                                          keyType:
                                              TextInputType.visiblePassword,
                                          isProtected: false,
                                          validation: (value) {
                                            if (value.isEmpty) {
                                              return kPassNullError;
                                            } else if (value.length < 4) {
                                              return kShortPassError;
                                            }
                                            return null;
                                          },
                                        ),
                                        InputFormWidget(
                                          labelText: 'Repeat New Password',
                                          hintText: 'Repeat New Password',
                                          keyType:
                                              TextInputType.visiblePassword,
                                          isProtected: false,
                                          validation: (value) {
                                            if (value.isEmpty) {
                                              return kPassNullError;
                                            } else if (value.length < 4) {
                                              return kShortPassError;
                                            } else if (value !=
                                                newPasswordController.text) {
                                              return 'Password not match';
                                            }
                                            return null;
                                          },
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                          width: double.infinity,
                                          child: DefaultBtn(
                                            title: 'Submit'.tr,
                                            onPress: () async {
                                              if (_passFormKey.currentState
                                                  .validate()) {
                                                var p = await NetworkServices()
                                                    .userPasswordChange(
                                                  context: context,
                                                  oldPassword:
                                                      oldPasswordController
                                                          .text,
                                                  newPassword:
                                                      newPasswordController
                                                          .text,
                                                );
                                                Map<String, dynamic> js = p;
                                                if (js.containsKey('error')) {
                                                  Navigator.of(context).pop();
                                                  print(p['error']);
                                                  Get.snackbar(
                                                    'Error',
                                                    p['error'],
                                                    colorText: kWhiteColor,
                                                  );
                                                } else {
                                                  Navigator.pop(context);
                                                  Navigator.pop(context);
                                                  Get.snackbar(
                                                    'Password Change Successfully',
                                                    '',
                                                    colorText: kWhiteColor,
                                                  );
                                                }
                                                print('ok');
                                              }
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          );
                        } else {
                          Get.snackbar('You are in Guest Mode',
                              'Log in for unlock more feature',
                              colorText: kWhiteColor);
                        }
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    prefs.containsKey('uid')
                        ? ListWidget(
                            title: 'Log Out'.tr,
                            icon: Icons.exit_to_app,
                            color: Colors.red,
                            onPress: () {
                              Get.defaultDialog(
                                title: 'Are you sure?',
                                content: Text('This will logout from the app'),
                                onCancel: () {},
                                buttonColor: kPrimaryColor,
                                confirmTextColor: kWhiteColor,
                                onConfirm: () {
                                  Navigator.pop(context);
                                  prefs.clear();
                                  Get.offAll(
                                    () => DashboardScreen(),
                                  );
                                },
                              );
                            },
                          )
                        : SizedBox(),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
