import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:flutter/material.dart';

class ListWidget extends StatelessWidget {
  final String title;
  final Color color;
  final IconData icon;
  final Function onPress;

  ListWidget({
    @required this.title,
    @required this.color,
    @required this.icon,
    this.onPress,
  });
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: ListTile(
        contentPadding: EdgeInsets.all(0),
        leading: Container(
          width: getProportionateScreenWidth(50),
          alignment: Alignment.center,
          decoration: BoxDecoration(color: color, shape: BoxShape.circle),
          child: Icon(
            icon,
            color: kWhiteColor,
          ),
        ),
        title: Text(
          title,
          style: TextStyle(fontWeight: FontWeight.w400, fontSize: 16),
        ),
      ),
    );
  }
}
