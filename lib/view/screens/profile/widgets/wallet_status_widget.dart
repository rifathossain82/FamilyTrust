import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/main.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:get/get.dart';

class WalletStatusWidget extends StatelessWidget {
  final String point;
  final String statusName;
  final Color statusColor;
  final double percentage;

  WalletStatusWidget({
    @required this.point,
    @required this.statusName,
    @required this.statusColor,
    @required this.percentage,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      padding: const EdgeInsets.symmetric(
        vertical: 20,
        horizontal: 15,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: statusColor,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                prefs.containsKey('uid')
                    ? (prefs.getString('name') != null
                        ? prefs.getString('name')
                        : prefs.getString('phone'))
                    : 'Guest'.tr,
                style: kRegularText.copyWith(
                  fontWeight: FontWeight.w600,
                  color: kWhiteColor,
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    Icons.stars_rounded,
                    color: kWhiteColor,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    point,
                    style: kRegularText.copyWith(
                      fontWeight: FontWeight.w600,
                      color: kWhiteColor,
                    ),
                  ),
                  Text(
                    ' Points',
                    style: kRegularText.copyWith(
                      color: kWhiteColor,
                    ),
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 15,
            ),
            child: Text(
              statusName.toUpperCase(),
              style: kRegularText.copyWith(
                fontWeight: FontWeight.w600,
                color: kWhiteColor,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          LinearPercentIndicator(
            animation: true,
            lineHeight: 25.0,
            animationDuration: 2000,
            percent: percentage,
            linearStrokeCap: LinearStrokeCap.roundAll,
            progressColor: kWhiteColor,
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            'Purchase and earn points to rank up your user package.',
            style: kDescriptionText.copyWith(
              color: kWhiteColor,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
