import 'package:flutter/material.dart';

class RowWidgets extends StatelessWidget {
  final IconData icon;
  final String title;
  final Color color;
  final Function onPress;

  RowWidgets({this.title, this.color, this.icon, this.onPress});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Column(
        children: [
          Container(
            alignment: Alignment.center,
            decoration:
                BoxDecoration(color: Colors.black12, shape: BoxShape.circle),
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Icon(
                icon,
                color: color,
              ),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Text(title)
        ],
      ),
    );
  }
}
