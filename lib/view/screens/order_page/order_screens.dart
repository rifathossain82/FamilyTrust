import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/grocery_page/order_list/grocery_order_list.dart';
import 'package:familytrust/view/screens/shop_page/shop_order_list/shop_order_list.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OrderScreens extends StatelessWidget {
  static const routeName = 'order_screens';
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Orders'.tr),
          bottom: TabBar(
            indicatorColor: kWhiteColor,
            labelStyle: TextStyle(
              fontSize: getProportionateScreenWidth(15),
              fontWeight: FontWeight.w700,
            ),
            tabs: [
              Tab(
                text: 'Grocery Orders'.tr,
              ),
              Tab(
                text: 'Shop Orders'.tr,
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            GroceryOrderList(),
            ShopOrderList(),
          ],
        ),
      ),
    );
  }
}
