import 'package:flutter/material.dart';

class AuthAskWidget extends StatelessWidget {
  AuthAskWidget({this.onPress, this.title});
  final String title;
  final Function onPress;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            title,
          ),
          SizedBox(
            width: 5,
          ),
          Icon(Icons.arrow_forward)
          // Image.asset(
          //   'assets/images/red_arrow.png',
          //   width: getProportionateScreenWidth(24),
          //   height: getProportionateScreenHeight(24),
          // )
        ],
      ),
    );
  }
}
