import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/grocery_home.dart';
import 'package:familytrust/view/widgets/default_btn.dart';
import 'package:familytrust/view/widgets/input_form_widget.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/homepage/homepage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignUp extends StatefulWidget {
  static const routeName = 'signup_screen';
  const SignUp({Key key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  bool clickBtn = false;
  final _formKey = GlobalKey<FormState>();
  TextEditingController _numberController = TextEditingController();
  TextEditingController _passController = TextEditingController();
  TextEditingController _otpController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _numberController.dispose();
    _passController.dispose();
    _otpController.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: getProportionateScreenWidth(15),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: getProportionateScreenHeight(60),
            ),
            Align(
              alignment: Alignment.center,
              child: Hero(
                tag: 'logo',
                child: Container(
                    width: SizeConfig.screenWidth / 3,
                    child: Image.asset('${kImageDir}slogo.png')),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                vertical: getProportionateScreenHeight(20),
              ),
              child: Text(
                'Create Account',
                style: kHeadLine,
              ),
            ),
            Form(
              key: _formKey,
              child: Column(
                children: [
                  InputFormWidget(
                    fieldController: _numberController,
                    labelText: 'Phone Number',
                    preText: '+88',
                    icon: Icons.phone,
                    keyType: TextInputType.number,
                    validation: (value) {
                      if (value.isEmpty) {
                        return kInvalidNumberError;
                      } else if (value.length < 11 && value.length > 11) {
                        return kInvalidNumberError;
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  clickBtn == false
                      ? Align(
                          alignment: Alignment.centerRight,
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: 5,
                            ),
                            width: SizeConfig.screenWidth / 2,
                            child: DefaultBtn(
                              isChange: true,
                              radius: 10,
                              color: kPrimaryColor,
                              title: 'Confirm'.tr,
                              onPress: () async {
                                if (_formKey.currentState.validate()) {
                                  var p = await NetworkServices().regOTP(
                                      number: _numberController.text,
                                      otpType: 'registration');
                                  Map<String, dynamic> js = p;
                                  if (js.containsKey('error')) {
                                    print(p['error']);
                                    Get.snackbar('Error', p['error']);
                                  } else {
                                    setState(() {
                                      clickBtn = true;
                                    });
                                    Get.snackbar(
                                        'OTP', "OTP Send Successfully");
                                  }
                                }
                              },
                            ),
                          ),
                        )
                      : Column(
                          children: [
                            InputFormWidget(
                              fieldController: _otpController,
                              labelText: 'Otp',
                              icon: Icons.vpn_key,
                              keyType: TextInputType.visiblePassword,
                              isProtected: false,
                              validation: (value) {
                                if (value.isEmpty) {
                                  return 'otp can\'t be empty';
                                } else if (value.length < 6) {
                                  return 'Invalid otp  ';
                                }

                                return null;
                              },
                            ),
                            InputFormWidget(
                              fieldController: _passController,
                              labelText: 'Password',
                              icon: Icons.lock,
                              keyType: TextInputType.visiblePassword,
                              isProtected: true,
                              validation: (value) {
                                if (value.isEmpty) {
                                  return kPassNullError;
                                } else if (value.length < 4) {
                                  return kShortPassError;
                                }
                                return null;
                              },
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                vertical: getProportionateScreenHeight(35),
                              ),
                              child: Container(
                                width: double.infinity,
                                child: DefaultBtn(
                                  title: 'Submit',
                                  isChange: true,
                                  radius: 10,
                                  color: kPrimaryColor,
                                  onPress: () async {
                                    if (_formKey.currentState.validate()) {
                                      var p = await NetworkServices()
                                          .userRegistration(
                                        context: context,
                                        number: _numberController.text,
                                        password: _passController.text,
                                        otp: _otpController.text,
                                      );
                                      Map<String, dynamic> js = p;
                                      if (js.containsKey('error')) {
                                        Navigator.of(context).pop();
                                        print(p['error']);
                                        Get.snackbar('Error', p['error']);
                                      } else {
                                        Navigator.of(context).pop();
                                        store(p, context);
                                      }
                                      print('ok');
                                    }
                                  },
                                ),
                              ),
                            ),
                          ],
                        )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void store(var mat, BuildContext context) async {
    prefs = await SharedPreferences.getInstance();
    prefs.setString('uid', mat['user']['id'].toString());
    prefs.setString('phone', mat['user']['phone'].toString());
    prefs.setString('token', mat['token'].toString());
    Navigator.pushNamed(context, GroceryHome.routeName);
  }
}
