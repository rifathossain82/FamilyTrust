import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/services/network_services.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/auth/forget_pass.dart';
import 'package:familytrust/view/screens/auth/signup.dart';
import 'package:familytrust/view/screens/auth/widgets/authask.dart';
import 'package:familytrust/view/screens/dashboard/dashboard_screen.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/grocery_home.dart';
import 'package:familytrust/view/screens/homepage/homepage.dart';
import 'package:familytrust/view/widgets/default_btn.dart';
import 'package:familytrust/view/widgets/input_form_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LogIn extends StatefulWidget {
  static const routeName = 'logIn_screen';
  final bool isOff;
  final String address;
  const LogIn({
    Key key,
    this.isOff = false,
    this.address,
  }) : super(key: key);

  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _numberController = TextEditingController();
  TextEditingController _passController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _numberController.dispose();
    _passController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: getProportionateScreenWidth(15),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: getProportionateScreenHeight(60),
            ),
            Align(
              alignment: Alignment.center,
              child: Hero(
                tag: 'logo',
                child: Container(
                    width: SizeConfig.screenWidth / 3,
                    child: Image.asset('${kImageDir}slogo.png')),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                vertical: getProportionateScreenHeight(20),
              ),
              child: Text(
                'Log In',
                style: kHeadLine,
              ),
            ),
            Form(
              key: _formKey,
              child: Column(
                children: [
                  InputFormWidget(
                    fieldController: _numberController,
                    labelText: 'Phone Number',
                    preText: '+88',
                    icon: Icons.phone,
                    keyType: TextInputType.number,
                    validation: (value) {
                      if (value.isEmpty) {
                        return kInvalidNumberError;
                      } else if (value.length < 11) {
                        return kInvalidNumberError;
                      }
                      return null;
                    },
                  ),
                  InputFormWidget(
                    fieldController: _passController,
                    labelText: 'Password',
                    icon: Icons.lock,
                    keyType: TextInputType.visiblePassword,
                    isProtected: true,
                    validation: (value) {
                      if (value.isEmpty) {
                        return kPassNullError;
                      } else if (value.length < 4) {
                        return kShortPassError;
                      }
                      return null;
                    },
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AuthAskWidget(
                        title: 'Create Account',
                        onPress: () {
                          Navigator.pushNamed(context, SignUp.routeName);
                        },
                      ),
                      AuthAskWidget(
                        title: 'Forget Password?',
                        onPress: () {
                          Navigator.pushNamed(context, ForgetPass.routeName);
                        },
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: getProportionateScreenHeight(35),
                    ),
                    child: Container(
                      width: double.infinity,
                      child: DefaultBtn(
                        title: 'Login',
                        isChange: true,
                        radius: 10,
                        color: kPrimaryColor,
                        onPress: () async {
                          if (_formKey.currentState.validate()) {
                            var p = await NetworkServices().userLogin(
                              context: context,
                              password: _passController.text,
                              number: _numberController.text,
                            );
                            Map<String, dynamic> js = p;
                            if (js.containsKey('error')) {
                              Navigator.of(context).pop();
                              print(p['error']);
                              Get.snackbar('Error', p['error']);
                            } else {
                              Navigator.of(context).pop();
                              store(p, context);
                            }
                            print('ok');
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void store(var mat, BuildContext context) async {
    prefs = await SharedPreferences.getInstance();
    prefs.setString('uid', mat['user']['id'].toString());
    prefs.setString('phone', mat['user']['phone'].toString());
    prefs.setString('email', mat['user']['email'].toString());
    prefs.setString('address', mat['user']['address'].toString());
    prefs.setString('name', mat['user']['name'].toString());
    prefs.setString('token', mat['token'].toString());
    if (widget.isOff) {
      Get.back();
      prefs.setString('address', widget.address);
    } else {
      Navigator.pushNamed(context, DashboardScreen.routeName);
    }
  }
}
