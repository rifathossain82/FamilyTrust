import 'package:familytrust/view/screens/grocery_page/grocery_home/grocery_home.dart';
import 'package:familytrust/view/screens/menu/menu_page.dart';
import 'package:flutter/material.dart';

class TabNavigator extends StatelessWidget {
  TabNavigator({this.navigatorKey, this.tabItem});

  final GlobalKey<NavigatorState> navigatorKey;
  final String tabItem;

  @override
  Widget build(BuildContext context) {
    Widget child;
    if (tabItem == GroceryHome.routeName) {
      child = GroceryHome();
    } else if (tabItem == MenuPage.routeName) {
      child = MenuPage();
    }

    return Navigator(
      key: navigatorKey,
      onGenerateRoute: (routeSettings) {
        return MaterialPageRoute(builder: (context) => child);
      },
    );
  }
}