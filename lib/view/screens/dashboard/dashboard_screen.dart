import 'dart:developer';
import 'package:familytrust/controllers/grocery_controllers/grocerycart_controller.dart';
import 'package:familytrust/helper/helper_method.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/auth/login.dart';
import 'package:familytrust/view/screens/dashboard/components/tab_navigator.dart';
import 'package:familytrust/view/screens/grocery_page/checkout/chekout_screen.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/grocery_home.dart';
import 'package:familytrust/view/screens/menu/menu_page.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class DashboardScreen extends StatefulWidget {
  DashboardScreen({Key key}) : super(key: key);
  static const routeName = 'dashboard-screen';

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  bool isCheckout;
  var selectedIndex = 0;
  String _currentPage = GroceryHome.routeName;
  final GroceryCartController groceryCartController = Get.find();

  List<String> pageKeys = [GroceryHome.routeName, MenuPage.routeName];
  Map<String, GlobalKey<NavigatorState>> _navigatorKeys = {
    GroceryHome.routeName: GlobalKey<NavigatorState>(),
    MenuPage.routeName: GlobalKey<NavigatorState>(),
  };

  void _selectTab(String tabItem, int index) {
    if (tabItem == _currentPage) {
      _navigatorKeys[tabItem].currentState.popUntil((route) => route.isFirst);
    } else {
      setState(() {
        _currentPage = pageKeys[index];
        selectedIndex = index;
      });
    }
  }

  @override
  void initState() {
    groceryCartController.onInit();
    super.initState();
  }

  Future<bool> _showExitDialog() async {
    bool result = await customAlert(
      context: context,
      confirmTitle: 'Exit',
      cancelTitle: 'No',
      title: 'Are you sure?',
      body: 'Do you want to exit this app!',
      color: kPrimaryColor,
      onConfirm: () => Get.back(result: true),
      onCancel: () => Get.back(result: false),
    );
    return result;
  }

  int get totalPrice {
    int total = 0;
    for (int i = 0; i < groceryCartController.groceryCart.length; i++) {
      total += groceryCartController.groceryCart[i].price *
          groceryCartController.groceryCart[i].qty;
    }
    return total;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return WillPopScope(
      onWillPop: ()async{
        final isFirstRouteInCurrentTab = !await _navigatorKeys[_currentPage].currentState.maybePop();
        print(isFirstRouteInCurrentTab);
        if (isFirstRouteInCurrentTab) {
          if (_currentPage != GroceryHome.routeName) {
            _selectTab(GroceryHome.routeName, 0);
            return false;
          } else{
            return await _showExitDialog();
          }
        }

        return isFirstRouteInCurrentTab;
      },
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            _buildOffstageNavigator(GroceryHome.routeName),
            _buildOffstageNavigator(MenuPage.routeName),
          ],
        ),
        bottomNavigationBar: Container(
          height: 80,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: kWhiteColor,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
            ),
            // boxShadow: [
            //   BoxShadow(
            //     offset: Offset(0, -12),
            //     blurRadius: 37,
            //     color: kSecondaryColor.withOpacity(0.10),
            //     spreadRadius: 0,
            //   ),
            // ],
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 8,
            ),
            child: Obx(
              () {
                if (groceryCartController.isLoading.isTrue) {
                  return Center(
                    child: CustomLoader(),
                  );
                } else {
                  return groceryCartController.groceryCart.isNotEmpty &&
                          totalPrice > 0
                      ? Row(
                          children: [
                            Expanded(
                              flex: 4,
                              child: _buildCheckoutOption(),
                            ),
                            const SizedBox(width: 8),
                            Expanded(
                              flex: 5,
                              child: _buildBottomNavItems(),
                            ),
                          ],
                        )
                      : _buildBottomNavItems();
                }
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildCheckoutOption() {
    return GestureDetector(
      onTap: () {
        prefs.setInt('total', totalPrice);
        log(prefs.getInt('total').toString());
        if (prefs.containsKey('uid')) {
          Get.to(() => CheckOutScreen());
        } else {
          Get.to(() => LogIn());
        }
      },
      child: Container(
        height: 48,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: kSecondaryColor,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 1),
              blurRadius: 2,
              spreadRadius: 0,
              color: kWhiteColor.withOpacity(0.10),
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              'Checkout'.tr,
              style: GoogleFonts.quicksand(
                textStyle: kDescriptionText.copyWith(
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            Container(
              height: 36,
              padding: const EdgeInsets.symmetric(
                horizontal: 8,
              ),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: const Color(0xFFFFB900).withOpacity(0.80),
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(3, 2),
                    blurRadius: 3,
                    spreadRadius: -2,
                    color: kWhiteColor.withOpacity(0.15),
                  ),
                ],
              ),
              child: Text(
                '$totalPrice Tk',
                style: GoogleFonts.quicksand(
                  textStyle: kDescriptionText.copyWith(
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomNavItems() {
    return SizedBox(
      height: 70,
      child: BottomNavigationBar(
        elevation: 0,
        backgroundColor: kWhiteColor,
        currentIndex: selectedIndex,
        onTap: (index) {
          if (index == 2) {
            launchMessengerChat();
          } else {
            _selectTab(pageKeys[index], index);
          }
        },
        selectedItemColor: kBottomItemColor,
        unselectedItemColor: kDarkColor,
        items: [
          BottomNavigationBarItem(
            icon: Image.asset(
              '${kImageDir}home.png',
              color: selectedIndex == 0 ? kBottomItemColor : kDarkColor,
            ),
            label: 'Home'.tr,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              '${kImageDir}menu.png',
              color: selectedIndex == 1 ? kBottomItemColor : kDarkColor,
            ),
            label: 'Menu'.tr,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              '${kImageDir}message.png',
              color: selectedIndex == 2 ? kBottomItemColor : kDarkColor,
            ),
            label: 'Message'.tr,
          ),
        ],
      ),
    );
  }

  Widget _buildOffstageNavigator(String tabItem) {
    return Offstage(
      offstage: _currentPage != tabItem,
      child: TabNavigator(
        navigatorKey: _navigatorKeys[tabItem],
        tabItem: tabItem,
      ),
    );
  }
}
