import 'dart:math';

import 'package:familytrust/controllers/grocery_controllers/categories_controller.dart';
import 'package:familytrust/services/extensions/build_context_extension.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/view/screens/grocery_page/child_categories/child_categories_screen.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/components/productlist_screen.dart';
import 'package:familytrust/view/screens/menu/components/category_item.dart';
import 'package:familytrust/view/widgets/cart_item_badge.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

class SubCategories extends StatelessWidget {
  final String title;
  final int index;

  SubCategories({Key key, this.title, this.index}) : super(key: key);

  final CategoriesController categoriesController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(title),
        elevation: 0,
        actions: [
          CartItemBadge(),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(
          left: 15,
          right: 15,
          top: 15,
        ),
        child: StaggeredGridView.countBuilder(
          crossAxisCount: 2,
          shrinkWrap: true,
          itemCount: categoriesController.categories.data[index].subcategories.length,
          staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
          mainAxisSpacing: 14.0,
          crossAxisSpacing: 14.0,
          itemBuilder: (BuildContext context, int index2) {
            int randomNumber = 0;
            if (index2 < categoryBgColor.length) {
              randomNumber = index2;
            } else {
              randomNumber = Random().nextInt(categoryBgColor.length);
            }
            return InkWell(
              onTap: () {
                if (categoriesController.categories.data[index].subcategories[index2].status == 0) {
                  context.push(
                    ProductListScreen(
                      id: categoriesController.categories.data[index].subcategories[index2].id.toString(),
                      title: LocalizationService().getCurrentLang() == 'Bangla'
                          ? categoriesController.categories.data[index].subcategories[index2].bnCategoryName
                          : categoriesController.categories.data[index].subcategories[index2].categoryName,
                    ),
                  );
                } else {
                  context.push(
                    ChildCatScreen(
                      catId: categoriesController.categories.data[index].subcategories[index2].id.toString(),
                      catName:
                          LocalizationService().getCurrentLang() == 'Bangla'
                              ? categoriesController.categories.data[index].subcategories[index2].bnCategoryName
                              : categoriesController.categories.data[index].subcategories[index2].categoryName,
                    ),
                  );
                }
              },
              child: CategoryItem(
                image: categoriesController.categories.data[index].subcategories[index2].categoryPicture ?? '',
                title: categoriesController.categories.data[index].subcategories[index2].categoryName ?? '',
                index: index2,
                bgColor: categoryBgColor[randomNumber],
                borderColor: categoryBorderColor[randomNumber],
              ),
            );
          },
        ),
      ),
    );
  }
}
