import 'dart:async';

import 'package:familytrust/helper/helper_method.dart';
import 'package:familytrust/services/extensions/build_context_extension.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/utils/size_config.dart';
import 'package:familytrust/view/screens/dashboard/dashboard_screen.dart';
import 'package:familytrust/view/screens/no_internet/no_internet_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  static const routeName = 'splash_screen';

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  String version = '1.0.12';
  var addresses;
  var first;
  var lanLong;
  AnimationController _animationController;
  Animation _animation;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  String pushToken;

  void getFCMToken() {
    _firebaseMessaging.getToken().then((token) {
      setState(() {
        print("token is " + token);
        pushToken = token;
        print("2nd " + pushToken);
      });
    });
  }

  @override
  void initState() {
    getFCMToken();
    SharedPreferences.getInstance().then((pr) {
      prefs = pr;
    });
    // _getCurrentLocation();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 2),
    );
    _animationController.forward();
    _animation = CurvedAnimation(
      parent: _animationController,
      curve: Curves.fastOutSlowIn,
    );
    Timer(Duration(seconds: 3), () {
      print('success');
      navigation();
    });
    super.initState();
  }

  void navigation()async{
    if (await hasInternet) {
      context.pushWithoutRoute(DashboardScreen());
    } else {
      context.pushWithoutRoute(NoInternetScreen());
    }
  }

  // void _getCurrentLocation() async {
  //   final position = await Geolocator.getCurrentPosition();
  //   final coordinate = new Coordinates(position.latitude, position.longitude);
  //   addresses = await Geocoder.google('AIzaSyDJu-xdHXPpyEE-MhyXUdwJIXRBO4pY2b8')
  //       .findAddressesFromCoordinates(coordinate);
  //   first = addresses.first;
  //   print("last address = ${first.addressLine}");
  //   prefs.setString('address', first.addressLine);
  // }

  @override
  void dispose() {
    // TODO: implement dispose
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Container(
        width: SizeConfig.screenWidth,
        color: kPrimaryColor,
        /*decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: [0.1, 0.5, 0.7, 0.9],
            colors: [
              Color(0xFFF3B019),
              Color(0xFFF9C50D),
              Color(0xFFF9C50D),
              Color(0xFFF3B019)
            ],
          ),
        ),*/
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              //width: SizeConfig.screenWidth / 2.5,
              child: FadeTransition(
                opacity: _animation,
                child: Hero(
                  tag: 'logo',
                  child: Image.asset('${kImageDir}slogo.png'),
                ),
              ),
            ),
            /*  SizedBox(
                    height: 20,
                  ),
                  FadeTransition(
                    opacity: _animation,
                    child: Container(
                      child: Text(
                        'source of quality products',
                        style: kRegularText.copyWith(
                          color: kPrimaryColor,
                          wordSpacing: 5,
                          letterSpacing: 2,
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                    ),
                  ),*/
          ],
        ),
      ),
    );
  }
}
