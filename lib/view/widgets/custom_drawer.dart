import 'package:familytrust/helper/helper_method.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/main.dart';
import 'package:familytrust/view/screens/auth/login.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_home/grocery_home.dart';
import 'package:familytrust/view/screens/homepage/homepage.dart';
import 'package:familytrust/view/screens/order_page/order_screens.dart';
import 'package:familytrust/view/screens/profile/profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

import 'custom_alert_dialog.dart';

class CustomDrawer extends StatefulWidget {
  const CustomDrawer({Key key}) : super(key: key);

  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  @override
  Widget build(BuildContext context) {
    print(prefs.getString('name'));
    print(prefs.containsKey('uid'));
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(color: kPrimaryColor),
            accountName: Text(
              prefs.containsKey('uid')
                  ? (prefs.getString('name') != null
                      ? prefs.getString('name')
                      : prefs.getString('phone'))
                  : 'Guest',
            ),
            accountEmail: prefs.containsKey('uid')
                ? Text(
                    prefs.getString('email') != null
                        ? prefs.getString('email')
                        : prefs.getString('phone'),
                  )
                : InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      Get.to(() => LogIn());
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('Login'),
                        SizedBox(
                          width: 5,
                        ),
                        Icon(
                          Icons.login,
                          color: kWhiteColor,
                        ),
                      ],
                    ),
                  ),
            currentAccountPicture: Image.asset(kImageDir + 'logo.png'),
          ),
          ListTile(
            onTap: () {
              Navigator.pushNamedAndRemoveUntil(
                  context, GroceryHome.routeName, (route) => false);
            },
            leading: Icon(
              Icons.home,
              color: kPrimaryColor,
            ),
            title: Text(
              'Home'.tr,
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.pop(context);
              Get.to(
                () => ProfileScreen(
                  backActive: true,
                  centerTitle: false,
                ),
              );
            },
            leading: Icon(
              Icons.person,
              color: kPrimaryColor,
            ),
            title: Text(
              'Profile'.tr,
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.pop(context);
              if (prefs.containsKey('uid')) {
                Get.to(() => OrderScreens());
              } else {
                Get.to(() => LogIn());
              }
            },
            leading: Icon(
              Icons.description,
              color: kPrimaryColor,
            ),
            title: Text(
              'Orders'.tr,
            ),
          ),

          // ListTile(
          //   onTap: () {},
          //   leading: Icon(
          //     Icons.shopping_cart,
          //     color: kPrimaryColor,
          //   ),
          //   title: Text(
          //     'Cart',
          //   ),
          // ),

          ListTile(
            onTap: () {
              Navigator.pop(context);
              CustomAlertDialog().changeLanguage(context);
            },
            leading: Icon(
              Icons.language,
              color: kPrimaryColor,
            ),
            title: Text(
              'Languages'.tr,
            ),
          ),
          ListTile(
            onTap: () async {
              Navigator.pop(context);
              createCall('$countryCode$phoneNumber');
            },
            leading: Icon(
              Icons.phone,
              color: kPrimaryColor,
            ),
            title: Text(
              'Customer Support'.tr,
            ),
          ),
          ListTile(
            onTap: () async {
              Navigator.pop(context);
              await launch(
                  'https://play.google.com/store/apps/details?id=com.hmb.familytrust');
            },
            leading: Icon(
              Icons.rate_review,
              color: kPrimaryColor,
            ),
            title: Text(
              'Rate Us'.tr,
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.pop(context);
              Share.share(
                  'https://play.google.com/store/apps/details?id=com.hmb.familytrust',
                  subject: 'Family Trust : Source of Quality Products');
            },
            leading: Icon(
              Icons.share,
              color: kPrimaryColor,
            ),
            title: Text(
              'Share App'.tr,
            ),
          ),
        ],
      ),
    );
  }
}
