import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../utils/constants.dart';

class GroceryProductListCard extends StatelessWidget {
  final String image, title, bnTitle, bnAttribute, attribute,bnPrice,bnDiscountPrice, bnDiscountAmount;
  final String lng;
  final int discountPrice, price, quantity, qty,discountAmount;
  final Function onIncPress, onIncPress2, onDecPress;

  const GroceryProductListCard({
    Key key,
    this.image,
    this.title,
    this.bnTitle,
    this.attribute,
    this.bnAttribute,
    this.discountPrice,
    this.bnDiscountPrice,
    this.price,
    this.bnPrice,
    this.qty,
    this.quantity,
    this.bnDiscountAmount,
    this.discountAmount,
    this.onIncPress,
    this.onIncPress2,
    this.onDecPress,
    @required this.lng,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Stack(
          children: [
            Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: FadeInImage.assetNetwork(
                          placeholder: '${kImageDir}placeholder.png',
                          image: kImageUrl + image,
                          height: 80,
                          width: 80,
                          imageScale: 1,
                          imageErrorBuilder: (context, url, error) =>
                              Image.asset('${kImageDir}placeholder.png'),
                        ),
                      ),
                      kWidthBox10,
                      Expanded(
                        flex: 5,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              lng == 'Bangla'
                                  ? bnTitle
                                  : title,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 15,
                                height: 1.10,
                                color: kBlackColor,
                              ),
                            ),
                            kHeightBox10,
                            Text(
                              lng == 'Bangla'
                                  ? bnAttribute
                                  : attribute,
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 14,
                                height: 1.0,
                                color: kSilverColor,
                              ),
                            ),
                            kHeightBox10,
                            Row(
                              children: [
                                Expanded(
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        discountPrice == 0
                                            ? (lng == 'Bangla'
                                                ? '$bnPrice TK'
                                                : '$price TK')
                                            : (lng == 'Bangla'
                                                ? '$bnDiscountPrice TK'
                                                : '$discountPrice TK'),
                                        style: TextStyle(
                                            color: kBlackColor,
                                            fontWeight: FontWeight.w600,
                                            fontSize: discountPrice != 0
                                                ? 13
                                                : 15,
                                            height: 1.0),
                                      ),
                                      kWidthBox10,
                                      discountPrice != 0
                                          ? Text(
                                              '${lng == 'Bangla' ? '$bnPrice TK' : '$price TK'}',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 13,
                                                  height: 1.0,
                                                  color: kAccentColor,
                                                  decoration: TextDecoration
                                                      .lineThrough),
                                            )
                                          : SizedBox(),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  child: !(quantity > qty)
                                      ? Container(
                                          decoration: BoxDecoration(
                                              color: kWhiteColor,
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              border: Border.all(
                                                  width: 1,
                                                  color: kAccentColor)),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 10, vertical: 4),
                                            child: Text(
                                              'Stock Out'.tr,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 12,
                                                  height: 1.0,
                                                  color: kAccentColor),
                                            ),
                                          ),
                                        )
                                      : SizedBox(
                                          child: qty > 0
                                              ? Container(
                                                  margin: EdgeInsets.all(2),
                                                  child: Row(
                                                    children: [
                                                      InkWell(
                                                        onTap: onDecPress as Function(),
                                                        child: Container(
                                                          width: 28,
                                                          height: 28,
                                                          decoration: BoxDecoration(
                                                              color:
                                                                  kWhiteColor,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color:
                                                                      kSecondaryColor)),
                                                          child: Icon(
                                                            Icons.remove,
                                                            color: kBlackColor,
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 10,
                                                      ),
                                                      Text(
                                                        qty.toString(),
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            fontSize: 18),
                                                      ),
                                                      SizedBox(
                                                        width: 10,
                                                      ),
                                                      InkWell(
                                                        onTap: onIncPress as Function(),
                                                        child: Container(
                                                          width: 28,
                                                          height: 28,
                                                          decoration: BoxDecoration(
                                                              color:
                                                                  kWhiteColor,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color:
                                                                      kSecondaryColor)),
                                                          child: Icon(
                                                            Icons.add,
                                                            color: kBlackColor,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              : Container(
                                                  padding: EdgeInsets.all(10),
                                                  decoration: BoxDecoration(
                                                    color: kSecondaryColor,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            17),
                                                  ),
                                                  child: InkWell(
                                                    onTap: onIncPress2 as Function(),
                                                    child: Icon(
                                                      Icons.add,
                                                      color: kWhiteColor,
                                                    ),
                                                  ),
                                                ),
                                        ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
            discountPrice != 0
                ? Positioned(
                    top: 0,
                    right: 0,
                    child: Container(
                      decoration: BoxDecoration(
                        color: kAccentColor,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(12),
                            bottomLeft: Radius.circular(12)),
                      ),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8, vertical: 4),
                      child: Center(
                        child: Text(
                          '-৳${lng == 'Bangla' ? bnDiscountAmount : discountAmount.toString()} ' + 'OFF'.tr,
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 12,
                              height: 1.0,
                              color: kWhiteColor),
                        ),
                      ),
                    ),
                  )
                : Positioned(
                    top: 0,
                    right: 0,
                    child: Container(),
                  )
          ],
        ),
      ),
    );
  }
}
