import 'package:familytrust/utils/constants.dart';
import 'package:flutter/material.dart';

class DefaultBtn extends StatelessWidget {
  final String title;
  final Function onPress;
  final bool isChange;
  final double radius;
  final Color color;
  DefaultBtn(
      {@required this.title,
      this.color,
      this.onPress,
      this.isChange = false,
      this.radius});
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPress,
      style: ElevatedButton.styleFrom(
        primary: isChange == false ? kPrimaryColor : color,
        // onPrimary: kPrimaryColor,
        shape: RoundedRectangleBorder(
          borderRadius: isChange == false
              ? BorderRadius.circular(25)
              : BorderRadius.circular(radius),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15),
        child: Text(
          title,
          // style: kDescriptionText.copyWith(color: kWhiteColor),
        ),
      ),
    );
  }
}
