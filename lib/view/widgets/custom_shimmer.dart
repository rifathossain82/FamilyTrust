import 'package:familytrust/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:shimmer/shimmer.dart';

sliderShimmer() {
  return AspectRatio(
    aspectRatio: 2.5,
    child: Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
        ),
        width: double.infinity,
      ),
    ),
  );
}

bannerShimmer() {
  return Container(
    height: 75,
    width: double.infinity,
    child: Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
        ),
        width: double.infinity,
      ),
    ),
  );
}

mainCatShimmer() {
  return ListView.builder(
      shrinkWrap: true,
      itemCount: 10,
      itemBuilder: (context, int index) {
        return listCard();
      });
}

childCatShimmer() {
  return Column(
    children: [
      Expanded(
        flex: 2,
        child: Card(
          elevation: 0,
          child: Container(
            height: 50,
            child: ListView.builder(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.only(right: 5),
              itemCount: 10,
              itemBuilder: (context, int index) {
                return Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  child: Container(
                    margin: EdgeInsets.only(right: 5),
                    height: 5,
                    width: 100,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
      Expanded(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 5,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: Container(
                  height: 10,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  width: 100,
                ),
              ),
              Spacer(),
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: Container(
                  height: 20,
                  width: 20,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                  ),
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: Container(
                  height: 20,
                  width: 20,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                  ),
                ),
              ),
            ],
          ),
        ),
      )
    ],
  );
}

productListShimmer() {
  return ListView.builder(
    shrinkWrap: true,
    itemCount: 10,
    itemBuilder: (context, int index) {
      return listCard();
    },
  );
}

productGridShimmer() {
  return StaggeredGridView.countBuilder(
    crossAxisCount: 2,
    shrinkWrap: true,
    itemCount: 10,
    physics: NeverScrollableScrollPhysics(),
    staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
    mainAxisSpacing: 4.0,
    crossAxisSpacing: 4.0,
    itemBuilder: (BuildContext context, int index) {
      return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 1,
        child: AspectRatio(
          aspectRatio: 1,
          child: Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[100],
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
              ),
              width: double.infinity,
            ),
          ),
        ),
      );
    },
  );
}

productHorizontalShimmer() {
  return ListView.separated(
    shrinkWrap: true,
    scrollDirection: Axis.horizontal,
    itemCount: 12,
    itemBuilder: (context, index) => Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 1,
      child: AspectRatio(
        aspectRatio: 1,
        child: Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            width: double.infinity,
          ),
        ),
      ),
    ),
    separatorBuilder: (context, index) => const SizedBox(width: 8),
  );
}

productDetails() {
  return ListView(
    shrinkWrap: true,
    physics: NeverScrollableScrollPhysics(),
    children: [
      AspectRatio(
        aspectRatio: 1,
        child: Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            width: double.infinity,
          ),
        ),
      ),
      SizedBox(
        height: 10,
      ),
      Shimmer.fromColors(
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[100],
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          height: 30,
          width: SizeConfig.screenWidth / 1.5,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
        ),
      ),
      SizedBox(
        height: 10,
      ),
      Row(
        children: [
          Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[100],
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              height: 20,
              width: 50,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
            ),
          ),
          Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[100],
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              height: 20,
              width: 50,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
      SizedBox(
        height: 20,
      ),
      Shimmer.fromColors(
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[100],
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          height: 15,
          width: getProportionateScreenWidth(100),
          decoration: BoxDecoration(
            color: Colors.white,
          ),
        ),
      ),
      SizedBox(
        height: 10,
      ),
      ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: 20,
        itemBuilder: (context, int index) {
          return Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[100],
            child: Container(
              margin: EdgeInsets.only(
                bottom: 5,
                right: 10,
                left: 10,
              ),
              height: 10,
              width: SizeConfig.screenWidth,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
            ),
          );
        },
      ),
    ],
  );
}

listCard() {
  return Card(
    child: Row(
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
          ),
          height: getProportionateScreenHeight(100),
          width: SizeConfig.screenWidth / 2.8,
          child: Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[100],
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
              ),
              width: double.infinity,
            ),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: Container(
                  height: 10,
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  width: 100,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: Container(
                  height: 10,
                  margin: EdgeInsets.only(right: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  width: double.infinity,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: Container(
                  height: 5,
                  margin: EdgeInsets.only(right: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  width: double.infinity,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: Container(
                  height: 5,
                  margin: EdgeInsets.only(right: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  width: double.infinity,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: Container(
                  height: 5,
                  margin: EdgeInsets.only(right: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  width: double.infinity,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: Container(
                  height: 5,
                  margin: EdgeInsets.only(right: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  width: double.infinity,
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}
