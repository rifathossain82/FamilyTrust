import 'package:familytrust/helper/helper_method.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/widgets/input_form_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomAlertDialog {
  void changeLanguage(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        String currentLng = LocalizationService().getCurrentLang();
        int selectedLang = currentLng == 'Bangla' ? 2 : 1;

        return AlertDialog(
          title: Text('Change Language'.tr),
          content: StatefulBuilder(
            // You need this, notice the parameters below:
            builder: (BuildContext context, StateSetter setState) {
              return Column(mainAxisSize: MainAxisSize.min, children: [
                RadioListTile(
                  activeColor: kPrimaryColor,
                  title: Row(
                    children: [
                      Image.asset(
                        'assets/images/usa_flag.png',
                        height: 20,
                        width: 20,
                      ),
                      const SizedBox(width: 12),
                      Text('English'),
                    ],
                  ),
                  value: 1,
                  groupValue: selectedLang,
                  onChanged: (value) {
                    setState(() {
                      selectedLang = value;
                    });
                  },
                ),
                RadioListTile(
                  activeColor: kPrimaryColor,
                  title: Row(
                    children: [
                      Image.asset(
                        'assets/images/bangladesh_flag.png',
                        height: 20,
                        width: 20,
                      ),
                      const SizedBox(width: 12),
                      Text('বাংলা'),
                    ],
                  ),
                  value: 2,
                  groupValue: selectedLang,
                  onChanged: (value) {
                    setState(() {
                      selectedLang = value;
                    });
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      color: kPrimaryColor,
                      child: TextButton(
                        onPressed: () {
                          setState(
                            () {
                              Navigator.pop(context);
                            },
                          );
                        },
                        child: Text(
                          "Cancel",
                          style: TextStyle(color: kWhiteColor),
                        ),
                      ),
                    ),
                    Container(
                      color: kPrimaryColor,
                      child: TextButton(
                        onPressed: () {
                          setState(
                            () {
                              LocalizationService().changeLocale(
                                  selectedLang == 2 ? 'Bangla' : 'English');
                              Navigator.pop(context);
                            },
                          );
                        },
                        child: Text(
                          "Ok",
                          style: TextStyle(color: kWhiteColor),
                        ),
                      ),
                    ),
                  ],
                ),
              ]);
            },
          ),
        );
      },
    );
  }

  static void addCouponDialog({
    BuildContext context,
    TextEditingController controller,
    Function couponMethod,
  }) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        String currentLng = LocalizationService().getCurrentLang();
        final _formKey = GlobalKey<FormState>();
        return AlertDialog(
          title: Text('ADD COUPON'),
          content: StatefulBuilder(
            // You need this, notice the parameters below:
            builder: (BuildContext context, StateSetter setState) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Form(
                    key: _formKey,
                    child: InputFormWidget(
                      fieldController: controller,
                      labelText: 'Coupon Name',
                      hintText: '',
                      keyType: TextInputType.text,
                      validation: (value) {
                        if (value.isEmpty) {
                          return 'Coupon Name Required!';
                        }
                        return null;
                      },
                    ),
                  ),
                  kHeightBox30,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextButton(
                        onPressed: () => Navigator.pop(context),
                        child: Text(
                          "CANCEL",
                          style: TextStyle(color: kDarkColor),
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          if(_formKey.currentState.validate()){
                            couponMethod();
                            Navigator.pop(context);
                          }
                        },
                        child: Text(
                          "APPLY",
                          style: TextStyle(
                            color: kPrimaryColor,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              );
            },
          ),
        );
      },
    );
  }
}
