import 'package:familytrust/controllers/grocery_controllers/grocerycart_controller.dart';
import 'package:familytrust/helper/helper_method.dart';
import 'package:familytrust/models/grocery/rpgrocerypopular_model.dart';
import 'package:familytrust/services/localization_services.dart';
import 'package:familytrust/view/screens/grocery_page/sheared/product_details.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../utils/constants.dart';
import '../../utils/size_config.dart';

class HorizontalGroceryListView extends StatefulWidget {
  List<dynamic> productList;
  HorizontalGroceryListView({
    Key key,
    @required this.productList,
  }) : super(key: key);

  @override
  State<HorizontalGroceryListView> createState() => _HorizontalGroceryListViewState();
}

class _HorizontalGroceryListViewState extends State<HorizontalGroceryListView> {
  final GroceryCartController groceryCartController = Get.find();
  final String lng = LocalizationService().getCurrentLang();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getProportionateScreenWidth(230),
      child: ListView.builder(
        physics: const BouncingScrollPhysics(
          parent: AlwaysScrollableScrollPhysics(),
        ),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: widget.productList.length,
        padding: EdgeInsets.zero,
        itemBuilder: (context, int index) {
          var popularProduct = widget.productList[index];
          return Column(
            children: [
              GestureDetector(
                onTap: (){
                  print('Product qty: ${popularProduct.quantity}');
                  Get.to(
                        () => ProductDetails(
                      prodId: popularProduct.id.toString(),
                    ),
                  );
                },
                child: Padding(
                  padding: EdgeInsets.only(right: index == 11 ? 0 : 15),
                  child: SizedBox(
                    width: SizeConfig.screenWidth / 2.0,
                    child: Container(
                      height: getProportionateScreenWidth(230),
                      width: SizeConfig.screenWidth,
                      decoration: BoxDecoration(
                          color: kWhiteColor,
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(width: 1, color: kBorderColor)),
                      child: Column(
                        children: [
                          Expanded(
                            child: SizedBox(
                              width: SizeConfig.screenWidth,
                              child: Stack(
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(2),
                                    child: Center(
                                      child: FadeInImage.assetNetwork(
                                        placeholder: '${kImageDir}placeholder.png',
                                        image: kImageUrl + popularProduct.picture,//kImageUrl + productsListController.productsList.data[index].picture,
                                        height: 100,
                                        width: 100,
                                        imageScale: 1,
                                        imageErrorBuilder: (context, url, error) => Image.asset('${kImageDir}placeholder.png'),
                                      ),
                                    ),
                                  ),
                                  popularProduct.discountPrice != 0
                                      ? Positioned(
                                    top: 0,
                                    right: 0,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: kAccentColor,
                                        borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(12),
                                            bottomLeft: Radius.circular(12)),
                                      ),
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8, vertical: 4),
                                      child: Center(
                                        child: Text(
                                          '-৳${lng == 'Bangla' ? popularProduct.bnDiscountAmount : popularProduct.discountAmount} ' +
                                              'OFF'.tr,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12,
                                              height: 1.0,
                                              color: kWhiteColor),
                                        ),
                                      ),
                                    ),
                                  )
                                      : Positioned(
                                    top: 0,
                                    right: 0,
                                    child: Container(),
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  lng == 'Bangla'
                                      ? popularProduct.bnProductName
                                      : popularProduct.productName,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 15,
                                    height: 1.10,
                                    color: kBlackColor,
                                  ),
                                ),
                                kHeightBox5,
                                Text(
                                  lng == 'Bangla'
                                      ? popularProduct.bnAttribute
                                      : popularProduct.attribute,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14,
                                    height: 1.0,
                                    color: kSilverColor,
                                  ),
                                ),
                                kHeightBox10,
                                Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            popularProduct.discountPrice ==
                                                0
                                                ? (lng == 'Bangla'
                                                ? '৳${popularProduct.bnPrice} TK'
                                                : '৳${popularProduct.price} TK')
                                                : (lng == 'Bangla'
                                                ? '${popularProduct.bnDiscountPrice} TK'
                                                : '${popularProduct.discountPrice} TK'),
                                            style: TextStyle(
                                                color: kBlackColor,
                                                fontWeight: FontWeight.w600,
                                                fontSize: popularProduct.discountPrice != 0
                                                    ? 13
                                                    : 15,
                                                height: 1.0),
                                          ),
                                          popularProduct.discountPrice != 0
                                              ? Text(
                                            '${lng == 'Bangla' ? popularProduct.bnPrice : popularProduct.price} TK',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 13,
                                                height: 1.0,
                                                color: kAccentColor,
                                                decoration: TextDecoration
                                                    .lineThrough),
                                          )
                                              : SizedBox(),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      child:  popularProduct.quantity == null?
                                      Container() : !(popularProduct.quantity > popularProduct.qty)
                                          ? Container(
                                        decoration: BoxDecoration(
                                            color: kWhiteColor,
                                            borderRadius: BorderRadius.circular(10),
                                            border: Border.all(width: 1, color: kAccentColor)),
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                                          child: Text(
                                            'Stock Out'.tr,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 12,
                                                height: 1.0,
                                                color: kAccentColor),
                                          ),
                                        ),
                                      ) : SizedBox(
                                        child: popularProduct.qty > 0
                                            ? Container(
                                          margin: EdgeInsets.all(2),
                                          child: Row(
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    popularProduct.qty -= 1;
                                                  });
                                                  if (popularProduct.qty != 0) {
                                                    groceryCartController.qtyDecrease(
                                                      popularProduct.id,
                                                      popularProduct.qty,
                                                      popularProduct.price,
                                                    );
                                                  } else {
                                                    groceryCartController.removeFromCart(popularProduct.id);
                                                    kSnackBar('Remove From Cart');
                                                  }
                                                },
                                                child: Container(
                                                  width: 28,
                                                  height: 28,
                                                  decoration: BoxDecoration(
                                                      color: kWhiteColor,
                                                      borderRadius:
                                                      BorderRadius
                                                          .circular(5),
                                                      border: Border.all(
                                                          width: 1,
                                                          color:
                                                          kSecondaryColor)),
                                                  child: Icon(
                                                    Icons.remove,
                                                    color: kBlackColor,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Text(
                                                popularProduct.qty.toString(),
                                                style: TextStyle(
                                                    fontWeight:
                                                    FontWeight.w500,
                                                    fontSize: 18),
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  if (popularProduct.quantity > popularProduct.qty) {
                                                    setState(() {
                                                      popularProduct.qty += 1;
                                                    });
                                                    groceryCartController
                                                        .addToCart(popularProduct);
                                                  } else {
                                                    kSnackBar('Stock Out');
                                                  }
                                                },
                                                child: Container(
                                                  width: 28,
                                                  height: 28,
                                                  decoration: BoxDecoration(
                                                      color: kWhiteColor,
                                                      borderRadius:
                                                      BorderRadius
                                                          .circular(5),
                                                      border: Border.all(
                                                          width: 1,
                                                          color:
                                                          kSecondaryColor)),
                                                  child: Icon(
                                                    Icons.add,
                                                    color: kBlackColor,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                            : Container(
                                          padding: EdgeInsets.all(10),
                                          decoration: BoxDecoration(
                                            color: kSecondaryColor,
                                            borderRadius:
                                            BorderRadius.circular(17),
                                          ),
                                          child: InkWell(
                                            onTap: () {
                                              setState(() {
                                                popularProduct.qty += 1;
                                              });
                                              groceryCartController.addToCart(popularProduct);
                                              kSnackBar('Added to Cart');
                                            },
                                            child: Icon(
                                              Icons.add,
                                              color: kWhiteColor,
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}