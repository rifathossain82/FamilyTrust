import 'package:familytrust/controllers/grocery_controllers/grocerycart_controller.dart';
import 'package:familytrust/utils/constants.dart';
import 'package:familytrust/view/screens/grocery_page/grocery_cart/cart_screen.dart';
import 'package:familytrust/view/widgets/custom_loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class CartItemBadge extends StatelessWidget {
  const CartItemBadge({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GroceryCartController groceryCartController = Get.find();
    return Stack(
      children: [
        IconButton(
          icon: SvgPicture.asset('$kIconsDir/cart.svg'),
          onPressed: () {
            Get.to(() => GroceryCartScreen());
          },
        ),
        Positioned(
          top: 0,
          right: 11,
          child: Container(
            padding: EdgeInsets.all(3),
            decoration: BoxDecoration(
              color: kSecondaryColor,
              shape: BoxShape.circle,
            ),
            child: Obx(
                  () {
                if (groceryCartController.isLoading.isTrue) {
                  return Center(
                    child: CustomLoader(),
                  );
                } else {
                  return Text(
                    groceryCartController.groceryCart.length.toString(),
                    style: TextStyle(
                        color: kPrimaryColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w600),
                  );
                }
              },
            ),
          ),
        ),
      ],
    );
  }
}
