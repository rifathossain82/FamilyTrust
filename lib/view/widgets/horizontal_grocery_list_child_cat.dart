import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../../utils/constants.dart';
import '../../utils/size_config.dart';
import 'custom_loader.dart';

class GroceryChildCat extends StatelessWidget {
  final String title;
  final String picture;
  final bool isSelected;

  const GroceryChildCat({
    Key key,
    @required this.title,
    @required this.picture,
    @required this.isSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: SizeConfig.screenWidth / 2.0,
          child: Container(
            height: getProportionateScreenWidth(90),
            decoration: BoxDecoration(
                color: kWhiteColor,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                  width: isSelected ? 2 : 1,
                  color: isSelected
                      ? const Color(0xFFFE9901)
                      : kBorderColor,
                )),
            child: SizedBox(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    kWidthBox10,
                    SizedBox(
                      width: 70,
                      child: ClipRRect(
                        clipBehavior: Clip.hardEdge,
                        borderRadius: BorderRadius.circular(5),
                        child: CachedNetworkImage(
                          imageUrl: kImageUrl + picture,
                          fit: BoxFit.cover,
                          color: Colors.black.withOpacity(.3),
                          colorBlendMode: BlendMode.darken,
                          placeholder: (context, url) => CustomLoader(),
                          errorWidget: (context, url, error) => Icon(
                            Icons.error,
                            color: Colors.red,
                          ),
                        ),
                      ),
                    ),
                    kWidthBox10,
                    Flexible(
                      child: Text(
                        title,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 12,
                          color: kBlackColor,
                          fontWeight: FontWeight.w600,
                          height: 1,
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ),
      ],
    );
  }
}
