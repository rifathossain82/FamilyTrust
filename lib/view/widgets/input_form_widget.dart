import 'package:familytrust/utils/constants.dart';
import 'package:flutter/material.dart';

class InputFormWidget extends StatelessWidget {
  final String hintText;
  final String labelText;
  final TextEditingController fieldController;
  final Function onSaved;
  final Function validation;
  final IconData icon;
  final String preText;
  final bool isProtected;
  final bool isEditable;
  final keyType;
  InputFormWidget({
    this.preText,
    this.isEditable,
    this.validation,
    @required this.labelText,
    this.isProtected = false,
    this.hintText,
    this.icon,
    this.fieldController,
    @required this.keyType,
    this.onSaved,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: TextFormField(
        enabled: isEditable,
        validator: validation,
        controller: fieldController,
        keyboardType: keyType,
        obscureText: isProtected,
        obscuringCharacter: '*',
        onChanged: onSaved,
        autofocus: false,
        cursorColor: kPrimaryColor,
        decoration: InputDecoration(
          errorStyle: TextStyle(fontSize: 11, height: 0.3),
          focusedBorder: OutlineInputBorder(
              // borderSide: BorderSide(color: kPrimaryColor),
              ),

          prefixText: preText,

          prefixStyle: TextStyle(
            fontSize: 16,
            color: kPrimaryColor,
          ),
          suffixIcon: Icon(
            icon,
            color: kPrimaryColor,
          ),
          labelText: labelText,

          labelStyle: kDescriptionText.copyWith(
            color: kPrimaryColor,
          ),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          hintText: hintText,
          // filled: true,
          // fillColor: kBackGroundColor,
          contentPadding:
              const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          // hintStyle: kDescriptionText.copyWith(color: kWhiteColor),
          alignLabelWithHint: true,
          border: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            // borderSide: BorderSide(color: kWhiteColor),
            gapPadding: 3,
          ),
        ),
      ),
    );
  }
}
