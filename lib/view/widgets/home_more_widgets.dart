import 'package:familytrust/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeMoreWidgets extends StatelessWidget {
  final String title;
  final Function onPressed;

  const HomeMoreWidgets({
    Key key,
    @required this.title,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          '$title'.tr,
          style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 24,
              height: 1.0,
              color: kBlackColor),
        ),
        GestureDetector(
          onTap: onPressed,
          child: Text(
            'See all'.tr,
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 16,
                height: 1.0,
                color: kSecondaryColor),
          ),
        ),
      ],
    );
  }
}
